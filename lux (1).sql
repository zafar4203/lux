-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 19, 2020 at 01:56 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lux`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_para` longtext,
  `main_heading` varchar(255) DEFAULT NULL,
  `para_1` longtext,
  `heading_1` varchar(255) DEFAULT NULL,
  `para_2` longtext,
  `heading_2` varchar(255) DEFAULT NULL,
  `para_3` longtext,
  `heading_3` varchar(255) DEFAULT NULL,
  `para_4` longtext,
  `heading_4` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `main_para`, `main_heading`, `para_1`, `heading_1`, `para_2`, `heading_2`, `para_3`, `heading_3`, `para_4`, `heading_4`, `image`, `status`) VALUES
(1, 'Lux Dxb \'Luxury Watch Market\" is an E-commerce platform that specialises in  luxury watch and jewellery accessoires.      Our mission is to work with the worlds leading manufacturers of  watch and jewellery                          hhdhdhdhaccessories, bringing the finest quality to the Middle East.', 'Great Reasons To Use Lux Dxb', 'We work with the world\'s finest manufacturers to guarantee the quality of products you receive never fall short of your expectations.', 'Quality', 'dhdfhjdfhfdhjfhj', 'We Support Local Charities', 'We run monthly competitions to give our our dedicated customers a chance to win a luxury prize. The rules are simple, visit our campaigns section, purchase a product and you will be entered into a complimentary prize draw', 'Get a chance to win!', 'In addition to presenting the best and latest collections from various brands, we also offer prices that are competitive with retailers from around the world.', 'Competitive Pricing', '1603476108Dubai Skyline .jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(191) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `country`, `address_1`, `address_2`, `city`, `state`, `postal_code`, `created_at`, `updated_at`) VALUES
(6, 22, 'United States', 'street 1 house 9525', 'azizabad', 'Rawalpindi', 'Punjab', '46000', '2020-10-31 06:09:12', '2020-10-31 06:47:30'),
(7, 22, 'Turkey', 'new address 1', 'new address 2', 'istanbuls', 'punjab', '12355', '2020-10-31 06:50:22', '2020-10-31 07:03:40');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(191) NOT NULL DEFAULT '0',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `role_id`, `photo`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `shop_name`) VALUES
(1, 'Admin', 'softgates_admin@gmail.com', '01629552892', 0, '1604827288Penguins.jpg', '$2y$10$2u23PSnROH3xB9FRMYDnVO1v0n1X8dj0aIQ8ZRefizjFV9wxsELAC', 1, 'NbR2hF84ZMirzBQygfi6QpdrPeKmRtQ4iCO9azJEznGgZrY6GIkCMs3Ty3OW', '2018-02-28 08:27:08', '2020-11-08 04:21:28', 'My Store'),
(5, 'Mr Mamu', 'mamun@gmail.com', '34534534', 17, '1568803644User.png', '$2y$10$3AEjcvFBiQHECgtH9ivXTeQZfMf.rw318G820TtVBsYaCt7UNOwGC', 1, NULL, '2019-09-17 13:47:24', '2020-07-13 00:36:03', NULL),
(7, 'Mr. Pratik', 'pratik@gmail.com', '34534534', 16, '1568863396user-admin.png', '$2y$10$u.93l4y6wOz6vq3BlAxvU.LuJ16/uBQ9s2yesRGTWUtLRiQSwoH1C', 1, 'iZPbEaxjSWBJMvncLqeMtAQsG7VoSirVMJ1EBfdJogvgXK2DM5mw236fBCOq', '2019-09-18 06:23:16', '2020-08-22 12:09:15', NULL),
(8, 'kaka', 'kaka@gmail.com', '1231231231231', 16, '1598170467Capture.PNG', '$2y$10$MaHDhSJQVW92ALF6292iz.LxfIBfQJhCx2MFl68//doaTkbNUCQx2', 1, NULL, '2020-08-22 12:09:51', '2020-08-22 12:14:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_languages`
--

DROP TABLE IF EXISTS `admin_languages`;
CREATE TABLE IF NOT EXISTS `admin_languages` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rtl` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_languages`
--

INSERT INTO `admin_languages` (`id`, `is_default`, `language`, `file`, `name`, `rtl`) VALUES
(1, 1, 'English', '1567232745AoOcvCtY.json', '1567232745AoOcvCtY', 0),
(2, 0, 'RTL English', '1584887310NzfWDhO8.json', '1584887310NzfWDhO8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_conversations`
--

DROP TABLE IF EXISTS `admin_user_conversations`;
CREATE TABLE IF NOT EXISTS `admin_user_conversations` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` enum('Ticket','Dispute') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_conversations`
--

INSERT INTO `admin_user_conversations` (`id`, `subject`, `user_id`, `message`, `created_at`, `updated_at`, `type`, `order_number`) VALUES
(1, 'Order Confirmation', 22, 'rfgdfgfd', '2020-01-20 10:18:38', '2020-01-20 10:18:38', 'Ticket', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_messages`
--

DROP TABLE IF EXISTS `admin_user_messages`;
CREATE TABLE IF NOT EXISTS `admin_user_messages` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_messages`
--

INSERT INTO `admin_user_messages` (`id`, `conversation_id`, `message`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'rfgdfgfd', 22, '2020-01-20 10:18:38', '2020-01-20 10:18:38');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attributable_id` int(11) DEFAULT NULL,
  `attributable_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `input_name` varchar(255) DEFAULT NULL,
  `price_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `details_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `attributable_id`, `attributable_type`, `name`, `input_name`, `price_status`, `details_status`, `created_at`, `updated_at`) VALUES
(14, 5, 'App\\Models\\Category', 'Warranty Type', 'warranty_type', 1, 1, '2019-09-23 07:56:07', '2019-09-23 07:56:07'),
(34, 4, 'App\\Models\\Category', 'Warranty', 'warranty', 1, 1, '2020-07-31 03:17:59', '2020-07-31 03:17:59'),
(36, 31, 'App\\Models\\Category', 'Brand', 'brand', 1, 1, '2020-09-04 18:53:28', '2020-09-04 18:53:28'),
(37, 22, 'App\\Models\\Category', 'Brand', 'brand', 1, 1, '2020-09-04 19:44:01', '2020-09-04 19:44:01'),
(44, 55, 'App\\Models\\Category', 'Sizes', 'sizes', 0, 1, '2020-11-10 05:03:43', '2020-11-10 05:03:43'),
(45, 55, 'App\\Models\\Category', 'Colour', 'colour', 0, 1, '2020-11-10 05:04:16', '2020-11-10 05:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

DROP TABLE IF EXISTS `attribute_options`;
CREATE TABLE IF NOT EXISTS `attribute_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attribute_options`
--

INSERT INTO `attribute_options` (`id`, `attribute_id`, `name`, `created_at`, `updated_at`) VALUES
(107, 14, 'No Warranty', '2019-09-23 07:56:07', '2019-09-23 07:56:07'),
(108, 14, 'Local seller Warranty', '2019-09-23 07:56:07', '2019-09-23 07:56:07'),
(109, 14, 'Non local warranty', '2019-09-23 07:56:07', '2019-09-23 07:56:07'),
(110, 14, 'International Manufacturer Warranty', '2019-09-23 07:56:07', '2019-09-23 07:56:07'),
(111, 14, 'International Seller Warranty', '2019-09-23 07:56:07', '2019-09-23 07:56:07'),
(256, 34, 'Warranty Local', '2020-07-31 03:17:59', '2020-07-31 03:17:59'),
(257, 34, 'Warranty International', '2020-07-31 03:17:59', '2020-07-31 03:17:59'),
(261, 36, 'samsung', '2020-09-04 18:53:28', '2020-09-04 18:53:28'),
(262, 36, 'lg', '2020-09-04 18:53:28', '2020-09-04 18:53:28'),
(263, 36, 'oppo', '2020-09-04 18:53:28', '2020-09-04 18:53:28'),
(264, 37, 'Open', '2020-09-04 19:44:01', '2020-09-04 19:44:01'),
(265, 37, 'Used', '2020-09-04 19:44:01', '2020-09-04 19:44:01'),
(266, 38, 'A', '2020-09-21 06:07:51', '2020-09-21 06:07:51'),
(267, 38, 'B', '2020-09-21 06:07:51', '2020-09-21 06:07:51'),
(268, 38, 'C', '2020-09-21 06:07:51', '2020-09-21 06:07:51'),
(269, 39, 'd', '2020-09-21 06:10:52', '2020-09-21 06:10:52'),
(270, 39, 's', '2020-09-21 06:10:52', '2020-09-21 06:10:52'),
(301, 44, 'S', '2020-11-10 05:03:43', '2020-11-10 05:03:43'),
(302, 44, 'M', '2020-11-10 05:03:43', '2020-11-10 05:03:43'),
(303, 44, 'L', '2020-11-10 05:03:43', '2020-11-10 05:03:43'),
(304, 44, 'XL', '2020-11-10 05:03:43', '2020-11-10 05:03:43'),
(305, 44, 'XXL', '2020-11-10 05:03:43', '2020-11-10 05:03:43'),
(306, 45, 'Red', '2020-11-10 05:04:16', '2020-11-10 05:04:16'),
(307, 45, 'Green', '2020-11-10 05:04:16', '2020-11-10 05:04:16'),
(308, 45, 'Blue', '2020-11-10 05:04:17', '2020-11-10 05:04:17'),
(309, 45, 'Yellow', '2020-11-10 05:04:17', '2020-11-10 05:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Large','TopSmall','BottomSmall') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `link`, `type`) VALUES
(1, '1568889151top2.jpg', 'https://www.google.com/', 'TopSmall'),
(2, '1568889146top1.jpg', NULL, 'TopSmall'),
(3, '1568889164bottom1.jpg', 'https://www.google.com/', 'Large'),
(4, '1564398600side-triple3.jpg', 'https://www.google.com/', 'BottomSmall'),
(5, '1564398579side-triple2.jpg', 'https://www.google.com/', 'BottomSmall'),
(6, '1564398571side-triple1.jpg', 'https://www.google.com/', 'BottomSmall');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(191) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `meta_tag` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `title`, `details`, `photo`, `source`, `views`, `status`, `meta_tag`, `meta_description`, `tags`, `created_at`) VALUES
(27, 2, 'Fresh Food Delivery Service', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Location</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Ladies and gentlemen, London’s new late-night destination has arrived. Opening on Friday 17th July, Baccarat Bar is serving up pure escapism. Crystal chandeliers illuminate the bar, while plush velvet and leather banquettes invite you to discover an awe-inspiring menu of delectable cocktails – each one designed by award-winning bar manager Cameron Attfield. And all served in exquisite Baccarat glasses. Ready for your debut? Look for the private entrance on Hans Crescent and prepare for sublime decadence</p>', '1602418465fresh-fruit.png', NULL, 1, 1, NULL, NULL, NULL, '2020-10-11 02:04:03'),
(28, 2, 'The Glorious Twelfth', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Location</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Fresh Market Hall, Ground Floor</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Get ready, get set, grouse! As of the 12th August, otherwise known as the Glorious Twelfth, British chefs and foodies begin the yearly race</p>', '1602418512twelfth.png', NULL, 0, 1, NULL, NULL, NULL, '2020-10-11 02:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`) VALUES
(2, 'Oil & gas', 'oil-and-gas'),
(3, 'Manufacturing', 'manufacturing'),
(4, 'Chemical Research', 'chemical_research'),
(5, 'Agriculture', 'agriculture'),
(6, 'Mechanical', 'mechanical'),
(7, 'Entrepreneurs', 'entrepreneurs'),
(8, 'Technology', 'technology');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `sequence` bigint(191) DEFAULT NULL,
  `status` int(191) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `sequence`, `status`, `created_at`, `updated_at`) VALUES
(1, 'New Brand', 'new-brand', 1, 1, NULL, NULL),
(2, 'Old Brand', 'old-brand', 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `draw_date` date DEFAULT NULL,
  `product_id` bigint(191) DEFAULT NULL,
  `prize_id` bigint(191) DEFAULT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `product_description` longtext,
  `prize_title` varchar(255) DEFAULT NULL,
  `prize_description` longtext,
  `campaign_description` varchar(255) DEFAULT NULL,
  `campaign_editor_note` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `type` tinyint(4) DEFAULT '0',
  `product_images` varchar(255) DEFAULT NULL,
  `prize_images` varchar(255) DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `prize_worth` varchar(255) DEFAULT NULL,
  `sold_out` bigint(191) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `name`, `quantity`, `draw_date`, `product_id`, `prize_id`, `product_title`, `product_description`, `prize_title`, `prize_description`, `campaign_description`, `campaign_editor_note`, `status`, `type`, `product_images`, `prize_images`, `product_price`, `prize_worth`, `sold_out`, `created_at`, `updated_at`) VALUES
(2, 'Campaign 1', '1000', '2020-10-30', 1251, 5, 'Blue Watch', 'Good Quality watch', 'Gold Ear Ring', '14 carat Gold Ear Ring', 'dddddddddddddddd', 'dddddddddddddddddddddd', 1, 1, NULL, NULL, NULL, NULL, 1, '2020-10-02 01:26:05', '2020-10-26 16:47:18'),
(3, 'Campaign 2', '1000', '2020-11-30', 1251, 5, 'Blues Watch', 'Good Quality watch', 'Gold Ear Ring', '14 carat Gold Ear Ring', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', 1, 1, NULL, NULL, NULL, NULL, 878, '2020-10-02 01:26:05', '2020-11-15 01:49:55'),
(4, 'Campaign 3', '1000', '2020-10-30', 1253, 5, 'Blue Watch', 'Good Quality watch', 'Gold Ear Ring', '14 carat Gold Ear Ring', 'dddddddddddddddd', 'dddddddddddddddddddddd', 1, 1, NULL, NULL, NULL, NULL, 0, '2020-10-02 01:26:05', '2020-10-05 21:23:44'),
(8, 'Luxury Watch', '500', '2020-11-30', NULL, NULL, 'Win A Luxury Watch', 'Good<br>', 'Awesome Ear Ring', 'Good<br>', 'Campaign Description<br>', 'Campaign Editor Note', 1, 0, '[\"16057910171601895732z5Hjs6yG.jpg\",\"16057910171601895747kUnJhwom.jpg\",\"16057910171601895757JhOMHzoM.jpg\"]', '[\"16057910171605426862bSUGXNxI.jpg\",\"16057910171605427332xQgqVie1.jpg\",\"16057910171605429391qx4iDhkk.jpg\"]', 136.23978201634878, '500', 35, '2020-11-18 02:33:58', '2020-11-19 08:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

DROP TABLE IF EXISTS `careers`;
CREATE TABLE IF NOT EXISTS `careers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `para_1` longtext,
  `main_heading` varchar(255) DEFAULT NULL,
  `heading_1` varchar(255) DEFAULT NULL,
  `para_2` longtext,
  `heading_2` varchar(255) DEFAULT NULL,
  `para_3` longtext,
  `heading_3` varchar(255) DEFAULT NULL,
  `para_4` longtext,
  `image` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `para_1`, `main_heading`, `heading_1`, `para_2`, `heading_2`, `para_3`, `heading_3`, `para_4`, `image`, `status`) VALUES
(1, '<font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">Work and be yourself!&nbsp;</span></font><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">&nbsp;If you have a passion for watches and fine Jewellery Lux Dxb could be the perfect home for you. Stay up to date with all our latest vacancies by visiting our careers sections.&nbsp;</span></font></div>', 'Open Positions', '', '<p style=\"margin-bottom: 15px; line-height: 26px; font-family: Lato, sans-serif;\"><b style=\"\"><font size=\"5\" style=\"\" color=\"#000000\">There are two Open Positions</font></b></p><p style=\"margin-bottom: 15px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\"><b><font size=\"4\" style=\"\">Server Administrator</font></b></p><p style=\"margin-bottom: 15px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\"><span style=\"font-size: medium;\">We Required Server Administrator for our renown organization.</span><span style=\"font-size: medium;\">Following Skills we are required</span></p><p style=\"margin-bottom: 15px; line-height: 26px;\"></p><ol style=\"color: rgb(128, 127, 127); font-family: Lato, sans-serif;\"><li><span style=\"font-size: medium;\">Php</span></li><li><span style=\"font-size: medium;\">Javascript</span></li><li><span style=\"font-size: medium;\">Laravel</span></li><li><span style=\"font-size: medium;\">Jquery</span></li></ol><p></p><p style=\"margin-bottom: 15px; line-height: 26px;\">If you possess following skills then kind send us cv at our email. <a href=\"http://google.com\" title=\"info@lux.com\" target=\"\">info@lux.com</a></p><p style=\"margin-bottom: 15px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\"><span style=\"font-size: large; font-weight: bolder;\">Network Administrator</span><br></p><p style=\"margin-bottom: 15px; color: rgb(128, 127, 127); line-height: 26px; font-family: Lato, sans-serif;\"><span style=\"font-size: medium;\">We Required Server Administrator for our renown organization.</span><span style=\"font-size: medium;\">Following Skills we are required</span></p><p style=\"margin-bottom: 15px; line-height: 26px;\"></p><ol style=\"color: rgb(128, 127, 127); font-family: Lato, sans-serif;\"><li><span style=\"font-size: medium;\">Php</span></li><li><span style=\"font-size: medium;\">Javascript</span></li><li><span style=\"font-size: medium;\">Laravel</span></li><li><span style=\"font-size: medium;\">Jquery</span></li></ol><p></p><p style=\"margin-bottom: 15px; line-height: 26px;\">If you possess following skills then kind send us cv at our email.</p>', NULL, '', NULL, '', '1602409289careers.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top_menu` int(1) DEFAULT '0',
  `home_category` int(1) DEFAULT '0',
  `sequence` int(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`, `top_menu`, `home_category`, `sequence`) VALUES
(53, 'Rolex', 'rolex', 1, '1601895691a959849f-aa0a-4391-9427-4c23915fa5c5_200x200.png', 0, NULL, 0, 0, 2),
(54, 'Jewellery Accessories', 'jewellery-accessories', 1, '1602390554il_1140xN.2076858685_5ksy.jpg', 0, NULL, 0, 0, 3),
(55, 'Watch Accessories', 'watch-accessories', 1, '1602390597wristwatch6.jpg', 0, NULL, 0, 0, 4),
(56, 'Campaign', 'Campaign-Category', 1, '1603727132Dubai Skyline 3.jpeg', 0, NULL, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `charities`
--

DROP TABLE IF EXISTS `charities`;
CREATE TABLE IF NOT EXISTS `charities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `para_1` longtext,
  `para_2` longtext,
  `get_in_touch_text` varchar(255) DEFAULT NULL,
  `call_us_now_text` varchar(255) DEFAULT NULL,
  `write_email_text` varchar(255) DEFAULT NULL,
  `image_1` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `charities`
--

INSERT INTO `charities` (`id`, `para_1`, `para_2`, `get_in_touch_text`, `call_us_now_text`, `write_email_text`, `image_1`, `image_2`, `status`) VALUES
(1, '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Our mission objective at Idealz is to transform lives, not just those of our customers but of children who are less fortunate around the world. To achieve that aim, we created ‘Give 2 Live’ as part of our ecommerce experience. ‘Give 2 Live’ allows you to donate your product, or a percentage of its sale, to our affiliate charity partners.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">For your generosity, Idealz promises to reward every donation by doubling the number of complimentary entries you receive for the prize draw. In this way, we hope to create a genuine win-win for all involved.</p>', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Since its inception, Dubai Cares, part of Mohammed bin Rashid Al Maktoum Global Initiatives, has been working towards providing children and young people in developing countries with access to quality education through the design and funding of programs that aim to be integrated, impactful, sustainable and scalable.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">Dubai Cares is playing a key role in helping achieve the United Nations Sustainable Development Goal (SDG) 4, which aims to ensure inclusive and quality education for all and promote lifelong learning by 2030, by supporting programs in early childhood development, access to quality primary and secondary education, technical and vocational education and training for youth as well as a particular focus on education in emergencies and protracted crises.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">NB* By supporting Dubai Cares through \'Give 2 Live\', you are donating a percentage of the sale price.</p>', 'Business Hours: Sun - Thurs: 9am - 6pm GST', '0800 50 50 50', 'info@Lux-Dxb.com', '1602406481charity.jpg', '1602406481charity2.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `childcategories`
--

DROP TABLE IF EXISTS `childcategories`;
CREATE TABLE IF NOT EXISTS `childcategories` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `subcategory_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sequence` int(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) NOT NULL,
  `city_code` varchar(255) NOT NULL,
  `state_id` bigint(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `city_code`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Rawalpindi', 'Rwp', 1, 1, '2020-08-15 16:28:57', '2020-08-22 17:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `competetion_orders`
--

DROP TABLE IF EXISTS `competetion_orders`;
CREATE TABLE IF NOT EXISTS `competetion_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) DEFAULT NULL,
  `totalQty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_amount` float NOT NULL,
  `method` varchar(255) DEFAULT NULL,
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` enum('pending','processing','completed','declined','on delivery') NOT NULL DEFAULT 'pending',
  `donate` bigint(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency_sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_value` double NOT NULL,
  `pay_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `discount_value` int(191) DEFAULT NULL,
  `campaign_id` bigint(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `competetion_orders`
--

INSERT INTO `competetion_orders` (`id`, `user_id`, `totalQty`, `pay_amount`, `method`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `status`, `donate`, `created_at`, `updated_at`, `currency_sign`, `currency_value`, `pay_id`, `discount_value`, `campaign_id`) VALUES
(163, 13, '10', 1362.4, 'Stripe', 'txn_1HlCdcHtn7YzSCYloDgtDHaM', 'ch_1HlCdcHtn7YzSCYlYyvP7wxA', 'Y1qz1604837087', 'Completed', 'vendor@gmail.com', 'Vendor', 'United Arab Emirates', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-08 07:04:52', '2020-11-08 07:04:52', 'AED', 3.67, NULL, 0, 3),
(164, 13, '10', 1362.4, 'Stripe', 'txn_1HlCjtHtn7YzSCYlzLEmQaz7', 'ch_1HlCjtHtn7YzSCYl2bkx1T07', 'b1sx1604837478', 'Completed', 'vendor@gmail.com', 'Vendor', 'United Arab Emirates', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-08 07:11:21', '2020-11-08 07:11:21', 'AED', 3.67, NULL, 0, 3),
(165, 13, '10', 1362.4, 'Stripe', 'txn_1HlCl4Htn7YzSCYlp95wGKe6', 'ch_1HlCl4Htn7YzSCYlsiPX0iSw', 'bEVd1604837551', 'Completed', 'vendor@gmail.com', 'Vendor', 'United Arab Emirates', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-08 07:12:34', '2020-11-08 07:12:34', 'AED', 3.67, NULL, 0, 3),
(166, 13, '10', 1362.4, 'Stripe', 'txn_1HlDPRHtn7YzSCYl6VbjALlX', 'ch_1HlDPRHtn7YzSCYlailZBeqw', '1zFE1604840054', 'Completed', 'vendor@gmail.com', 'Vendor', 'United Arab Emirates', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 10, '2020-11-08 07:54:16', '2020-11-08 07:54:16', 'AED', 3.67, NULL, 0, 3),
(167, 22, '20', 2452.32, 'Stripe', 'txn_1HlTqOHtn7YzSCYldVv4j3KB', 'ch_1HlTqNHtn7YzSCYl5CMW0ZgP', 'QkHW1604903228', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 5, '2020-11-09 01:27:12', '2020-11-09 01:27:12', 'AED', 3.67, NULL, 10, 3),
(168, 22, '10', 1362.4, 'Stripe', 'txn_1HnerSHtn7YzSCYlQfbpHG92', 'ch_1HnerSHtn7YzSCYlSpxt2ryY', 'l5Jr1605422234', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-15 01:37:18', '2020-11-15 01:37:18', 'AED', 3.67, NULL, 0, 3),
(169, 22, '10', 1362.4, 'Stripe', 'txn_1HnerSHtn7YzSCYlsQZg3XJX', 'ch_1HnerSHtn7YzSCYlyfOXgWOX', '07EQ1605422234', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-15 01:37:18', '2020-11-15 01:37:18', 'AED', 3.67, NULL, 0, 3),
(170, 22, '10', 1362.4, 'Stripe', 'txn_1HnetUHtn7YzSCYls96L1N0N', 'ch_1HnetUHtn7YzSCYlhIEh0K2k', 'Dm991605422361', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-15 01:39:24', '2020-11-15 01:39:24', 'AED', 3.67, NULL, 0, 3),
(171, 22, '10', 1362.4, 'Stripe', 'txn_1Hnf3fHtn7YzSCYlRgl48Hld', 'ch_1Hnf3fHtn7YzSCYlbXLd86El', '4yBf1605422992', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-15 01:49:55', '2020-11-15 01:49:55', 'AED', 3.67, NULL, 0, 3),
(172, 22, '5', 681.199, 'Stripe', 'txn_1Hon8oHtn7YzSCYlgsb90pbN', 'ch_1Hon8oHtn7YzSCYldHhumqMH', 'PItC1605692387', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 5, '2020-11-18 04:39:50', '2020-11-18 04:39:50', 'AED', 3.67, NULL, 0, 8),
(173, 22, '5', 681.199, 'Stripe', 'txn_1HonAcHtn7YzSCYlfMV3c5Mf', 'ch_1HonAcHtn7YzSCYlHN0IKME0', '0x9E1605692499', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 5, '2020-11-18 04:41:42', '2020-11-18 04:41:42', 'AED', 3.67, NULL, 0, 8),
(174, 22, '5', 681.199, 'Stripe', 'txn_1HonBrHtn7YzSCYl08nwkwUx', 'ch_1HonBrHtn7YzSCYld1uCkPx0', 'BLPO1605692576', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 5, '2020-11-18 04:42:59', '2020-11-18 04:42:59', 'AED', 3.67, NULL, 0, 8),
(175, 22, '10', 1362.4, 'Stripe', 'txn_1HonaBHtn7YzSCYlwb1R5veD', 'ch_1HonaBHtn7YzSCYlVbBRQxgw', 'bmXl1605694084', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 1, '2020-11-18 05:08:07', '2020-11-18 05:08:07', 'AED', 3.67, NULL, 0, 8),
(176, 22, '5', 681.199, 'Stripe', 'txn_1Honf2Htn7YzSCYlJOIqtTWA', 'ch_1Honf2Htn7YzSCYlKXBVEMU5', 'LaHn1605694385', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 5, '2020-11-18 05:13:08', '2020-11-18 05:13:08', 'AED', 3.67, NULL, 0, 8),
(177, 22, '5', 681.199, 'Stripe', 'txn_1HonhGHtn7YzSCYlC8Bl6MZT', 'ch_1HonhGHtn7YzSCYl48Xm8ae0', 'wOhO1605694524', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'completed', 5, '2020-11-18 05:15:26', '2020-11-18 05:15:26', 'AED', 3.67, NULL, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

DROP TABLE IF EXISTS `conversations`;
CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) NOT NULL,
  `recieved_user` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

DROP TABLE IF EXISTS `counters`;
CREATE TABLE IF NOT EXISTS `counters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('referral','browser') NOT NULL DEFAULT 'referral',
  `referral` varchar(255) DEFAULT NULL,
  `total_count` int(11) NOT NULL DEFAULT '0',
  `todays_count` int(11) NOT NULL DEFAULT '0',
  `today` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`id`, `type`, `referral`, `total_count`, `todays_count`, `today`) VALUES
(1, 'referral', 'www.facebook.com', 5, 0, NULL),
(2, 'referral', 'geniusocean.com', 2, 0, NULL),
(3, 'browser', 'Windows 10', 1848, 0, NULL),
(4, 'browser', 'Linux', 225, 0, NULL),
(5, 'browser', 'Unknown OS Platform', 413, 0, NULL),
(6, 'browser', 'Windows 7', 573, 0, NULL),
(7, 'referral', 'yandex.ru', 15, 0, NULL),
(8, 'browser', 'Windows 8.1', 536, 0, NULL),
(9, 'referral', 'www.google.com', 6, 0, NULL),
(10, 'browser', 'Android', 390, 0, NULL),
(11, 'browser', 'Mac OS X', 581, 0, NULL),
(12, 'referral', 'l.facebook.com', 1, 0, NULL),
(13, 'referral', 'codecanyon.net', 6, 0, NULL),
(14, 'browser', 'Windows XP', 2, 0, NULL),
(15, 'browser', 'Windows 8', 1, 0, NULL),
(16, 'browser', 'iPad', 10, 0, NULL),
(17, 'browser', 'Ubuntu', 1, 0, NULL),
(18, 'browser', 'iPhone', 96, 0, NULL),
(19, 'referral', 'www.responsinator.com', 10, 0, NULL),
(20, 'referral', 'responsivetesttool.com', 1, 0, NULL),
(21, 'referral', 'mobiletest.me', 3, 0, NULL),
(22, 'referral', 'iphone5simulator.com', 3, 0, NULL),
(23, 'referral', 'mobile-internet-check.browserstack.com', 5, 0, NULL),
(24, 'referral', 'localhost', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(3) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  `phone_code` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`, `phone_code`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(2, 'AL', 'Albania', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(3, 'DZ', 'Algeria', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(4, 'DS', 'American Samoa', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(5, 'AD', 'Andorra', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(6, 'AO', 'Angola', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(7, 'AI', 'Anguilla', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(8, 'AQ', 'Antarctica', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(9, 'AG', 'Antigua and Barbuda', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(10, 'AR', 'Argentina', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(11, 'AM', 'Armenia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(12, 'AW', 'Aruba', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(13, 'AU', 'Australia', NULL, 'AU.png', 0, '2020-08-15 20:04:48', '2020-10-21 19:51:09'),
(14, 'AT', 'Austria', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(15, 'AZ', 'Azerbaijan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(16, 'BS', 'Bahamas', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(17, 'BH', 'Bahrain', NULL, 'BH.png', 1, '2020-08-15 20:04:48', '2020-08-15 22:30:45'),
(18, 'BD', 'Bangladesh', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(19, 'BB', 'Barbados', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(20, 'BY', 'Belarus', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(21, 'BE', 'Belgium', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(22, 'BZ', 'Belize', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(23, 'BJ', 'Benin', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(24, 'BM', 'Bermuda', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(25, 'BT', 'Bhutan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(26, 'BO', 'Bolivia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(27, 'BA', 'Bosnia and Herzegovina', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(28, 'BW', 'Botswana', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(29, 'BV', 'Bouvet Island', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(30, 'BR', 'Brazil', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(31, 'IO', 'British Indian Ocean Territory', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(32, 'BN', 'Brunei Darussalam', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(33, 'BG', 'Bulgaria', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(34, 'BF', 'Burkina Faso', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(35, 'BI', 'Burundi', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(36, 'KH', 'Cambodia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(37, 'CM', 'Cameroon', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(38, 'CA', 'Canada', NULL, 'CA.png', 1, '2020-08-15 20:04:48', '2020-08-15 22:33:18'),
(39, 'CV', 'Cape Verde', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(40, 'KY', 'Cayman Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(41, 'CF', 'Central African Republic', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(42, 'TD', 'Chad', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(43, 'CL', 'Chile', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(44, 'CN', 'China', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(45, 'CX', 'Christmas Island', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(46, 'CC', 'Cocos (Keeling) Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(47, 'CO', 'Colombia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(48, 'KM', 'Comoros', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(49, 'CD', 'Democratic Republic of the Congo', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(50, 'CG', 'Republic of Congo', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(51, 'CK', 'Cook Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(52, 'CR', 'Costa Rica', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(53, 'HR', 'Croatia (Hrvatska)', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(54, 'CU', 'Cuba', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(55, 'CY', 'Cyprus', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(56, 'CZ', 'Czech Republic', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(57, 'DK', 'Denmark', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(58, 'DJ', 'Djibouti', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(59, 'DM', 'Dominica', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(60, 'DO', 'Dominican Republic', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(61, 'TP', 'East Timor', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(62, 'EC', 'Ecuador', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(63, 'EG', 'Egypt', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(64, 'SV', 'El Salvador', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(65, 'GQ', 'Equatorial Guinea', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(66, 'ER', 'Eritrea', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(67, 'EE', 'Estonia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(68, 'ET', 'Ethiopia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(69, 'FK', 'Falkland Islands (Malvinas)', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(70, 'FO', 'Faroe Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(71, 'FJ', 'Fiji', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(72, 'FI', 'Finland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(73, 'FR', 'France', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(74, 'FX', 'France, Metropolitan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(75, 'GF', 'French Guiana', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(76, 'PF', 'French Polynesia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(77, 'TF', 'French Southern Territories', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(78, 'GA', 'Gabon', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(79, 'GM', 'Gambia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(80, 'GE', 'Georgia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(81, 'DE', 'Germany', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(82, 'GH', 'Ghana', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(83, 'GI', 'Gibraltar', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(84, 'GK', 'Guernsey', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(85, 'GR', 'Greece', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(86, 'GL', 'Greenland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(87, 'GD', 'Grenada', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(88, 'GP', 'Guadeloupe', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(89, 'GU', 'Guam', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(90, 'GT', 'Guatemala', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(91, 'GN', 'Guinea', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(92, 'GW', 'Guinea-Bissau', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(93, 'GY', 'Guyana', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(94, 'HT', 'Haiti', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(95, 'HM', 'Heard and Mc Donald Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(96, 'HN', 'Honduras', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(97, 'HK', 'Hong Kong', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(98, 'HU', 'Hungary', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(99, 'IS', 'Iceland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(100, 'IN', 'India', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(101, 'IM', 'Isle of Man', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(102, 'ID', 'Indonesia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(103, 'IR', 'Iran (Islamic Republic of)', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(104, 'IQ', 'Iraq', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(105, 'IE', 'Ireland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(106, 'IL', 'Israel', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(107, 'IT', 'Italy', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(108, 'CI', 'Ivory Coast', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(109, 'JE', 'Jersey', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(110, 'JM', 'Jamaica', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(111, 'JP', 'Japan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(112, 'JO', 'Jordan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(113, 'KZ', 'Kazakhstan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(114, 'KE', 'Kenya', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(115, 'KI', 'Kiribati', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(116, 'KP', 'Korea, Democratic People\'s Republic of', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(117, 'KR', 'Korea, Republic of', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(118, 'XK', 'Kosovo', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(119, 'KW', 'Kuwait', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(120, 'KG', 'Kyrgyzstan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(121, 'LA', 'Lao People\'s Democratic Republic', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(122, 'LV', 'Latvia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(123, 'LB', 'Lebanon', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(124, 'LS', 'Lesotho', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(125, 'LR', 'Liberia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(126, 'LY', 'Libyan Arab Jamahiriya', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(127, 'LI', 'Liechtenstein', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(128, 'LT', 'Lithuania', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(129, 'LU', 'Luxembourg', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(130, 'MO', 'Macau', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(131, 'MK', 'North Macedonia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(132, 'MG', 'Madagascar', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(133, 'MW', 'Malawi', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(134, 'MY', 'Malaysia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(135, 'MV', 'Maldives', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(136, 'ML', 'Mali', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(137, 'MT', 'Malta', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(138, 'MH', 'Marshall Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(139, 'MQ', 'Martinique', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(140, 'MR', 'Mauritania', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(141, 'MU', 'Mauritius', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(142, 'TY', 'Mayotte', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(143, 'MX', 'Mexico', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(144, 'FM', 'Micronesia, Federated States of', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(145, 'MD', 'Moldova, Republic of', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(146, 'MC', 'Monaco', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(147, 'MN', 'Mongolia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(148, 'ME', 'Montenegro', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(149, 'MS', 'Montserrat', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(150, 'MA', 'Morocco', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(151, 'MZ', 'Mozambique', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(152, 'MM', 'Myanmar', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(153, 'NA', 'Namibia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(154, 'NR', 'Nauru', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(155, 'NP', 'Nepal', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(156, 'NL', 'Netherlands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(157, 'AN', 'Netherlands Antilles', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(158, 'NC', 'New Caledonia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(159, 'NZ', 'New Zealand', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(160, 'NI', 'Nicaragua', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(161, 'NE', 'Niger', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(162, 'NG', 'Nigeria', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(163, 'NU', 'Niue', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(164, 'NF', 'Norfolk Island', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(165, 'MP', 'Northern Mariana Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(166, 'NO', 'Norway', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(167, 'OM', 'Oman', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(168, 'PK', 'Pakistan', NULL, NULL, 0, '2020-08-15 20:04:48', '2020-08-15 15:07:29'),
(169, 'PW', 'Palau', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(170, 'PS', 'Palestine', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(171, 'PA', 'Panama', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(172, 'PG', 'Papua New Guinea', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(173, 'PY', 'Paraguay', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(174, 'PE', 'Peru', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(175, 'PH', 'Philippines', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(176, 'PN', 'Pitcairn', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(177, 'PL', 'Poland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(178, 'PT', 'Portugal', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(179, 'PR', 'Puerto Rico', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(180, 'QA', 'Qatar', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(181, 'RE', 'Reunion', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(182, 'RO', 'Romania', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(183, 'RU', 'Russian Federation', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(184, 'RW', 'Rwanda', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(185, 'KN', 'Saint Kitts and Nevis', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(186, 'LC', 'Saint Lucia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(187, 'VC', 'Saint Vincent and the Grenadines', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(188, 'WS', 'Samoa', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(189, 'SM', 'San Marino', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(190, 'ST', 'Sao Tome and Principe', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(191, 'SA', 'Saudi Arabia', NULL, 'SA.png', 0, '2020-08-15 20:04:48', '2020-10-21 19:51:03'),
(192, 'SN', 'Senegal', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(193, 'RS', 'Serbia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(194, 'SC', 'Seychelles', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(195, 'SL', 'Sierra Leone', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(196, 'SG', 'Singapore', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(197, 'SK', 'Slovakia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(198, 'SI', 'Slovenia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(199, 'SB', 'Solomon Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(200, 'SO', 'Somalia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(201, 'ZA', 'South Africa', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(202, 'GS', 'South Georgia South Sandwich Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(203, 'SS', 'South Sudan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(204, 'ES', 'Spain', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(205, 'LK', 'Sri Lanka', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(206, 'SH', 'St. Helena', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(207, 'PM', 'St. Pierre and Miquelon', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(208, 'SD', 'Sudan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(209, 'SR', 'Suriname', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(210, 'SJ', 'Svalbard and Jan Mayen Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(211, 'SZ', 'Swaziland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(212, 'SE', 'Sweden', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(213, 'CH', 'Switzerland', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(214, 'SY', 'Syrian Arab Republic', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(215, 'TW', 'Taiwan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(216, 'TJ', 'Tajikistan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(217, 'TZ', 'Tanzania, United Republic of', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(218, 'TH', 'Thailand', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(219, 'TG', 'Togo', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(220, 'TK', 'Tokelau', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(221, 'TO', 'Tonga', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(222, 'TT', 'Trinidad and Tobago', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(223, 'TN', 'Tunisia', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(224, 'TR', 'Turkey', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(225, 'TM', 'Turkmenistan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(226, 'TC', 'Turks and Caicos Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(227, 'TV', 'Tuvalu', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(228, 'UG', 'Uganda', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(229, 'UA', 'Ukraine', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(230, 'UAE', 'United Arab Emirates', '971', 'AE.png', 1, '2020-08-15 20:04:48', '2020-10-21 19:50:53'),
(231, 'GB', 'United Kingdom', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(232, 'US', 'United States', NULL, 'US.png', 0, '2020-08-15 20:04:48', '2020-10-21 19:51:04'),
(233, 'UM', 'United States minor outlying islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(234, 'UY', 'Uruguay', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(235, 'UZ', 'Uzbekistan', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(236, 'VU', 'Vanuatu', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(237, 'VA', 'Vatican City State', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(238, 'VE', 'Venezuela', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(239, 'VN', 'Vietnam', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(240, 'VG', 'Virgin Islands (British)', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(241, 'VI', 'Virgin Islands (U.S.)', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(242, 'WF', 'Wallis and Futuna Islands', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(243, 'EH', 'Western Sahara', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(244, 'YE', 'Yemen', NULL, NULL, NULL, '2020-08-15 20:04:48', '0000-00-00 00:00:00'),
(245, 'ZM', 'Zambia', NULL, NULL, 0, '2020-08-15 20:04:48', '2020-08-22 15:01:13'),
(246, 'ZW', 'Zimbabwe', NULL, NULL, 0, '2020-08-15 20:04:48', '2020-08-22 15:01:40');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `times` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `type`, `price`, `times`, `used`, `status`, `start_date`, `end_date`) VALUES
(1, 'eqwe', 1, 12.22, '957', 51, 1, '2020-09-21', '2020-09-21'),
(2, 'sdsdsasd', 0, 11, NULL, 2, 1, '2019-05-23', '2022-05-26'),
(3, 'werwd', 0, 22, NULL, 3, 1, '2019-05-23', '2023-06-08'),
(4, 'asdasd', 1, 23.5, NULL, 1, 1, '2019-05-23', '2020-05-28'),
(5, 'kopakopakopa', 0, 40, NULL, 3, 1, '2019-05-23', '2032-05-20'),
(6, 'rererere', 1, 9, '665', 1, 1, '2019-05-23', '2022-05-26');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `sign`, `value`, `is_default`, `country_id`) VALUES
(1, 'USD', '$', 1, 0, 232),
(10, 'AUS', '$', 1.38, 0, 13),
(11, 'CAD', '$', 1.32, 0, 38),
(12, 'BHD', 'BHD', 0.38, 0, 17),
(13, 'SAR', 'SAR', 3.67, 0, 191),
(14, 'AED', 'AED', 3.67, 1, 230);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subject` mediumtext COLLATE utf8_unicode_ci,
  `email_body` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_type`, `email_subject`, `email_body`, `status`) VALUES
(1, 'new_order', 'Your Order Placed Successfully', '<p>Hello {customer_name},<br>Your Order Number is {order_number}<br>Your order has been placed successfully</p>', 1),
(2, 'new_registration', 'Welcome To Royal Commerce', '<p>Hello {customer_name},<br>You have successfully registered to {website_title}, We wish you will have a wonderful experience using our service.</p><p>Thank You<br></p>', 1),
(3, 'vendor_accept', 'Your Vendor Account Activated', '<p>Hello {customer_name},<br>Your Vendor Account Activated Successfully. Please Login to your account and build your own shop.</p><p>Thank You<br></p>', 1),
(4, 'subscription_warning', 'Your subscrption plan will end after five days', '<p>Hello {customer_name},<br>Your subscription plan duration will end after five days. Please renew your plan otherwise all of your products will be deactivated.</p><p>Thank You<br></p>', 1),
(5, 'vendor_verification', 'Request for verification.', '<p>Hello {customer_name},<br>You are requested verify your account. Please send us photo of your passport.</p><p>Thank You<br></p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type` bigint(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `details`, `status`, `type`) VALUES
(6, 'How do I create your account?', '<div style=\"text-align: center;\"><div style=\"text-align: center;\"><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif; text-align: left;\"><span style=\"color: rgb(48, 48, 48);\">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS.</span><br></p></div></div>', 0, 1),
(7, 'How do I create your account?', '<span style=\"color: rgb(128, 127, 127); font-family: Lato, sans-serif; font-size: 16px;\">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</span><br>', 0, 1),
(8, 'What Credit cards do Lux Dxb accept?', '<span style=\"color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\">Lux Dxb accepts all major credit/debit cards including (but not limited to): Visa, MasterCard and Amex&nbsp;</span>', 0, 1),
(9, 'How do I create your account?', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">To create an account you need to sign-in/Register in the top right-hand corner of your screen and enter the required information</p>', 0, 2),
(10, 'How do I edit my address information', '<font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">1. Sign Into your account&nbsp;</span></font><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">2. Select Manage addresses&nbsp;</span></font></div><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">3. Edit your existing address or add a new address&nbsp;<br></span></font><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\"><br></span></font></div></div>', 0, 2),
(11, 'How do I change my password?', '<font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">1. Sign in to your account&nbsp;</span></font><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">2.&nbsp; Select update password&nbsp;</span></font></div><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\">3. Enter current password, then add your new password below.&nbsp;</span></font></div><div><font color=\"#807f7f\" face=\"Lato, sans-serif\"><span style=\"font-size: 16px;\"><br></span></font></div>', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faq_cats`
--

DROP TABLE IF EXISTS `faq_cats`;
CREATE TABLE IF NOT EXISTS `faq_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_cats`
--

INSERT INTO `faq_cats` (`id`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Account', '2020-10-31 03:25:55', '2020-10-31 03:52:18'),
(2, 'Payments', '2020-10-31 03:52:33', '2020-10-31 03:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_sellers`
--

DROP TABLE IF EXISTS `favorite_sellers`;
CREATE TABLE IF NOT EXISTS `favorite_sellers` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `vendor_id` int(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite_sellers`
--

INSERT INTO `favorite_sellers` (`id`, `user_id`, `vendor_id`) VALUES
(1, 22, 13),
(2, 29, 13),
(4, 13, 13);

-- --------------------------------------------------------

--
-- Table structure for table `footer_pages`
--

DROP TABLE IF EXISTS `footer_pages`;
CREATE TABLE IF NOT EXISTS `footer_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_pages`
--

INSERT INTO `footer_pages` (`id`, `name`, `slug`, `category`, `type`, `visible`) VALUES
(2, 'Privacy & Policy', 'privacy', 'customer service', 0, 1),
(3, 'Delivery', 'delivery', 'shopping online', 0, 0),
(4, 'Return Policy', 'return', 'shopping online', 0, 1),
(5, 'Cookies Policy', 'cookie', 'customer service', 0, 1),
(6, 'Lux Rewards', 'lux-rewards', 'shopping online', 0, 1),
(7, 'Mini Lux', 'mini-lux', 'about', 0, 0),
(8, 'Gift Card Balance', 'gift-card-balance', 'shopping online', 0, 1),
(9, 'Lux Group', 'lux-group', 'shopping online', 0, 0),
(10, 'Become An Affiliate', 'affiliate', 'about', 0, 0),
(11, 'Corporate Responsibility', 'corporate-responsibility', 'shopping online', 0, 1),
(12, 'Press', 'press', 'shopping online', 0, 1),
(13, 'Accessibility', 'accessibility', 'shopping online', 0, 1),
(14, 'Career', 'career', 'about', 1, 0),
(15, 'FAQ', 'faq', 'customer service', 1, 0),
(16, 'Privacy', 'privacy', 'shopping online', 1, 1),
(17, 'Terms', 'terms', 'customer service', 1, 0),
(18, 'About', 'about', 'about', 1, 0),
(19, 'Charity', 'charity', 'customer service', 1, 0),
(20, 'How It Work', 'how_it_work', 'customer service', 1, 0),
(22, 'My Account', 'user/dashboard', 'shopping online', 1, 0),
(23, 'My Orders', 'user/orders', 'shopping online', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(191) UNSIGNED NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `product_id`, `photo`) VALUES
(6, 93, '156801646314-min.jpg'),
(7, 93, '156801646315-min.jpg'),
(8, 93, '156801646316-min.jpg'),
(22, 129, '15680254328Ei8T0MB.jpg'),
(23, 129, '1568025432wRmpve8d.jpg'),
(24, 129, '1568025432kkRYzLsF.jpg'),
(25, 129, '1568025432zxQBe6Gk.jpg'),
(26, 128, '1568025537sJbDPnFk.jpg'),
(27, 128, '1568025537NBmHxJOz.jpg'),
(28, 128, '1568025537hxqeFbS8.jpg'),
(29, 128, '1568025538zK3tJpmL.jpg'),
(34, 126, '1568025693kKLReNYO.jpg'),
(35, 126, '1568025694Iv3pkz1q.jpg'),
(36, 126, '1568025694T8HhdLVS.jpg'),
(37, 126, '1568025695vTdg7ndt.jpg'),
(38, 125, '15680257894Waz2tuN.jpg'),
(39, 125, '1568025789vd0P4TBv.jpg'),
(40, 125, '15680257899bih5sGh.jpg'),
(41, 125, '156802578924sLIgzl.jpg'),
(42, 124, '1568025825cC2Pmuit.jpg'),
(43, 124, '1568025825EACzLFld.jpg'),
(44, 124, '1568025825MfCyCqtD.jpg'),
(45, 124, '15680258252yabMeAz.jpg'),
(46, 123, '15680258512fKQla5g.jpg'),
(47, 123, '1568025851pIjl0mWp.jpg'),
(48, 123, '1568025851tQw7JXXG.jpg'),
(49, 123, '1568025851ewjtSDkZ.jpg'),
(50, 96, '1568025891wWAAbOjc.jpg'),
(51, 96, '1568025891fyMNeXRy.jpg'),
(52, 96, '1568025891OdV64Tw1.jpg'),
(53, 96, '1568025891xQF7Zufe.jpg'),
(62, 101, '1568026331Y6UMgMcI.jpg'),
(63, 101, '1568026331xZbT4OWG.jpg'),
(64, 101, '1568026331y7eIFJXZ.jpg'),
(65, 101, '1568026331i2wH8RI0.jpg'),
(66, 100, '1568026374xCTjQYZ8.jpg'),
(67, 100, '1568026374DzmvqA9d.jpg'),
(68, 100, '1568026374OEH73u5X.jpg'),
(69, 100, '1568026374vZhqRv8c.jpg'),
(70, 99, '15680264120LdBSU1v.jpg'),
(71, 99, '1568026412eMjsI940.jpg'),
(72, 99, '1568026412GFjvHiZv.jpg'),
(73, 99, '15680264122fwGi20d.jpg'),
(78, 97, '1568026469hSlmBpzE.jpg'),
(79, 97, '15680264697AI8LicQ.jpg'),
(80, 97, '15680264691xyFt5Y6.jpg'),
(81, 97, '1568026469dC3hrMz0.jpg'),
(86, 109, '1568026737EBGSE78G.jpg'),
(87, 109, '1568026737B8hO1RRr.jpg'),
(88, 109, '1568026737tf0rwVoz.jpg'),
(89, 109, '1568026737GGIPSqYo.jpg'),
(95, 107, '1568026797FFNrNPxK.jpg'),
(96, 107, '1568026797UwY9ZLfQ.jpg'),
(97, 107, '1568026797Kl6eZLx5.jpg'),
(98, 107, '1568026797h3R48VaO.jpg'),
(99, 107, '15680267989kXwH40I.jpg'),
(100, 106, '1568026836ErM5FJxg.jpg'),
(101, 106, '1568026836VLrxIk0u.jpg'),
(102, 106, '1568026836lgLuMV6p.jpg'),
(103, 106, '1568026836JBUTQX8v.jpg'),
(104, 105, '1568026861YorsLvUa.jpg'),
(105, 105, '1568026861PikoX1Qb.jpg'),
(106, 105, '1568026861SBJqjw66.jpg'),
(107, 105, '1568026861WYh54Arp.jpg'),
(108, 104, '1568026885rmo0LDoo.jpg'),
(109, 104, '15680268851m939o7O.jpg'),
(110, 104, '1568026885fVXYCGKu.jpg'),
(111, 104, '1568026885GDRL3thY.jpg'),
(112, 103, '1568026903LbVQUxIr.jpg'),
(113, 103, '1568026914IpRVYDV4.jpg'),
(114, 103, '15680269141gKO8x5X.jpg'),
(115, 103, '1568026914Q938xXM2.jpg'),
(116, 93, '1568026950y7ihS4wE.jpg'),
(125, 122, '1568027503rFK94cnU.jpg'),
(126, 122, '1568027503i1X2FtIi.jpg'),
(127, 122, '156802750316jxawoZ.jpg'),
(128, 122, '1568027503QRBf290F.jpg'),
(129, 121, '1568027539SQqUc8Bu.jpg'),
(130, 121, '1568027539Zs5OTzjq.jpg'),
(131, 121, '1568027539C45VRZq1.jpg'),
(132, 121, '15680275398ovCzFnJ.jpg'),
(133, 120, '1568027565bJgX744G.jpg'),
(134, 120, '1568027565j0RPFUgX.jpg'),
(135, 120, '1568027565QGi6Lhyo.jpg'),
(136, 120, '15680275658MAY3VKp.jpg'),
(137, 119, '1568027610p9R6ivC6.jpg'),
(138, 119, '1568027610t2Aq7E5D.jpg'),
(139, 119, '1568027611ikz4n0fx.jpg'),
(140, 119, '15680276117BLgrCub.jpg'),
(141, 118, '156802763634t0c8tG.jpg'),
(142, 118, '1568027636fuJplSf3.jpg'),
(143, 118, '1568027636MXcgCQHU.jpg'),
(144, 118, '1568027636lfexGTpt.jpg'),
(145, 117, '1568027665rFHWlsAJ.jpg'),
(146, 117, '15680276655LPktA9k.jpg'),
(147, 117, '1568027665vcNWWq3u.jpg'),
(148, 117, '1568027665gQnqKhCw.jpg'),
(149, 116, '1568027692FPQpwtWN.jpg'),
(150, 116, '1568027692zBaGjOIC.jpg'),
(151, 116, '1568027692UXpDx63F.jpg'),
(152, 116, '1568027692KdIWbIGK.jpg'),
(153, 95, '1568027743xS8gHocM.jpg'),
(154, 95, '1568027743aVUOljdD.jpg'),
(155, 95, '156802774327OOA1Zj.jpg'),
(156, 95, '1568027743kGBx6mxa.jpg'),
(172, 130, '1568029084hQT5ZO0j.jpg'),
(173, 130, '1568029084ncGXxQzN.jpg'),
(174, 130, '1568029084b0OonKFy.jpg'),
(175, 130, '15680290857TD4iOWP.jpg'),
(180, 114, '1568029158brS7xQCe.jpg'),
(181, 114, '1568029158QlC0tg5a.jpg'),
(182, 114, '1568029158RrN4AEtQ.jpg'),
(187, 112, '1568029210JSAwjRPr.jpg'),
(188, 112, '1568029210EiVUkcK6.jpg'),
(189, 112, '1568029210fJSo5hya.jpg'),
(190, 112, '15680292101vCcGfq8.jpg'),
(191, 111, '1568029272lB0JETcn.jpg'),
(192, 111, '1568029272wF3ldKgv.jpg'),
(193, 111, '1568029272NI33ExCu.jpg'),
(194, 111, '15680292724TXrpokz.jpg'),
(197, 134, '15693932021.jpg'),
(198, 134, '15693932022.jpg'),
(199, 135, '15698200931.jpg'),
(217, 159, '1570085246audi-automobile-car-909907.jpg'),
(218, 159, '1570085246automobile-automotive-car-112460.jpg'),
(219, 160, '1570085654asphalt-auto-automobile-575386.jpg'),
(220, 160, '1570085654asphalt-auto-automobile-831475.jpg'),
(221, 161, '1570086479audi-automobile-car-909907.jpg'),
(222, 162, '1570255905asphalt-auto-automobile-831475.jpg'),
(223, 162, '1570255905audi-automobile-car-909907.jpg'),
(224, 167, '1570874976asphalt-auto-automobile-831475.jpg'),
(225, 167, '1570874976audi-automobile-car-909907.jpg'),
(226, 167, '1570874976automobile-automotive-car-112460.jpg'),
(227, 168, '1570875445automobile-automotive-car-112460.jpg'),
(228, 168, '1570875445automobile-automotive-car-358070.jpg'),
(231, 185, '1594163275Untitled.png'),
(233, 185, '1594164410gvFcBazM.jpg'),
(238, 217, '1600331927LKvzRUZ6.jpg'),
(239, 217, '1600331927zSNOaGoh.jpg'),
(240, 217, '1600331927q9avGctR.jpg'),
(241, 1251, '1604833731Xi8LZeat.jpg'),
(242, 1251, '1604833732YtlTLM7k.jpg'),
(243, 1251, '16048337321zsLHcTi.jpg'),
(244, 1251, '1604833733mGuuSTfN.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `generalsettings`
--

DROP TABLE IF EXISTS `generalsettings`;
CREATE TABLE IF NOT EXISTS `generalsettings` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_email` text COLLATE utf8mb4_unicode_ci,
  `header_phone` text COLLATE utf8mb4_unicode_ci,
  `footer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loader` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_loader` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_talkto` tinyint(1) NOT NULL DEFAULT '1',
  `talkto` text COLLATE utf8mb4_unicode_ci,
  `is_language` tinyint(1) NOT NULL DEFAULT '1',
  `is_loader` tinyint(1) NOT NULL DEFAULT '1',
  `map_key` text COLLATE utf8mb4_unicode_ci,
  `is_disqus` tinyint(1) NOT NULL DEFAULT '0',
  `disqus` longtext COLLATE utf8mb4_unicode_ci,
  `is_contact` tinyint(1) NOT NULL DEFAULT '0',
  `is_faq` tinyint(1) NOT NULL DEFAULT '0',
  `guest_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_check` tinyint(1) NOT NULL DEFAULT '0',
  `cod_check` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_key` text COLLATE utf8mb4_unicode_ci,
  `stripe_secret` text COLLATE utf8mb4_unicode_ci,
  `currency_format` tinyint(1) NOT NULL DEFAULT '0',
  `withdraw_fee` double NOT NULL DEFAULT '0',
  `withdraw_charge` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `smtp_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_smtp` tinyint(1) NOT NULL DEFAULT '0',
  `is_comment` tinyint(1) NOT NULL DEFAULT '1',
  `is_currency` tinyint(1) NOT NULL DEFAULT '1',
  `add_cart` text COLLATE utf8mb4_unicode_ci,
  `out_stock` text COLLATE utf8mb4_unicode_ci,
  `add_wish` text COLLATE utf8mb4_unicode_ci,
  `already_wish` text COLLATE utf8mb4_unicode_ci,
  `wish_remove` text COLLATE utf8mb4_unicode_ci,
  `add_compare` text COLLATE utf8mb4_unicode_ci,
  `already_compare` text COLLATE utf8mb4_unicode_ci,
  `compare_remove` text COLLATE utf8mb4_unicode_ci,
  `color_change` text COLLATE utf8mb4_unicode_ci,
  `coupon_found` text COLLATE utf8mb4_unicode_ci,
  `no_coupon` text COLLATE utf8mb4_unicode_ci,
  `already_coupon` text COLLATE utf8mb4_unicode_ci,
  `order_title` text COLLATE utf8mb4_unicode_ci,
  `order_text` text COLLATE utf8mb4_unicode_ci,
  `is_affilate` tinyint(1) NOT NULL DEFAULT '1',
  `affilate_charge` int(100) NOT NULL DEFAULT '0',
  `affilate_banner` text COLLATE utf8mb4_unicode_ci,
  `already_cart` text COLLATE utf8mb4_unicode_ci,
  `fixed_commission` double NOT NULL DEFAULT '0',
  `percentage_commission` double NOT NULL DEFAULT '0',
  `multiple_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `multiple_packaging` tinyint(4) NOT NULL DEFAULT '0',
  `vendor_ship_info` tinyint(1) NOT NULL DEFAULT '0',
  `reg_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `cod_text` text COLLATE utf8mb4_unicode_ci,
  `paypal_text` text COLLATE utf8mb4_unicode_ci,
  `stripe_text` text COLLATE utf8mb4_unicode_ci,
  `header_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin_loader` tinyint(1) NOT NULL DEFAULT '0',
  `menu_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_hover_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_verification_email` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_text` text COLLATE utf8mb4_unicode_ci,
  `is_instamojo` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_sandbox` tinyint(1) NOT NULL DEFAULT '0',
  `is_paystack` tinyint(1) NOT NULL DEFAULT '0',
  `paystack_key` text COLLATE utf8mb4_unicode_ci,
  `paystack_email` text COLLATE utf8mb4_unicode_ci,
  `paystack_text` text COLLATE utf8mb4_unicode_ci,
  `wholesell` int(191) NOT NULL DEFAULT '0',
  `is_capcha` tinyint(1) NOT NULL DEFAULT '0',
  `error_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_popup` tinyint(1) NOT NULL DEFAULT '0',
  `popup_title` text COLLATE utf8mb4_unicode_ci,
  `popup_text` text COLLATE utf8mb4_unicode_ci,
  `popup_background` text COLLATE utf8mb4_unicode_ci,
  `invoice_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_secure` tinyint(1) NOT NULL DEFAULT '0',
  `is_report` tinyint(1) NOT NULL,
  `paypal_check` tinyint(1) DEFAULT '0',
  `paypal_business` text COLLATE utf8mb4_unicode_ci,
  `footer_logo` text COLLATE utf8mb4_unicode_ci,
  `email_encryption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paytm_merchant` text COLLATE utf8mb4_unicode_ci,
  `paytm_secret` text COLLATE utf8mb4_unicode_ci,
  `paytm_website` text COLLATE utf8mb4_unicode_ci,
  `paytm_industry` text COLLATE utf8mb4_unicode_ci,
  `header_login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_paytm` int(11) NOT NULL DEFAULT '1',
  `paytm_text` text COLLATE utf8mb4_unicode_ci,
  `paytm_mode` enum('sandbox','live') CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `is_molly` tinyint(1) NOT NULL DEFAULT '0',
  `molly_key` text COLLATE utf8mb4_unicode_ci,
  `molly_text` text COLLATE utf8mb4_unicode_ci,
  `is_razorpay` int(11) NOT NULL DEFAULT '1',
  `razorpay_key` text COLLATE utf8mb4_unicode_ci,
  `razorpay_secret` text COLLATE utf8mb4_unicode_ci,
  `razorpay_text` text COLLATE utf8mb4_unicode_ci,
  `show_stock` tinyint(1) NOT NULL DEFAULT '0',
  `is_maintain` tinyint(1) NOT NULL DEFAULT '0',
  `maintain_text` text COLLATE utf8mb4_unicode_ci,
  `home_campaign_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_campaign_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_prize_given_away` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section1_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section1_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section2_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section2_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_short_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_short_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_charges` double DEFAULT NULL,
  `free_delivery` double DEFAULT NULL,
  `contact_store_inquiries` longtext COLLATE utf8mb4_unicode_ci,
  `contact_online_inquiries` longtext COLLATE utf8mb4_unicode_ci,
  `all_timing` longtext COLLATE utf8mb4_unicode_ci,
  `sunday_timing` longtext COLLATE utf8mb4_unicode_ci,
  `browsing_timing` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generalsettings`
--

INSERT INTO `generalsettings` (`id`, `logo`, `favicon`, `title`, `header_email`, `header_phone`, `footer`, `copyright`, `colors`, `loader`, `admin_loader`, `is_talkto`, `talkto`, `is_language`, `is_loader`, `map_key`, `is_disqus`, `disqus`, `is_contact`, `is_faq`, `guest_checkout`, `stripe_check`, `cod_check`, `stripe_key`, `stripe_secret`, `currency_format`, `withdraw_fee`, `withdraw_charge`, `tax`, `shipping_cost`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `from_email`, `from_name`, `is_smtp`, `is_comment`, `is_currency`, `add_cart`, `out_stock`, `add_wish`, `already_wish`, `wish_remove`, `add_compare`, `already_compare`, `compare_remove`, `color_change`, `coupon_found`, `no_coupon`, `already_coupon`, `order_title`, `order_text`, `is_affilate`, `affilate_charge`, `affilate_banner`, `already_cart`, `fixed_commission`, `percentage_commission`, `multiple_shipping`, `multiple_packaging`, `vendor_ship_info`, `reg_vendor`, `cod_text`, `paypal_text`, `stripe_text`, `header_color`, `footer_color`, `copyright_color`, `is_admin_loader`, `menu_color`, `menu_hover_color`, `is_home`, `is_verification_email`, `instamojo_key`, `instamojo_token`, `instamojo_text`, `is_instamojo`, `instamojo_sandbox`, `is_paystack`, `paystack_key`, `paystack_email`, `paystack_text`, `wholesell`, `is_capcha`, `error_banner`, `is_popup`, `popup_title`, `popup_text`, `popup_background`, `invoice_logo`, `user_image`, `vendor_color`, `is_secure`, `is_report`, `paypal_check`, `paypal_business`, `footer_logo`, `email_encryption`, `paytm_merchant`, `paytm_secret`, `paytm_website`, `paytm_industry`, `header_login`, `is_paytm`, `paytm_text`, `paytm_mode`, `is_molly`, `molly_key`, `molly_text`, `is_razorpay`, `razorpay_key`, `razorpay_secret`, `razorpay_text`, `show_stock`, `is_maintain`, `maintain_text`, `home_campaign_heading`, `home_campaign_desc`, `home_prize_given_away`, `home_section1_heading`, `home_section1_description`, `home_section2_heading`, `home_section2_description`, `delivery_short_desc`, `return_short_desc`, `delivery_charges`, `free_delivery`, `contact_store_inquiries`, `contact_online_inquiries`, `all_timing`, `sunday_timing`, `browsing_timing`) VALUES
(1, '', '1594169140logo.png', 'Luxury Watches', 'smtp', '0123 456789', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae', 'COPYRIGHT © 2019. All Rights Reserved By <a href=\"http://geniusocean.com/\">GeniusOcean.com</a>', '#0f78f2', '1564224328loading3.gif', '1564224329loading3.gif', 0, '<script type=\"text/javascript\">\r\nvar Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n(function(){\r\nvar s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\ns1.async=true;\r\ns1.src=\'https://embed.tawk.to/5bc2019c61d0b77092512d03/default\';\r\ns1.charset=\'UTF-8\';\r\ns1.setAttribute(\'crossorigin\',\'*\');\r\ns0.parentNode.insertBefore(s1,s0);\r\n})();\r\n</script>', 1, 1, 'AIzaSyB1GpE4qeoJ__70UZxvX9CTMUTZRZNHcu8', 0, '<div id=\"disqus_thread\">         \r\n    <script>\r\n    /**\r\n    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.\r\n    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/\r\n    /*\r\n    var disqus_config = function () {\r\n    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page\'s canonical URL variable\r\n    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page\'s unique identifier variable\r\n    };\r\n    */\r\n    (function() { // DON\'T EDIT BELOW THIS LINE\r\n    var d = document, s = d.createElement(\'script\');\r\n    s.src = \'https://junnun.disqus.com/embed.js\';\r\n    s.setAttribute(\'data-timestamp\', +new Date());\r\n    (d.head || d.body).appendChild(s);\r\n    })();\r\n    </script>\r\n    <noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>\r\n    </div>', 1, 1, 1, 1, 1, 'pk_test_51H6HWuHtn7YzSCYl7TjB0D7hNXgqKJgP9OjjAtYGSL7GUQTK2z8GyVkkJOuTYXbj6i3eNIoDPbYSMcrcZo85cXw500Apl9A6vq', 'sk_test_51H6HWuHtn7YzSCYlNBkDwcCjpME5nsqQTSm7zT1ONVKsBD5RMHfG2lOHVaNjTgIZ6ScrnzTLyAmEGwLtfKUPhA7700qjWOJDtU', 0, 5, 5, 0, 5, NULL, NULL, NULL, NULL, 'zafar4203@gmail.com', 'ManaraTest', 1, 1, 1, 'Successfully Added To Cart', 'Out Of Stock', 'Add To Wishlist', 'Already Added To Wishlist', 'Successfully Removed From The Wishlist', 'Successfully Added To Compare', 'Already Added To Compare', 'Successfully Removed From The Compare', 'Successfully Changed The Color', 'Coupon Found', 'No Coupon Found', 'Coupon Already Applied', 'THANK YOU FOR YOUR PURCHASE.', 'We\'ll email you an order confirmation with details and tracking info.', 1, 8, '15587771131554048228onepiece.jpeg', 'Already Added To Cart', 0, 0, 1, 1, 1, 1, 'Pay with cash upon delivery.', 'Pay via your PayPal account.', 'Pay via your Credit Card.', '#ffffff', '#143250', '#02020c', 1, '#ff5500', '#02020c', 0, 1, 'test_172371aa837ae5cad6047dc3052', 'test_4ac5a785e25fc596b67dbc5c267', 'Pay via your Instamojo account.', 1, 1, 1, 'pk_test_162a56d42131cbb01932ed0d2c48f9cb99d8e8e2', 'junnuns@gmail.com', 'Pay via your Paystack account.', 6, 1, '1566878455404.png', 1, 'NEWSLETTER', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita porro ipsa nulla, alias, ab minus.', '1584934329adv-banner.jpg', '1604563434logo.png', '1567655174profile.jpg', '#666666', 0, 1, 1, 'shaon143-facilitator-1@gmail.com', '', NULL, 'tkogux49985047638244', 'LhNGUUKE9xCQ9xY8', 'WEBSTAGING', 'Retail', '1603307985login-bg.png', 1, 'Pay via your Paytm account.', 'sandbox', 1, 'test_5HcWVs9qc5pzy36H9Tu9mwAyats33J', 'Pay with Molly Payment.', 1, 'rzp_test_xDH74d48cwl8DF', 'cr0H1BiQ20hVzhpHfHuNbGri', 'Pay via your Razorpay account.', 0, 0, '<div style=\"text-align: center;\"><font size=\"5\"><br></font></div><h1 style=\"text-align: center;\"><font size=\"6\">UNDER MAINTENANCE</font></h1>', 'Lux Campaigns', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', 'We\'ve given away AED20000 worth of timepieces -and counting', 'Lux Watch Pop up', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco Ut enim ad minim veniam, quis nostrud exercitation ullamco', 'Lux Watch Pop up', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco Ut enim ad minim veniam, quis nostrud exercitation ullamco', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', 0, 0, '+97(0)33322232', 'customer.support@luxwatches.com', 'Monday-Saturday:11am-7pm', 'Sunday:11.30am-6pm*', 'browsing only 11.30am-12pm');

-- --------------------------------------------------------

--
-- Table structure for table `homes`
--

DROP TABLE IF EXISTS `homes`;
CREATE TABLE IF NOT EXISTS `homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_url` varchar(255) DEFAULT NULL,
  `para_1` longtext,
  `para_1_heading` longtext,
  `para_1_find_out_more` longtext,
  `para_2` longtext,
  `para_2_heading` longtext,
  `para_2_find_out_more` longtext,
  `image_1` longtext,
  `image_2` longtext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homes`
--

INSERT INTO `homes` (`id`, `video_url`, `para_1`, `para_1_heading`, `para_1_find_out_more`, `para_2`, `para_2_heading`, `para_2_find_out_more`, `image_1`, `image_2`, `status`) VALUES
(1, 'https://www.youtube.com/embed/tgbNymZ7vqY', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco Ut enim ad minim veniam, quis nostrud exercitation ullamco<br>', 'Lux Watch Pop up', 'Find out more', '<span style=\"color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 18px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco Ut enim ad minim veniam, quis nostrud exercitation ullamco</span><br>', 'Lux Watch Pop up', 'Find out More', '1604926080left-img.jpg', '1604926080watch-inner.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hows`
--

DROP TABLE IF EXISTS `hows`;
CREATE TABLE IF NOT EXISTS `hows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `para_1` longtext,
  `para_2` longtext,
  `box_1_heading` longtext,
  `box_1_details` longtext,
  `box_2_heading` varchar(255) DEFAULT NULL,
  `box_2_details` longtext,
  `box_3_heading` varchar(255) DEFAULT NULL,
  `box_3_details` longtext,
  `box_4_heading` varchar(255) DEFAULT NULL,
  `box_4_details` longtext,
  `image` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hows`
--

INSERT INTO `hows` (`id`, `para_1`, `para_2`, `box_1_heading`, `box_1_details`, `box_2_heading`, `box_2_details`, `box_3_heading`, `box_3_details`, `box_4_heading`, `box_4_details`, `image`, `status`) VALUES
(1, '<span style=\"color: rgb(128, 127, 127); font-family: Lato, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labor et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess cill dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa offici deserunt mollit anim id est laborum.</span>', '<span style=\"color: rgb(128, 127, 127); font-family: Lato, sans-serif; font-size: 16px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudant totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae di sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</span><br>', 'FREE SHIPPING', 'Free shipping on all order<br>', 'ONLINE SUPPORT', 'Online support 24 hours a day<br>', 'MONEY RETURN', 'Back guarantee under 5 days<br>', 'MEMBER DISCOUNT', 'Onevery order over $150<br>', '1604825395Desert.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `is_default`, `language`, `file`) VALUES
(1, 1, 'English', '1579926860LzpDa1Y7.json'),
(2, 0, 'RTL English', '1579927527QjLMUGyj.json');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) DEFAULT NULL,
  `recieved_user` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `order_id` int(191) UNSIGNED DEFAULT NULL,
  `user_id` int(191) DEFAULT NULL,
  `vendor_id` int(191) DEFAULT NULL,
  `product_id` int(191) DEFAULT NULL,
  `conversation_id` int(191) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `order_id`, `user_id`, `vendor_id`, `product_id`, `conversation_id`, `is_read`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 168, NULL, 1, '2020-07-06 13:41:22', '2020-07-06 13:58:21'),
(2, NULL, 29, NULL, NULL, NULL, 1, '2020-07-06 15:44:36', '2020-07-06 23:26:18'),
(3, NULL, 31, NULL, NULL, NULL, 1, '2020-07-14 01:56:57', '2020-07-31 03:15:35'),
(4, NULL, 32, NULL, NULL, NULL, 1, '2020-07-14 02:07:23', '2020-07-31 03:15:35'),
(5, NULL, 54, NULL, NULL, NULL, 1, '2020-07-14 03:08:13', '2020-07-31 03:15:35'),
(6, NULL, 61, NULL, NULL, NULL, 1, '2020-07-14 03:24:20', '2020-07-31 03:15:35'),
(7, NULL, NULL, NULL, 168, NULL, 1, '2020-07-14 03:29:46', '2020-08-21 14:56:17'),
(10, NULL, NULL, NULL, 168, NULL, 1, '2020-07-14 07:07:08', '2020-08-21 14:56:17'),
(15, NULL, 62, NULL, NULL, NULL, 1, '2020-07-30 14:51:52', '2020-07-31 03:15:35'),
(23, NULL, NULL, NULL, 168, NULL, 1, '2020-08-11 05:19:10', '2020-08-21 14:56:17'),
(57, 124, NULL, NULL, NULL, NULL, 1, '2020-10-04 09:09:35', '2020-11-04 08:04:38'),
(58, 125, NULL, NULL, NULL, NULL, 1, '2020-10-05 09:05:58', '2020-11-04 08:04:38'),
(59, 126, NULL, NULL, NULL, NULL, 1, '2020-10-05 09:08:25', '2020-11-04 08:04:38'),
(60, 127, NULL, NULL, NULL, NULL, 1, '2020-10-05 21:15:10', '2020-11-04 08:04:38'),
(61, 128, NULL, NULL, NULL, NULL, 1, '2020-10-22 11:41:14', '2020-11-04 08:04:38'),
(62, 129, NULL, NULL, NULL, NULL, 1, '2020-11-04 05:33:22', '2020-11-04 08:04:38'),
(63, 130, NULL, NULL, NULL, NULL, 1, '2020-11-04 05:56:33', '2020-11-04 08:04:38'),
(64, 131, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:01:23', '2020-11-04 08:04:38'),
(65, 132, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:02:15', '2020-11-04 08:04:38'),
(66, 133, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:02:41', '2020-11-04 08:04:38'),
(67, 134, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:05:12', '2020-11-04 08:04:38'),
(68, 135, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:05:33', '2020-11-04 08:04:38'),
(69, 136, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:05:52', '2020-11-04 08:04:38'),
(70, 137, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:08:53', '2020-11-04 08:04:38'),
(71, 138, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:09:36', '2020-11-04 08:04:38'),
(72, 139, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:09:44', '2020-11-04 08:04:37'),
(73, 140, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:09:59', '2020-11-04 08:04:37'),
(74, 141, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:10:10', '2020-11-04 08:04:37'),
(75, 142, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:10:28', '2020-11-04 08:04:37'),
(76, 143, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:10:39', '2020-11-04 08:04:37'),
(77, 144, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:10:54', '2020-11-04 08:04:37'),
(78, 145, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:12:06', '2020-11-04 08:04:37'),
(79, 146, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:28:38', '2020-11-04 08:04:37'),
(80, 147, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:29:31', '2020-11-04 08:04:37'),
(81, 148, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:47:24', '2020-11-04 08:04:37'),
(82, 149, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:48:45', '2020-11-04 08:04:37'),
(83, 150, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:49:12', '2020-11-04 08:04:37'),
(84, 151, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:51:52', '2020-11-04 08:04:37'),
(85, 152, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:52:24', '2020-11-04 08:04:37'),
(86, 153, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:54:06', '2020-11-04 08:04:37'),
(87, 154, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:02:11', '2020-11-04 08:04:37'),
(88, 155, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:04:34', '2020-11-04 08:04:37'),
(89, 156, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:05:33', '2020-11-04 08:04:36'),
(90, 157, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:06:52', '2020-11-04 08:04:36'),
(91, 158, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:07:18', '2020-11-04 08:04:36'),
(92, 159, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:12:03', '2020-11-04 08:04:36'),
(93, 130, NULL, NULL, NULL, NULL, 1, '2020-11-04 07:14:32', '2020-11-04 08:04:36'),
(94, 160, NULL, NULL, NULL, NULL, 1, '2020-11-04 08:02:00', '2020-11-04 08:04:36'),
(95, 161, NULL, NULL, NULL, NULL, 1, '2020-11-05 02:28:30', '2020-11-07 06:54:36'),
(96, 162, NULL, NULL, NULL, NULL, 1, '2020-11-05 03:40:54', '2020-11-07 06:54:36'),
(97, 131, NULL, NULL, NULL, NULL, 1, '2020-11-05 05:33:46', '2020-11-07 06:54:36'),
(98, 132, NULL, NULL, NULL, NULL, 1, '2020-11-05 05:37:16', '2020-11-07 06:54:36'),
(99, 133, NULL, NULL, NULL, NULL, 1, '2020-11-05 05:48:13', '2020-11-07 06:54:36'),
(100, 134, NULL, NULL, NULL, NULL, 1, '2020-11-05 05:50:14', '2020-11-07 06:54:36'),
(101, 135, NULL, NULL, NULL, NULL, 1, '2020-11-07 04:59:54', '2020-11-07 06:54:36'),
(102, 136, NULL, NULL, NULL, NULL, 1, '2020-11-07 05:03:09', '2020-11-07 06:54:36'),
(103, 137, NULL, NULL, NULL, NULL, 1, '2020-11-07 05:17:31', '2020-11-07 06:54:36'),
(104, 138, NULL, NULL, NULL, NULL, 1, '2020-11-08 02:02:32', '2020-11-10 04:37:20'),
(105, 163, NULL, NULL, NULL, NULL, 1, '2020-11-08 07:04:52', '2020-11-10 04:37:20'),
(106, 164, NULL, NULL, NULL, NULL, 1, '2020-11-08 07:11:21', '2020-11-10 04:37:20'),
(107, 165, NULL, NULL, NULL, NULL, 1, '2020-11-08 07:12:34', '2020-11-10 04:37:20'),
(108, 139, NULL, NULL, NULL, NULL, 1, '2020-11-08 07:17:49', '2020-11-10 04:37:20'),
(109, 140, NULL, NULL, NULL, NULL, 1, '2020-11-08 07:17:49', '2020-11-10 04:37:20'),
(110, 166, NULL, NULL, NULL, NULL, 1, '2020-11-08 07:54:17', '2020-11-10 04:37:20'),
(111, 167, NULL, NULL, NULL, NULL, 1, '2020-11-09 01:27:12', '2020-11-10 04:37:20'),
(112, 141, NULL, NULL, NULL, NULL, 1, '2020-11-09 07:16:18', '2020-11-10 04:37:20'),
(113, 169, NULL, NULL, NULL, NULL, 0, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(114, 168, NULL, NULL, NULL, NULL, 0, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(115, 170, NULL, NULL, NULL, NULL, 0, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(116, 171, NULL, NULL, NULL, NULL, 0, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(117, 172, NULL, NULL, NULL, NULL, 0, '2020-11-18 04:39:50', '2020-11-18 04:39:50'),
(118, 173, NULL, NULL, NULL, NULL, 0, '2020-11-18 04:41:42', '2020-11-18 04:41:42'),
(119, 174, NULL, NULL, NULL, NULL, 0, '2020-11-18 04:42:59', '2020-11-18 04:42:59'),
(120, 175, NULL, NULL, NULL, NULL, 0, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(121, 176, NULL, NULL, NULL, NULL, 0, '2020-11-18 05:13:08', '2020-11-18 05:13:08'),
(122, 177, NULL, NULL, NULL, NULL, 0, '2020-11-18 05:15:26', '2020-11-18 05:15:26'),
(123, 142, NULL, NULL, NULL, NULL, 0, '2020-11-19 07:58:24', '2020-11-19 07:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `product_id` bigint(191) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `name`, `description`, `image`, `product_id`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Zafaraaaa', 'Grade A', '160053896623.jpg', 243, '2020-09-15', '2020-09-28', 1, '2020-09-21 07:25:56', '2020-09-19 03:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) DEFAULT NULL,
  `cart` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pickup_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalQty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_amount` float NOT NULL,
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `coupon_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `affilate_user` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `affilate_charge` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_value` double NOT NULL,
  `shipping_cost` double DEFAULT NULL,
  `packing_cost` double NOT NULL DEFAULT '0',
  `tax` int(191) DEFAULT NULL,
  `dp` tinyint(1) NOT NULL DEFAULT '0',
  `pay_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_shipping_id` int(191) DEFAULT '0',
  `vendor_packing_id` int(191) NOT NULL DEFAULT '0',
  `inbuild_discount` bigint(100) DEFAULT NULL,
  `discount_value` int(191) DEFAULT NULL,
  `delivery_charges` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `cart`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`, `inbuild_discount`, `discount_value`, `delivery_charges`) VALUES
(139, 13, 'BZh91AY&SY.(\0ï_@@Pø+ü¿ÿÿúP¼:\ZF$Õ=Mé©ê¨òDÐ 44PÒQ\0\0Ð\0\04\0MSQ@\0\0\0\0z\0À\0\0\0\0\0\0\0	h\"&¦ÔÉ£FÔ\Zq¬	öwAâQëqîaÅâS1CÝEbPQíTU´/°rA0ÇÙLää©hU×,BdËe±Î¯³ñ$âk¡\'t\'³a¨¶EÍ§.%»H-uòx@q.o< Ci#:däBB¿j¬«èêy `£3)\Z0ëYIf|F\Z¨ê:¦Â!\rq7­¨HÆÈèS\"¤@õ0l­çU$ú)#Ð©Ø5DÁ6¶ÓSjVmõUNF*´*\rsJJ.Ú¤¡UB¥Ýâ¤å%¥âªpÙ\n];Å¸à¨¨ï-<`c aKbK(pÁÆDc%¹E¸zÝ­+9f$E-íjl%!1Òurîá:ª2lp¨\nt©$UKJ©4ÝDçL¢` È,\nµ >L´VÖ,ÁeKBÅÂr ÀB0Hb¦\0\0ÚäÏnãvÈHY¤%FHøfÉ­W¤´ÛVMö¶y¹­NnrÀ7Æc\\9Ä¶7LmeÝÉJ:i\Znàã35G²Tj1r4Äé¢¨Ëg¡Qã©ö}Lx>?}ãû¾öºlÌ«Ö²ôëf¬0mâ¢aGöh(:r×Y»yàvyé?eªÈ}<\rìXp;cxI8úGÌ>\ZäF\rf°¹¨F5#!Ý`vÊÐÚ]l`6Uhj;¼02¡¬Ë7~#)¿;»öðåBÁ{kWp%Úhý¢lÙ\"¢ÇqXÓð2#`ÇlI.®ÆxXÍ£ÉéSQºuw÷¿ØÔÚá¡ÛiöT±âÈÈÐÅãÐOfµæ{Ci´\0pçrÆ\"yÌ¯O ·¥Ê§S2BZ×ªõº$g±C¸)s4/S½r;t2¶ðöfñ6O#ah2,\ZÕaIj¢ óT\r¤ð ÌY5$P£\"ôIEH&%	R0º#-£mÐ[»Q©\r2õþE1Í;&Þ5³yðÆ\\E\'(£apùLÌDuÖY(¥S#I 2Y¤eV·2ÓÛ*çK%1ÝBj5È!AÐi`âAtA^|í²¤rx÷ üÁ^´H1Â#!a@Á(`0i&ÂMÅÎzû÷YÊ#z\\eà\\±°Dp¢3$\\wAsâà§´YÆG³:¬fKPö&133¦RCp°k ®´I2_¬¦Í\Z*0%:àéJc¦K#Eô0ßÀä1@Qd&óbØÁA«Jö>ãóÌuëäØ6(`Ý¹\"(HD\0', 'Stripe', NULL, NULL, '1', 286.1, 'txn_1HlCq9Htn7YzSCYlXnSP1N6I', 'ch_1HlCq8Htn7YzSCYlQ0wehgRE', 'R7JN1604837866', 'Completed', 'vendor@gmail.com', 'Vendor', 'United Arab Emirates', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'pending', '2020-11-08 07:17:48', '2020-11-08 07:17:48', NULL, NULL, 'AED', 3.67, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, 0),
(140, 13, 'BZh91AY&SY.(\0ï_@@Pø+ü¿ÿÿúP¼:\ZF$Õ=Mé©ê¨òDÐ 44PÒQ\0\0Ð\0\04\0MSQ@\0\0\0\0z\0À\0\0\0\0\0\0\0	h\"&¦ÔÉ£FÔ\Zq¬	öwAâQëqîaÅâS1CÝEbPQíTU´/°rA0ÇÙLää©hU×,BdËe±Î¯³ñ$âk¡\'t\'³a¨¶EÍ§.%»H-uòx@q.o< Ci#:däBB¿j¬«èêy `£3)\Z0ëYIf|F\Z¨ê:¦Â!\rq7­¨HÆÈèS\"¤@õ0l­çU$ú)#Ð©Ø5DÁ6¶ÓSjVmõUNF*´*\rsJJ.Ú¤¡UB¥Ýâ¤å%¥âªpÙ\n];Å¸à¨¨ï-<`c aKbK(pÁÆDc%¹E¸zÝ­+9f$E-íjl%!1Òurîá:ª2lp¨\nt©$UKJ©4ÝDçL¢` È,\nµ >L´VÖ,ÁeKBÅÂr ÀB0Hb¦\0\0ÚäÏnãvÈHY¤%FHøfÉ­W¤´ÛVMö¶y¹­NnrÀ7Æc\\9Ä¶7LmeÝÉJ:i\Znàã35G²Tj1r4Äé¢¨Ëg¡Qã©ö}Lx>?}ãû¾öºlÌ«Ö²ôëf¬0mâ¢aGöh(:r×Y»yàvyé?eªÈ}<\rìXp;cxI8úGÌ>\ZäF\rf°¹¨F5#!Ý`vÊÐÚ]l`6Uhj;¼02¡¬Ë7~#)¿;»öðåBÁ{kWp%Úhý¢lÙ\"¢ÇqXÓð2#`ÇlI.®ÆxXÍ£ÉéSQºuw÷¿ØÔÚá¡ÛiöT±âÈÈÐÅãÐOfµæ{Ci´\0pçrÆ\"yÌ¯O ·¥Ê§S2BZ×ªõº$g±C¸)s4/S½r;t2¶ðöfñ6O#ah2,\ZÕaIj¢ óT\r¤ð ÌY5$P£\"ôIEH&%	R0º#-£mÐ[»Q©\r2õþE1Í;&Þ5³yðÆ\\E\'(£apùLÌDuÖY(¥S#I 2Y¤eV·2ÓÛ*çK%1ÝBj5È!AÐi`âAtA^|í²¤rx÷ üÁ^´H1Â#!a@Á(`0i&ÂMÅÎzû÷YÊ#z\\eà\\±°Dp¢3$\\wAsâà§´YÆG³:¬fKPö&133¦RCp°k ®´I2_¬¦Í\Z*0%:àéJc¦K#Eô0ßÀä1@Qd&óbØÁA«Jö>ãóÌuëäØ6(`Ý¹\"(HD\0', 'Stripe', NULL, NULL, '1', 286.1, 'txn_1HlCq9Htn7YzSCYlSQms5NpK', 'ch_1HlCq8Htn7YzSCYl8HqnNLsL', 'uWas1604837866', 'Completed', 'vendor@gmail.com', 'Vendor', 'United Arab Emirates', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'pending', '2020-11-08 07:17:48', '2020-11-08 07:17:48', NULL, NULL, 'AED', 3.67, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, 0),
(141, 22, 'BZh91AY&SY¡pµÔ\0î_D\0Pø;ö¿ÿÿúPû< 1Ü:r»$RO)´£ Ú\Z@§¤\0\04h%4$©½)a	£	èiJ\0 \0Ð\0@\0À&\0L\0`\0&\0D&£)úP@=O)`RàS0Âj\0¸ìíHBâ·Î2IB!7ÈCZù_88r³ñôFÅ®XÆ)ÕSëj6uN$¤ÖR&qb{°3)ySÀé±R\nU[Rz@l	TÐîä;	Y&¤4ß¼5s.}Ø8	EÀÓI¢¶µ¦¨É(@ ;¼Á&	EöZJµ!`Ò	ÀI\0ÜUÐ2ÊdI2j¨¹¨!!M)&aªä ¦L$ ©¦°à)´6+©2±fdDKÊ&¯Éª°ó5ÈFÑ$#QÈ\r;53¨e!NìÊx0e¦F#\nëM-¨jÎ)<Dâ³vj*CVÊ%T$Øk§º2ÜíÕõQöq\rdRBp!u©	4É*#fBÅ@ÕHR Å m@T\0F^_9ôgfLÃ5¦ff?ëMAU+äçu\'EÞö¿;¢çu«8@oÀæxhÜAºsj[ÀUmäÎhâõ¬ÙÆ6RyÝMB1¦§!Æ¨ðÕ\rÂÃ rIlv9\"æ_8E¦æ¬ÛUo	\"MH`Û¦¦«H@¦â}ÛÌ©¢S8ròËöÓ¨¸ô7eÇ3ñ±EiáÐß¨Ãc~d²eäÏvSÉTÄØ!¨dexé¡´·-`ÎåÈÈÌ¡³±(c.ì3yê@@Cì2\\4W^:O\ZÃs§C`IHØÃdÿ£³e	± ¥	Eä	Uc;!½ò¸ÄñA\"ñ<þ#PukNF\ZtÍp9HqÔ%bÆÀÆ(½÷b¾\'ìwM N¶!b!rñK¼»T!p$p<È;d$¦ÖÖê¢c=BU7!~æ¨è63õ8s0®ðög6F¥:ö<¦ÆùÈ k*\ne.D!â·Û²Ç3MDAzþQ-.SÒ:ä´&KÌR)/[Æ!¿¨·êfHÊd,ÖQ=)É4ÊK§«$4â2J#a\ZÎÆdH+E+ÍB´Y3:Æ;øfLÑÇÑÔÒÈeaJJÅÔCJÆ¤p,W=¥$+tó î14nÖA&FJ\"È\0HPYâò§Lµñ£#z\\ÅEÌ©C2EBHÀ¶ñ\0é³[E\"/9á#lIb<ÓÈ\nK\\L,¬Gª.&ª0kçKÕ¬¿1é2ÌÔ6JjfnW­\Z^£×BAæ&¯x1@ò/$5Y¯a©ÓD/ø`Á¾ Ô	Oø»)Â® ', 'Stripe', NULL, NULL, '1', 136.24, 'txn_1HlZIDHtn7YzSCYliE7Ut0wX', 'ch_1HlZICHtn7YzSCYlGLzvfo9t', 'SDvl1604924172', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'pending', '2020-11-09 07:16:17', '2020-11-09 07:16:17', NULL, NULL, 'AED', 3.67, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, 0),
(142, 22, 'BZh91AY&SYL]\03_E\0Pø;Küî¿ÿÿú`ÿ>\0P¥¤\nU#ÀL\0LL&4ÄÓÀL\0LL&4ÄÓÀL\0LL&4ÄÓÀL\0LL&4ÄÓ)ªm54OSAê0PÉ¦âôÔy\0© A!éSÆ£(ÈÈdÃMÔoTÂP::?ÝûÝä#ïPþ¯ÆRTújQ¢b\r?áy¼ÄÌ©z?ôn«Ám=lml4>eQÀ`¥à0Ë	N¯ÅGÛ2viFÍÞ02ÊÎ,E.äþì6`ÃuÔWb]èý6q2ðo-Ðñ7bm<­êõnl{ÎR«w	àïôvzÍýäø<.ú\"%(ôRH¥HRÓúGäûºz¾MÏµùÏ&Çç}ñ%¨0±y!§Ú´Â¨ÊSL-£ìXéhâ ²¤cÉzoGU!ºP¥]dZmL©Êì¹b]¥£h:1tûJ,2³4tTK¡ÊÖS\\­F,o,)rR$¥,ÙvÍ\rÙ,f(JR¥))2Êí0ËL0Þ¬¥*RR¡HeuãM.d³ÜÁ¹eÔ¨¥-¥.»-,êrkQ¢lÓMviÖ®!D.Ù\Z2ÝÂü¸njmláfeÕÌ³­¦cm8swöÚ	¥¥²³Ùt¦Ë.ÂË8hQ»MÔ²óctÃ(©²í©¦[¦Ó-Û2]uÆ2Ý¦XeqCm0\\XÌÂ³u)6M2³LÍÓIÏüÕ v)à2E(b° ¡r,¨)ü*,¥,´²¡JåÆ0ÕÖQC\nJ¥¡$Z\nH¤%!V«(B$,v»^NO%ü/$4j ü%E<Ã+\Zf4Ó-2ÄãZYÂl²y°å\r]£¢ó]¯,nè¹Ð§+®rÊÇ*eb¶dÒ§+nSMÖnÌèÓ\r5:6reErÝË\\%e¬£ Àj0hS²úN7Ì÷ÞGin¤ó+ z0s£ßÃÐhõòxOÀ3éau\ZÈ<ïk±¥<³õ¿ÉcHÒE\nR¬K3,³À ÒakA°³ü`/PhCÎf{iÈò±ïéîx!·`YþÈîO&óI»Ý\'¹ÚQ£ÌîO	èé*K¾^óRÇ3\'ú»1!ïPvì6e¦î®¢G½©ÕÊnÊiÊéH¥DòxÉ³	äÔ{çiã4tR¬,Þ]eJ2:»ðQì>ÒYO¢ïkºÊl¦ÏsØÃgg-§K(¥2MÛ·´;®tÛ-æY±¡hÖÚ|f&É²¦|¥xJRÒÔêy0í%Ógw¶~ã*,|Tßrï7SO÷w\'ï äku¾0#¦`aÂlÄ=ÏÔRî¨NïyhQR\nSë8(ò89ÎSSäSmâyÝm<¦À-´ÈËÌ¶F%O¤¼ö¥Û¢Oú;J)êö=óÏ9>{	J»OÖ|Ø¢eu%K4Ruhe,¼³vÌ×xñ\Zèw\Z\Zn\Z[\0É±²Ó0©ç10IõÈ¼¥£\'	ç(~S ó|_{]ÑÖ7hq=_»w	M7>ùIÃýk=]§¤ÌKO	ô=ùNòÏR¢tèú:4FTváðÍÒz§¶SwiíçÅµË²>ÚGiSeËâÍøji¡²Àæ`»iÞ3Qæ9\rG	£fJ¡_ÐcÒFIQ***T%J±D¥	*T$²¢J©/<¦ÓÕÕgwJey¤ød÷ÍNÅ¤¥I¦ÌÌ²Éèò´æOÑKzif³ß:9]9Í(ï°ácó£¥0ä¹a²tïÌ&Í¡ÊéC¢´³GÞðdøNÏÜz$´L&r³Ý.?Ä§wyiý%ñ¹)N\"Ò&òòL\'ÒQè>ñ&ð$ùÏú¥D(:Ë¸»)Âbè0`', 'Stripe', NULL, NULL, '8', 2179.84, 'txn_1HpCiRHtn7YzSCYlFzDFnX9J', 'ch_1HpCiRHtn7YzSCYlVOViBwxC', 'uG3q1605790696', 'Completed', 'user@gmail.com', 'User', 'Turkey', '3155054781231', 'new address 1', 'istanbul', '12355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'pending', '2020-11-19 07:58:23', '2020-11-19 07:58:23', NULL, NULL, 'AED', 3.67, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_tracks`
--

DROP TABLE IF EXISTS `order_tracks`;
CREATE TABLE IF NOT EXISTS `order_tracks` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `order_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tracks`
--

INSERT INTO `order_tracks` (`id`, `order_id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 29, 'Pending', 'You have successfully placed your order.', '2020-07-14 06:59:56', '2020-07-14 06:59:56'),
(2, 31, 'Pending', 'You have successfully placed your order.', '2020-07-14 07:07:08', '2020-07-14 07:07:08'),
(3, 32, 'Pending', 'You have successfully placed your order.', '2020-07-14 07:09:01', '2020-07-14 07:09:01'),
(4, 33, 'Pending', 'You have successfully placed your order.', '2020-07-14 07:29:47', '2020-07-14 07:29:47'),
(5, 34, 'Pending', 'You have successfully placed your order.', '2020-07-14 07:34:32', '2020-07-14 07:34:32'),
(6, 35, 'Pending', 'You have successfully placed your order.', '2020-07-14 07:57:26', '2020-07-14 07:57:26'),
(7, 36, 'Pending', 'You have successfully placed your order.', '2020-07-30 22:12:03', '2020-07-30 22:12:03'),
(8, 37, 'Pending', 'You have successfully placed your order.', '2020-08-10 16:58:05', '2020-08-10 16:58:05'),
(9, 38, 'Pending', 'You have successfully placed your order.', '2020-08-11 05:15:18', '2020-08-11 05:15:18'),
(10, 39, 'Pending', 'You have successfully placed your order.', '2020-08-11 05:15:59', '2020-08-11 05:15:59'),
(11, 40, 'Pending', 'You have successfully placed your order.', '2020-08-11 05:16:08', '2020-08-11 05:16:08'),
(12, 41, 'Pending', 'You have successfully placed your order.', '2020-08-11 05:16:55', '2020-08-11 05:16:55'),
(13, 42, 'Pending', 'You have successfully placed your order.', '2020-08-11 05:19:10', '2020-08-11 05:19:10'),
(14, 43, 'Pending', 'You have successfully placed your order.', '2020-08-11 07:27:55', '2020-08-11 07:27:55'),
(15, 44, 'Pending', 'You have successfully placed your order.', '2020-08-11 07:33:12', '2020-08-11 07:33:12'),
(16, 45, 'Pending', 'You have successfully placed your order.', '2020-08-11 13:21:14', '2020-08-11 13:21:14'),
(17, 46, 'Pending', 'You have successfully placed your order.', '2020-08-11 13:21:22', '2020-08-11 13:21:22'),
(18, 47, 'Pending', 'You have successfully placed your order.', '2020-08-11 17:02:13', '2020-08-11 17:02:13'),
(19, 48, 'Pending', 'You have successfully placed your order.', '2020-08-11 17:38:10', '2020-08-11 17:38:10'),
(20, 49, 'Pending', 'You have successfully placed your order.', '2020-08-11 18:52:26', '2020-08-11 18:52:26'),
(21, 51, 'Pending', 'You have successfully placed your order.', '2020-08-16 21:47:34', '2020-08-16 21:47:34'),
(22, 52, 'Pending', 'You have successfully placed your order.', '2020-08-16 21:47:47', '2020-08-16 21:47:47'),
(23, 53, 'Pending', 'You have successfully placed your order.', '2020-08-16 21:49:38', '2020-08-16 21:49:38'),
(24, 54, 'Pending', 'You have successfully placed your order.', '2020-08-17 15:49:31', '2020-08-17 15:49:31'),
(25, 55, 'Pending', 'You have successfully placed your order.', '2020-08-17 15:58:37', '2020-08-17 15:58:37'),
(26, 56, 'Pending', 'You have successfully placed your order.', '2020-08-17 16:11:42', '2020-08-17 16:11:42'),
(27, 57, 'Pending', 'You have successfully placed your order.', '2020-08-17 16:14:29', '2020-08-17 16:14:29'),
(29, 58, 'Pending', 'You have successfully placed your order.', '2020-08-21 11:28:31', '2020-08-21 11:28:31'),
(30, 59, 'Pending', 'You have successfully placed your order.', '2020-08-21 11:29:00', '2020-08-21 11:29:00'),
(31, 60, 'Pending', 'You have successfully placed your order.', '2020-08-23 12:10:28', '2020-08-23 12:10:28'),
(32, 61, 'Pending', 'You have successfully placed your order.', '2020-08-23 12:28:42', '2020-08-23 12:28:42'),
(33, 62, 'Pending', 'You have successfully placed your order.', '2020-08-23 12:28:45', '2020-08-23 12:28:45'),
(34, 63, 'Pending', 'You have successfully placed your order.', '2020-08-23 12:45:54', '2020-08-23 12:45:54'),
(35, 64, 'Pending', 'You have successfully placed your order.', '2020-08-23 12:58:51', '2020-08-23 12:58:51'),
(36, 65, 'Pending', 'You have successfully placed your order.', '2020-08-23 13:22:58', '2020-08-23 13:22:58'),
(37, 66, 'Pending', 'You have successfully placed your order.', '2020-08-23 13:28:28', '2020-08-23 13:28:28'),
(38, 67, 'Pending', 'You have successfully placed your order.', '2020-08-23 13:33:54', '2020-08-23 13:33:54'),
(39, 68, 'Pending', 'You have successfully placed your order.', '2020-08-23 13:44:10', '2020-08-23 13:44:10'),
(40, 69, 'Pending', 'You have successfully placed your order.', '2020-08-23 13:50:19', '2020-08-23 13:50:19'),
(41, 70, 'Pending', 'You have successfully placed your order.', '2020-08-27 23:24:38', '2020-08-27 23:24:38'),
(42, 94, 'Pending', 'You have successfully placed your order.', '2020-08-28 22:11:33', '2020-08-28 22:11:33'),
(43, 95, 'Pending', 'You have successfully placed your order.', '2020-08-28 22:13:31', '2020-08-28 22:13:31'),
(44, 96, 'Pending', 'You have successfully placed your order.', '2020-08-28 22:14:09', '2020-08-28 22:14:09'),
(45, 97, 'Pending', 'You have successfully placed your order.', '2020-08-28 22:16:17', '2020-08-28 22:16:17'),
(46, 98, 'Pending', 'You have successfully placed your order.', '2020-08-28 22:18:55', '2020-08-28 22:18:55'),
(47, 100, 'Pending', 'You have successfully placed your order.', '2020-08-28 22:22:21', '2020-08-28 22:22:21'),
(48, 124, 'Pending', 'You have successfully placed your order.', '2020-10-04 09:09:35', '2020-10-04 09:09:35'),
(49, 125, 'Pending', 'You have successfully placed your order.', '2020-10-05 09:05:58', '2020-10-05 09:05:58'),
(50, 126, 'Pending', 'You have successfully placed your order.', '2020-10-05 09:08:25', '2020-10-05 09:08:25'),
(51, 127, 'Pending', 'You have successfully placed your order.', '2020-10-05 21:15:10', '2020-10-05 21:15:10'),
(52, 128, 'Pending', 'You have successfully placed your order.', '2020-10-22 11:41:14', '2020-10-22 11:41:14'),
(53, 129, 'Completed', 'Your order has completed successfully.', '2020-11-04 05:33:22', '2020-11-04 05:33:22'),
(54, 130, 'Pending', 'You have successfully placed your order.', '2020-11-04 07:14:32', '2020-11-04 07:14:32'),
(55, 131, 'Pending', 'You have successfully placed your order.', '2020-11-05 05:33:46', '2020-11-05 05:33:46'),
(56, 132, 'Pending', 'You have successfully placed your order.', '2020-11-05 05:37:15', '2020-11-05 05:37:15'),
(57, 133, 'Pending', 'You have successfully placed your order.', '2020-11-05 05:48:13', '2020-11-05 05:48:13'),
(58, 134, 'Pending', 'You have successfully placed your order.', '2020-11-05 05:50:14', '2020-11-05 05:50:14'),
(59, 135, 'Pending', 'You have successfully placed your order.', '2020-11-07 04:59:54', '2020-11-07 04:59:54'),
(60, 136, 'Pending', 'You have successfully placed your order.', '2020-11-07 05:03:09', '2020-11-07 05:03:09'),
(61, 137, 'Pending', 'You have successfully placed your order.', '2020-11-07 05:17:31', '2020-11-07 05:17:31'),
(62, 138, 'Pending', 'You have successfully placed your order.', '2020-11-08 02:02:32', '2020-11-08 02:02:32'),
(63, 139, 'Pending', 'You have successfully placed your order.', '2020-11-08 07:17:49', '2020-11-08 07:17:49'),
(64, 140, 'Pending', 'You have successfully placed your order.', '2020-11-08 07:17:49', '2020-11-08 07:17:49'),
(65, 141, 'Pending', 'You have successfully placed your order.', '2020-11-09 07:16:17', '2020-11-09 07:16:17'),
(66, 142, 'Pending', 'You have successfully placed your order.', '2020-11-19 07:58:23', '2020-11-19 07:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'Default Packaging', 'Default packaging by store', 0),
(2, 0, 'Gift Packaging', 'Exclusive Gift packaging', 15);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `header` tinyint(1) NOT NULL DEFAULT '0',
  `footer` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `details`, `meta_tag`, `meta_description`, `header`, `footer`, `status`) VALUES
(2, 'Privacy & Policy', 'privacy', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><img src=\"https://i.imgur.com/BobQuyA.jpg\" width=\"591\"></h2><h2><font size=\"6\">Title number 1</font></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" 51);\"=\"\" style=\"font-family: \"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'test,test1,test2,test3', 'Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 0, 0, NULL),
(5, 'Delivery', 'delivery', 'ALl About US Delivery', NULL, NULL, 0, 0, NULL),
(6, 'Return Poicy', 'return', 'ALl Return Policy', NULL, NULL, 0, 0, NULL),
(7, 'Cookies Policy', 'cookie', 'Coodie Data', NULL, NULL, 0, 0, NULL),
(8, 'Lux Rewards', 'lux-rewards', 'Lux Rewards Page', NULL, NULL, 0, 0, NULL),
(9, 'mini Lux', 'mini-lux', 'Mini Lux Page', NULL, NULL, 0, 0, NULL),
(10, 'Gift Card Balance', 'gift-card-balance', 'Gift Card Balance', NULL, NULL, 0, 0, NULL),
(11, 'Lux Group', 'lux-group', 'Lux Group Data', NULL, NULL, 0, 0, NULL),
(12, 'Become An Affiliate', 'affiliate', 'Affiliate Page', NULL, NULL, 0, 0, NULL),
(13, 'Corporate Responsibility', 'corporate-responsibility', 'corporate Responsibility', NULL, NULL, 0, 0, NULL),
(14, 'Press', 'press', 'Press', NULL, NULL, 0, 0, NULL),
(15, 'Accessibility', 'accessibility', 'Accessibility Page', NULL, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pagesettings`
--

DROP TABLE IF EXISTS `pagesettings`;
CREATE TABLE IF NOT EXISTS `pagesettings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_success` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_title` text COLLATE utf8mb4_unicode_ci,
  `contact_text` text COLLATE utf8mb4_unicode_ci,
  `side_title` text COLLATE utf8mb4_unicode_ci,
  `side_text` text COLLATE utf8mb4_unicode_ci,
  `street` text COLLATE utf8mb4_unicode_ci,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `fax` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `site` text COLLATE utf8mb4_unicode_ci,
  `slider` tinyint(1) NOT NULL DEFAULT '1',
  `service` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '1',
  `small_banner` tinyint(1) NOT NULL DEFAULT '1',
  `best` tinyint(1) NOT NULL DEFAULT '1',
  `top_rated` tinyint(1) NOT NULL DEFAULT '1',
  `large_banner` tinyint(1) NOT NULL DEFAULT '1',
  `big` tinyint(1) NOT NULL DEFAULT '1',
  `hot_sale` tinyint(1) NOT NULL DEFAULT '1',
  `partners` tinyint(1) NOT NULL DEFAULT '0',
  `review_blog` tinyint(1) NOT NULL DEFAULT '1',
  `best_seller_banner` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link` text COLLATE utf8mb4_unicode_ci,
  `bottom_small` tinyint(1) NOT NULL DEFAULT '0',
  `flash_deal` tinyint(1) NOT NULL DEFAULT '0',
  `best_seller_banner1` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `featured_category` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pagesettings`
--

INSERT INTO `pagesettings` (`id`, `contact_success`, `contact_email`, `contact_title`, `contact_text`, `side_title`, `side_text`, `street`, `phone`, `fax`, `email`, `site`, `slider`, `service`, `featured`, `small_banner`, `best`, `top_rated`, `large_banner`, `big`, `hot_sale`, `partners`, `review_blog`, `best_seller_banner`, `best_seller_banner_link`, `big_save_banner`, `big_save_banner_link`, `bottom_small`, `flash_deal`, `best_seller_banner1`, `best_seller_banner_link1`, `big_save_banner1`, `big_save_banner_link1`, `featured_category`) VALUES
(1, 'Success! Thanks for contacting us, we will get back to you shortly.', 'admin@geniusocean.com', '<h4 class=\"subtitle\" style=\"margin-bottom: 6px; font-weight: 600; line-height: 28px; font-size: 28px; text-transform: uppercase;\">WE\'D LOVE TO</h4><h2 class=\"title\" style=\"margin-bottom: 13px;font-weight: 600;line-height: 50px;font-size: 40px;color: #0f78f2;text-transform: uppercase;\">HEAR FROM YOU</h2>', '<span style=\"color: rgb(119, 119, 119);\">Send us a message and we\' ll respond as soon as possible</span><br>', '<h4 class=\"title\" style=\"margin-bottom: 10px; font-weight: 600; line-height: 28px; font-size: 28px;\">Let\'s Connect</h4>', '<span style=\"color: rgb(51, 51, 51);\">Get in touch with us</span>', '3584 Hickory Heights Drive ,Hanover MD 21076, USA', '00 000 000 000', '00 000 000 000', 'admin@geniusocean.com', 'https://geniusocean.com/', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '1568889138banner1.jpg', 'http://google.com', '1565150264banner3.jpg', 'http://google.com', 1, 1, '1568889138banner2.jpg', 'http://google.com', '1565150264banner4.jpg', 'http://google.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `photo`, `link`) VALUES
(1, '1563165366brand-1.png', 'https://www.google.com/'),
(2, '1563165383brand-2.png', 'https://www.google.com/'),
(3, '1563165393brand-3.png', 'https://www.google.com/'),
(4, '1563165400brand-1.png', NULL),
(5, '1563165411brand-2.png', NULL),
(6, '1563165444brand-3.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

DROP TABLE IF EXISTS `payment_gateways`;
CREATE TABLE IF NOT EXISTS `payment_gateways` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `subtitle` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pickups`
--

DROP TABLE IF EXISTS `pickups`;
CREATE TABLE IF NOT EXISTS `pickups` (
  `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickups`
--

INSERT INTO `pickups` (`id`, `location`) VALUES
(2, 'Azampur'),
(3, 'Dhaka'),
(4, 'Kazipara'),
(5, 'Kamarpara'),
(6, 'Uttara');

-- --------------------------------------------------------

--
-- Table structure for table `privacies`
--

DROP TABLE IF EXISTS `privacies`;
CREATE TABLE IF NOT EXISTS `privacies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_heading` varchar(255) DEFAULT NULL,
  `main_para` longtext,
  `heading_1` varchar(255) DEFAULT NULL,
  `para_1` longtext,
  `heading_2` varchar(255) DEFAULT NULL,
  `para_2` longtext,
  `heading_3` varchar(255) DEFAULT NULL,
  `para_3` longtext,
  `heading_4` varchar(255) DEFAULT NULL,
  `para_4` longtext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacies`
--

INSERT INTO `privacies` (`id`, `main_heading`, `main_para`, `heading_1`, `para_1`, `heading_2`, `para_2`, `heading_3`, `para_3`, `heading_4`, `para_4`, `status`) VALUES
(1, 'Security & Privacy Policy', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">LUX respects your privacy and your desire to understand how your information will be handled and used. This Privacy Policy explains how we collect, use and store your information. Using your information in a way that you are comfortable with and keeping your information secure is an integral part of the luxury retail experience we aim to provide. LUX will ensure that all information-protection and customer-legislation standards are met when handling any of your personal information.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(128, 127, 127); font-family: Lato, sans-serif;\">References in this Privacy Policy to ‘we’, ‘us’ and ‘LUX’ are to LUX Limited (company number 30209), and LUX Beauty Limited (company number 00099442) registered offices 87–135 Brompton Road, Knightsbridge, London SW1X 7XL (trading as H Beauty). LUX Limited is a registered information controller with registration number Z603738X. LUX Beauty Limited is a registered information controller with registration number ZA787790.</p>', 'What information do we collect, and how do we use it?', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We collect and use information about you to provide you with a luxury retail experience. You provide us with your information directly when you make a purchase, sign up to become a LUX Rewards member, shop online, use the LUX app, or register for a competition. We also collect elements of your information indirectly such as the route you have taken around our stores.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you instruct Royal Mail to redirect your post, we will receive your updated address information and update our records. Similarly, if we are informed that you are no longer at the address we have on file for you, we will remove it from our records.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We may analyse the information that you supply to us and share your personal data with third parties who help us segment and understand our customers by providing additional information. We use this to provide you with a more personalised experience.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Should you choose to take advantage of one of our Personal Shopping services, including clientelling, you will have an appointed Personal Shopper or sales associate assigned to you. Your shopper will collect information relevant to you in order to curate the most exclusive and luxurious personalised retail experience. Your Personal Shopper will always keep you updated on the information that they hold and how it affects you, and will follow your instructions as to how you would like to be contacted by them.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you receive treatments at LUX sites including H Beauty, we will collect additional information which will be used to assess your suitability for the treatment and to ensure it is safe for you to receive. You should refer to individual data collection forms to understand the relevance of the personal data collected for the treatment.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">In line with UK Government guidance in relation to the Covid-19 pandemic, LUX is required to conduct a screening questionnaire for Covid-19 symptoms, prior to accepting hair, beauty or alteration appointments. The results of this questionnaire will not be retained. LUX is also required to support the NHS Test and Trace service by retaining names, contact numbers, date and time of arrival and departure, for those customers who attend or make reservations with: the Hair and Beauty salon; cafes, restaurants; and alteration service.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">To the extent that LUX does not already hold this information, LUX will ask for, record and store these details in a secure system for 21 days, after which they will be deleted. LUX will use this information solely to assist the NHS Test and Trace service, should they request this data. Customer details which we already hold for other permitted purposes will continue to be used for those purposes and will not be deleted after 21 days but will be shared with the NHS if requested. To learn more about how your information will be used, including how to make changes to it or request its rectification, you can (by law) make a data subject request. To do so, email data.protection@LUX.com</p>', 'Information Collected When You Make a Purchase', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">When you make a purchase at any of our retail outlets or online, we will collect necessary information about you to ensure we can fulfil your order or chosen service.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you choose to pay by card, we will need to collect your payment information in order to process payment. We may ask for additional information from you for a number of reasons: for example, to prevent fraudulent transactions; to comply with anti-money laundering obligations for high-value transactions; or to ensure that we comply with our legal obligations such as age restrictions applicable to the sale of guns, knives or alcohol, or the legal requirement to share your details with TV Licensing when you purchase a television.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">When you place an order with LUX for products or services that will be delivered, collected or provided at a later date, we will need to take additional contact details and delivery information from you so that we may complete your order. We may use third-party couriers to deliver your order and will need to share your contact information with them for this purpose</p>', 'Managing Your Marketing Preferences', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">At any point that you interact with us, you may be given the opportunity to opt in to receive LUX marketing communications. We will only send you marketing communications when you have consented to receive them, and we will do so using your preferred channels of communication. If you have not updated your marketing preferences in two years, we will contact you to remind you that you can opt out of receiving marketing communications at any time:</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- In cases where you have not made a purchase with LUX during this two-year period, we will only continue to send you marketing communications if you expressly tell us to do so. If you do nothing, then we will stop sending you marketing communications.</li><li style=\"list-style: none; padding: 10px 0px;\">- If you have made a purchase with LUX during this period, we will continue to communicate with you in the same way until you tell us otherwise. If you do nothing, we will assume that you are happy to continue receiving marketing materials from LUX in the same way.</li></ul><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you have opted in to receive marketing communications from us, you can manage your preferences online at any time. We have created a Preference Centre where you can manage your marketing communications preferences, and tell us what you want to hear about and how you want LUX to communicate with you. To manage your preferences for the LUX Hair and Beauty Salon, please log in to your Hair and Beauty account.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We will send you marketing material that we believe you will be interested in based on your previous spending behaviour. You may not receive all types of communications that you have chosen to receive. To understand more about the criteria we use to determine which marketing communications you receive, please contact our Customer Service team.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you have given us your email address or mobile phone number and agreed to receive marketing communications from us, we may supply this data to digital advertising or social media companies, including Facebook and Google. These companies work on our behalf to identify individuals with similar characteristics to our customers, and they provide these individuals with advertising content from us. We may also work with these companies to target or retarget you directly with advertisements that we believe you will be interested in.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">The data we provide to these organisations is limited to email address or mobile phone number and is always provided in an encrypted format. We use your personal data in this manner as it is in our legitimate interest to advertise to potential new customers.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">This use of your personal data constitutes profiling. If you would prefer not to see targeted advertising from us on social media, please refer to the instructions provided by Facebook or Google, or contact our Customer Service team for advice on how to opt out.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">lease note: when you visit one of LUX’ brand partners in-store or participate in a branded promotion or event, our brand partners may also ask if you want to sign up to receive marketing communications directly from them about their events, products and services. If you provide your personal details to any of our brand partners, the use of that information will be governed by their privacy policy and cannot be managed through LUX or the Preference Centre. Please ask the relevant sales associate for details about their privacy policy and how you can manage your marketing communications preferences with them.</p>', 'Joining the Rewards Programme', '<h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.1. Delivery Options</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Our delivery options vary by shipping destination, product and service. Please visit our Delivery and Returns page for further details.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.2. Delivery Carriers and Time</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Deliveries will be carried out by third-party couriers. By placing an order on the website, you agree to the conditions of carriage of our delivery partners. Please refer to their website for full details or see our&nbsp;<a href=\"file:///C:/Users/baaz_/Desktop/watch/faq.html\" style=\"color: rgb(104, 104, 104); touch-action: manipulation; transition: all 0.3s ease-out 0s; outline-width: medium;\">FAQs.</a></p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">The estimated delivery date of the products will be stated in your order confirmation email. If you do not receive an estimated delivery date from us, we will deliver the order within 30 days after the date of dispatch. We will try to ensure that your order is delivered by the estimated delivery date; however, there may be circumstances where delivery is delayed because of events beyond our reasonable control. If this happens, we will try and arrange for your products to be delivered as soon as possible; however, we will not be liable for any losses caused as a result of such delay. Please note: there may be occasional delays with delivery due to busier periods such as the LUX Sale or Rewards promotional weekends. In such instances, our&nbsp;<a href=\"file:///C:/Users/baaz_/Desktop/watch/contact-us.html\" style=\"color: rgb(104, 104, 104); touch-action: manipulation; transition: all 0.3s ease-out 0s; outline-width: medium;\">Customer Service&nbsp;</a>team can advise you further on the expected delivery date for your order.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If the product you are ordering contains certain exotic animal skins or fur, and is being delivered outside of Europe, your delivery will take longer (by approximately 20 days on top of the destination’s standard delivery time), as we have to obtain licences for the export and import of the products in accordance with international treaties.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">The most exclusive news about events, products and services is reserved for LUX Rewards members.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Should you become a Rewards member and opt in to hear about Rewards (which includes news and exclusive offers dependent on your LUX Rewards tier), we will contact you about the many benefits reserved for Rewards members. Some of these benefits include a Cash Reward, a birthday treat, complimentary copies of LUX Magazine, complimentary tea and coffee, free delivery or special discount days. Your information will also be used in-store to check your eligibility for each of our benefits, and to track your usage so that we can monitor customer engagement to improve our service.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">From time to time, we may also collect publicly available information about you which is not essential to the Rewards programme, but which enables us to learn more about you. In turn, we can provide a more tailored service. If you would like to stop LUX using this information about you in this way, please contact us at data.protection@LUX.com.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">As part of the LUX Rewards programme, LUX operates the Mini LUX programme, which offers customers additional exclusive benefits, promotions and events tailored towards children up to the age of 10. LUX only collects information about children where the parent or guardian has given their consent. LUX does not knowingly collect information from customers under the age of 13 directly. In the event that we learn that we have collected personal information from anyone under the age of 13 without parental consent, we will delete it immediately.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">From time to time as a Rewards member, you will be emailed by us with a request for anonymous feedback about your experience visiting us. Feedback is used to improve the experience we provide and is anonymous.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you apply for, or already have, a LUX American Express card as part of your LUX Rewards membership benefits, we are required to share certain information collected by LUX in connection with your LUX card with American Express UK Limited. For the American Express Data Protection and Privacy</p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prizes`
--

DROP TABLE IF EXISTS `prizes`;
CREATE TABLE IF NOT EXISTS `prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `price` bigint(191) DEFAULT NULL,
  `price_eng` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prizes`
--

INSERT INTO `prizes` (`id`, `name`, `description`, `price`, `price_eng`, `status`, `created_at`, `updated_at`) VALUES
(5, 'Gold Ear Ring', '14 Carat Gold Ring', 5000, 'Five Thousand', 1, '2020-10-02 01:24:15', '2020-10-02 01:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `prize_galleries`
--

DROP TABLE IF EXISTS `prize_galleries`;
CREATE TABLE IF NOT EXISTS `prize_galleries` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `prize_id` int(191) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prize_galleries`
--

INSERT INTO `prize_galleries` (`id`, `prize_id`, `photo`) VALUES
(1, 4, '160135887945139195-bfe2-4098-94c6-14ee9f7b3c62.jpg'),
(2, 4, '160135887915844526370.png'),
(18, 5, '16044733821601637855pin2.png'),
(17, 5, '16044733821601637855pin2 - Copy.png');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) DEFAULT NULL,
  `product_type` enum('normal','affiliate') NOT NULL DEFAULT 'normal',
  `affiliate_link` text,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `category_id` int(191) UNSIGNED NOT NULL,
  `subcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `childcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `subchildcategory_id` int(191) DEFAULT NULL,
  `brand_id` bigint(191) DEFAULT NULL,
  `attributes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL,
  `previous_price` double DEFAULT NULL,
  `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(191) DEFAULT NULL,
  `policy` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `views` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text,
  `colors` text,
  `product_condition` tinyint(1) NOT NULL DEFAULT '0',
  `ship` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_meta` tinyint(1) NOT NULL DEFAULT '0',
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `youtube` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Physical','Digital','License') NOT NULL,
  `license` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `license_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `platform` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `licence_type` varchar(255) DEFAULT NULL,
  `measure` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `best` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `top` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `hot` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `latest` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `big` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `trending` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_date` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_discount` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_catalog` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_id` int(191) NOT NULL DEFAULT '0',
  `vat` int(100) DEFAULT NULL,
  `vat_amt` int(100) DEFAULT NULL,
  `dsc` int(100) DEFAULT NULL,
  `dsc_amt` int(100) DEFAULT NULL,
  `side_details` varchar(255) DEFAULT NULL,
  `materials` varchar(255) DEFAULT NULL,
  `short_desc` varchar(255) DEFAULT NULL,
  `total_quantity` bigint(191) DEFAULT NULL,
  `current_quantity` bigint(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1255 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `subchildcategory_id`, `brand_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `vat`, `vat_amt`, `dsc`, `dsc_amt`, `side_details`, `materials`, `short_desc`, `total_quantity`, `current_quantity`) VALUES
(1251, '1111111111', 'normal', NULL, 0, 54, 41, NULL, NULL, 1, NULL, 'Blue Watch', 'blue-watch-1111111111', '16048361250YpuCXmd.jpg', '1604836125UiyeJysu.jpg', NULL, NULL, NULL, NULL, NULL, 136.23978201634878, 141.68937329700273, 'Good Watch&nbsp;', NULL, 'Perfect Watch', 1, 36, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, '2020-10-02 00:37:59', '2020-11-19 08:42:54', 0, NULL, NULL, NULL, 0, 0, 5, 7, 0, 0, '<br>', '<br>', 'Small Blue Watch', NULL, NULL),
(1252, '22222222', 'normal', NULL, 0, 55, 34, NULL, NULL, 1, '{\"sizes\":{\"values\":[\"S\"],\"prices\":[\"0\"],\"details_status\":1}}', 'Black Watch', 'black-watch-22222222', '1605002769HcSgokQr.jpg', '16050027697pgZGWsQ.jpg', NULL, NULL, NULL, NULL, NULL, 136.23978201634878, 141.68937329700273, 'Good Watch&nbsp;', NULL, 'Perfect Watch', 1, 57, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, '2020-10-02 00:38:41', '2020-11-19 08:42:53', 0, NULL, NULL, NULL, 0, 0, 5, 7, 0, 0, '<br>', '<br>', 'black watch', NULL, NULL),
(1253, '333333333333', 'normal', NULL, 0, 54, 41, NULL, NULL, 1, NULL, 'Silver Watch', 'silver-watch-333333333333', '1605002736BAkzFUE3.jpg', '1605002737nwVOBm2X.jpg', NULL, NULL, NULL, NULL, NULL, 95.36784741144415, 141.68937329700273, 'Good Watch&nbsp;', NULL, 'Perfect Watch', 1, 136, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, '2020-10-02 00:39:15', '2020-11-19 08:42:55', 0, NULL, NULL, NULL, 0, 0, 5, 5, 0, 0, '<li>Pierced backs </li>\r\n                                 <li>Model numbers: Xb3 </li>\r\n                                 <li>Presented in a Maria Tash box</li>', 'adsaddsa dsadsa', 'White Gold Single Ball Stud 3mm', NULL, NULL),
(1254, 'h2v9340p5i', 'normal', NULL, 13, 55, 35, NULL, NULL, 1, NULL, 'New Watch', 'new-watch-h2v9340p5i', '1604923635DccXXoq4.jpg', '1604923636leYHQn3J.jpg', NULL, NULL, NULL, NULL, NULL, 13.623978201634877, 0, 'Good', NULL, 'Good<br>', 1, 1, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-09 03:10:08', '2020-11-18 07:01:31', 0, NULL, NULL, NULL, 0, 0, 5, 1, 0, 0, 'Good<br>', 'Good<br>', 'Good Product', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_clicks`
--

DROP TABLE IF EXISTS `product_clicks`;
CREATE TABLE IF NOT EXISTS `product_clicks` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `product_id` int(191) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=852 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_clicks`
--

INSERT INTO `product_clicks` (`id`, `product_id`, `date`) VALUES
(1, 171, '2020-01-23'),
(2, 171, '2020-01-23'),
(12, 175, '2020-03-23'),
(14, 101, '2020-03-23'),
(15, 180, '2020-03-23'),
(17, 180, '2020-07-07'),
(18, 97, '2020-07-07'),
(19, 99, '2020-07-07'),
(20, 111, '2020-07-07'),
(21, 179, '2020-07-07'),
(23, 144, '2020-07-07'),
(24, 135, '2020-07-07'),
(25, 185, '2020-07-07'),
(26, 168, '2020-07-09'),
(27, 179, '2020-07-09'),
(28, 168, '2020-07-09'),
(29, 168, '2020-07-09'),
(30, 168, '2020-07-09'),
(31, 168, '2020-07-09'),
(32, 168, '2020-07-09'),
(33, 168, '2020-07-09'),
(34, 168, '2020-07-09'),
(35, 168, '2020-07-09'),
(36, 168, '2020-07-09'),
(37, 168, '2020-07-09'),
(38, 168, '2020-07-09'),
(39, 168, '2020-07-09'),
(40, 103, '2020-07-09'),
(41, 168, '2020-07-09'),
(42, 168, '2020-07-09'),
(43, 168, '2020-07-09'),
(44, 168, '2020-07-09'),
(45, 168, '2020-07-09'),
(46, 128, '2020-07-09'),
(47, 128, '2020-07-09'),
(48, 128, '2020-07-09'),
(49, 128, '2020-07-09'),
(50, 128, '2020-07-09'),
(51, 128, '2020-07-09'),
(52, 128, '2020-07-09'),
(53, 128, '2020-07-09'),
(54, 128, '2020-07-09'),
(55, 128, '2020-07-09'),
(56, 128, '2020-07-09'),
(57, 128, '2020-07-09'),
(58, 128, '2020-07-09'),
(59, 100, '2020-07-09'),
(60, 128, '2020-07-09'),
(61, 128, '2020-07-09'),
(62, 128, '2020-07-09'),
(63, 128, '2020-07-09'),
(64, 100, '2020-07-09'),
(65, 100, '2020-07-09'),
(66, 100, '2020-07-09'),
(67, 99, '2020-07-09'),
(68, 96, '2020-07-09'),
(69, 96, '2020-07-09'),
(70, 96, '2020-07-09'),
(71, 96, '2020-07-09'),
(72, 175, '2020-07-09'),
(73, 171, '2020-07-09'),
(74, 96, '2020-07-09'),
(75, 96, '2020-07-09'),
(76, 96, '2020-07-09'),
(77, 96, '2020-07-09'),
(78, 96, '2020-07-09'),
(79, 96, '2020-07-09'),
(80, 96, '2020-07-09'),
(81, 96, '2020-07-09'),
(82, 96, '2020-07-09'),
(83, 96, '2020-07-09'),
(84, 96, '2020-07-09'),
(85, 96, '2020-07-09'),
(86, 96, '2020-07-09'),
(87, 96, '2020-07-09'),
(88, 101, '2020-07-09'),
(89, 101, '2020-07-09'),
(90, 101, '2020-07-09'),
(91, 101, '2020-07-09'),
(92, 101, '2020-07-09'),
(93, 101, '2020-07-09'),
(94, 101, '2020-07-09'),
(95, 101, '2020-07-09'),
(96, 101, '2020-07-09'),
(97, 101, '2020-07-09'),
(98, 101, '2020-07-09'),
(99, 101, '2020-07-09'),
(100, 101, '2020-07-09'),
(101, 101, '2020-07-09'),
(102, 101, '2020-07-09'),
(103, 101, '2020-07-09'),
(104, 101, '2020-07-09'),
(105, 101, '2020-07-09'),
(106, 101, '2020-07-09'),
(107, 101, '2020-07-09'),
(108, 96, '2020-07-09'),
(109, 101, '2020-07-09'),
(110, 101, '2020-07-09'),
(111, 101, '2020-07-09'),
(112, 99, '2020-07-09'),
(113, 101, '2020-07-09'),
(114, 99, '2020-07-09'),
(115, 101, '2020-07-09'),
(116, 101, '2020-07-09'),
(117, 101, '2020-07-09'),
(118, 101, '2020-07-09'),
(119, 101, '2020-07-09'),
(120, 101, '2020-07-09'),
(121, 101, '2020-07-09'),
(122, 101, '2020-07-09'),
(123, 101, '2020-07-09'),
(124, 101, '2020-07-09'),
(125, 101, '2020-07-09'),
(126, 101, '2020-07-09'),
(127, 101, '2020-07-09'),
(128, 101, '2020-07-09'),
(129, 101, '2020-07-09'),
(130, 101, '2020-07-09'),
(131, 101, '2020-07-09'),
(132, 101, '2020-07-09'),
(133, 101, '2020-07-09'),
(134, 101, '2020-07-09'),
(135, 101, '2020-07-09'),
(136, 101, '2020-07-09'),
(137, 101, '2020-07-09'),
(138, 97, '2020-07-09'),
(139, 101, '2020-07-09'),
(140, 101, '2020-07-09'),
(141, 101, '2020-07-09'),
(142, 99, '2020-07-09'),
(143, 100, '2020-07-09'),
(144, 168, '2020-07-09'),
(145, 168, '2020-07-09'),
(146, 168, '2020-07-09'),
(147, 100, '2020-07-09'),
(148, 168, '2020-07-09'),
(149, 100, '2020-07-09'),
(150, 168, '2020-07-09'),
(151, 168, '2020-07-09'),
(152, 168, '2020-07-09'),
(153, 168, '2020-07-09'),
(154, 168, '2020-07-09'),
(155, 168, '2020-07-09'),
(156, 100, '2020-07-09'),
(157, 100, '2020-07-09'),
(158, 100, '2020-07-09'),
(159, 100, '2020-07-09'),
(160, 100, '2020-07-09'),
(161, 100, '2020-07-09'),
(162, 100, '2020-07-09'),
(163, 100, '2020-07-09'),
(164, 100, '2020-07-09'),
(165, 100, '2020-07-09'),
(166, 100, '2020-07-09'),
(167, 100, '2020-07-09'),
(168, 100, '2020-07-09'),
(169, 100, '2020-07-09'),
(170, 100, '2020-07-09'),
(171, 100, '2020-07-09'),
(172, 100, '2020-07-09'),
(173, 100, '2020-07-09'),
(174, 100, '2020-07-09'),
(175, 100, '2020-07-09'),
(176, 100, '2020-07-09'),
(177, 168, '2020-07-09'),
(178, 168, '2020-07-09'),
(179, 168, '2020-07-09'),
(180, 168, '2020-07-09'),
(181, 168, '2020-07-09'),
(182, 168, '2020-07-09'),
(183, 168, '2020-07-09'),
(184, 168, '2020-07-09'),
(185, 168, '2020-07-09'),
(186, 168, '2020-07-09'),
(187, 168, '2020-07-09'),
(188, 168, '2020-07-09'),
(189, 168, '2020-07-09'),
(190, 168, '2020-07-09'),
(191, 168, '2020-07-09'),
(192, 168, '2020-07-09'),
(193, 168, '2020-07-09'),
(194, 168, '2020-07-09'),
(195, 168, '2020-07-09'),
(196, 168, '2020-07-09'),
(197, 168, '2020-07-09'),
(198, 161, '2020-07-09'),
(199, 160, '2020-07-09'),
(200, 162, '2020-07-09'),
(201, 180, '2020-07-09'),
(202, 179, '2020-07-09'),
(203, 175, '2020-07-09'),
(204, 164, '2020-07-09'),
(205, 179, '2020-07-09'),
(206, 179, '2020-07-09'),
(207, 179, '2020-07-09'),
(208, 179, '2020-07-09'),
(209, 125, '2020-07-09'),
(210, 179, '2020-07-09'),
(211, 179, '2020-07-09'),
(212, 179, '2020-07-09'),
(213, 179, '2020-07-09'),
(214, 179, '2020-07-09'),
(215, 99, '2020-07-09'),
(216, 99, '2020-07-09'),
(217, 99, '2020-07-09'),
(218, 99, '2020-07-09'),
(219, 99, '2020-07-09'),
(220, 99, '2020-07-09'),
(221, 99, '2020-07-09'),
(222, 100, '2020-07-09'),
(223, 100, '2020-07-09'),
(224, 168, '2020-07-09'),
(225, 168, '2020-07-09'),
(226, 100, '2020-07-09'),
(227, 100, '2020-07-09'),
(228, 168, '2020-07-09'),
(229, 180, '2020-07-09'),
(230, 179, '2020-07-09'),
(231, 100, '2020-07-09'),
(232, 100, '2020-07-09'),
(233, 100, '2020-07-09'),
(234, 100, '2020-07-13'),
(235, 185, '2020-07-13'),
(236, 168, '2020-07-13'),
(237, 168, '2020-07-13'),
(238, 179, '2020-07-13'),
(239, 179, '2020-07-13'),
(240, 179, '2020-07-13'),
(241, 179, '2020-07-13'),
(242, 178, '2020-07-13'),
(243, 166, '2020-07-13'),
(244, 166, '2020-07-13'),
(245, 167, '2020-07-13'),
(246, 166, '2020-07-13'),
(247, 168, '2020-07-16'),
(248, 168, '2020-07-16'),
(249, 168, '2020-07-16'),
(250, 168, '2020-07-16'),
(251, 168, '2020-07-16'),
(252, 168, '2020-07-16'),
(253, 168, '2020-07-16'),
(254, 168, '2020-07-16'),
(255, 166, '2020-07-16'),
(256, 166, '2020-07-16'),
(257, 168, '2020-07-16'),
(258, 166, '2020-07-16'),
(259, 180, '2020-07-16'),
(260, 180, '2020-07-16'),
(261, 166, '2020-07-16'),
(262, 166, '2020-07-16'),
(263, 166, '2020-07-16'),
(264, 166, '2020-07-16'),
(265, 166, '2020-07-16'),
(266, 166, '2020-07-16'),
(267, 166, '2020-07-16'),
(268, 166, '2020-07-16'),
(269, 166, '2020-07-16'),
(270, 166, '2020-07-16'),
(271, 178, '2020-07-16'),
(272, 178, '2020-07-16'),
(273, 179, '2020-07-16'),
(274, 180, '2020-07-16'),
(275, 99, '2020-07-16'),
(276, 179, '2020-07-16'),
(277, 179, '2020-07-16'),
(278, 179, '2020-07-16'),
(279, 179, '2020-07-16'),
(280, 179, '2020-07-16'),
(281, 166, '2020-07-16'),
(282, 166, '2020-07-16'),
(283, 166, '2020-07-16'),
(284, 97, '2020-07-16'),
(285, 97, '2020-07-16'),
(286, 188, '2020-07-31'),
(287, 185, '2020-07-31'),
(288, 188, '2020-07-31'),
(289, 188, '2020-07-31'),
(290, 188, '2020-07-31'),
(291, 188, '2020-07-31'),
(292, 188, '2020-07-31'),
(293, 188, '2020-07-31'),
(294, 188, '2020-07-31'),
(295, 188, '2020-07-31'),
(296, 188, '2020-07-31'),
(297, 97, '2020-07-31'),
(298, 97, '2020-07-31'),
(299, 97, '2020-07-31'),
(300, 189, '2020-07-31'),
(301, 189, '2020-07-31'),
(302, 189, '2020-07-31'),
(303, 189, '2020-07-31'),
(304, 189, '2020-07-31'),
(305, 100, '2020-07-31'),
(306, 100, '2020-07-31'),
(307, 189, '2020-07-31'),
(308, 135, '2020-07-31'),
(309, 168, '2020-07-31'),
(310, 180, '2020-07-31'),
(311, 189, '2020-08-11'),
(312, 194, '2020-08-11'),
(313, 194, '2020-08-11'),
(314, 97, '2020-08-11'),
(315, 180, '2020-08-11'),
(316, 194, '2020-08-11'),
(317, 194, '2020-08-11'),
(318, 194, '2020-08-11'),
(319, 194, '2020-08-11'),
(320, 194, '2020-08-11'),
(321, 194, '2020-08-11'),
(322, 194, '2020-08-11'),
(323, 194, '2020-08-11'),
(324, 194, '2020-08-11'),
(325, 194, '2020-08-11'),
(326, 194, '2020-08-11'),
(327, 194, '2020-08-11'),
(328, 194, '2020-08-11'),
(329, 189, '2020-08-11'),
(330, 168, '2020-08-11'),
(331, 194, '2020-08-11'),
(332, 189, '2020-08-11'),
(333, 168, '2020-08-11'),
(334, 167, '2020-08-11'),
(335, 194, '2020-08-11'),
(336, 194, '2020-08-11'),
(337, 194, '2020-08-11'),
(338, 194, '2020-08-12'),
(339, 194, '2020-08-12'),
(340, 168, '2020-08-12'),
(341, 167, '2020-08-12'),
(342, 167, '2020-08-12'),
(343, 194, '2020-08-12'),
(344, 194, '2020-08-12'),
(345, 194, '2020-08-12'),
(346, 194, '2020-08-12'),
(347, 194, '2020-08-12'),
(348, 194, '2020-08-12'),
(349, 194, '2020-08-12'),
(350, 194, '2020-08-12'),
(351, 194, '2020-08-12'),
(352, 167, '2020-08-12'),
(353, 135, '2020-08-12'),
(354, 194, '2020-08-12'),
(355, 168, '2020-08-12'),
(356, 194, '2020-08-12'),
(357, 194, '2020-08-12'),
(358, 194, '2020-08-12'),
(359, 194, '2020-08-12'),
(360, 194, '2020-08-12'),
(361, 194, '2020-08-12'),
(362, 194, '2020-08-12'),
(363, 194, '2020-08-12'),
(364, 194, '2020-08-12'),
(365, 194, '2020-08-12'),
(366, 194, '2020-08-12'),
(367, 194, '2020-08-12'),
(368, 194, '2020-08-12'),
(369, 194, '2020-08-12'),
(370, 194, '2020-08-12'),
(371, 194, '2020-08-12'),
(372, 194, '2020-08-12'),
(373, 194, '2020-08-12'),
(374, 194, '2020-08-12'),
(375, 194, '2020-08-12'),
(376, 194, '2020-08-12'),
(377, 159, '2020-08-12'),
(378, 161, '2020-08-12'),
(379, 180, '2020-08-12'),
(380, 179, '2020-08-12'),
(381, 175, '2020-08-12'),
(382, 175, '2020-08-12'),
(383, 202, '2020-08-12'),
(384, 159, '2020-08-12'),
(385, 144, '2020-08-12'),
(386, 135, '2020-08-12'),
(387, 134, '2020-08-12'),
(390, 207, '2020-08-17'),
(391, 166, '2020-08-17'),
(392, 207, '2020-08-17'),
(393, 144, '2020-08-17'),
(394, 213, '2020-08-19'),
(395, 213, '2020-08-19'),
(396, 213, '2020-08-19'),
(397, 213, '2020-08-19'),
(398, 213, '2020-08-19'),
(399, 213, '2020-08-19'),
(400, 214, '2020-08-20'),
(401, 214, '2020-08-20'),
(402, 214, '2020-08-20'),
(403, 214, '2020-08-20'),
(404, 217, '2020-08-24'),
(405, 217, '2020-08-24'),
(406, 217, '2020-08-24'),
(407, 217, '2020-08-24'),
(408, 218, '2020-08-28'),
(409, 218, '2020-09-05'),
(410, 214, '2020-09-05'),
(411, 214, '2020-09-05'),
(412, 212, '2020-09-05'),
(413, 213, '2020-09-05'),
(415, 212, '2020-09-05'),
(416, 218, '2020-09-05'),
(417, 218, '2020-09-05'),
(418, 218, '2020-09-05'),
(419, 218, '2020-09-05'),
(420, 218, '2020-09-05'),
(421, 218, '2020-09-05'),
(422, 218, '2020-09-05'),
(423, 218, '2020-09-05'),
(424, 218, '2020-09-05'),
(425, 213, '2020-09-05'),
(426, 213, '2020-09-05'),
(427, 212, '2020-09-05'),
(428, 211, '2020-09-05'),
(429, 210, '2020-09-05'),
(430, 217, '2020-09-05'),
(431, 217, '2020-09-05'),
(432, 218, '2020-09-05'),
(433, 217, '2020-09-05'),
(434, 217, '2020-09-05'),
(435, 217, '2020-09-05'),
(436, 217, '2020-09-05'),
(437, 217, '2020-09-05'),
(438, 217, '2020-09-05'),
(439, 217, '2020-09-05'),
(440, 217, '2020-09-05'),
(441, 217, '2020-09-05'),
(442, 217, '2020-09-05'),
(443, 217, '2020-09-05'),
(444, 217, '2020-09-05'),
(445, 217, '2020-09-05'),
(446, 217, '2020-09-05'),
(447, 217, '2020-09-05'),
(448, 217, '2020-09-05'),
(449, 217, '2020-09-05'),
(450, 217, '2020-09-05'),
(451, 217, '2020-09-05'),
(452, 217, '2020-09-05'),
(453, 217, '2020-09-05'),
(454, 217, '2020-09-05'),
(455, 217, '2020-09-05'),
(456, 217, '2020-09-05'),
(457, 217, '2020-09-06'),
(458, 217, '2020-09-06'),
(459, 217, '2020-09-06'),
(460, 217, '2020-09-06'),
(461, 217, '2020-09-06'),
(462, 217, '2020-09-06'),
(463, 217, '2020-09-06'),
(464, 217, '2020-09-06'),
(465, 217, '2020-09-06'),
(466, 217, '2020-09-06'),
(467, 217, '2020-09-06'),
(468, 217, '2020-09-06'),
(469, 217, '2020-09-06'),
(470, 217, '2020-09-06'),
(471, 217, '2020-09-06'),
(472, 217, '2020-09-06'),
(473, 217, '2020-09-06'),
(474, 217, '2020-09-06'),
(475, 217, '2020-09-06'),
(476, 217, '2020-09-06'),
(477, 217, '2020-09-06'),
(478, 217, '2020-09-06'),
(479, 217, '2020-09-06'),
(480, 217, '2020-09-06'),
(481, 217, '2020-09-06'),
(482, 217, '2020-09-06'),
(483, 217, '2020-09-06'),
(484, 217, '2020-09-06'),
(485, 217, '2020-09-06'),
(486, 217, '2020-09-06'),
(487, 217, '2020-09-06'),
(488, 217, '2020-09-06'),
(489, 217, '2020-09-06'),
(490, 217, '2020-09-06'),
(491, 212, '2020-09-08'),
(492, 224, '2020-09-16'),
(493, 224, '2020-09-16'),
(494, 224, '2020-09-16'),
(495, 212, '2020-09-16'),
(496, 224, '2020-09-17'),
(497, 215, '2020-09-17'),
(498, 217, '2020-09-17'),
(499, 218, '2020-09-17'),
(501, 217, '2020-09-17'),
(502, 217, '2020-09-17'),
(503, 217, '2020-09-17'),
(504, 217, '2020-09-17'),
(505, 217, '2020-09-17'),
(506, 217, '2020-09-17'),
(507, 217, '2020-09-17'),
(508, 217, '2020-09-17'),
(509, 217, '2020-09-17'),
(510, 217, '2020-09-17'),
(511, 217, '2020-09-17'),
(512, 217, '2020-09-17'),
(513, 217, '2020-09-17'),
(514, 217, '2020-09-17'),
(515, 217, '2020-09-17'),
(516, 217, '2020-09-17'),
(517, 217, '2020-09-17'),
(518, 217, '2020-09-17'),
(519, 217, '2020-09-17'),
(520, 217, '2020-09-17'),
(521, 217, '2020-09-17'),
(522, 217, '2020-09-17'),
(523, 217, '2020-09-17'),
(524, 217, '2020-09-17'),
(525, 217, '2020-09-17'),
(526, 217, '2020-09-17'),
(527, 217, '2020-09-17'),
(528, 217, '2020-09-17'),
(529, 217, '2020-09-17'),
(530, 217, '2020-09-17'),
(531, 217, '2020-09-17'),
(532, 217, '2020-09-17'),
(533, 217, '2020-09-17'),
(534, 217, '2020-09-17'),
(535, 217, '2020-09-17'),
(536, 217, '2020-09-17'),
(537, 217, '2020-09-17'),
(538, 217, '2020-09-17'),
(539, 217, '2020-09-17'),
(540, 217, '2020-09-17'),
(541, 217, '2020-09-17'),
(542, 217, '2020-09-17'),
(543, 217, '2020-09-17'),
(544, 217, '2020-09-17'),
(545, 217, '2020-09-17'),
(546, 217, '2020-09-17'),
(547, 215, '2020-09-17'),
(548, 217, '2020-09-17'),
(549, 224, '2020-09-17'),
(550, 224, '2020-09-17'),
(551, 217, '2020-09-17'),
(552, 217, '2020-09-17'),
(553, 217, '2020-09-17'),
(554, 217, '2020-09-17'),
(555, 217, '2020-09-17'),
(556, 224, '2020-09-17'),
(557, 226, '2020-09-17'),
(558, 224, '2020-09-19'),
(559, 224, '2020-09-19'),
(560, 211, '2020-09-19'),
(561, 211, '2020-09-19'),
(562, 211, '2020-09-19'),
(563, 211, '2020-09-19'),
(564, 211, '2020-09-19'),
(565, 211, '2020-09-19'),
(566, 211, '2020-09-19'),
(567, 211, '2020-09-19'),
(568, 211, '2020-09-19'),
(569, 211, '2020-09-19'),
(570, 224, '2020-09-20'),
(571, 224, '2020-09-20'),
(572, 224, '2020-09-20'),
(573, 224, '2020-09-20'),
(574, 224, '2020-09-20'),
(575, 224, '2020-09-23'),
(576, 224, '2020-09-23'),
(577, 224, '2020-09-23'),
(578, 224, '2020-09-23'),
(579, 224, '2020-09-23'),
(580, 224, '2020-09-23'),
(581, 224, '2020-09-23'),
(582, 224, '2020-09-23'),
(583, 224, '2020-09-23'),
(584, 224, '2020-09-23'),
(585, 224, '2020-09-23'),
(586, 224, '2020-09-23'),
(587, 224, '2020-09-23'),
(588, 224, '2020-09-23'),
(589, 224, '2020-09-23'),
(590, 224, '2020-09-23'),
(591, 232, '2020-09-23'),
(592, 224, '2020-09-23'),
(593, 224, '2020-09-23'),
(594, 224, '2020-09-23'),
(595, 224, '2020-09-23'),
(596, 224, '2020-09-23'),
(597, 224, '2020-09-23'),
(598, 224, '2020-09-23'),
(599, 224, '2020-09-23'),
(600, 224, '2020-09-23'),
(601, 224, '2020-09-23'),
(602, 224, '2020-09-23'),
(603, 224, '2020-09-23'),
(604, 224, '2020-09-23'),
(605, 224, '2020-09-23'),
(606, 224, '2020-09-23'),
(607, 224, '2020-09-23'),
(608, 224, '2020-09-23'),
(609, 224, '2020-09-23'),
(610, 224, '2020-09-23'),
(611, 325, '2020-09-23'),
(612, 325, '2020-09-23'),
(613, 325, '2020-09-23'),
(614, 325, '2020-09-23'),
(615, 325, '2020-09-23'),
(616, 325, '2020-09-23'),
(617, 325, '2020-09-23'),
(618, 325, '2020-09-23'),
(619, 1253, '2020-10-05'),
(620, 1253, '2020-10-05'),
(621, 1252, '2020-10-05'),
(622, 1253, '2020-10-05'),
(623, 1253, '2020-10-05'),
(624, 1252, '2020-10-05'),
(625, 1253, '2020-10-05'),
(626, 1253, '2020-10-05'),
(627, 1253, '2020-10-05'),
(628, 1253, '2020-10-05'),
(629, 1253, '2020-10-05'),
(630, 1253, '2020-10-05'),
(631, 1253, '2020-10-05'),
(632, 1253, '2020-10-05'),
(633, 1253, '2020-10-05'),
(634, 1253, '2020-10-05'),
(635, 1253, '2020-10-05'),
(636, 1253, '2020-10-05'),
(637, 1253, '2020-10-05'),
(638, 1253, '2020-10-05'),
(639, 1253, '2020-10-05'),
(640, 1253, '2020-10-05'),
(641, 1253, '2020-10-05'),
(642, 1253, '2020-10-05'),
(643, 1253, '2020-10-05'),
(644, 1253, '2020-10-05'),
(645, 1253, '2020-10-05'),
(646, 1253, '2020-10-05'),
(647, 1253, '2020-10-05'),
(648, 1253, '2020-10-05'),
(649, 1253, '2020-10-05'),
(650, 1253, '2020-10-05'),
(651, 1253, '2020-10-05'),
(652, 1253, '2020-10-05'),
(653, 1253, '2020-10-05'),
(654, 1253, '2020-10-05'),
(655, 1253, '2020-10-05'),
(656, 1253, '2020-10-05'),
(657, 1253, '2020-10-05'),
(658, 1253, '2020-10-05'),
(659, 1253, '2020-10-05'),
(660, 1253, '2020-10-05'),
(661, 1253, '2020-10-05'),
(662, 1253, '2020-10-05'),
(663, 1253, '2020-10-05'),
(664, 1253, '2020-10-05'),
(665, 1253, '2020-10-05'),
(666, 1253, '2020-10-05'),
(667, 1253, '2020-10-05'),
(668, 1253, '2020-10-05'),
(669, 1253, '2020-10-05'),
(670, 1252, '2020-10-05'),
(671, 1253, '2020-10-05'),
(672, 1253, '2020-10-05'),
(673, 1253, '2020-10-05'),
(674, 1251, '2020-10-05'),
(675, 1253, '2020-10-05'),
(676, 1253, '2020-10-05'),
(677, 1253, '2020-10-05'),
(678, 1253, '2020-10-06'),
(679, 1252, '2020-10-06'),
(680, 1253, '2020-10-06'),
(681, 1252, '2020-10-06'),
(682, 1253, '2020-10-06'),
(683, 1253, '2020-10-06'),
(684, 1253, '2020-10-06'),
(685, 1253, '2020-10-06'),
(686, 1253, '2020-10-06'),
(687, 1253, '2020-10-06'),
(688, 1253, '2020-10-06'),
(689, 1251, '2020-10-06'),
(690, 1252, '2020-10-06'),
(691, 1252, '2020-10-06'),
(692, 1252, '2020-10-06'),
(693, 1252, '2020-10-06'),
(694, 1252, '2020-10-06'),
(695, 1252, '2020-10-06'),
(696, 1252, '2020-10-06'),
(697, 1253, '2020-10-07'),
(698, 1251, '2020-10-07'),
(699, 1253, '2020-10-07'),
(700, 1252, '2020-10-07'),
(701, 1252, '2020-10-07'),
(702, 1252, '2020-10-07'),
(703, 1252, '2020-10-07'),
(704, 1253, '2020-10-07'),
(705, 1251, '2020-10-07'),
(706, 1253, '2020-10-07'),
(707, 1252, '2020-10-07'),
(708, 1251, '2020-10-11'),
(709, 1252, '2020-10-11'),
(710, 1253, '2020-10-11'),
(711, 1253, '2020-10-11'),
(712, 1253, '2020-10-11'),
(713, 1253, '2020-10-11'),
(714, 1253, '2020-10-11'),
(715, 1253, '2020-10-11'),
(716, 1253, '2020-10-11'),
(717, 1253, '2020-10-11'),
(718, 1253, '2020-10-11'),
(719, 1253, '2020-10-18'),
(720, 1253, '2020-10-18'),
(721, 1253, '2020-10-18'),
(722, 1253, '2020-10-18'),
(723, 1253, '2020-10-18'),
(724, 1253, '2020-10-18'),
(725, 1253, '2020-10-18'),
(726, 1253, '2020-10-18'),
(727, 1253, '2020-10-18'),
(728, 1253, '2020-10-18'),
(729, 1253, '2020-10-18'),
(730, 1253, '2020-10-18'),
(731, 1253, '2020-10-18'),
(732, 1253, '2020-10-18'),
(733, 1253, '2020-10-18'),
(734, 1253, '2020-10-18'),
(735, 1253, '2020-10-18'),
(736, 1253, '2020-10-18'),
(737, 1253, '2020-10-18'),
(738, 1253, '2020-10-18'),
(739, 1253, '2020-10-18'),
(740, 1253, '2020-10-18'),
(741, 1253, '2020-10-18'),
(742, 1253, '2020-10-18'),
(743, 1253, '2020-10-22'),
(744, 1252, '2020-10-22'),
(745, 1253, '2020-10-22'),
(746, 1253, '2020-10-23'),
(747, 1253, '2020-10-31'),
(748, 1253, '2020-10-31'),
(749, 1253, '2020-10-31'),
(750, 1252, '2020-10-31'),
(751, 1252, '2020-10-31'),
(752, 1253, '2020-10-31'),
(753, 1253, '2020-10-31'),
(754, 1252, '2020-10-31'),
(755, 1252, '2020-10-31'),
(756, 1253, '2020-10-31'),
(757, 1253, '2020-11-04'),
(758, 1253, '2020-11-04'),
(759, 1251, '2020-11-04'),
(760, 1253, '2020-11-04'),
(761, 1251, '2020-11-05'),
(762, 1251, '2020-11-05'),
(763, 1251, '2020-11-05'),
(764, 1251, '2020-11-05'),
(765, 1251, '2020-11-05'),
(766, 1251, '2020-11-05'),
(767, 1253, '2020-11-05'),
(768, 1253, '2020-11-05'),
(769, 1253, '2020-11-05'),
(770, 1251, '2020-11-05'),
(771, 1251, '2020-11-05'),
(772, 1251, '2020-11-05'),
(773, 1259, '2020-11-07'),
(774, 1259, '2020-11-07'),
(775, 1259, '2020-11-07'),
(776, 1251, '2020-11-08'),
(777, 1251, '2020-11-08'),
(778, 1251, '2020-11-08'),
(779, 1251, '2020-11-08'),
(780, 1251, '2020-11-08'),
(781, 1251, '2020-11-08'),
(782, 1251, '2020-11-08'),
(783, 1251, '2020-11-08'),
(784, 1253, '2020-11-08'),
(785, 1253, '2020-11-09'),
(786, 1253, '2020-11-09'),
(787, 1252, '2020-11-09'),
(788, 1252, '2020-11-09'),
(789, 1252, '2020-11-09'),
(790, 1251, '2020-11-09'),
(791, 1252, '2020-11-09'),
(792, 1253, '2020-11-09'),
(793, 1253, '2020-11-09'),
(794, 1253, '2020-11-09'),
(795, 1253, '2020-11-09'),
(796, 1253, '2020-11-09'),
(797, 1253, '2020-11-10'),
(798, 1252, '2020-11-10'),
(799, 1253, '2020-11-18'),
(800, 1253, '2020-11-18'),
(801, 1254, '2020-11-18'),
(802, 1252, '2020-11-18'),
(803, 1252, '2020-11-18'),
(804, 1252, '2020-11-19'),
(805, 1252, '2020-11-19'),
(806, 1252, '2020-11-19'),
(807, 1252, '2020-11-19'),
(808, 1252, '2020-11-19'),
(809, 1252, '2020-11-19'),
(810, 1252, '2020-11-19'),
(811, 1252, '2020-11-19'),
(812, 1251, '2020-11-19'),
(813, 1251, '2020-11-19'),
(814, 1251, '2020-11-19'),
(815, 1251, '2020-11-19'),
(816, 1251, '2020-11-19'),
(817, 1251, '2020-11-19'),
(818, 1251, '2020-11-19'),
(819, 1251, '2020-11-19'),
(820, 1252, '2020-11-19'),
(821, 1253, '2020-11-19'),
(822, 1252, '2020-11-19'),
(823, 1252, '2020-11-19'),
(824, 1252, '2020-11-19'),
(825, 1252, '2020-11-19'),
(826, 1252, '2020-11-19'),
(827, 1252, '2020-11-19'),
(828, 1252, '2020-11-19'),
(829, 1252, '2020-11-19'),
(830, 1252, '2020-11-19'),
(831, 1252, '2020-11-19'),
(832, 1252, '2020-11-19'),
(833, 1252, '2020-11-19'),
(834, 1252, '2020-11-19'),
(835, 1252, '2020-11-19'),
(836, 1251, '2020-11-19'),
(837, 1253, '2020-11-19'),
(838, 1252, '2020-11-19'),
(839, 1252, '2020-11-19'),
(840, 1251, '2020-11-19'),
(841, 1253, '2020-11-19'),
(842, 1253, '2020-11-19'),
(843, 1253, '2020-11-19'),
(844, 1253, '2020-11-19'),
(845, 1253, '2020-11-19'),
(846, 1252, '2020-11-19'),
(847, 1251, '2020-11-19'),
(848, 1253, '2020-11-19'),
(849, 1252, '2020-11-19'),
(850, 1251, '2020-11-19'),
(851, 1253, '2020-11-19');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `review` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rating` tinyint(2) NOT NULL,
  `review_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `user_id`, `product_id`, `review`, `rating`, `review_date`) VALUES
(1, 22, 224, 'Good Very Good', 3, '2020-09-23 09:32:47');

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

DROP TABLE IF EXISTS `replies`;
CREATE TABLE IF NOT EXISTS `replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) UNSIGNED NOT NULL,
  `comment_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `product_id` int(192) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `photo`, `title`, `subtitle`, `details`) VALUES
(4, '1557343012img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.'),
(5, '1557343018img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.'),
(6, '1557343024img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `section`) VALUES
(16, 'Manager', 'orders , products , customers , vendors , categories , product_discussion , set_coupons , emails_settings , payment_settings , social_settings , seo_tools'),
(17, 'Moderator', 'orders , products , categories'),
(18, 'Staff', 'orders , products , vendors , vendor_subscription_plans , categories , blog , home_page_settings , menu_page_settings , language_settings , seo_tools , subscribers');

-- --------------------------------------------------------

--
-- Table structure for table `seotools`
--

DROP TABLE IF EXISTS `seotools`;
CREATE TABLE IF NOT EXISTS `seotools` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `google_analytics` text COLLATE utf8mb4_unicode_ci,
  `meta_keys` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seotools`
--

INSERT INTO `seotools` (`id`, `google_analytics`, `meta_keys`) VALUES
(1, '<script>//Google Analytics Scriptfffffffffffffffffffffffssssfffffs</script>', 'Manara');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `title`, `details`, `photo`) VALUES
(2, 0, 'FREE SHIPPING', 'Free Shipping All Order', '1561348133service1.png'),
(3, 0, 'PAYMENT METHOD', 'Secure Payment', '1561348138service2.png'),
(4, 0, '30 DAY RETURNS', '30-Day Return Policy', '1561348143service3.png'),
(5, 0, 'HELP CENTER', '24/7 Support System', '1561348147service4.png'),
(6, 13, 'FREE SHIPPING', 'Free Shipping All Order', '1563247602brand1.png'),
(7, 13, 'PAYMENT METHOD', 'Secure Payment', '1563247614brand2.png'),
(8, 13, '30 DAY RETURNS', '30-Day Return Policy', '1563247620brand3.png'),
(9, 13, 'HELP CENTER', '24/7 Support System', '1563247670brand4.png');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

DROP TABLE IF EXISTS `shippings`;
CREATE TABLE IF NOT EXISTS `shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text,
  `subtitle` text,
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'Free Shipping', '(10 - 12 days)', 0),
(2, 0, 'Express Shipping', '(5 - 6 days)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subtitle_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle_size` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_color` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_anime` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `title_size` varchar(50) DEFAULT NULL,
  `title_color` varchar(50) DEFAULT NULL,
  `title_anime` varchar(50) DEFAULT NULL,
  `details_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details_size` varchar(50) DEFAULT NULL,
  `details_color` varchar(50) DEFAULT NULL,
  `details_anime` varchar(50) DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `subtitle_text`, `subtitle_size`, `subtitle_color`, `subtitle_anime`, `title_text`, `title_size`, `title_color`, `title_anime`, `details_text`, `details_size`, `details_color`, `details_anime`, `photo`, `position`, `link`) VALUES
(9, 'SPRING / SUMMER COLLECTION 2017', '16', '#111111', 'slideInUp', 'Get Up to 40% Off New Arrivals', '72', '#252525', 'slideInDown', 'Highlight your personality  and look with these fabulous and exquisite fashion.', '30', '#111111', 'slideInDown', '1598268858hero-2.jpg', 'slide-one', 'https://www.google.com/'),
(10, 'SPRING/SUMMER COLLECTION 2020', '16', '#111111', 'slideInUp', 'Get Up to 40% Off New Arrivals', '72', '#252525', 'slideInDown', 'Highlight your personality  and look with these fabulous and exquisite fashion.', '30', '#252525', 'slideInLeft', '1598268795hero-1.jpg', 'slide-one', 'https://www.google.com/');

-- --------------------------------------------------------

--
-- Table structure for table `socialsettings`
--

DROP TABLE IF EXISTS `socialsettings`;
CREATE TABLE IF NOT EXISTS `socialsettings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gplus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dribble` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_status` tinyint(4) NOT NULL DEFAULT '1',
  `g_status` tinyint(4) NOT NULL DEFAULT '1',
  `t_status` tinyint(4) NOT NULL DEFAULT '1',
  `l_status` tinyint(4) NOT NULL DEFAULT '1',
  `d_status` tinyint(4) NOT NULL DEFAULT '1',
  `f_check` tinyint(10) DEFAULT NULL,
  `g_check` tinyint(10) DEFAULT NULL,
  `insta_status` int(191) DEFAULT NULL,
  `tiktok_status` int(191) DEFAULT NULL,
  `youtube_status` int(191) DEFAULT NULL,
  `instagram` text COLLATE utf8mb4_unicode_ci,
  `tiktok` text COLLATE utf8mb4_unicode_ci,
  `youtube` text COLLATE utf8mb4_unicode_ci,
  `fclient_id` text COLLATE utf8mb4_unicode_ci,
  `fclient_secret` text COLLATE utf8mb4_unicode_ci,
  `fredirect` text COLLATE utf8mb4_unicode_ci,
  `gclient_id` text COLLATE utf8mb4_unicode_ci,
  `gclient_secret` text COLLATE utf8mb4_unicode_ci,
  `gredirect` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socialsettings`
--

INSERT INTO `socialsettings` (`id`, `facebook`, `gplus`, `twitter`, `linkedin`, `dribble`, `f_status`, `g_status`, `t_status`, `l_status`, `d_status`, `f_check`, `g_check`, `insta_status`, `tiktok_status`, `youtube_status`, `instagram`, `tiktok`, `youtube`, `fclient_id`, `fclient_secret`, `fredirect`, `gclient_id`, `gclient_secret`, `gredirect`) VALUES
(1, 'https://www.facebook.com/', 'https://plus.google.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'https://dribbble.com/', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'https://dribbble.com/', 'https://dribbble.com/', 'https://dribbble.com/', '503140663460329', 'f66cd670ec43d14209a2728af26dcc43', 'https://localhost/geniuscart1.7.4/auth/facebook/callback', '904681031719-sh1aolu42k7l93ik0bkiddcboghbpcfi.apps.googleusercontent.com', 'yGBWmUpPtn5yWhDAsXnswEX3', 'http://localhost/geniuscart1.7.4/auth/google/callback');

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

DROP TABLE IF EXISTS `social_providers`;
CREATE TABLE IF NOT EXISTS `social_providers` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `provider_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(255) NOT NULL,
  `state_code` varchar(255) NOT NULL,
  `country_id` bigint(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `state_code`, `country_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dubai', 'DB', 230, 1, '2020-08-15 15:53:29', '2020-08-15 15:53:41'),
(2, 'Ajman', 'AJ', 230, 1, '2020-08-15 15:53:54', '2020-08-15 16:04:39'),
(3, 'Sharjah', 'SJ', 230, 1, '2020-08-15 18:47:50', '2020-08-15 18:48:17'),
(4, 'Al-Ain', 'AA', 230, 1, '2020-08-15 18:48:14', '2020-08-22 16:36:00'),
(5, 'Rias', 'RS', 230, 1, '2020-08-15 18:48:49', '2020-08-15 18:49:05'),
(6, 'Um-Quin', 'UU', 230, 1, '2020-08-15 18:49:37', '2020-08-15 18:50:17'),
(7, 'Ajmaaaan', 'aj', 230, 1, '2020-08-22 16:33:54', '2020-08-22 16:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `category_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `sequence` int(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `slug`, `status`, `sequence`) VALUES
(32, 53, 'Rolex', 'rolex', 1, 1),
(33, 55, 'Watch Stands', 'watch-stands', 1, 2),
(34, 55, 'Watch Boxes & Cases', 'watch-boxes-and-cases', 1, 3),
(35, 55, 'Watch Pouches', 'watch-pouches', 1, 4),
(36, 55, 'Watch Straps', 'watch-straps', 1, 5),
(37, 55, 'Cleaning Tools', 'cleaning-tools', 1, 6),
(38, 55, 'Watch Winders', 'watch-winders', 1, 7),
(39, 55, 'Watch Safes', 'watch-safes', 1, 8),
(40, 54, 'Jewellery Boxes', 'jewellery-boxes', 1, 9),
(41, 54, 'Jewellwey Cases', 'jewellery-cases', 1, 11),
(42, 54, 'Jewellery Organizers', 'jewellery-organizers', 1, 12),
(43, 54, 'Jewellery Stands', 'jewellery-stands', 1, 13),
(44, 54, 'Jewellery Safes', 'jewellery-safes', 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `subchildcategories`
--

DROP TABLE IF EXISTS `subchildcategories`;
CREATE TABLE IF NOT EXISTS `subchildcategories` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `childcategory_id` int(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sequence` int(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subchildcategories`
--

INSERT INTO `subchildcategories` (`id`, `childcategory_id`, `name`, `slug`, `status`, `sequence`) VALUES
(1, 21, 'kuku', 'kuku', 1, NULL),
(2, 25, 'asdddd', 'assddd', 1, NULL),
(3, 29, 'Zafar', 'aaaaaaaa', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `first_name`, `last_name`) VALUES
(4, 'zafarkhan1183@gmail.com', 'Zafar', 'Khan');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`) VALUES
(5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>'),
(6, 'Premium', '$', 'USD', 120, 90, 90, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(7, 'Unlimited', '$', 'USD', 250, 365, 0, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

DROP TABLE IF EXISTS `terms`;
CREATE TABLE IF NOT EXISTS `terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_1` varchar(255) DEFAULT NULL,
  `details_1` longtext,
  `title_2` varchar(255) DEFAULT NULL,
  `details_2` longtext,
  `title_3` varchar(255) DEFAULT NULL,
  `details_3` longtext,
  `title_4` varchar(255) DEFAULT NULL,
  `details_4` longtext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title_1`, `details_1`, `title_2`, `details_2`, `title_3`, `details_3`, `title_4`, `details_4`, `status`) VALUES
(1, 'Introduction', '<p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">LUX.com (the ‘website’) is owned and operated by LUX Limited, a British retailer registered in the United Kingdom with the company number 00030209 and VAT number 629 273 423. Our registered office and flagship store is located at 87–135 Brompton Road, London SW1X 7XL. More information on other LUX outlets across airports in the UK and international locations can be found in our FAQs.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">References to ‘LUX’, ‘we’, ‘us’ or ‘our’ are references to LUX Limited, another LUX company within our Group (where applicable). The LUX Group includes LUX Limited, LUX International Limited, LUX Estates Limited, LUX Aviation Limited and Air LUX Limited.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">LUX has subcontracted the operation of certain aspects of the website to Black &amp; White, a trading division of Farfetch. This includes payment processing, hosting and delivery logistics (please see clause 3.3 for more information on payment).</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Please note that the delivery logistics service is being provided by Farfetch to you, the customer, and as such you are entering into a contract for delivery services provided by Farfetch. Farfetch may make a charge for these services which will be shown in at prior to checkout and your purchase of the products.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">By using the website or placing an order, you agree to be bound by these Terms and Conditions (the ‘terms’). We reserve the right to make changes to the terms at any time. Our new terms will be displayed on the website, and by continuing to use and access the website, you agree to be bound by any variation.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">These terms are available in the English language only. Nothing in these terms will affect your statutory rights.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">For any further information regarding these terms, please contact our&nbsp;<a href=\"file:///C:/Users/baaz_/Desktop/watch/contact-us.html\" style=\"color: rgb(104, 104, 104); touch-action: manipulation; transition: all 0.3s ease-out 0s; outline-width: medium;\">Customer Service team.</a></p>', 'Use Of Website', '<h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">2.1 Access</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">You are provided access to the website for your personal and non-commercial use only. Any access to, or use of, this website will be in accordance with these terms. Please note that we may suspend your access to the website if you do not comply (or we reasonably believe that you are not complying) with these terms, any other terms or policies to which they refer, or any applicable laws.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">You may not use the website or its contents to:</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- download or modify the website under any circumstances;</li><li style=\"list-style: none; padding: 10px 0px;\">- interfere with or disrupt any network connected to the website;</li><li style=\"list-style: none; padding: 10px 0px;\">- gain unauthorised access to other computer systems owned by LUX Group;</li><li style=\"list-style: none; padding: 10px 0px;\">- for any purpose which is unlawful.</li></ul><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">In addition, you may not, in relation to the website, use any:</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- data mining, robots, or similar data-gathering and extraction tools;</li><li style=\"list-style: none; padding: 10px 0px;\">- framing techniques to enclose the trademarks, logos and other proprietary images, text layouts and formats that we use on the website;</li><li style=\"list-style: none; padding: 10px 0px;\">- meta tags or any other ‘hidden text’ that uses our name or trademarks</li></ul><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Using the website does not give you permission to link to it or to use any of the trademarks, designs, get-up and/or logos contained within it.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We do not represent or warrant that the material contained in the website, or any of the functions of the website and the server, will operate without interruption or delay or will be error-free, free from defects, viruses or bugs, or compatible with any other software or material. The website is provided ‘as is’. Accessing the website is entirely at your own risk.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Please note: if you choose to access the website from locations outside the UK, you are responsible for compliance with local laws where applicable.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If you have any difficulties using the website, please contact our Customer Service team or refer to our Accessibility Statement.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">2.2. Ownership, Use and Intellectual Property Rights</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">This website and all intellectual property rights in it, including but not limited to any content, are owned by LUX Limited. Intellectual property rights means rights such as copyright, trademarks, domain names, design rights, database rights, patents and all other intellectual property rights of any kind, whether or not they are registered or unregistered (anywhere in the world). We reserve all of our rights in any intellectual property in connection with these terms and conditions.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Nothing in these terms and conditions grants you any legal rights in the website other than as necessary to enable you to access the website. You agree not to adjust, circumvent or delete any notices contained on the website (including any intellectual property notices), or any digital rights or other security technology embedded or contained in the website.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">2.3. Hyperlinks and Third-Party Sites</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">The website may contain hyperlinks or references to third-party websites other than the website. Any such hyperlinks or references are provided for your convenience only. We have no control over third-party websites and accept no legal responsibility for any content, material or information contained in them. The display of any hyperlink and reference to any third-party website does not mean that we endorse the third party’s website, products or services. Your use of a third-party website may be governed by the terms and conditions of that third-party website.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">While navigating the Rolex section of our website, you may interact with an embedded website from rolex.com. In this case, solely Terms of Use, Privacy Notice and Cookies of rolex.com are applicable.</p>', 'Online Orders', '<h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">3.1. Eligibility to order</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">You must be over 18 years of age and:</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- provide us with your real name, contact information and payment details when creating an order;</li><li style=\"list-style: none; padding: 10px 0px;\">- provide us with a delivery address;</li><li style=\"list-style: none; padding: 10px 0px;\">- possess a valid payment method that works on our website.</li></ul><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">3.2. Order Acceptance</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">All orders placed on the website are subject to acceptance in accordance with the following:</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- You will be given the opportunity to confirm the details of your order, including your address and payment details, prior to completing your order.</li><li style=\"list-style: none; padding: 10px 0px;\">- When you place an order, we will send you an acknowledgement email to let you know that we have received your order. This is not an order confirmation or acceptance.</li><li style=\"list-style: none; padding: 10px 0px;\">- We will send you a confirmation email to confirm that your order has been dispatched for delivery. This is confirmation and acceptance of your order.</li><li style=\"list-style: none; padding: 10px 0px;\">- Unless otherwise agreed, your order will be delivered within 30 days of confirmation of your order. Where we are unable to deliver your order within this period, we reserve the right to cancel your order.</li><li style=\"list-style: none; padding: 10px 0px;\">- Once you press the ‘Pay Now’ button, you have given us permission to process your order as requested.</li></ul><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We may be unable, or refuse, to accept your order because:</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- one or more products in your order are unavailable;</li><li style=\"list-style: none; padding: 10px 0px;\">- we are unable to process payment for your order, or your chosen payment method has been refused;</li><li style=\"list-style: none; padding: 10px 0px;\">- we have identified a pricing or product description error;</li><li style=\"list-style: none; padding: 10px 0px;\">- your order fails to meet the requirements of local import restrictions or requirements at your delivery destination.</li><li style=\"list-style: none; padding: 10px 0px;\">- you fail to meet our order eligibility criteria or have failed to comply with our terms.</li></ul><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If we are unable to accept your order, our Customer Service team will be in touch with you as soon as possible.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">3.3. Prices, Payment and Charges</h6><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- LUX has subcontracted certain aspects of the website’s operation, including payment processing, to Black &amp; White, a trading division of Farfetch. The specific Farfetch entity procuring such payment processing services will depend on your location. If you are located in the USA, Farfetch.com US LLC will procure the payment processing services; if you are located anywhere else in the world, Farfetch UK Limited will procure the payment processing services. Farfetch UK Limited is a retailer registered in the United Kingdom with the company number 06400760 and VAT number GB 204 0769 35. Its registered office is at The Bower, 211 Old Street, London EC1V 9NR, United Kingdom. Farfetch.com US LLC is a company registered in the USA and its registered office is at 2301, East 7th Street, Suite A–250 Los Angeles, California 90023 USA.</li><li style=\"list-style: none; padding: 10px 0px;\">- We take care to ensure products are priced correctly on our website, but occasionally pricing errors may occur. If we have made an error in the price of a product that you have ordered, our Customer Service team will contact you. You will have the option to reconfirm your order at the correct price or cancel it.</li><li style=\"list-style: none; padding: 10px 0px;\">- Please note: prices for products on our website may change and offers may be withdrawn at any time. Offers and promotions may be available online only or in-store only. Please check the terms of the relevant offer.</li><li style=\"list-style: none; padding: 10px 0px;\">- If you are viewing the website from the UK, the product prices advertised are inclusive of UK VAT. Delivery costs are not included in the prices and will be charged in addition at checkout.</li><li style=\"list-style: none; padding: 10px 0px;\">- Depending on your delivery address, different taxation rules and additional charges may apply. If you are shipping items internationally, you may need to pay import duties upon receipt of the products. We will notify you during the checkout process if import duties are included or if you need to pay them separately. If they are not included, we do not have any control over these charges and we cannot advise on their amount. You will be responsible for payment of any such import duties and taxes that are not included. Please contact your local Customs office for further information and a ‘landed cost estimate’ before placing your order.</li><li style=\"list-style: none; padding: 10px 0px;\">- All credit and debit cards are subject to validation checks and authorisation by your card issuer. If the issuer of your payment card refuses to or does not, for any reason, authorise payment, we will not be liable for any delay or non-delivery.</li><li style=\"list-style: none; padding: 10px 0px;\">- Should you choose to cancel or return products purchased in a promotion or at a discounted price, you will be refunded for the goods at the price charged at the Checkout. Please note: you are subject to our Returns Policy (see clause 5.4. below) when returning products purchased in a promotion or at a discounted price.</li></ul><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">3.4. Product Descriptions, Ingredients and Restrictions</h6><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(48, 48, 48); font-family: Lato, sans-serif; font-size: 16px;\"><li style=\"list-style: none; padding: 10px 0px;\">- We do not guarantee that the product images you see on our website are an accurate representation; occasionally, colours may differ from the images shown on the website due to your computer/device screen resolution or settings. For further information on products prior to making a purchase with us, please contact our Customer Service team.</li><li style=\"list-style: none; padding: 10px 0px;\">- All products are subject to availability at the point of purchase. Once a product is sold out, it will be taken off the website and may not be available again. If any products you order are out of stock, we will contact you so that you can cancel the whole or part of your order.</li><li style=\"list-style: none; padding: 10px 0px;\">- Substitutions will only be made with your prior consent, with the exception of hampers as stated in clause 5.1. If a replacement product has a higher price, you will be liable to pay for the difference in price. If we are unable to contact you, we may cancel the whole order, or cancel the relevant parts and proceed with the transaction.</li><li style=\"list-style: none; padding: 10px 0px;\">- We cannot be held liable for any allergic reactions in relation to the use or consumption of products. For specific information about ingredients and allergies in any of our food products, please refer to the specific product page or contact our Customer Service team.</li><li style=\"list-style: none; padding: 10px 0px;\">- You must be over the age of 18 to purchase alcohol or products containing alcohol from our website</li></ul>', 'Delivery', '<h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.1. Delivery Options</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Our delivery options vary by shipping destination, product and service. Please visit our Delivery and Returns page for further details.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.2. Delivery Carriers and Time</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Deliveries will be carried out by third-party couriers. By placing an order on the website, you agree to the conditions of carriage of our delivery partners. Please refer to their website for full details or see our&nbsp;<a href=\"file:///C:/Users/baaz_/Desktop/watch/faq.html\" style=\"color: rgb(104, 104, 104); touch-action: manipulation; transition: all 0.3s ease-out 0s; outline-width: medium;\">FAQs.</a></p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">The estimated delivery date of the products will be stated in your order confirmation email. If you do not receive an estimated delivery date from us, we will deliver the order within 30 days after the date of dispatch. We will try to ensure that your order is delivered by the estimated delivery date; however, there may be circumstances where delivery is delayed because of events beyond our reasonable control. If this happens, we will try and arrange for your products to be delivered as soon as possible; however, we will not be liable for any losses caused as a result of such delay. Please note: there may be occasional delays with delivery due to busier periods such as the LUX Sale or Rewards promotional weekends. In such instances, our&nbsp;<a href=\"file:///C:/Users/baaz_/Desktop/watch/contact-us.html\" style=\"color: rgb(104, 104, 104); touch-action: manipulation; transition: all 0.3s ease-out 0s; outline-width: medium;\">Customer Service&nbsp;</a>team can advise you further on the expected delivery date for your order.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">If the product you are ordering contains certain exotic animal skins or fur, and is being delivered outside of Europe, your delivery will take longer (by approximately 20 days on top of the destination’s standard delivery time), as we have to obtain licences for the export and import of the products in accordance with international treaties.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">In certain circumstances, our delivery partner may offer alternative delivery options, including provides the following options: signature release – opting out of the requirement to provide a signature on delivery; and/or leave with neighbour – redirecting the delivery to a neighbour (‘On Demand Delivery Service’). By selecting to receive your order via one of these options the On Demand Delivery Service, you acknowledge and agree that we are neither responsible or liable for any loss or damage that may result from this service.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Nominated Day Delivery</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">This service is only available on products purchased on our website. The nominated delivery day will be stated in your order confirmation email. On occasion, we may experience delivery delays for reasons beyond our control. In those instances, our Customer Service team will contact you as soon as possible. Orders must be placed before 3pm to arrive the next day using our nominated day delivery service. Please note: product exclusions apply including but not limited to third party companies and our brand partners.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.3. Proof of ID</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">For some orders, products may require proof of ID upon delivery.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.4. Delivery Restrictions</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Please note: we are unable to deliver certain products outside of the UK (such as alcohol, beauty and fragrance, wood products, and perishable items). These restrictions are subject to change dependent on a number of factors, including local import restrictions. Please note: information on delivery restrictions is also shown on individual product pages. We can only deliver to one shipping address per order, including hamper orders.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.5. Delivery Taxes &amp; Duties</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Depending on your delivery address, different taxation rules and additional charges may apply. If you are shipping items internationally, you may need to pay import duties upon receipt of the products. We will notify you during the check-out process if import duties are included. Unfortunately, we have no control over these charges and we cannot advise on their amount. The payment of any such import duties or taxes is your responsibility. [SO3] Before placing your order, please contact your local Customs office for further information and a ‘landed cost estimate’. LUX Limited is not responsible for items confiscated or delayed by local Customs authorities.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">It is your responsibility to comply with all requirements imposed by the local Customs authority in relation to your order. This may include the provision of identification information in order to receive your order. Before placing an order, we recommend that you make yourself aware of local import duties or other requirements. We will not be liable or responsible if you breach any such laws.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">Certain countries may also require other certification or information for your order to be delivered, which may delay your delivery (by approximately seven days on top of the destination’s standard delivery time). Please note: if we are unable to obtain the relevant documentation required for your delivery, we may need to cancel your order.</p><h6 style=\"margin-top: 30px; font-family: Lato, sans-serif; color: rgb(48, 48, 48); font-size: 18px;\">4.6. Click and Collect Orders</h6><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We offer a Click and Collect service to collect your order at certain LUX locations. If Click and Collect is available for your order, this will be displayed at the checkout. Please note that Click and Collect is not available for all purchases or at all LUX locations. Once you have selected Click and Collect as your delivery option and completed your order, we cannot re-direct your order to another location or address.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We will notify you when your order is ready to collect at your chosen Click and Collect location during the relevant store opening hours (please note that delivery timeframes are provided as an estimate only). You must bring your order confirmation email and valid proof of ID to collect your order.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">You may also be permitted (at our discretion and subject to availability) to try on some or all of your items at the Click and Collect location (limited to fashion items only).</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">We will keep Click and Collect orders for 10 days at your chosen LUX location before retuning items to our warehouse. We will issue a refund when the order has been received at our warehouse.</p><p style=\"margin-bottom: 15px; font-size: 16px; line-height: 26px; color: rgb(0, 0, 0); padding: 5px 0px; font-family: Lato, sans-serif;\">LUX shall not be liable for any losses or expenses suffered, or any disappointment caused by your failure to collect your order within the timeframes above, if you fail to provide appropriate documentation to collect your order, or if the fittings rooms are unavailable at the time of collection.</p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `uuid` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tickets_user_id_foreign` (`user_id`),
  KEY `tickets_order_id_foreign` (`order_id`),
  KEY `tickets_campaign_id_foreign` (`campaign_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1455 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `status`, `uuid`, `user_id`, `order_id`, `campaign_id`, `created_at`, `updated_at`) VALUES
(1454, 0, 'Goodu_160569452774215', 22, 177, 8, '2020-11-18 05:15:27', '2020-11-18 05:15:27'),
(1453, 0, 'Goodu_160569452783042', 22, 177, 8, '2020-11-18 05:15:27', '2020-11-18 05:15:27'),
(1452, 0, 'Goodu_160569452723265', 22, 177, 8, '2020-11-18 05:15:27', '2020-11-18 05:15:27'),
(1451, 0, 'Goodu_160569452799865', 22, 177, 8, '2020-11-18 05:15:27', '2020-11-18 05:15:27'),
(1450, 0, 'Goodu_160569452792486', 22, 177, 8, '2020-11-18 05:15:27', '2020-11-18 05:15:27'),
(1449, 0, 'Goodu_160569438866104', 22, 176, 8, '2020-11-18 05:13:08', '2020-11-18 05:13:08'),
(1448, 0, 'Goodu_160569438845873', 22, 176, 8, '2020-11-18 05:13:08', '2020-11-18 05:13:08'),
(1447, 0, 'Goodu_160569438859034', 22, 176, 8, '2020-11-18 05:13:08', '2020-11-18 05:13:08'),
(1446, 0, 'Goodu_160569438818562', 22, 176, 8, '2020-11-18 05:13:08', '2020-11-18 05:13:08'),
(1445, 0, 'Goodu_160569438862281', 22, 176, 8, '2020-11-18 05:13:08', '2020-11-18 05:13:08'),
(1444, 0, 'Goodu_160569408776905', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1443, 0, 'Goodu_160569408762421', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1442, 0, 'Goodu_16056940877561', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1441, 0, 'Goodu_160569408789976', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1440, 0, 'Goodu_1605694087783', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1439, 0, 'Goodu_160569408742964', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1438, 0, 'Goodu_160569408726857', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1437, 0, 'Goodu_160569408780470', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1436, 0, 'Goodu_160569408715139', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1435, 0, 'Goodu_160569408713451', 22, 175, 8, '2020-11-18 05:08:07', '2020-11-18 05:08:07'),
(1434, 0, 'Goodu_160569258059519', 22, 174, 8, '2020-11-18 04:43:00', '2020-11-18 04:43:00'),
(1433, 0, 'Goodu_160569257993769', 22, 174, 8, '2020-11-18 04:42:59', '2020-11-18 04:42:59'),
(1432, 0, 'Goodu_160569257961601', 22, 174, 8, '2020-11-18 04:42:59', '2020-11-18 04:42:59'),
(1431, 0, 'Goodu_160569257985379', 22, 174, 8, '2020-11-18 04:42:59', '2020-11-18 04:42:59'),
(1430, 0, 'Goodu_160569257952129', 22, 174, 8, '2020-11-18 04:42:59', '2020-11-18 04:42:59'),
(1429, 0, 'Goodu_16056925023071', 22, 173, 8, '2020-11-18 04:41:42', '2020-11-18 04:41:42'),
(1428, 0, 'Goodu_160569250282318', 22, 173, 8, '2020-11-18 04:41:42', '2020-11-18 04:41:42'),
(1427, 0, 'Goodu_160569250231075', 22, 173, 8, '2020-11-18 04:41:42', '2020-11-18 04:41:42'),
(1426, 0, 'Goodu_160569250234711', 22, 173, 8, '2020-11-18 04:41:42', '2020-11-18 04:41:42'),
(1425, 0, 'Goodu_160569250241981', 22, 173, 8, '2020-11-18 04:41:42', '2020-11-18 04:41:42'),
(1424, 0, 'Goodu_160569239066004', 22, 172, 8, '2020-11-18 04:39:50', '2020-11-18 04:39:50'),
(1423, 0, 'Goodu_16056923904017', 22, 172, 8, '2020-11-18 04:39:50', '2020-11-18 04:39:50'),
(1422, 0, 'Goodu_160569239057399', 22, 172, 8, '2020-11-18 04:39:50', '2020-11-18 04:39:50'),
(1421, 0, 'Goodu_160569239089341', 22, 172, 8, '2020-11-18 04:39:50', '2020-11-18 04:39:50'),
(1420, 0, 'Goodu_16056923909719', 22, 172, 8, '2020-11-18 04:39:50', '2020-11-18 04:39:50'),
(1419, 0, 'Campa_160542299598001', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1418, 0, 'Campa_160542299589732', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1417, 0, 'Campa_160542299542745', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1416, 0, 'Campa_160542299513171', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1415, 0, 'Campa_160542299561323', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1414, 0, 'Campa_160542299557391', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1413, 0, 'Campa_160542299523630', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1412, 0, 'Campa_160542299589656', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1411, 0, 'Campa_160542299594193', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1410, 0, 'Campa_160542299523788', 22, 171, 3, '2020-11-15 01:49:55', '2020-11-15 01:49:55'),
(1409, 0, 'Campa_160542236454289', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1408, 0, 'Campa_160542236437483', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1407, 0, 'Campa_160542236413619', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1406, 0, 'Campa_160542236455286', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1405, 0, 'Campa_160542236492131', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1404, 0, 'Campa_160542236468711', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1403, 0, 'Campa_160542236431718', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1402, 0, 'Campa_160542236498768', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1401, 0, 'Campa_160542236464300', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1400, 0, 'Campa_16054223643820', 22, 170, 3, '2020-11-15 01:39:24', '2020-11-15 01:39:24'),
(1399, 0, 'Campa_160542223823211', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1398, 0, 'Campa_160542223832596', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1397, 0, 'Campa_160542223843325', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1396, 0, 'Campa_160542223878199', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1395, 0, 'Campa_160542223851733', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1394, 0, 'Campa_160542223820580', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1393, 0, 'Campa_16054222382126', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1392, 0, 'Campa_160542223876269', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1391, 0, 'Campa_160542223812985', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1390, 0, 'Campa_160542223832175', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1389, 0, 'Campa_160542223814234', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1388, 0, 'Campa_160542223821354', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1387, 0, 'Campa_160542223866985', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1386, 0, 'Campa_160542223820594', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1385, 0, 'Campa_160542223852981', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1384, 0, 'Campa_160542223865892', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1383, 0, 'Campa_160542223843534', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1382, 0, 'Campa_16054222383437', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1381, 0, 'Campa_160542223861790', 22, 168, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1380, 0, 'Campa_160542223813554', 22, 169, 3, '2020-11-15 01:37:18', '2020-11-15 01:37:18'),
(1379, 0, 'Campa_16049032336125', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1378, 0, 'Campa_160490323382016', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1377, 0, 'Campa_160490323389054', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1376, 0, 'Campa_160490323339964', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1375, 0, 'Campa_160490323320101', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1374, 0, 'Campa_160490323351989', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1373, 0, 'Campa_160490323322307', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1372, 0, 'Campa_160490323373575', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1371, 0, 'Campa_160490323374537', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1370, 0, 'Campa_160490323353671', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1369, 0, 'Campa_160490323312023', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1368, 0, 'Campa_160490323345479', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1367, 0, 'Campa_160490323382917', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1366, 0, 'Campa_160490323342539', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1365, 0, 'Campa_16049032336457', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1364, 0, 'Campa_160490323310479', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1363, 0, 'Campa_160490323325542', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1362, 0, 'Campa_160490323316721', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1361, 0, 'Campa_160490323359138', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1360, 0, 'Campa_160490323327442', 22, 167, 3, '2020-11-09 01:27:13', '2020-11-09 01:27:13'),
(1359, 0, 'Campa_160484005730534', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1358, 0, 'Campa_160484005759577', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1357, 0, 'Campa_160484005781036', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1356, 0, 'Campa_160484005779021', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1355, 0, 'Campa_160484005779536', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1354, 0, 'Campa_160484005797985', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1353, 0, 'Campa_160484005782564', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1352, 0, 'Campa_16048400577480', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1351, 0, 'Campa_160484005793708', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1350, 0, 'Campa_16048400577039', 13, 166, 3, '2020-11-08 07:54:17', '2020-11-08 07:54:17'),
(1349, 0, 'Campa_160483755582760', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1348, 0, 'Campa_160483755539214', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1347, 0, 'Campa_160483755543269', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1346, 0, 'Campa_16048375555547', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1345, 0, 'Campa_160483755523551', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1344, 0, 'Campa_160483755541681', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1343, 0, 'Campa_160483755581979', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1342, 0, 'Campa_160483755535603', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1341, 0, 'Campa_16048375557484', 13, 165, 3, '2020-11-08 07:12:35', '2020-11-08 07:12:35'),
(1340, 0, 'Campa_160483755448494', 13, 165, 3, '2020-11-08 07:12:34', '2020-11-08 07:12:34'),
(1339, 0, 'Campa_160483748171842', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1338, 0, 'Campa_160483748169340', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1337, 0, 'Campa_160483748121354', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1336, 0, 'Campa_160483748121573', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1335, 0, 'Campa_160483748112311', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1334, 0, 'Campa_160483748152627', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1333, 0, 'Campa_160483748110373', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1332, 0, 'Campa_160483748157372', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1331, 0, 'Campa_160483748173819', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1330, 0, 'Campa_160483748127644', 13, 164, 3, '2020-11-08 07:11:21', '2020-11-08 07:11:21'),
(1329, 0, 'Campa_160483709278930', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1328, 0, 'Campa_16048370928476', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1327, 0, 'Campa_16048370928166', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1326, 0, 'Campa_16048370926511', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1325, 0, 'Campa_160483709297290', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1324, 0, 'Campa_160483709243830', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1323, 0, 'Campa_160483709251189', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1322, 0, 'Campa_160483709245223', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1321, 0, 'Campa_160483709243785', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52'),
(1320, 0, 'Campa_160483709290010', 13, 163, 3, '2020-11-08 07:04:52', '2020-11-08 07:04:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_provider` tinyint(10) NOT NULL DEFAULT '0',
  `status` tinyint(10) NOT NULL DEFAULT '0',
  `verification_link` text COLLATE utf8mb4_unicode_ci,
  `email_verified` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `affilate_code` text COLLATE utf8mb4_unicode_ci,
  `affilate_income` double NOT NULL DEFAULT '0',
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `owner_name` text COLLATE utf8mb4_unicode_ci,
  `shop_number` text COLLATE utf8mb4_unicode_ci,
  `shop_address` text COLLATE utf8mb4_unicode_ci,
  `reg_number` text COLLATE utf8mb4_unicode_ci,
  `shop_message` text COLLATE utf8mb4_unicode_ci,
  `shop_details` text COLLATE utf8mb4_unicode_ci,
  `shop_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_url` text COLLATE utf8mb4_unicode_ci,
  `g_url` text COLLATE utf8mb4_unicode_ci,
  `t_url` text COLLATE utf8mb4_unicode_ci,
  `l_url` text COLLATE utf8mb4_unicode_ci,
  `is_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `f_check` tinyint(1) NOT NULL DEFAULT '0',
  `g_check` tinyint(1) NOT NULL DEFAULT '0',
  `t_check` tinyint(1) NOT NULL DEFAULT '0',
  `l_check` tinyint(1) NOT NULL DEFAULT '0',
  `mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `current_balance` double NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  `shop_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` int(100) DEFAULT NULL,
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cm_preference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `f_name`, `l_name`, `photo`, `zip`, `city`, `country`, `address`, `phone`, `fax`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_provider`, `status`, `verification_link`, `email_verified`, `affilate_code`, `affilate_income`, `shop_name`, `owner_name`, `shop_number`, `shop_address`, `reg_number`, `shop_message`, `shop_details`, `shop_image`, `f_url`, `g_url`, `t_url`, `l_url`, `is_vendor`, `f_check`, `g_check`, `t_check`, `l_check`, `mail_sent`, `shipping_cost`, `current_balance`, `date`, `ban`, `shop_logo`, `code`, `date_of_birth`, `cm_preference`, `state`) VALUES
(13, 'Vendor', NULL, NULL, '1596195870rdb0cx47_400x400.jpg', '1234', 'Washington, DC', 'United Arab Emirates', 'Space Needle 400 Broad St, Seattles', '3453453345453411', '23123121', 'vendor@gmail.com', '$2y$10$.4NrvXAeyToa4x07EkFvS.XIUEc/aXGsxe1onkQ.Udms4Sl2W9ZYq', 'HUUugrpyEWP0TcrC0L97dxtAmXCjzmvbYtBJgT46HygCpt2jrOlLR93bqsnF', '2018-03-07 03:05:44', '2020-11-18 05:59:51', 0, 2, '$2y$10$oIf1at.0LwscVwaX/8h.WuSwMKEAAsn8EJ.9P7mWzNUFIcEBQs8ry', 'Yes', '$2y$10$oIf1at.0LwscVwaX/8h.WuSwMKEAAsn8EJ.9P7mWzNUFIcEBQs8rysdfsdfds', 5000, 'Test Stores', 'User', '43543534', 'Space Needle 400 Broad St, Seattles', 'asdasd', 'sdf', 'asdsadsadsads dsa dsddsa', '1596228234109921130_1419223891612412_7271284074505538649_n.jpg', 'http://www.facebook.com', 'http://www.google.com', 'asd', 'asd', 2, 1, 1, 0, 0, 1, 0, 4992.02, '2019-11-24', 0, '1605697191Jellyfish.jpg', NULL, NULL, NULL, NULL),
(22, 'User', NULL, NULL, '1600345791pic.jpg', '12355', 'istanbul', 'Turkey', 'new address 1', '3155054781231', NULL, 'user@gmail.com', '$2y$10$sVZc1jHAzhjSVnuJS12Y/eNWGyIqI7DAs2KvJdRpZyMLt6darxwSC', 'A6epjTdpD5Gn93e4ifXx18DX2rx8V2ysKdCjabKays7xAmBZKfKjI00JhUMZ', '2019-06-19 21:26:24', '2020-10-31 06:50:35', 0, 0, '1edae93935fba69d9542192fb854a80a', 'Yes', '8f09b9691613ecb8c3f7e36e34b97b80', 4963.6900000000005, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, NULL, '26/03/2025', 'email', 'punjab'),
(27, 'Test User', NULL, NULL, NULL, NULL, NULL, NULL, 'Space Needle 400 Broad St, Seattles', '34534534', NULL, 'junajunnun@gmail.com', '$2y$10$pxNqceuvTvYLuwA.gZ15aejOTtXGHrDT7t2m8wfIZhNO1e52z7aLS', '0JCtVQpxMyV5DBdB7g8x07MfN2Wtozyyx4U2lxmYh94WVJIfgdVPGG4yKRaS', '2019-10-04 13:15:08', '2020-08-21 15:38:51', 0, 0, '0521bba4c819528b6a18a581a5842f17', 'Yes', 'bb9d23401cd70f11998fe36ea7677797', 0, 'Test Store', 'User', '43543534', 'Space Needle 400 Broad St, Seattles', 'asdasd', 'ds', '<br>', NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2019-11-24', 0, NULL, NULL, NULL, NULL, NULL),
(28, 'User', NULL, NULL, NULL, '46000', 'rawalpindi', NULL, 'street 1', '1231231231238', '123', 'junnun@gmail.com', '$2y$10$YDfElg7O3K6eQK5enu.TBOyo.8TIr6Ynf9hFQ8dsIDeWAfmmg6hA.', 'pNFebTvEQ3jRaky9p7XnCetHs9aNFFG7nqRFho0U7nWrgT7phS6MoX8f9EYz', '2019-10-12 14:39:13', '2020-08-21 16:02:05', 0, 0, '8036978c6d71501e893ba7d3f3ecc15d', 'Yes', '33899bafa30292165430cb90b545728a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL),
(79, 'Zohra baji', 'Zohra', 'baji', NULL, NULL, NULL, NULL, NULL, '971552772678', NULL, 'zafar4203@gmail.com', '$2y$10$3TLzuk1PFB3dMvwanEN0/.RLPxAh/YdJ5YM2QuvGgwB1k34GG4cta', NULL, '2020-09-28 00:08:16', '2020-11-18 06:03:26', 0, 0, '04916df4059c8efac05f3f46adf1bd96', 'Yes', '1d1aae0d2ea298df9e47703d3d717942', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 9358, NULL, NULL, NULL),
(80, 'Paimon Hamed', 'Paimon', 'Hamed', NULL, '00', 'Dubai', 'United Arab Emirates', 'Marina Residence 3', '971529102475', NULL, 'paimonhamed@gmail.com', '$2y$10$gM4QrpSer5Jw7SwJvhaYreiUpy6tCHeZ9B83QfXf/4uVCMiNm10a2', NULL, '2020-10-12 09:27:03', '2020-10-19 12:28:02', 0, 0, 'b9b4ab7de4b0a3374740b2db27b58bc5', 'Yes', '311abc208cbf1e56eb63e057ca821c20', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 2896, NULL, NULL, 'Dubai'),
(81, 'dimple k', 'dimple', 'k', NULL, NULL, NULL, NULL, NULL, '971422323232', NULL, 'test@gmail.com', '$2y$10$BUsQZkq5rsuTCHH399Wy1eN24Fd2FSSevtwwc6mUDP78FCZZ7UKQe', NULL, '2020-10-18 16:56:46', '2020-10-18 16:56:46', 0, 0, '1e93f9462f51aa8f52eac7aa88e705d2', 'No', '1aedb8d9dc4751e229a335e371db8058', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 4513, NULL, NULL, NULL),
(82, 'dimple k', 'dimple', 'k', NULL, NULL, NULL, NULL, NULL, '971422323232', NULL, 'test123@gmail.com', '$2y$10$GDE3BM2pJPFVA97Li5XNtOFAoWXW9fP1J6Go9NCfDPiSs6wOWK/Jy', NULL, '2020-10-18 16:56:54', '2020-10-18 16:56:54', 0, 0, 'c706b795b3c881c45f48bfbb53ef4329', 'No', '9a93efa79aa9f5d35e14bc55a3e16dc4', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 146, NULL, NULL, NULL),
(83, 'dimple k', 'dimple', 'k', NULL, NULL, NULL, NULL, NULL, '971654356754', NULL, 'dimplekash@gmail.com', '$2y$10$N1mZxvyEb3HSkdNaudKxp.qhRhjBwPf3dN1ZWZLKV0dBpyPoju2mG', NULL, '2020-10-18 17:23:10', '2020-10-18 17:23:10', 0, 0, '4675298dce184c106594ed3774f9a346', 'No', '866fe116d3c3120ecad87cc9280a1c74', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 6134, NULL, NULL, NULL),
(84, 'dimple k', 'dimple', 'k', NULL, NULL, NULL, NULL, NULL, '971552772678', NULL, 'twinkle@gmail.com', '$2y$10$z8g3rc5EtrYpW0FjjN2r7O6awzYHNJIDS6XhqWBqGI6Mg3YKEkZq6', NULL, '2020-10-18 17:34:19', '2020-10-18 17:34:19', 0, 0, 'c16c4701c4065c4b0a11d870d6320b9c', 'Yes', 'e1a07ca03f6823232340110758b9ef7b', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 3268, NULL, NULL, NULL),
(85, 'Pooja Kanaujia', 'Pooja', 'Kanaujia', NULL, '121907', 'Dubai', 'United Arab Emirates', '2712-Gold crest executive', '552772678', NULL, 'Kanaujia.pooja121@gmail.com', '$2y$10$jlTNfkh2iKUqL20SucU3Muaw.YMOgQf/wX4.3Wg1y16mkNzgHnR4W', NULL, '2020-10-22 10:06:12', '2020-10-22 11:36:23', 0, 0, '1ed04ff82fe73134218dac7c954d8c1e', 'Yes', '791deabe068f56dd9d19d3a1be192216', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 2579, '16/02/1992', 'email', 'Uae'),
(87, 'zafar kan', NULL, NULL, NULL, '42031', 'Rawalpindi', 'United Arab Emirates', NULL, '971155053968', NULL, 'zafar4203@gmail.coma', '$2y$10$CEUqR/tD.IrTmlUemozvtuwbQTnQSvw8rucvKPrZcRDUrz1PjXS3C', NULL, '2020-11-07 06:38:36', '2020-11-18 06:23:27', 0, 0, 'b8f542d93dab843b9d9d241946cf8598', 'Yes', '316f06e844152eff026348c34e01771b', 0, 'Good', NULL, '12345', 'street', '12345', 'Good', 'Good<br>', NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

DROP TABLE IF EXISTS `user_notifications`;
CREATE TABLE IF NOT EXISTS `user_notifications` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `order_number`, `is_read`, `created_at`, `updated_at`) VALUES
(4, 29, 'g0CO1594637635', 1, '2020-07-12 14:53:55', '2020-07-12 14:55:15'),
(6, 29, 'iuZF1594637698', 1, '2020-07-12 14:54:59', '2020-07-12 14:55:15'),
(38, 13, 'vKC71597150680', 1, '2020-08-10 16:58:05', '2020-08-11 06:41:11'),
(39, 13, 'GdcR1597194909', 1, '2020-08-11 05:15:18', '2020-08-11 06:41:11'),
(40, 13, 'pWRV1597194954', 1, '2020-08-11 05:15:59', '2020-08-11 06:41:11'),
(41, 13, 'kBjL1597194960', 1, '2020-08-11 05:16:08', '2020-08-11 06:41:11'),
(42, 13, 'agaE1597195002', 1, '2020-08-11 05:16:56', '2020-08-11 06:41:11'),
(43, 13, 'tohl1597195128', 1, '2020-08-11 05:19:10', '2020-08-11 06:41:11'),
(44, 13, 'AXuA1597202870', 1, '2020-08-11 07:27:56', '2020-08-27 23:37:06'),
(45, 13, 'wEU81597203146', 1, '2020-08-11 07:33:13', '2020-08-27 23:37:06'),
(46, 13, 'EySj1597224066', 1, '2020-08-11 13:21:14', '2020-08-27 23:37:06'),
(47, 13, 'SSMt1597224076', 1, '2020-08-11 13:21:22', '2020-08-27 23:37:06'),
(48, 13, 'ygpZ1597237328', 1, '2020-08-11 17:02:13', '2020-08-27 23:37:06'),
(49, 13, 'RgAc1597239486', 1, '2020-08-11 17:38:10', '2020-08-27 23:37:06'),
(50, 13, 'zTvs1597243943', 1, '2020-08-11 18:52:27', '2020-08-27 23:37:06'),
(51, 13, 'Y4cH1597686422', 1, '2020-08-16 21:47:02', '2020-08-27 23:37:06'),
(52, 13, 'x65C1597686441', 1, '2020-08-16 21:47:34', '2020-08-27 23:37:06'),
(53, 13, 'nkJs1597686434', 1, '2020-08-16 21:47:47', '2020-08-27 23:37:06'),
(54, 13, 'n2zK1597686557', 1, '2020-08-16 21:49:38', '2020-08-27 23:37:06'),
(55, 13, 'pSfh1597751366', 1, '2020-08-17 15:49:31', '2020-08-27 23:37:06'),
(56, 77, 'pSfh1597751366', 0, '2020-08-17 15:49:31', '2020-08-17 15:49:31'),
(57, 13, '6t4m1597751915', 1, '2020-08-17 15:58:37', '2020-08-27 23:37:06'),
(58, 77, '6t4m1597751915', 0, '2020-08-17 15:58:37', '2020-08-17 15:58:37'),
(59, 13, 'FarI1597752700', 1, '2020-08-17 16:11:42', '2020-08-27 23:37:06'),
(60, 77, 'FarI1597752700', 0, '2020-08-17 16:11:42', '2020-08-17 16:11:42'),
(61, 13, 'mSka1597752866', 1, '2020-08-17 16:14:29', '2020-08-27 23:37:06'),
(62, 13, 'kbHz1598081303', 1, '2020-08-21 11:28:31', '2020-08-27 23:37:06'),
(63, 13, '2QAV1598081338', 1, '2020-08-21 11:29:01', '2020-08-27 23:37:06'),
(64, 13, 'BBLZ1598256623', 1, '2020-08-23 12:10:28', '2020-08-27 23:37:06'),
(65, 13, '6Wzr1598257719', 1, '2020-08-23 12:28:42', '2020-08-27 23:37:06'),
(66, 13, 'bVe21598257723', 1, '2020-08-23 12:28:45', '2020-08-27 23:37:06'),
(67, 13, 'GJie1598258750', 1, '2020-08-23 12:45:54', '2020-08-27 23:37:06'),
(68, 13, 'yBTo1598259528', 1, '2020-08-23 12:58:51', '2020-08-27 23:37:06'),
(69, 13, '8M8G1598260974', 1, '2020-08-23 13:22:58', '2020-08-27 23:37:07'),
(70, 13, 'FGTG1598261304', 1, '2020-08-23 13:28:28', '2020-08-27 23:37:07'),
(71, 13, '63UX1598261631', 1, '2020-08-23 13:33:54', '2020-08-27 23:37:07'),
(72, 13, 'CkvE1598262247', 1, '2020-08-23 13:44:11', '2020-08-27 23:37:07'),
(73, 13, 'dLgC1598262615', 1, '2020-08-23 13:50:19', '2020-08-27 23:37:07'),
(74, 13, 'oI1B1598642675', 1, '2020-08-27 23:24:38', '2020-08-27 23:37:07'),
(75, 13, 'RF9U1598719147', 1, '2020-08-28 20:39:07', '2020-11-07 05:06:24'),
(76, 13, '3y3G1598719179', 1, '2020-08-28 20:39:39', '2020-11-07 05:06:24'),
(77, 13, 'atIl1598719216', 1, '2020-08-28 20:40:16', '2020-11-07 05:06:24'),
(78, 13, 'K08l1598719295', 1, '2020-08-28 20:41:35', '2020-11-07 05:06:24'),
(79, 13, 'pBtB1598719324', 1, '2020-08-28 20:42:04', '2020-11-07 05:06:24'),
(80, 13, 'KTOX1598720207', 1, '2020-08-28 20:56:47', '2020-11-07 05:06:24'),
(81, 13, 'JFhv1598720228', 1, '2020-08-28 20:57:08', '2020-11-07 05:06:24'),
(82, 13, 'r2eU1598720325', 1, '2020-08-28 20:58:45', '2020-11-07 05:06:24'),
(83, 13, 'Wkhg1598720337', 1, '2020-08-28 20:58:57', '2020-11-07 05:06:24'),
(84, 13, 'ooPG1598720374', 1, '2020-08-28 20:59:35', '2020-11-07 05:06:24'),
(85, 13, 'Phg71598720544', 1, '2020-08-28 21:02:24', '2020-11-07 05:06:24'),
(86, 13, 'NjSq1598720787', 1, '2020-08-28 21:06:27', '2020-11-07 05:06:24'),
(87, 13, 'qGKf1598720899', 1, '2020-08-28 21:08:19', '2020-11-07 05:06:24'),
(88, 13, 'P5ho1598721062', 1, '2020-08-28 21:11:02', '2020-11-07 05:06:25'),
(89, 13, 'qmz51598721211', 1, '2020-08-28 21:13:31', '2020-11-07 05:06:25'),
(90, 13, 'yQKo1598721633', 1, '2020-08-28 21:20:33', '2020-11-07 05:06:25'),
(91, 13, 'QFDW1598722123', 1, '2020-08-28 21:28:43', '2020-11-07 05:06:25'),
(92, 13, 'QCj01598722313', 1, '2020-08-28 21:31:53', '2020-11-07 05:06:25'),
(93, 13, 'kvsq1598722560', 1, '2020-08-28 21:36:00', '2020-11-07 05:06:25'),
(94, 13, 'sjPC1598722874', 1, '2020-08-28 21:41:14', '2020-11-07 05:06:25'),
(95, 13, 'MluC1598723150', 1, '2020-08-28 21:45:50', '2020-11-07 05:06:25'),
(96, 13, 'j6my1598723958', 1, '2020-08-28 21:59:18', '2020-11-07 05:06:25'),
(97, 13, '0vhh1598724305', 1, '2020-08-28 22:05:05', '2020-11-07 05:06:25'),
(98, 13, '0GjA1598724688', 1, '2020-08-28 22:11:33', '2020-11-07 05:06:25'),
(99, 13, 'uKJX1598724808', 1, '2020-08-28 22:13:31', '2020-11-07 05:06:25'),
(100, 13, 'LoZo1598724846', 1, '2020-08-28 22:14:09', '2020-11-07 05:06:25'),
(101, 13, 'CBm61598724974', 1, '2020-08-28 22:16:18', '2020-11-07 05:06:25'),
(102, 13, 'q6Th1598725132', 1, '2020-08-28 22:18:55', '2020-11-07 05:06:25'),
(103, 13, 'QJRW1598725217', 1, '2020-08-28 22:20:17', '2020-11-07 05:06:26'),
(104, 13, 'do7v1598725337', 1, '2020-08-28 22:22:21', '2020-11-07 05:06:26'),
(105, 13, 'wgtR1598725576', 1, '2020-08-28 22:26:16', '2020-11-07 05:06:26'),
(106, 13, 'UW2F1598726131', 1, '2020-08-28 22:35:31', '2020-11-07 05:06:26'),
(107, 13, 'OJX21598728048', 1, '2020-08-28 23:07:28', '2020-11-07 05:06:26'),
(108, 13, '8fQm1598725015', 1, '2020-08-28 22:16:55', '2020-11-07 05:06:26'),
(109, 13, 'o7RA1598726421', 1, '2020-08-28 22:40:21', '2020-11-07 05:06:26'),
(110, 13, '4RCg1598727421', 1, '2020-08-28 22:57:02', '2020-11-07 05:06:26'),
(111, 13, 'BCc51598727444', 1, '2020-08-28 22:57:24', '2020-11-07 05:06:26'),
(112, 13, 'c8Ed1598728127', 1, '2020-08-28 23:08:47', '2020-11-07 05:06:26'),
(113, 13, 'zqRH1598739029', 1, '2020-08-29 02:10:29', '2020-11-07 05:06:26'),
(114, 13, 'Hqk31598739730', 1, '2020-08-29 02:22:10', '2020-11-07 05:06:26'),
(115, 13, 'rPU11598744600', 1, '2020-08-29 03:43:20', '2020-11-07 05:06:26'),
(116, 13, 'Qvg21598744782', 1, '2020-08-29 03:46:22', '2020-11-07 05:06:27'),
(117, 13, 'plfK1599018772', 1, '2020-09-01 07:52:52', '2020-11-07 05:06:27'),
(118, 13, '9lek1599019641', 1, '2020-09-01 08:07:21', '2020-11-07 05:06:27'),
(119, 13, 'IGLB1599291642', 1, '2020-09-04 16:40:42', '2020-11-07 05:06:27'),
(120, 13, 'GM3P1599338697', 1, '2020-09-05 05:44:57', '2020-11-07 05:06:27'),
(121, 13, 'xjeu1599338817', 1, '2020-09-05 05:46:57', '2020-11-07 05:06:27'),
(122, 13, 'VEXX1599338916', 1, '2020-09-05 05:48:36', '2020-11-07 05:06:27'),
(123, 13, 'vVEe1599338971', 1, '2020-09-05 05:49:31', '2020-11-07 05:06:27'),
(124, 13, 'FXVO1599339142', 1, '2020-09-05 05:52:22', '2020-11-07 05:06:27'),
(125, 13, 'jtPq1599339431', 1, '2020-09-05 05:57:11', '2020-11-07 05:06:27'),
(126, 13, 'AI2C1600350070', 1, '2020-09-16 22:41:10', '2020-11-07 05:06:27'),
(127, 13, 'wjvS1600850341', 1, '2020-09-22 17:39:01', '2020-11-07 05:06:27'),
(128, 13, 'GhuW1604743189', 1, '2020-11-07 04:59:54', '2020-11-07 05:06:27'),
(129, 13, 'mXuL1604743387', 1, '2020-11-07 05:03:10', '2020-11-07 05:06:27'),
(130, 13, 'RvPV1604744248', 1, '2020-11-07 05:17:31', '2020-11-07 05:32:34'),
(131, 13, 'E92u1604818946', 0, '2020-11-08 02:02:32', '2020-11-08 02:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_subscriptions`
--

DROP TABLE IF EXISTS `user_subscriptions`;
CREATE TABLE IF NOT EXISTS `user_subscriptions` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `subscription_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Free',
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payment_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_subscriptions`
--

INSERT INTO `user_subscriptions` (`id`, `user_id`, `subscription_id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`, `method`, `txnid`, `charge_id`, `created_at`, `updated_at`, `status`, `payment_number`) VALUES
(81, 27, 5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Paystack', '688094995', NULL, '2019-10-09 06:32:57', '2019-10-09 06:32:57', 1, NULL),
(84, 13, 5, 'Standard', '$', 'NGN', 60, 45, 500, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Paystack', '242099342', NULL, '2019-10-09 11:35:29', '2019-10-09 11:35:29', 1, NULL),
(85, 29, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-06 15:45:44', '2020-07-06 15:45:44', 1, NULL),
(86, 30, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-30 16:58:26', '2020-07-30 16:58:26', 1, NULL),
(87, 30, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-31 00:50:11', '2020-07-31 00:50:11', 1, NULL),
(88, 62, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-31 01:01:30', '2020-07-31 01:01:30', 1, NULL),
(89, 62, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-31 01:06:14', '2020-07-31 01:06:14', 1, NULL),
(90, 62, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-31 01:07:27', '2020-07-31 01:07:27', 1, NULL),
(91, 62, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-31 01:46:44', '2020-07-31 01:46:44', 1, NULL),
(92, 62, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-07-31 01:50:05', '2020-07-31 01:50:05', 1, NULL),
(93, 76, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-08-11 12:45:35', '2020-08-11 12:45:35', 1, NULL),
(94, 76, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-08-11 12:50:19', '2020-08-11 12:50:19', 1, NULL),
(95, 76, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-08-11 12:54:30', '2020-08-11 12:54:30', 1, NULL),
(96, 77, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-08-16 20:13:19', '2020-08-16 20:13:19', 1, NULL),
(97, 77, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-08-16 20:14:56', '2020-08-16 20:14:56', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_orders`
--

DROP TABLE IF EXISTS `vendor_orders`;
CREATE TABLE IF NOT EXISTS `vendor_orders` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `qty` int(191) NOT NULL,
  `price` double NOT NULL,
  `order_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

DROP TABLE IF EXISTS `verifications`;
CREATE TABLE IF NOT EXISTS `verifications` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) NOT NULL,
  `attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('Pending','Verified','Declined') DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `admin_warning` tinyint(1) NOT NULL DEFAULT '0',
  `warning_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`id`, `user_id`, `attachments`, `status`, `text`, `admin_warning`, `warning_reason`, `created_at`, `updated_at`) VALUES
(4, 13, '1573723849Baby.tux-800x800.png,1573723849Baby.tux-800x800.png', 'Verified', 'TEst', 0, NULL, '2019-11-13 12:30:49', '2019-11-13 12:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `winners`
--

DROP TABLE IF EXISTS `winners`;
CREATE TABLE IF NOT EXISTS `winners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(191) NOT NULL,
  `campaign_id` bigint(191) NOT NULL,
  `ticket_id` bigint(191) NOT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `winner_comment` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `winners`
--

INSERT INTO `winners` (`id`, `user_id`, `campaign_id`, `ticket_id`, `video_url`, `winner_comment`, `created_at`, `updated_at`) VALUES
(1, 22, 2, 531, 'asd', 'asd', '2020-10-06 01:26:35', '2020-10-06 01:26:35'),
(2, 22, 2, 531, 'ddddddddd', 'ddddddddddd', '2020-10-06 01:37:47', '2020-10-06 01:37:47');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

DROP TABLE IF EXISTS `wishlists`;
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`) VALUES
(49, 87, 1253),
(50, 87, 1252),
(52, 13, 1253),
(53, 13, 1252),
(57, 22, 1251),
(58, 22, 1253),
(59, 22, 1252);

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

DROP TABLE IF EXISTS `withdraws`;
CREATE TABLE IF NOT EXISTS `withdraws` (
  `id` int(191) NOT NULL AUTO_INCREMENT,
  `user_id` int(191) DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `swift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` float DEFAULT NULL,
  `fee` float DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('pending','completed','rejected') NOT NULL DEFAULT 'pending',
  `type` enum('user','vendor') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `products` ADD FULLTEXT KEY `attributes` (`attributes`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
