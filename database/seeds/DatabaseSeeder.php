<?php

use Illuminate\Database\Seeder;
use App\Models\FooterPage;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // FooterPage::create([
        //     'slug' => 'privacy',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Privacy & Policy',
        // ]);

        // FooterPage::create([
        //     'slug' => 'delivery',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Delivery',
        // ]);

        // FooterPage::create([
        //     'slug' => 'return',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Return Policy',
        // ]);

        // FooterPage::create([
        //     'slug' => 'cookie',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Cookies Policy',
        // ]);

        // FooterPage::create([
        //     'slug' => 'lux-rewards',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Lux Rewards',
        // ]);

        // FooterPage::create([
        //     'slug' => 'mini-lux',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Mini Lux',
        // ]);

        // FooterPage::create([
        //     'slug' => 'gift-card-balance',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Gift Card Balance',
        // ]);

        // FooterPage::create([
        //     'slug' => 'lux-group',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Lux Group',
        // ]);

        // FooterPage::create([
        //     'slug' => 'affiliate',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Become An Affiliate',
        // ]);

        // FooterPage::create([
        //     'slug' => 'corporate-responsibility',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Corporate Responsibility',
        // ]);

        // FooterPage::create([
        //     'slug' => 'press',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Press',
        // ]);

        // FooterPage::create([
        //     'slug' => 'accessibility',
        //     'type' => 0,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Accessibility',
        // ]);

        // FooterPage::create([
        //     'slug' => 'career',
        //     'type' => 1,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Career',
        // ]);

        // FooterPage::create([
        //     'slug' => 'faq',
        //     'type' => 1,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'FAQ',
        // ]);

        // FooterPage::create([
        //     'slug' => 'privacy',
        //     'type' => 1,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Privacy',
        // ]);

        // FooterPage::create([
        //     'slug' => 'terms',
        //     'type' => 1,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Terms',
        // ]);

        // FooterPage::create([
        //     'slug' => 'about',
        //     'type' => 1,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'About',
        // ]);

        // FooterPage::create([
        //     'slug' => 'charity',
        //     'type' => 1,
        //     'category' => 'shopping online',
        //     'visible' => 0,
        //     'name' => 'Charity',
        // ]);

        FooterPage::create([
            'slug' => 'user/orders',
            'type' => 1,
            'category' => 'shopping online',
            'visible' => 0,
            'name' => 'My Orders',
        ]);

    }
}
