<?php

namespace App\Http\Controllers\Admin;

use App\Models\Home;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
        //*** GET Request
        public function create()
        {
            $home = Home::find(1);
            return view('admin.home.create',compact('home'));
        }
    
        //*** POST Request
        public function store(Request $request)
        {        
            if($request->id){

                $data = Home::where(['id' => $request->id])->first();
                $input = $request->all();

                if ($file = $request->file('image_1'))
                {
                    if(!empty($data->image_1))
                    if (file_exists('public/assets/images/home/'.$data->image_1)) {
                    unlink('public/assets/images/home/'.$data->image_1);
                    }

                   $name = time().$file->getClientOriginalName();
                   $file->move('public/assets/images/home',$name);
                   $input['image_1'] = $name;
                }

                if ($file = $request->file('image_2'))
                {

                    if(!empty($data->image_2))
                    if (file_exists('public/assets/images/home/'.$data->image_2)) {
                    unlink('public/assets/images/home/'.$data->image_2);
                    }

                    $name = time().$file->getClientOriginalName();
                    $file->move('public/assets/images/home',$name);
                    $input['image_2'] = $name;
                }
                $data->update($input);            

            }else{
                $data = new Home();
                $input = $request->all();

                if ($file = $request->file('image_1'))
                {
                   $name = time().$file->getClientOriginalName();
                   $file->move('public/assets/images/home',$name);
                   $input['image_1'] = $name;
                }

                if ($file = $request->file('image_2'))
                {
                    $name = time().$file->getClientOriginalName();
                    $file->move('public/assets/images/home',$name);
                    $input['image_2'] = $name;
                }

                $data->fill($input)->save();
            }
    
            $msg = 'Data Updated Successfully';
            return response()->json($msg);      
        }
}
