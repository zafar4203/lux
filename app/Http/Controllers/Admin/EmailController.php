<?php

namespace App\Http\Controllers\Admin;

use Datatables;
use App\Classes\GeniusMailer;
use App\Models\EmailTemplate;
use App\Models\Generalsetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;
use App\Models\Subscriber;
use App\Models\User;

class EmailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
         $datas = EmailTemplate::orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('action', function(EmailTemplate $data) {
                                return '<div class="action-list"><a href="' . route('admin-mail-edit',$data->id) . '"> <i class="fas fa-edit"></i>Edit</a></div>';
                            }) 
                            ->toJson();//--- Returning Json Data To Client Side
    }

    public function index()
    {
        return view('admin.email.index');
    }

    public function config()
    {
        return view('admin.email.config');
    }

    public function edit($id)
    {
        $data = EmailTemplate::findOrFail($id);
        return view('admin.email.edit',compact('data'));
    }

    public function groupemail()
    {
        $config = Generalsetting::findOrFail(1);
        return view('admin.email.group',compact('config'));
    }

    public function groupemailpost(Request $request)
    {
        $config = Generalsetting::findOrFail(1);
        if($request->type == "email")
            {
                $users = User::where(['cm_preference' => 'email'])->get();
                //Sending Email To Users
                foreach($users as $user)
                {
                    if($config->is_smtp == 1)
                    {
                        $data = [
                            'to' => $user->email,
                            'subject' => $request->subject,
                            'body' => $request->body,
                        ];

                        $mailer = new GeniusMailer();
                        $mailer->sendCustomMail($data);            
                    }
                } 
                //--- Redirect Section          
                $msg = 'Email Sent Successfully.';
                return response()->json($msg);    
            //--- Redirect Section Ends  
            }

        if($request->type == 'mobile')
        {
            $users = User::where(['cm_preference' => 'mobile'])->get();
            //Sending Email To Vendors        
            foreach($users as $user)
            {
				// Sending Code to Mobile		
				$sMessage = $request->body;
				$iPhoneNumber = $user->phone;
				$iPhoneNumber = str_replace('+','',$iPhoneNumber);
				$url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx";
				$post = ['User'=> "SoftGates",'passwd'=>"76393134",'sid'=> "8282",'mobilenumber'=> $iPhoneNumber,'message'=> $sMessage,'mtype'=> "N",'DR'=>'Y'];
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
				$response = curl_exec($ch);
				$err = curl_error($ch);
				curl_close($ch);		
				if ($err) {		
					return response()->json('Error In SMS .. '.$err);
                }  
                
            }
        } 

        //--- Redirect Section          
        $msg = 'Email Sent Successfully.';
        return response()->json($msg);    
        //--- Redirect Section Ends  
    }


    

    public function update(Request $request, $id)
    {
        $data = EmailTemplate::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Redirect Section          
        $msg = 'Data Updated Successfully.'.'<a href="'.route("admin-mail-index").'">View Template Lists</a>';
        return response()->json($msg);    
        //--- Redirect Section Ends  
    }

}
