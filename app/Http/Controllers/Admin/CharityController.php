<?php

namespace App\Http\Controllers\Admin;

use App\Models\Charity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CharityController extends Controller
{
        //*** GET Request
        public function create()
        {
            $charity = Charity::find(1);
            return view('admin.charity.create',compact('charity'));
        }
    
        //*** POST Request
        public function store(Request $request)
        {        
            if($request->id){

                $data = Charity::where(['id' => $request->id])->first();
                $input = $request->all();

                if ($file = $request->file('image_1'))
                {
                    if(!empty($data->image_1))
                    if (file_exists('public/assets/images/charities/'.$data->image_1)) {
                    unlink('public/assets/images/charities/'.$data->image_1);
                    }

                   $name = time().$file->getClientOriginalName();
                   $file->move('public/assets/images/charities',$name);
                   $input['image_1'] = $name;
                }

                if ($file = $request->file('image_2'))
                {

                    if(!empty($data->image_2))
                    if (file_exists('public/assets/images/charities/'.$data->image_2)) {
                    unlink('public/assets/images/charities/'.$data->image_2);
                    }

                    $name = time().$file->getClientOriginalName();
                    $file->move('public/assets/images/charities',$name);
                    $input['image_2'] = $name;
                }
                $data->update($input);            

            }else{
                $data = new Charity();
                $input = $request->all();

                if ($file = $request->file('image_1'))
                {
                   $name = time().$file->getClientOriginalName();
                   $file->move('public/assets/images/charities',$name);
                   $input['image_1'] = $name;
                }

                if ($file = $request->file('image_2'))
                {
                    $name = time().$file->getClientOriginalName();
                    $file->move('public/assets/images/charities',$name);
                    $input['image_2'] = $name;
                }

                $data->fill($input)->save();
            }
    
            $msg = 'Data Updated Successfully';
            return response()->json($msg);      
        }
}
