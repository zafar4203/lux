<?php

namespace App\Http\Controllers\Admin;

use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermController extends Controller
{
            //*** GET Request
            public function create()
            {
                $term = Term::find(1);
                return view('admin.term.create',compact('term'));
            }
        
            //*** POST Request
            public function store(Request $request)
            {        
                if($request->id){
                    $data = Term::where(['id' => $request->id])->first();
                    $input = $request->all();
                    $data->update($input);
                
                }else{
                    $data = new Term();
                    $input = $request->all();
                    $data->fill($input)->save();
                }
        
                $msg = 'Data Updated Successfully';
                return response()->json($msg);      
            }
}
