<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Privacy;

class PrivacyController extends Controller
{
        //*** GET Request
        public function create()
        {
            $privacy = Privacy::find(1);
            return view('admin.privacy.create',compact('privacy'));
        }
    
        //*** POST Request
        public function store(Request $request)
        {
            //--- Validation Section
    
            //--- Validation Section Ends
    
            //--- Logic Section
            if($request->id){
                $data = Privacy::where(['id' => $request->id])->first();
                $input = $request->all();
                $data->update($input);
                //--- Logic Section Ends    
            }else{
                $data = new Privacy();
                $input = $request->all();
                $data->fill($input)->save();
                //--- Logic Section Ends    
            }
    
            //--- Redirect Section        
            $msg = 'Data Updated Successfully';
            return response()->json($msg);      
            //--- Redirect Section Ends   
        }
}
