<?php

namespace App\Http\Controllers\Admin;

use App\Models\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
           //*** GET Request
           public function create()
           {
               $career = Career::find(1);
               return view('admin.career.create',compact('career'));
           }
       
           //*** POST Request
           public function store(Request $request)
           {        
               if($request->id){
                   $data = Career::where(['id' => $request->id])->first();
                   $input = $request->all();

                   if ($file = $request->file('image'))
                   {
                       if (file_exists('public/assets/images/career/'.$data->image)) {
                       unlink('public/assets/images/career/'.$data->image);
                       }
   
                      $name = time().$file->getClientOriginalName();
                      $file->move('public/assets/images/career',$name);
                      $input['image'] = $name;
                   }
                   $data->update($input);
               
               }else{
                   $data = new Career();
                   $input = $request->all();

                   if ($file = $request->file('image'))
                   {
                       $name = time().$file->getClientOriginalName();
                       $file->move('public/assets/images/career',$name);
                       $input['image'] = $name;
                   }
                   $data->fill($input)->save();
               }
       
               $msg = 'Data Updated Successfully';
               return response()->json($msg);      
           }
}
