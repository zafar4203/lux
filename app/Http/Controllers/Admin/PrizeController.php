<?php

namespace App\Http\Controllers\Admin;

use App\Models\Prize;
use App\Models\PrizeGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\Input;
use Validator;

class PrizeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
         $datas = Prize::orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
        ->addColumn('status', function(Prize $data) {
            $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
            $s = $data->status == 1 ? 'selected' : '';
            $ns = $data->status == 0 ? 'selected' : '';
            return '<div class="action-list"><select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('admin-prize-status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><option data-val="0" value="'. route('admin-prize-status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option>/select></div>';
        })
        ->addColumn('action', function(Prize $data) {
            return '<div class="action-list"><a href="' . route('admin-prize-edit',$data->id) . '" class="edit"> <i class="fas fa-edit"></i>Edit</a><a href="javascript:;" data-href="' . route('admin-prize-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
        })
        ->rawColumns(['status','action'])
        ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index()
    {
        return view('admin.prize.index');
    }

    //*** GET Request
    public function create()
    {
        return view('admin.prize.create');
    }

    //*** POST Request
    public function store(Request $request)
    {
        //--- Validation Section
        $rules = [
            'images' => 'required',
            'name' => 'required|unique:prizes',
            'description' => 'required',
            'price' => 'required',
            'price_eng' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new Prize();
        $input = $request->all();
        $data->fill($input)->save();

        if ($files = $request->file('images')){
            foreach ($files as  $key => $file){
                $gallery = new PrizeGallery;
                $name = time().$file->getClientOriginalName();
                $file->move('assets/images/prizes',$name);
                $gallery['photo'] = $name;
                $gallery['prize_id'] = $data->id;
                $gallery->save();
            }
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($id)
    {
        $data = Prize::findOrFail($id);
        return view('admin.prize.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:prizes,id,'.$id,
            'description' => 'required',
            'price' => 'required',
            'price_eng' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = Prize::findOrFail($id);
        $input = $request->all();

            if ($files = $request->file('images')){
                $images = PrizeGallery::where(['prize_id' => $data->id])->get();
                if($images){
                    foreach($images as $image){
                        if (file_exists('assets/images/prizes/'.$image->photo)) {
                            unlink('assets/images/prizes/'.$image->photo);
                        }    
                        $image->delete();
                    }
                }

                foreach ($files as  $key => $file){
                    $gallery = new PrizeGallery;
                    $name = time().$file->getClientOriginalName();
                    $file->move('assets/images/prizes',$name);
                    $gallery['photo'] = $name;
                    $gallery['prize_id'] = $data->id;
                    $gallery->save();
                }
            }

        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

      //*** GET Request Status
      public function status($id1,$id2)
      {
          $data = Prize::findOrFail($id1);
          $data->status = $id2;
          $data->update();
      }


    //*** GET Request Delete
    public function destroy($id)
    {
        $data = Prize::findOrFail($id);

        //If Photo Exist
        foreach($data->galleries as $image){
            if (file_exists(public_path().'/public/assets/images/prizes/'.$image->photo)) {
                unlink(public_path().'/public/assets/images/prizes/'.$image->photo);
            }    
        }

        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
