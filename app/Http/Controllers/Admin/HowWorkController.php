<?php

namespace App\Http\Controllers\Admin;

use App\Models\How;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HowWorkController extends Controller
{
        //*** GET Request
        public function create()
        {
            $how = How::find(1);
            return view('admin.how.create',compact('how'));
        }
    
        //*** POST Request
        public function store(Request $request)
        {        
            if($request->id){

                $data = How::where(['id' => $request->id])->first();
                $input = $request->all();

                if ($file = $request->file('image'))
                {
                    if(!empty($data->image))
                    if (file_exists('public/assets/images/how/'.$data->image)) {
                        unlink('public/assets/images/how/'.$data->image);
                    }

                   $name = time().$file->getClientOriginalName();
                   $file->move('public/assets/images/how',$name);
                   $input['image'] = $name;
                }

                $data->update($input);            

            }else{
                $data = new How();
                $input = $request->all();

                if ($file = $request->file('image'))
                {
                   $name = time().$file->getClientOriginalName();
                   $file->move('public/assets/images/how',$name);
                   $input['image'] = $name;
                }
                $data->fill($input)->save();
            }
    
            $msg = 'Data Updated Successfully';
            return response()->json($msg);      
        }
}
