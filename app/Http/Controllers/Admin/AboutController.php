<?php

namespace App\Http\Controllers\Admin;

use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
         //*** GET Request
         public function create()
         {
             $about = About::find(1);
             return view('admin.about.create',compact('about'));
         }
     
         //*** POST Request
         public function store(Request $request)
         {        
             if($request->id){
                 $data = About::where(['id' => $request->id])->first();
                 $input = $request->all();


                 if ($file = $request->file('image'))
                 {
                     if (file_exists('public/assets/images/about/'.$data->image)) {
                     unlink('public/assets/images/about/'.$data->image);
                     }
 
                    $name = time().$file->getClientOriginalName();
                    $file->move('public/assets/images/about',$name);
                    $input['image'] = $name;
                 }
                 
                 $data->update($input);
             
             }else{
                 $data = new About();
                 $input = $request->all();
                 if ($file = $request->file('image'))
                 {
                     $name = time().$file->getClientOriginalName();
                     $file->move('public/assets/images/about',$name);
                     $input['image'] = $name;
                 }
                 $data->fill($input)->save();
             }
     
             $msg = 'Data Updated Successfully';
             return response()->json($msg);      
         }
}
