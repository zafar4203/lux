<?php

namespace App\Http\Controllers\Admin;

use App\Models\Campaign;
use App\Models\Product;
use App\Models\Prize;
use App\Models\Ticket;
use App\Models\Winner;
use App\Models\Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\Input;
use Validator;
use File;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
            $datas = Campaign::orderBy('id','desc')->get();
            //--- Integrating This Collection Into Datatables
            return Datatables::of($datas)
        ->addColumn('status', function(Campaign $data) {
            $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
            $s = $data->status == 1 ? 'selected' : '';
            $ns = $data->status == 0 ? 'selected' : '';
            return '<div class="action-list"><select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('admin-campaign-status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><option data-val="0" value="'. route('admin-campaign-status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option>/select></div>';
        })
        ->addColumn('action', function(Campaign $data) {
            return '<div class="action-list"><a href="' . route('admin-campaign-customer-list',$data->id) . '"> <i class="fas fa-file"></i>View</a></div><div class="action-list"><a href="' . route('admin-campaign-edit',$data->id) . '"> <i class="fas fa-edit"></i>Edit</a><a href="javascript:;" data-href="' . route('admin-campaign-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
        }) 
        ->rawColumns(['status','action'])
        ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index()
    {
        return view('admin.campaign.index');
    }

    //*** GET Request
    public function create()
    {
        // return view('admin.campaign.create',compact('products','prizes'));
        return view('admin.campaign.create');
    }

    //*** POST Request
    public function store(Request $request)
    {
        if($request->prize_description == "<br>"){
            $request->merge(['prize_description' => ""]);
        }
        if($request->product_description == "<br>"){
            $request->merge(['product_description' => ""]);
        }
        if($request->campaign_description == "<br>"){
            $request->merge(['campaign_description' => ""]);
        }
        if($request->campaign_editor_note == "<br>"){
            $request->merge(['campaign_editor_note' => ""]);
        }

        //--- Validation Section
        $rules = [
            'name' => 'required|unique:campaigns',
            'quantity' => 'required',
            'draw_date' => 'required',
            'product_title' => 'required',
            'product_description' => 'required',
            'prize_title' => 'required',
            'prize_description' => 'required',
            'campaign_description' => 'required',
            'campaign_editor_note' => 'required',
            'type' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        $check = 0;
        if($request->type == 0){
            $campaigns = Campaign::where(['status' => 1])->get();
            foreach($campaigns as $campaign){
                if($campaign->type == 0){
                    $check = 1;
                }
            }    
        }

        $input = $request->all();
        $sign = Currency::where('is_default','=',1)->first();
        $input['product_price'] = $input['product_price']/$sign->value;
        if($request->hasFile('product_images'))
        {
            $p_images = [];
            foreach($request->file('product_images') as $image)
            {
                $name = time().$image->getClientOriginalName();
                $image->move('public/assets/images/campaigns',$name);
                array_push($p_images, $name);          
            }
            $input['product_images'] = json_encode($p_images);
        }

        if($request->hasFile('prize_images'))
        {
            $names = [];
            foreach($request->file('prize_images') as $image)
            {
                $name = time().$image->getClientOriginalName();
                $image->move('public/assets/images/campaigns',$name);
                array_push($names, $name);          
            }
            $input['prize_images'] = json_encode($names);
        }

        if($check == 0){
            $data = new Campaign();
            $data->fill($input)->save();
            $msg = 'New Data Added Successfully.';
        }else{
            $msg = 'One other Campaign is already Live.';
        }
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($id)
    {
        $data = Campaign::findOrFail($id);
        $sign = Currency::where('is_default','=',1)->first();
        $data->product_price = round($data->product_price*$sign->value , 0); 
        return view('admin.campaign.edit',compact('data'));
    }
    
    //*** POST Request
    public function update(Request $request, $id)
    {

        if($request->prize_description == "<br>"){
            $request->merge(['prize_description' => ""]);
        }
        if($request->product_description == "<br>"){
            $request->merge(['product_description' => ""]);
        }
        if($request->campaign_description == "<br>"){
            $request->merge(['campaign_description' => ""]);
        }
        if($request->campaign_editor_note == "<br>"){
            $request->merge(['campaign_editor_note' => ""]);
        }

        //--- Validation Section
        $rules = [
            'name' => 'required|unique:campaigns,name,'.$id.',id',
            'quantity' => 'required',
            'draw_date' => 'required',
            'product_title' => 'required',
            'product_description' => 'required',
            'prize_title' => 'required',
            'prize_description' => 'required',
            'campaign_description' => 'required',
            'type' => 'required',
        ];
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        $check = 0;
        if($request->type == 0){
         $campaigns = Campaign::where(['status' => 1])->get();
            foreach($campaigns as $campaign){
            if($campaign->type == 0 && $campaign->id != $id){
                $check = 1;
                }
            }   
        }


        $input = $request->all();
        $sign = Currency::where('is_default','=',1)->first();
        $input['product_price'] = $input['product_price']/$sign->value;
        $camp = Campaign::where(['id' => $id])->first();
        if($request->hasFile('product_images'))
        {
            $images = json_decode($camp->product_images);
            if(!$images){
                $images = [];
            }
            $p_images = $images;
            foreach($request->file('product_images') as $image)
            {
                $name = time().$image->getClientOriginalName();
                $image->move('public/assets/images/campaigns',$name);
                array_push($p_images, $name);          
            }
            $input['product_images'] = json_encode($p_images);
        }

        if($request->hasFile('prize_images'))
        {

            $images = json_decode($camp->prize_images);
            if(!$images){
                $images = [];
            }
            $names = $images;
            foreach($request->file('prize_images') as $image)
            {
                $name = time().$image->getClientOriginalName();
                $image->move('public/assets/images/campaigns',$name);
                array_push($names, $name);          
            }
            $input['prize_images'] = json_encode($names);
        }

        if($check == 0){
            $data = campaign::findOrFail($id);

            $data->update($input);
            $msg = 'Data Updated Successfully.';
        }else{
            $msg = 'Data Updated Successfully But One Other Campaign is Already Live.';
        }

        return response()->json($msg);
        //--- Redirect Section Ends
    }

      //*** GET Request Status
      public function status($id1,$id2)
      {
          $data = Campaign::findOrFail($id1);
          $data->status = $id2;
          $data->update();
      }


    //*** GET Request Delete
    public function destroy($id)
    {
        $data = Campaign::findOrFail($id);
        $product_images = json_decode($data->product_images);
        if($product_images)
        foreach($product_images as $image){
            if (file_exists('public/assets/images/campaigns/'.$image)) {
                unlink('public/assets/images/campaigns/'.$image);
            }                
        }
        $prize_images = json_decode($data->prize_images);
        if($prize_images)
        foreach($prize_images as $image){
            if (file_exists('public/assets/images/campaigns/'.$image)) {
                unlink('public/assets/images/campaigns/'.$image);
            }                
        }
        
        $winners = Winner::where(['campaign_id' => $id])->get();
        if($winners)
        foreach($winners as $winner){
            $winner->delete();    
        }
        
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }    

    public function customer_list($id){
        $campaign = Campaign::where(['id' => $id])->first();
        $tickets = Ticket::where(['campaign_id' => $id])->get();

        $check = 0;
        foreach($tickets as $ticket){
            if($ticket->status == 1){
                $check = 1;
            }
        }

        if($check == 0){
            $winner = 0;
        }else{
            $winner = 1;
        }

        return view('admin.campaign.customer_list',compact('tickets' , 'id','winner','campaign'));
    }

    public function campaign_closed($id){
        $campaign = Campaign::where(['id' => $id])->first();
        $campaign->type = 1;
        $campaign->save();

        return redirect(route('admin-campaign-index'))->with('message', 'Campaign Closed Successfully');
    }

    public function campaign_winner($id){
        $ticket = Ticket::where(['id' => $id])->first();
        return view('admin.campaign.winner',compact('ticket'));
    }

    public function campaign_winner_create(Request $request){
        $rules = [
            'winner_comment' => 'required',
            'video_url' => 'required',
        ];
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends
        //--- Logic Section
        $data = new Winner();
        $input = $request->all();

        $data->fill($input)->save();
        //--- Logic Section Ends

        $campaign = Campaign::where(['id' => $request->campaign_id])->first();
        $campaign->type = 1;
        $campaign->save();

        $ticket = Ticket::where(['id' => $request->ticket_id])->first();
        $ticket->status = 1;
        $ticket->save();


        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);

    }

    public function delete_product_images(Request $request){
        $campaign = Campaign::where(['id' => $request->id])->first();
        $product_images = json_decode($campaign->product_images);
        $p_images = [];
        foreach($product_images as $image){
            if($image == $request->image){
                if (file_exists('public/assets/images/campaigns/'.$image)) {
                    unlink('public/assets/images/campaigns/'.$image);
                }                
            }else{
                array_push($p_images, $image);          
            }
        }
        $campaign->product_images = json_encode($p_images);
        $campaign->save();

        return response()->json(1);
    }

    public function delete_prize_images(Request $request){
        $campaign = Campaign::where(['id' => $request->id])->first();
        $prize_images = json_decode($campaign->prize_images);
        $p_images = [];

        foreach($prize_images as $image){
            if($image == $request->image){
                if (file_exists('public/assets/images/campaigns/'.$image)) {
                    unlink('public/assets/images/campaigns/'.$image);
                }                
            }else{
                array_push($p_images, $image);          
            }
        }
        $campaign->prize_images = json_encode($p_images);
        $campaign->save();

        return response()->json(1);
    }

}
