<?php

namespace App\Http\Controllers\Front;

use Auth;
use Validator;
use App\Models\Address;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function show_address_form(){
        $user = Auth::user();  
        return view('user.new_address',compact('user'));
    }

    public function store(Request $request){
        $rules =
        [
            'country' => 'required',
            'city' => 'required',
            'address_1' => 'required',
            'postal_code' => 'required|numeric'
        ];

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $data = new Address();
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section        

        return redirect(route('user-address'))->with('message', 'New Address Added Successfully!');   
    
        //--- Redirect Section Ends    
    }

    public function edit($id){
        $user = Auth::user();
        $address = Address::where(['id' => $id])->first();
        return view('user.edit_address',compact('user','address'));
    }


    public function update(Request $request){
        $data = Address::where(['id' => $request->id])->first();
        $input = $request->all();
        $data->update($input);

        return redirect(route('user-address'))->with('message', 'Address Updated Successfully!');   
    }

    public function delete($id){
        $address = Address::where(['id' => $id])->first();
        $address->delete();

        return redirect(route('user-address'))->with('message', 'Address Deleted Successfully!');   

    }


}
