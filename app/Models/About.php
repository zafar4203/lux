<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'main_para',
        'main_heading',
        'para_1',
        'heading_1',
        'para_2',
        'heading_2',
        'para_3',
        'heading_3',
        'para_4',
        'heading_4',
        'image'
    ];
    public $timestamps = false;
}
