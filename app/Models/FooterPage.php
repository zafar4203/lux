<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FooterPage extends Model
{
    protected $fillable = [
        'name',
        'type',
        'category',
        'slug',
        'visible',
    ];
    public $timestamps = false;
}
