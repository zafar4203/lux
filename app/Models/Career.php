<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = [
        'para_1',
        'main_heading',
        'heading_1',
        'para_2',
        'heading_2',
        'para_3',
        'heading_3',
        'para_4',
        'image'
    ];
    public $timestamps = false;
}
