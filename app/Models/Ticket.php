<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ["order_id", "user_id", "campaign_id", "uuid","status"];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, "campaign_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    } 
}
