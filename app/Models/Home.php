<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $fillable = [
        'para_1',
        'para_1_heading',
        'para_1_find_out_more',

        'para_2',
        'para_2_heading',
        'para_2_find_out_more',

        'image_1',
        'image_2',

        'video_url'
    ];
    public $timestamps = false;
}