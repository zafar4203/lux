<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCat extends Model
{
    protected $fillable = ['category'];
}
