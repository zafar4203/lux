<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['name','description','image','product_id','status','start_date','end_date'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
