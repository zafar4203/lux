<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class How extends Model
{
    protected $fillable = [
        'para_1',
        'para_2',
        'box_1_heading',
        'box_2_heading',
        'box_3_heading',
        'box_4_heading',
        'box_1_details',
        'box_2_details',
        'box_3_details',
        'box_4_details',
        'image'
    ];
    public $timestamps = false;
}