<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrizeGallery extends Model
{
    protected $fillable = ['prize_id','photo'];
    public $timestamps = false;
}

