<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'name',
        'quantity',
        'draw_date',
        'sold_out',
        'quantity',
        'product_title',
        'product_price',
        'product_description',
        'prize_title',
        'prize_worth',
        'prize_description',
        'campaign_description',
        'campaign_editor_note',
        'status',
        'type',
        'product_images',
        'prize_images',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    public function prize()
    {
        return $this->belongsTo('App\Models\Prize','prize_id');
    }

}
