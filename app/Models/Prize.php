<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    protected $fillable = ['name','description','status','price','price_eng'];
    public function galleries()
    {
        return $this->hasMany('App\Models\PrizeGallery');
    }
}
