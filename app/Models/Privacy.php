<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{
    protected $fillable = [
        'main_heading',
        'main_para',
        'heading_1',
        'para_1',
        'heading_2',
        'para_2',
        'heading_3',
        'para_3',
        'heading_4',
        'para_4'
    ];
    public $timestamps = false;
}
