<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = [
        'title_1',
        'details_1',
        'title_2',
        'details_2',
        'title_3',
        'details_3',
        'title_4',
        'details_4'
    ];
    public $timestamps = false;
}
