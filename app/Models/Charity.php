<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charity extends Model
{
    protected $fillable = [
        'para_1',
        'para_2',
        'get_in_touch_text',
        'call_us_now_text',
        'write_email_text',
        'image_1',
        'image_2'
    ];
    public $timestamps = false;
}
