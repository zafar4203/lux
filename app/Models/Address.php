<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'country',
        'title',
        'city',
        'state',
        'postal_code',
        'user_id',
        'address_1',
        'address_2',
    ];
}
