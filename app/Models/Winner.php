<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $fillable = [
        'campaign_id',
        'user_id',
        'ticket_id',
        'winner_comment',
        'video_url'
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, "campaign_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    } 

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, "ticket_id");
    } 
}
