<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetetionOrder extends Model
{
	protected $fillable = [
        
        'user_id', 
        'discount_value' , 
        'totalQty', 
        'pay_amount', 
        'txnid', 
        'charge_id', 
        'order_number', 
        'payment_status', 
        'customer_email', 
        'customer_name', 
        'customer_phone', 
        'customer_address', 
        'customer_city', 
        'customer_zip',
        'shipping_name', 
        'shipping_email', 
        'shipping_phone', 
        'shipping_address', 
        'shipping_city', 
        'shipping_zip', 
        'order_note', 
        'status',
        'currency_sign',
        'currency_value',
        'campaign_id',
        'donate',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function campaign()
    {
        return $this->hasOne('App\Models\Campaign','id','campaign_id');
    }

    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket','order_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\CompetetionOrder','order_id');
    }

}
