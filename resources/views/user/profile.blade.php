@extends('layouts.front')
@section('styles')
    <style>
.login-form-container input[type="number"] {
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
    min-width: 100%;
    max-width: 100%;
}
.alert {
    margin-bottom: 0px;
}

.login-form-container select {
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
    border-right:0px;
}
    </style>
@endsection
@section('content')      

<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
               
                       @include('includes.user-dashboard-sidebar')
                    
                  <div class="col-md-8">
                     <div class="lux-enter mt-15">
                        
                        <div class="account-listing">
                           <div class="row">
                              <div class="col-md-8">
                                <div class="login-form-container login-form-container2  pt-0">
                                    <div class="login-form">
<form id="userform" action="{{route('user-profile-update')}}" method="POST"
                                            enctype="multipart/form-data">
{{ csrf_field() }}
@include('includes.admin.form-both')

    <input type="hidden" name="from" value="profile" />
    <div class="login-form-container login-form-container2">
    
    <div class="checkbox-form">						
        
        <div class="row">
        <div class="col-md-12">
            <div class="checkout-form-list">
                <label class="black-text">Name</label>
                <p class="black-text">{{ Auth::user()->name }}</p>
            </div>
        </div>
    </div>
        <div class="row"><div class="col-md-12">
            <label>Date of birth</label>
            <input type="hidden" name="date_of_birth" id="date_of_birth">
        </div></div>
        <div class="row no-gutters">
        <div class="col-md-2">
            <div class="country-select form-group">                                              
                <input type="number" min="0" placeholder="DD" id="date" @if(!empty($user->date_of_birth)) value="{{explode('/' , $user->date_of_birth)[0]}}" @endif required>
            </div>
        </div>
        <div class="col-md-1 text-center space-line">/</div>
        <div class="col-md-3">
            <div class="country-select form-group">
                                                
            <input type="number" min="0" placeholder="MM" id="month"  @if(!empty($user->date_of_birth)) value="{{explode('/' , $user->date_of_birth)[1]}}" @endif required>
            </div>
        </div>
    
        <div class="col-md-1 text-center space-line">/</div>
        <div class="col-md-3">
            <div class="country-select form-group">
                                                    
            <input type="number" min="0" placeholder="YY" id="year" @if(!empty($user->date_of_birth)) value="{{explode('/' , $user->date_of_birth)[2]}}" @endif required>
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="checkout-form-list form-group">
                    <label>Email address</label>
                    <input name="email" type="email"
                                    placeholder="{{ $langg->lang265 }}" required=""
                                    value="{{ $user->email }}" disabled>
                </div>
            </div>
            </div>
            
            
            
            <!-- Added by Prashant 23-11-2020 -->
            <div class="col-md-12 p-0">
                <label>Contact number</label>
            </div>
            <div class="row">
                <div class="col-4 pr-0">
                    <div class="form-group">
                    <select name="country_code" id="" required>
                        @foreach($countries as $country)
                            <option @if($user->country == $country->country_name) selected @endif value="{{$country->phone_code}}">{{$country->country_code}} ({{$country->phone_code}})</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <input type="number" name="phone" placeholder="529102475" value="{{ $user->phone }}" required>
                    </div>
                </div>
            </div>
            <!-- Added by Prashant 23-11-2020 -->
            
            
            
        <!--    <div class="row"><div class="col-md-12">-->
        <!--    <label>Contact number (optional)-->
        <!--    </label>-->
        <!--</div></div>-->
            <!--<div class="row ">-->
            
            <!--<div class="col-md-4">-->
            <!--    <div class="country-select form-group">-->
                                                        
            <!--        <select>-->
            <!--            <option value="971">UAE (+971)</option>-->
            <!--        </select>-->
            <!--    </div>-->
            <!--</div>-->
        
            
            <!--<div class="col-md-5">-->
            <!--    <div class="checkout-form-list form-group">-->
                                                        
            <!--    <input name="phone" type="text"-->
            <!--                    placeholder="{{ $langg->lang266 }}" required=""-->
            <!--                    value="{{ $user->phone }}">-->
            <!--    </div>-->
            <!--</div>-->
            <!--</div>-->
            
            
            
            
            
            
            
            <div class="payment-hr2 ">
            <hr/>
            </div>
        <div class="row">
            <div class="col-md-12">
                <div class="checkout-form-list form-group">
                    <label>Enter password to save</label>
                    <input type="password" name="c_password" placeholder="">										
                </div>
            </div>
        </div>
        
        </div>	
        </div>
                                                    
    </div>
    
    
    <p class="mt-20neg">
        <button id="submit" type="submit" class="btn-style2"><span>Save Changes </span></button>
    </p>

</form>
                                 </div>
                                    
                                 </div>
                              </div>
                            </div>
                           </div>
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')











@endsection

@section('scripts')
    <script>
        $('#submit').click(function(e){
            e.preventDefault();
            let valid = true;
    $('[required]').each(function() {
        if ($(this).is(':invalid') || !$(this).val()) valid = false;
    })
        if (!valid) alert("Please fill Date fields!");
        else{
            if($('#date').val() >30 || $('#month').val() > 12){
                alert("Please Enter Valid Date");
            }else{
                $('#date_of_birth').val($('#date').val()+"/"+$('#month').val()+"/"+$('#year').val());
                $('#userform').submit();
            }
        }

        });
    </script>
@endsection