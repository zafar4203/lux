@extends('layouts.front')

@section('styles')
    <style>
.login-form-container select {
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
}
.alert {
    margin-bottom: 0px;
}

.account-listing li {
    padding: 0px 0 !important;
}
.error{
    color:red;
}
    </style>
@endsection
@section('content')

<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
             
                       @include('includes.user-dashboard-sidebar')
                    
                  <div class="col-md-8">
                     <div class="lux-enter mt-50">
                        
<div class="account-listing">
<div class="row">
<div class="col-md-12">
<div class="login-form-container login-form-container2  pt-0">
    <div class="login-form">
        <h3 class="border-bottom-gray">Add New Address
        </h3>
        <div class="col-md-10">
        <form action="{{route('user-add-address')}}" method="POST"
                                            enctype="multipart/form-data">
{{ csrf_field() }}
@include('includes.admin.form-both')
        <input type="hidden" name="from" value="address" />
            <div class="login-form-container login-form-container2">
            
            <div class="checkbox-form">						
                
                <!-- <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>You don't have any saved addresses</label>
                        <p>
                            <div class="cart-btn3 text-left mt-30 mb-15">
                                <a href="#">Add an address </a>                                                            </a>
                                </div>
                            </p>									
                    </div>
                </div>
            </div> -->

            <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Title</label>
                            <input name="title" type="text" placeholder="Home , Office" value="{{ old('title') }}">
                            @if($errors->has('title'))
                                <small class="form-text error">{{ $errors->first('title') }}</small>
                            @endif
                        </div>
                    </div>
                </div>                
                
                <div class="row mt-20">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Country or region</label>
                            <select class="input-field" name="country">
                                    <option value="">{{ $langg->lang157 }}</option>
                                    @foreach (DB::table('countries')->where(['status' => 1])->get() as $data)
                                        <option value="{{ $data->country_name }}" {{ $user->country == $data->country_name ? 'selected' : '' }}>
                                            {{ $data->country_name }}
                                    </option>		
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    </div>
                    
                
                     <div class="bill-new-address">
                  
                <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address_1" value="{{ old('address_1') }}" placeholder="{{ $langg->lang270 }}" />
                            <input type="text" name="address_2" value="{{ old('address_2') }}" placeholder="{{ $langg->lang270 }}" />
                            @if($errors->has('address_1'))
                                <small class="form-text error">{{ $errors->first('address_1') }}</small>
                            @endif
                        </div>
                    </div>
                    </div>
                    <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Own/City</label>
                            <input name="city" type="text" placeholder="{{ $langg->lang268 }}" value="{{ old('city') }}">
                            @if($errors->has('city'))
                                <small class="form-text error">{{ $errors->first('city') }}</small>
                            @endif
                        </div>
                    </div>
                    </div>
                    <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>state (option)</label>
                            <input name="state" type="text" placeholder="Enter State" value="{{ old('state') }}">
                            @if($errors->has('state'))
                                <small class="form-text error">{{ $errors->first('state') }}</small>
                            @endif
                        </div>
                    </div>
                    </div>
                    <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-12 no-padding ">Postal Code</label>
                            <input name="postal_code" type="text" placeholder="{{ $langg->lang269 }}" value="{{ old('postal_code') }}">
                            @if($errors->has('postal_code'))
                                <small class="form-text error">{{ $errors->first('postal_code') }}</small>
                            @endif
                        </div>
                    </div>
                    </div>
                      
                      </div>
                      </div>                      
                </div>	
                </div>
            
        
            <div class="col-md-7">
            <p class="mt-20neg">
                <button type="submit" class="btn-style2 wid100">Save Address
                </button>
            </p>
            <!-- <p >
                <button type="submit" class="cancel-style2 wid100"><span>Cancel
                </span></button>
            </p> -->
            </div>
    </form>
                                 </div>
                              </div>
                                 </div>
                              </div>
                            </div>
                           </div>
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')


@endsection

 