@extends('layouts.front')

@section('styles')
 
@endsection
@section('content')

<div class="header-height"></div>
<!-- main-search start -->
<div class="main-search-active">
<div class="sidebar-search-icon">
    <button class="search-close"><span class="ti-close"></span></button>
</div>
<div class="sidebar-search-input">
    <form>
        <div class="form-search">
            <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
            <button>
            <i class="ti-search"></i>
            </button>
        </div>
    </form>
</div>
</div>

<div class="mt-120" >
<div class="container">
    <div class="row">
        @include('includes.user-dashboard-sidebar')
        <div class="col-md-8">
            <div class="lux-enter mt-50">
            
            <div class="account-listing">
                <div class="row">
                    <div class="col-md-12">
                    <div class="login-form-container login-form-container2  pt-0">
                        <div class="login-form">
                            <h3 class="border-bottom-gray">LUX Campaigns
                            </h3>
            <div class=" mt-40">
                <h4>Upcoming Campaigns</h4>
                <div id="accordion2" class="accordion mb-20 mt-30">
                    <div class="card mb-0">
                    @foreach($campaigns as $campaign)
                    @if($campaign->type == 0)
                    <div class="card-header collapsed active-acc" data-toggle="collapse" href="#camp_{{$campaign->id}}">
                        <a class="card-title">
                            {{ $campaign->name }}
                        </a>
                    </div>
                    <div id="camp_{{$campaign->id}}" class="card-body collapse show" data-parent="#accordion2" >
                        <div class="row">
                            <div class="col-md-4">
                            <div class="product-box-img">
                                @php $product_images = json_decode($campaign->product_images); @endphp
                                <img src="{{ asset('public/assets/images/campaigns')}}/{{$product_images[0]}}" alt="">
                                <p>Ending on: <br> {{ $campaign->draw_date }}</p>
                            </div>

                            </div>
                            <div class="col-md-8">
                            <div class="detial-product-accordian">
                            <h4 class="mt-20">{{ $campaign->name }}</h4>
                            <p><small>{{ $campaign->prize_name }}</small></p>
                            <p>{{ \App\Models\Product::convertPrice($campaign->product_price) }}</p>
                            <button class="btn-style">Campaign code: #{{ $campaign->id }} </button>
                            </div>
                            </div>
                        </div>
                    </div>
                    @endif


                    @if($campaign->type == 1)
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#camp_{{$campaign->id}}">
                        <a class="card-title">
                            <div class="row">
                                <div class="col-md-5">{{ $campaign->name }}
                                </div>
                                <div class="col-md-5 fadecolor">Campaign code: #{{ $campaign->id }} 
                                </div>
                            </div>
                        
                        </a>
                    </div>
                    <div id="camp_{{$campaign->id}}" class="card-body collapse" data-parent="#accordion2" >
                        <p>
                            {!! $campaign->campaign_description !!}
                        </p>
                    </div>
                    @endif
                    @endforeach

                    <!-- <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                        <a class="card-title">
                            <div class="row">
                                <div class="col-md-5">Campaign ABC
                                </div>
                                <div class="col-md-5 fadecolor">Campaign code: #2231 
                                </div>
                            </div>                        
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse" data-parent="#accordion2" >
                        <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                            aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS.
                        </div>
                    </div> -->
                    </div>
                </div>
        </div>
    </div>

            
                    <div class="login-form">
                        <div class="mt-40">
                        <h4 style="margin-bottom:5%;">Campaign History</h4>
                        @foreach($orders as $order)
                        <div id="ticku_{{$order->id}}" class="accordion mt-5">
                            <div class="card mb-0">
                                <div class="card-header collapsed" data-toggle="collapse" href="#ticket_{{$order->id}}">
                                <a class="card-title">
                                    {{ $order->campaign->name }} ({{ count($order->tickets) }} Tickets )
                                </a>
                                </div>
                                <div id="ticket_{{$order->id}}" class="card-body collapse" data-parent="#ticku_{{$order->id}}" >
                                <div class="row">


                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td>S.no</td>
                                                <!-- <td>Campaign</td> -->
                                                <td>Prize</td>
                                                <td>Ticket UID</td>
                                                <td>Purchase Date</td>
                                                <td width="20%">Ending Date</td>
                                            </tr>
                                        </thead>

                                    <tbody>
                                    @foreach($order->tickets as $ticket)

                                    @php $prize_images = json_decode($campaign->prize_images); @endphp
                                    <!-- <div class="col-md-4">
                                        <div class="product-box-img">
                                        <img src="{{ asset('public/assets/images/campaigns')}}/{{$prize_images[0]}}" alt="">
                                        <p>Purchase Date: <br> {{ $ticket->created_at }}</p>
                                        </div>

                                    </div>
                                    <div class="col-md-8">
                                        <div class="detial-product-accordian">
                                        <h4 class="mt-20">{{ $ticket->campaign->name }}</h4>
                                        <p><small>{{ $ticket->campaign->prize_name }}</small></p>
                                        <p>Ticket #<small>{{ $ticket->uuid }}</small></p>
                                        <p>Price: AED 500</p>
                                        <p>Draw Date: {{ $campaign->draw_date }}</p>
                                        <button class="btn-style">Campaign code: #{{ $ticket->campaign->id }} </button>
                                    </div> -->

                                            <tr>
                                                <td>{{ $ticket->id }}</td>
                                                <!-- <td>{{ $ticket->campaign->name }}</td> -->
                                                <td>AED{{ $ticket->campaign->prize_worth  }}</td>
                                                <td>{{ $ticket->uuid }}</td>
                                                <td>{{ $ticket->created_at->format('Y-m-d') }}</td>
                                                <td>{{ $ticket->campaign->draw_date }}</td>
                                            </tr>
                                    @endforeach
                                    </tbody>
                                    </table>

                                </div>
                                </div>
                            </div>
                        @endforeach

                </div>
                        </div>
                        
                        
      
            
                </div>
            </div>
                        </div>
                    </div>
                </div>
                </div>
                
                

            
            </div>
            </div>
        </div>
    </div>
</div>

             @include('front.chunks.contacts')
            @include('front.chunks.footer')

@endsection

 