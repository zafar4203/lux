@extends('layouts.front')
@section('title' , 'Dashboard')
@section('styles')
  <style>
  </style>
@endsection
@section('content')

<div class="header-height"></div>
<!-- main-search start -->
<div class="main-search-active">
   <div class="sidebar-search-icon">
      <button class="search-close"><span class="ti-close"></span></button>
   </div>
   <div class="sidebar-search-input">
      <form>
         <div class="form-search">
            <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
            <button>
            <i class="ti-search"></i>
            </button>
         </div>
      </form>
   </div>
</div>
<div class="banner-area hm-4-padding">
   <div class="container-fluid">
       <div class=" banner-img2">
           <!--<a href="#">-->
           <!--   <img src="{{ asset('public/assets/front/img/banner/account-bg.png') }}" alt="">-->
           <!--</a>-->
       </div>
   </div>
</div>
<div class="mt-40" >
   <div class="container">
      <div class="row">
     
              @include('includes.user-dashboard-sidebar')
       
         <div class="col-md-8">
            <div class="lux-enter">
               <h2>Enter Lux Competition</h2>
               <p class="mt-20"><strong> DISCOVER OUR COMPETITIONS
               </strong></p>
               <p class="mt-20">Enter competitions to earn gift prizes on purchases and<br/> access exclusive beneﬁts.
               </p>
               <button type="submit" class="btn-style2 ">
               <a href="{{ route('front.campaign') }}"><span>Enter Competitions</span></a>
               <b></b></button>
              <p class="border-bottom-gray pt-20 mb-30">&nbsp;</p>
               <div class="account-listing ">
                  <h3 class="mt-50 mb-30">Update Your Details </h3>
                  <ul>
                     <li class="border-bottom-gray">
                      
                       <div class="row">
                          <div class="col-md-8"><h4>ADD YOUR BIRTHDAY</h4>
                         <p>This is so that we can email you a birthday treat.
                        </p>
                        </div>
                          <div class="col-md-4"><button type="submit" class="btn-style2 "><span><a href="{{ route('user-profile') }}">Add my Birthday</a>
                        </span></button></div>
                       </div>
                     
                     </li>
                     <li class="border-bottom-gray">
                      
                        <div class="row">
                           <div class="col-md-8"><h4>ADD YOUR ADDRESS </h4>
                          <p>Provide a postal address for future orders.

                         </p>
                         </div>
                           <div class="col-md-4"><button type="submit" class="btn-style2 "><span><a href="{{ route('user-address') }}">Add my address</a>
                         </span></button></div>
                        </div>
         
                      </li>
                      <li class="border-bottom-gray">
                      
                        <div class="row">
                           <div class="col-md-8"><h4>ADD YOUR NUMBER </h4>
                          <p>A phone number will allow us to contact you about your order.

                         </p>
                         </div>
                           <div class="col-md-4"><button type="submit" class="btn-style2 "><span><a href="{{ route('user-profile') }}">Add my number</a>
                         </span></button></div>
                        </div>

                      </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>

   @include('front.chunks.contacts')
   @include('front.chunks.footer')

@endsection