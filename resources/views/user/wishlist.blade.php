@extends('layouts.front')
	@section('styles')
		<style>
			.header-height{
				margin-top:10%;
			}
		</style>
	@endsection
@section('content')



<div class="header-height"></div>
       
      


<div class="row">

@if(count($wishlists) > 0)

<div class="container"><hr/></div></div>
         <div class="product-area pb-80">
            
            <div class="container ">
               <h3 class="text-center">Your Wishlist</h3>
               <p class="text-center">Here you can see the styles in your Wishlist, make edits to your selection and add to your shopping bag.
  </p>
  
  <div class="row">
     <div class="container mt-20">
      <div class="row">
	  @foreach($wishlists as $wishlist)
		<input type="hidden" id="product_id" value="{{ $wishlist->product->id }}">

        <div class="col-md-4" >
         <div class="product-wrapper white-box">
            <p class="pt-10 text-center brown-text"></p>
			<!-- <div class="star-top">
				<a href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$wishlist->product->id) }}"><i class="ion-ios-star-outline"></i></a>
			</div> -->
			<div class="product-img">
				<a href="{{ route('front.product',$wishlist->product->slug) }}">
					<img style="padding:20px 30px; width:370px; height:421px" src="{{ $wishlist->product->photo ? asset('public/assets/images/products/'.$wishlist->product->photo):asset('public/assets/images/noimage.png') }}" alt="">
				</a>
			</div>
            <div class="product-content text-center">

               <h4><a href="{{ route('front.product',$wishlist->product->slug) }}">{{ $wishlist->product->name }}</a></h4>
               <h4><a href="{{ route('front.product',$wishlist->product->slug) }}">{{ $wishlist->product->category->name }}</a></h4>
               <div class="product-price">
                  <span>{{ $wishlist->product->setCurrency() }}</span>
               </div>
            </div>
			<div class="row">
				<div class="col-md-12">
					<button class="btn-style btn-block add-to-cart-btn cart-btn add-to-cart" data-href="{{ route('product.cart.add',$wishlist->product->id) }}" href="">
						Add to Cart
					</button>
				</div>
				<div class="col-md-12 mt-2">
					<button class="btn-style btn-block remove wishlist-remove" data-href="{{ route('user-wishlist-remove', App\Models\Wishlist::where('user_id','=',$user->id)->where('product_id','=',$wishlist->product->id)->first()->id ) }}">
						Remove Wishlist
					</button>
				</div>
			</div>
         </div>
        </div>
		@endforeach

      </div>
     </div>
  </div>

  @if($recentlyViewed)
  <div class="row">
  <div class="container"><hr/>
  </div>
  </div>
               <div class="tab-content">
                  <h3  class="text-center mt-50">RECENTLY VIEWED</h3>
                  <div class="col-md-12">
                  
                     <div class="owl-slider">
                        <div id="carousel3" class="owl-carousel">
						   @foreach($recentlyViewed as $prod)
                           <div class="item">
                              <div class="product-wrapper white-box">
                                 <p class="pt-10 text-center brown-text"><a href="#" ><span></span>New Brand</a></p>
                                 <div class="star-top">
   								    @php $check=0; @endphp
									@foreach($wishs as $wishlist)
									@if($wishlist->product_id == $prod->id)
									@php $check=1; @endphp
									@endif
									@endforeach
									<a href="javascript:;" class="add-to-wish" id="{{ $prod->id }}" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($check == 0) ion-ios-star-outline @else ion-ios-star @endif"></i></a>
                                 </div>
								 <div class="product-imga">
									<a href="{{ route('front.product',$prod->slug) }}">
										<img src="{{ $prod->thumbnail ? asset('public/assets/images/thumbnails/'.$prod->thumbnail):asset('public/assets/images/noimage.png') }}" alt="">
									</a>
								</div>
                                 <div class="product-content text-center">
									<h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName()}}</a></h4>
									<h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->category->name }}</a></h4>
									<div class="product-price">
										<span>{{ $prod->setCurrency() }}</span>
									</div>
                                 </div>
                              </div>
                           </div>
                           @endforeach

                        </div>
                     </div>                 
               </div>

               </div>
            </div>
			@endif

			@else
			<div class="mt-5 mb-5 pt-5 pb-5 col-md-12 text-center">
				<h1 style="width:100%;">Wishlists Empty</h1>
			</div>
			@endif
         </div>

    @include('front.chunks.footer')
	
@endsection

@section('scripts')

<script type="text/javascript">
        $("#sortby").on('change',function () {
        var sort = $("#sortby").val();
        window.location = "{{url('/user/wishlists')}}?sort="+sort;
		});
		

		$('.minuso').click(function(e){
			e.preventDefault();
			var val = $('#'+this.id+'_input').val();
			if(val>1){
				val--;
				$('#'+this.id+'_input').attr('value', val);
			}
		});

		$('.pluso').click(function(e){
			e.preventDefault();
			var val = $('#'+this.id+'_input').val();
			val++;
			$('#'+this.id+'_input').attr('value', val);
		});
</script>

@endsection
