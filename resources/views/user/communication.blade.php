@extends('layouts.front')

@section('styles')

@endsection
@section('content')

<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
                 
                       @include('includes.user-dashboard-sidebar')
                  
                  <div class="col-md-8">
                     <div class="lux-enter mt-50">
                        <h3 class="border-bottom-gray mb-4">Communication Preferences             
                        </h3>
                       
                        <div class="account-listing commn-pref">
                           
                        <form id="userform" action="{{route('user-communication-update')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('includes.admin.form-both') 

                           <ul>
                              <li>
                               
                                <div class="row">
                                   <div class="col-md-12"><h4>Lux News & Stories
                                 </h4>
                                  <p>Get the latest LUX news and stories – plus special offers and more – sent straight to your inbox. We may also remind you about incomplete orders.
                                  </p>
                                 </div>
                                 <!-- Commented by Prashant 23-11-2020 -->
                                <!-- <div class="col-md-2 pt-30 text-center"><p>Receive Via</p></div>-->
                                <!--   <div class="col-md-2  text-right"><div class="form-check mt-30">-->
                                <!--    <input class="form-check-input" type="radio" name="cm_preference" id="email-2" value="email" checked="">-->
                                <!--    <label class="form-check-label" for="email-2">-->
                                <!--       Email-->
                                <!--    </label>-->
                                <!-- </div></div>-->
                                 
                                <!--<div class="col-md-2  text-right"><div class="form-check mt-30">-->
                                <!--    <input class="form-check-input" type="radio" name="cm_preference" id="email-2" value="mobile" checked="">-->
                                <!--    <label class="form-check-label" for="email-2">-->
                                <!--       Mobile-->
                                <!--    </label>-->
                                <!-- </div></div>-->
                                 <!-- Commented by Prashant 23-11-2020 -->
                                </div>

                              </li>
                              <!-- Added by Prashant 23-11-2020 -->
                              <li class="pt-0">
                                  <div class="row">
                                      <div class="col-md-3 text-left"><p>Receive Via</p></div>
                                       <div class="col-md-2"><div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="cm_preference" id="email-2" value="email" checked="">
                                        <label class="form-check-label" for="email-2">
                                           Email
                                        </label>
                                     </div></div>
                                     
                                    <div class="col-md-2"><div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="cm_preference" id="email-2" value="mobile" checked="">
                                        <label class="form-check-label" for="email-2">
                                           Mobile
                                        </label>
                                     </div></div>
                                  </div>
                              </li>
                              <!-- Added by Prashant 23-11-2020 -->
                              
                               <li>
                               
                                 <div class="row">
                                    <div class="col-md-6">&nbsp;
                                  </div>
                                    <div class="col-md-6 text-right">
                                    <button type="submit" class="btn-style2 "><span>Update Preferences

                                  </span></button></div>
                                 </div>
 
                               </li>
                           </ul>
                           </form>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')

@endsection

 