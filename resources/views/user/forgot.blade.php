@extends('layouts.front')

@section('title' , 'Login')

@section('styles')
    <style>
.login-form-container input[type="email"]{
    width:100%;
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
}

.login-register-tab-list.nav a{
    width:100%;
}
    </style>
@endsection
@section('class', 'login-bg')
@section('content')


<div class="header-height"></div>
         <div class="login-register-area ptb-130 hm-3-padding">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-7 ml-auto mr-auto">
                     <div class="login-register-wrapper">

                     <div class="login-register-tab-list nav">
                           <a class="active">
                              <h4> Forgot password </h4>
                           </a>
                        </div>

                        <div class="tab-content">
                              <div class="login-form-container">
                                 <div class="">

                                 @include('includes.admin.form-login')
                                 <form id="forgotform" action="{{route('user-forgot-submit')}}" method="POST">
                                     @csrf
                                        <div class="form-group">
                                          <label for="username">Username or email address *</label>
                                          <input type="email" name="email" id="username" required>
                                       </div>
                                       <input class="authdata" type="hidden" value="{{ $langg->lang195 }}">
                                       <div class="button-box">
                                          <p class="text-center"> <button type="submit" class="btn-style2 cr-btn"><span>Submit</span></button></p>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

@endsection

@section('scripts')

@endsection