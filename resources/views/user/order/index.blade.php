@extends('layouts.front')
@section('title' , 'Order')
@section('content')



<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
              
						@include('includes.user-dashboard-sidebar')
               

                
                  <div class="col-md-8">
                     <div class="lux-enter mt-50">
                        
                        <div class="account-listing">
                           <div class="row border-bottom-gray">
                               <div class="col-md-6">Orders & Returns   </div>
                               <div class="col-md-6 text-right">{{ count($orders) }} Orders placed
                            </div>
                           </div>
                           <!-- <ul >
                              <li class="pb-120"> -->
                               
					@if(count($orders) == 0)
					<div class="row">
						<div class="col-md-12 text-center mt-80"><h4>You have not placed any orders</h4></div>
					</div>
					@else
					<div class="row">
					<div class="table-responsive">
						<table id="example" class="table table-hover dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<!--<th>#Order No</th>-->
									<th>{{ $langg->lang279 }}</th>
									<th>{{ $langg->lang280 }}</th>
									<th>{{ $langg->lang281 }}</th>
									<th>{{ $langg->lang282 }}</th>
								</tr>
							</thead>
							<tbody>
									@foreach($orders as $order)
								<tr>
									<!--<td>-->
									<!--		{{$order->order_number}}-->
									<!--</td>-->
									<td>
											{{date('d M Y',strtotime($order->created_at))}}
									</td>
									<td>
											{{$order->currency_sign}}{{ round($order->pay_amount * $order->currency_value , 2) }}
									</td>
									<td>
										<div class="order-status {{ $order->status }}">
												{{ucwords($order->status)}}
										</div>
									</td>
									<td>
										<a class="mybtn2 sm" href="{{route('user-order',$order->id)}}">
												{{ $langg->lang283 }}
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
									</div>
								</div>
								@endif

                              <!-- </li>                            
                           </ul> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')


@endsection