@extends('layouts.front')

@section('styles')
    <style>
.login-form-container select {
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
}
.alert {
    margin-bottom: 0px;
}
.box{
    border:3px solid #ab8e66;
    padding:10px 5px;
}
    </style>
@endsection
@section('content')

<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
             
                       @include('includes.user-dashboard-sidebar')
                    
                  <div class="col-md-8">
                     <div class="lux-enter mt-50">
                        
<div class="account-listing">
<div class="row">
<div class="col-md-12">
<div class="login-form-container login-form-container2  pt-0">
    <div class="login-form">
        <h3 class="border-bottom-gray">Manage Addresses
        </h3>
        <div class="col-md-10">
        <form id="userform" action="{{route('user-profile-update')}}" method="POST"
                                            enctype="multipart/form-data">
{{ csrf_field() }}
@include('includes.admin.form-both')
           <input type="hidden" name="from" value="address" />
            <div class="login-form-container login-form-container2">
            
            <div class="checkbox-form">						
                
                <!-- <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>You don't have any saved addresses</label>
                        <p>
                            <div class="cart-btn3 text-left mt-30 mb-15">
                                <a href="#">Add an address </a></a>
                                </div>
                            </p>									
                    </div>
                </div>
            </div> -->

                @if(Session::has('message'))
                    <div class="alert alert-success mb-3">
                            {{ Session::get('message') }}
                    </div>
                @endif             


                <div class="row">
                @foreach($addresses as $address)
                    <div class="col-md-4 box">
                    <div class="row">
                        <div class="col-md-12" id="address_{{$address->id}}">
                            <span id="country_{{$address->id}}">{{$address->country}}</span>,<br><span id="city_{{$address->id}}">{{$address->city}}</span>,<br><span id="address1_{{$address->id}}">{{$address->address_1}}</span><span id="address2_{{$address->id}}">{{$address->address_2}}</span>,<br><span id="postal_{{$address->id}}">{{$address->postal_code}}</span><span id="state_{{$address->id}}">{{$address->state}}</span>                    
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-md-4">
                                <a id="{{ $address->id }}" class="select_address" href="javascript:;"><i class="ti-plus"></i></a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('edit_address_form',$address->id) }}"><i class="ti-pencil"></i></a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('delete_address',$address->id) }}"><i class="ti-trash"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                
                <div class="row mt-20">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Country or region</label>
                            <select id="country" class="input-field" name="country">
                                    <option value="">{{ $langg->lang157 }}</option>                                    
                                    @foreach (DB::table('countries')->where(['status' => 1])->get() as $data)
                                        <option value="{{ $data->country_name }}" {{ $user->country == $data->country_name ? 'selected' : '' }}>
                                            {{ $data->country_name }}
                                    </option>		
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    </div>
                    
                
                     <div class="bill-new-address">
                  
                <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address</label>
                            <input id="address1" type="text" name="address" value="{{ $user->address }}" placeholder="{{ $langg->lang270 }}" />
                            <input id="address2" type="text" name="address2" placeholder="{{ $langg->lang270 }}" />
                        </div>
                    </div>
                    </div>
                    <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Own/City</label>
                            <input id="city" name="city" type="text" placeholder="{{ $langg->lang268 }}" value="{{ $user->city }}">
                        </div>
                    </div>
                    </div>
                    <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>state (option)</label>
                            <input id="state" name="state" type="text" placeholder="Enter State" value="{{ $user->state }}">
                        </div>
                    </div>
                    </div>
                    <div class="row ">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-12 no-padding ">Postal Code</label>
                            <input id="postal" name="zip" type="text" placeholder="{{ $langg->lang269 }}" value="{{ $user->zip }}">

                        </div>
                    </div>
                    </div>
                      
                      </div>
                      <div class="row"><div class="col-md-12 text-center text-md-right mb-3"><a href="{{ route('show_address_form') }}" target="_self" class="btn-style2">+ Add New Address</a></div></div>


                     <form>
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                          <label class="form-check-label" for="defaultCheck1">
                            Set as default delivery address
                          </label>
                        </div>
                      
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                          <label class="form-check-label" for="defaultCheck2">
                            Set as default billing address
                          </label>
                        </div>
                        
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                          <label class="form-check-label" for="defaultCheck3">
                            Set as default contact address
                          </label>
                        </div>
                    </form> 
                    <!--<p class="pl-10">Set as default delivery address</p>-->
                    <!--<p class="pl-10">Set as default billing address</p>-->
                    <!--<p class="pl-10">Set as default contact address</p>-->
                </div>	
                </div>
            
        
            <div class="col-md-7">
            <p class="mt-20neg">
                <button type="submit" class="btn-style2 wid100"><span>Save Address
                </span></button>
            </p>
            <!-- <p >
                <button type="submit" class="cancel-style2 wid100"><span>Cancel
                </span></button>
            </p> -->
            </div>
    </form>
                                 </div>
                              </div>
                                 </div>
                              </div>
                            </div>
                           </div>
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')


@endsection
@section('scripts')
    <script>
        $('.select_address').click(function(){

            var id = this.id;
            var country = $('#country_'+id).text();
            var city = $('#city_'+id).text();
            var address1 = $('#address1_'+id).text();
            var address2 = $('#address2_'+id).text();
            var state = $('#state_'+id).text();
            var postal = $('#postal_'+id).text();
           

           $("#country").val(country);
           $("#state").val(state);
           $("#city").val(city);
           $("#address1").val(address1);
           $("#address2").val(address2);
           $("#postal").val(postal);


        });
    </script>
@endsection 