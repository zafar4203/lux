@extends('layouts.front')

@section('styles')
    <style>
            #pswd-div i ,#confirm-pswd-div i ,
            #pswd-div1 i ,#confirm-pswd-div i ,
            #pswd-div2 i ,#confirm-pswd-div i {
            position:absolute;
            bottom:40px;
            right:25px;
            font-size:20px;
            cursor: pointer;
        }
        .login-form-container2{
            padding-top:5px !important;
        }

    </style>
@endsection
@section('content')




<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
               
                           @include('includes.user-dashboard-sidebar')
               
               
                  <div class="col-md-8">
                     <div class="lux-enter mt-50">
                        
                        <div class="account-listing">
                           <div class="row">
                              <div class="col-md-12">
                                <div class="login-form-container login-form-container2  pt-0">
                                    <div class="login-form">
                                        <h3 class="border-bottom-gray">Update Password

                                        </h3>
                                        <div class="col-md-10">
                    <form id="userform" action="{{route('user-reset-submit')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('includes.admin.form-both') 
                    <div class="login-form-container login-form-container2">                                            
                    <div class="checkbox-form">						
                    
                        
                        <div class="row mt-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Current Password </label>
                                    <input type="password" name="cpass"  class="input-field" placeholder="{{ $langg->lang273 }}" id="password" value="" required="">
                                </div>
                            </div>
                            </div>
                            
                            <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>New Password
                                </label>
                                <input type="password" name="newpass"  class="input-field" placeholder="{{ $langg->lang274 }}" id="password1" value="" required="">
                                </div>
                            </div>
                            </div>
                            <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Please repeat your new password
                                </label>
                                <input type="password" name="renewpass"  class="input-field" placeholder="{{ $langg->lang275 }}" id="password2" value="" required="">
                                </div>
                            </div>
                            </div>
                        
                        </div>	
                        </div>
                                                                    
                    </div>
                                         
                                      
                    <div class="col-md-12">
                    <p class="mt-20neg">
                        <button type="submit" class="btn-style2"><span>Update new password
                        </span></button>
                    </p>
                                         
                                          </div>
                                    </form>
                                 </div>
                              </div>
                                 </div>
                              </div>
                            </div>
                           </div>
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')


@endsection
@section('scripts')
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');    

        const togglePassword1 = document.querySelector('#togglePassword1');
        const password1 = document.querySelector('#password1');    

        const togglePassword2 = document.querySelector('#togglePassword2');
        const password2 = document.querySelector('#password2');    
        
        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });


        togglePassword1.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password1.getAttribute('type') === 'password' ? 'text' : 'password';
            password1.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });

        togglePassword2.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password2.getAttribute('type') === 'password' ? 'text' : 'password';
            password2.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });

    </script>
@endsection