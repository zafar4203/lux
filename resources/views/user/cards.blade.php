@extends('layouts.front')

@section('styles')
 
@endsection
@section('content')

<div class="header-height"></div>
         <!-- main-search start -->
         <div class="main-search-active">
            <div class="sidebar-search-icon">
               <button class="search-close"><span class="ti-close"></span></button>
            </div>
            <div class="sidebar-search-input">
               <form>
                  <div class="form-search">
                     <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                     <button>
                     <i class="ti-search"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
        
         <div class="mt-120" >
            <div class="container">
               <div class="row">
                
                       @include('includes.user-dashboard-sidebar')

                   
                  <div class="col-md-8">
                     <div class="lux-enter mt-50">
                        
                        <div class="account-listing">
                           <div class="row">
                              <div class="col-md-12">
                                <div class="login-form-container login-form-container2  pt-0">
                                    <div class="login-form">
                                        <h3 class="border-bottom-gray">Payment Cards
                                        </h3>
                                        <div class="col-md-9">
                                       <form action="#" method="post">
                                          <div class="login-form-container login-form-container2">
                                            
                                          <div class="checkbox-form">						
                                             
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                        <label>You don't have any card associated with your account
                                                      </label>
                                                        <p>
                                                            <div class="cart-btn3 text-left mt-30 mb-15">
                                                                <a href="#">Add a card
                                                               </a>                                                            </a>
                                                             </div>
                                                         </p>									
                                                    </div>
                                                </div>
                                            </div>
                                                
                                                <div class="row mt-40">
                                                    <div class="col-md-12">
                                                        <div class="checkout-form-list">
                                                            <label><span class="float-left">Card Number</span><span class="float-right"><img src="{{ asset('public/assets/front/img/icon-img/visa.png') }}"></span></span></label>
                                                            <input type="text" placeholder="">										
                                                        </div>
                                                    </div>
                                                   </div>
                                                   <div class="row no-gutters">
                                                    <div class="col-md-3">
                                                        <div class="country-select">
                                                            <label>Expiry Date</label>										
                                                           <select><option>MM</option></select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 text-center">  <label class="mt-30">/</label>	</div>
                                                    <div class="col-md-4">
                                                        <div class="country-select">
                                                            <label>&nbsp;</label>										
                                                            <select><option>YYYY</option></select>
                                                        </div>
                                                    </div>
                                                   </div>
                                                   <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="checkout-form-list">
                                                            <label>Number of Card</label>
                                                            <input type="text" placeholder="eg Johsn Bare">
                                                        </div>
                                                    </div>
                                                   </div>
                                                   <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="checkout-form-list">
                                                            <label>Security Code</label>
                                                            <input type="text" placeholder="Street address">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 pt-20"> card's <br/> CVV number</div>
                                                   </div>		
                                                <div class="row">
                                                   <div class="col-md-8">
                                                      <p>
                                                         <button type="submit" class="btn-style2 wid100"><span>Add Card
                                                         </span></button>
                                                      </p>
                                                      <p >
                                                         <button type="submit" class="cancel-style2 wid100"><span>Cancel
                                                         </span></button>
                                                      </p>
                                                      </div>
                                                </div>
                                                                                  
                                          
                                                </div>	
                                             </div>
                                                  
                                         
                                    </form>
                                 </div>
                              </div>
                                 </div>
                              </div>
                            </div>
                           </div>
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         
            @include('front.chunks.contacts')
            @include('front.chunks.footer')


@endsection

 