@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
<div class="header-height"></div>
     
         <div class="white-bg mt-120">
            <div class="container">
               <div class="product-cart-area hm-3-padding  pb-80 cart-content">
                  <div class="banner-area mt-0">
                     <div class="banner-img">
                        <a href="#"><img src="{{ asset('public/assets/images/charities') }}/{{ $charity->image_1 }}" alt=""></a>
                     </div>
                  </div>
                  <div class="row mt-40">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        {!! $charity->para_1 !!}
                     </div>
                  </div>
                  <div class="banner-area mt-0">
                     <div class="banner-img">
                        <a href="#"><img src="{{ asset('public/assets/images/charities') }}/{{ $charity->image_2 }}" alt=""></a>
                     </div>
                  </div>
                  <div class="row mt-40">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         {!! $charity->para_2 !!}
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class=" grey-bg-2">
         <div class="container">
            <div class="row pt-30 pb-30">
               <div class="col-md-6 text-left">
                  <h4>Get in Touch</h4>
                  <p>{{ $charity->get_in_touch_text }}</p>
               </div>
               <div class="col-md-3 text-left">
                  <h4>Call us now</h4>
                  <p>{{ $charity->call_us_now_text }}</p>
               </div>
               <div class="col-md-3 text-left">
                  <h4>Write us an email</h4>
                  <p>
                     {{ $charity->write_email_text }}
                  </p>
               </div>
            </div>
         </div>
           </div>

           @include('front.chunks.footer')
@endsection