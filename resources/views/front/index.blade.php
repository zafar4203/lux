@extends('layouts.front')
@section('title' , 'Home')
@section('styles')
<style>
.timer span span{
    margin: 0 0px;
}

.timer span {
    margin: 0 10px;
}
    </style>
@endsection
@section('content')


         <!-- banner start -->
         <div class="slider-area slider-1" style="position: relative;">
            <div class="banner-bg"></div>
          
         </div>
         <div class="white-bg">
            <div class="banner-area hm1-banner watch-bg mt-200">
               <div class="container">
                   
                  <div class="row  pb-0">
                     <div class="col-md-12 col-lg-6">
                        <div class="banner-wrapper mrg-mb-md neg-top-120">
                           <div class="banner-img" data-toggle="modal" data-target="#exampleModal" href="#">
                              <img src="{{ asset('public/assets/front/img/product/video-poster.png') }}" alt="">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-lg-1"></div>
                     <div class="col-md-12 col-lg-4 pt-5 pb-80">
                        @foreach($campaigns as $campaign)
                        @if($campaign->type == 0)

                        <div class="product-wrapper white-box">
                            <h6 class="brown-text2">WIN</h6>
                           <a href="{{ route('front.campaign') }}">
                           <div class="product-img">
                                @php $images = json_decode($campaign->prize_images); @endphp
                              <img src="{{asset('public/assets/images/campaigns/'.$images[0])}}" alt="">
                           </div>
                           </a>
                           <div class="product-content text-center">
                              <h4><a href="{{ route('front.campaign') }}">{{ $campaign->prize_title }}</a></h4>
                              <div class="product-price">
                                 <span>{{$curr->sign}} {{ number_format($campaign->prize_worth) }}</span>
                              </div>
                           </div>
                        </div>
                        @endif
                        @endforeach

                     </div>
                  </div>
               </div>
            </div>
            <div class="product-area pb-80 pt-80">
               <div class="container">
                   <div class="section-title text-center detail-text">
                     <h2>Handcrafted Elegance</h2>
                     <p class="mb-0">The perfect addition for any watch enthusiast looking to display their timepieces in style</p>
                  </div>
                  <div class="tab-content">
                     <div class="row">
                        <div class="col-md-12">
                        <div class="owl-slider">
                           <div id="carousel" class="owl-carousel">
                              @foreach($products as $product)
                              <div class="item">
                                 <div class="product-wrapper white-box">
                                    <!--<p class="pt-10 text-center brown-text"><a href="#" ><span></span>New Brand</a></p> Commented by Prashant as per client requirements -->
                                    <div class="star-top">
                                    @php $check=0; @endphp
                                    @foreach($wishs as $wishlist)
                                        @if($wishlist->product_id == $product->id)
                                          @php $check=1; @endphp
                                        @endif
                                    @endforeach
                                      <a href="javascript:;" class="add-to-wish" id="{{ $product->id }}" data-href="{{ route('user-wishlist-add',$product->id) }}"><i class="wish_{{ $product->id }} @if($check == 0) ion-ios-star-outline @else ion-ios-star @endif"></i></a>
                                    </div>
                                    <a href="{{ route('front.product',$product->slug) }}">
                                    <div class="product-img">
                                       <img style="padding: 10px 10px; width:370px; height:421px" src="{{ $product->photo ? asset('public/assets/images/products/'.$product->photo):asset('public/assets/images/noimage.png') }}" alt="">

                                    </div>
                                    </a>
                                    <div class="product-content text-center">
                                       <h4><a href="{{ route('front.product',$product->slug) }}">{{  $product->showName()  }}</a></h4>
                                       <h4><a href="{{ route('front.product',$product->slug) }}">{{ $product->category->name }}</a></h4>
                                       <div class="product-price">
                                          <span>{{ $product->setCurrency() }}</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                           </div>
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="new-collection-area pb-90">
               <div class="container">
                  <div class="section-title text-center mb-55 detail-text">
                     <!--<h2>{{ $gs->home_campaign_heading }}</h2> Commented by Prashant-->
                     <h2>Win Your Dream Watch</h2>
                     <div class="col-md-12">
                        <!--<p>{{ $gs->home_campaign_desc }}</p> Commented by Prashant-->
                        <p>We’re giving our valued customers the chance to win a brand new luxury watch every month.</p>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-8">
                        <div class="row no-gutters">
                           <div class="col-md-6">
                              <div class="time-piece">
                                 <!--<p>{{ $gs->home_prize_given_away }}</p> temporary commented by Prashant - 12-11-2020 -->
                                 <p>Could you be our first winner?</p>
                              </div>
                           </div>
                        </div>
                        <div class="owl-slider">
                           <div id="carousel2" class="owl-carousel">
                              
                              @foreach($campaigns as $campaign)
                              @if($campaign->type == 0 || $campaign->type == 1)
                              <div class="item">
                                 <div class="product-wrapper white-box">
                                    <!-- <div class="star-top">
                                       <i class="ion-ios-star-outline"></i>
                                    </div> -->
                                    <a href="{{ route('front.campaign') }}">
                                    <div class="product-img">
                                       @php $images = json_decode($campaign->prize_images); @endphp
                                       <img src="{{asset('public/assets/images/campaigns/'.$images[0]) }}" alt="">
                                    </div>
                                    </a>
                                    <div class="product-content text-center">
                                       <h4><a href="{{ route('front.campaign') }}">{{ $campaign->product_title }}</a></h4>
                                       {{-- <h4><a href="{{ route('front.campaign') }}">{{ $campaign->product->category->name }}</a></h4> --}}
                                       <div class="product-price">
                                          <span>{{$curr->sign}} {{ number_format($campaign->prize_worth) }}</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endif
                              @endforeach
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-12 ">
                              <div class="draw-time">
                                 <div class="time-box">
                                    <h3 class="text-center">Draw Time</h3>
                                    <div class="row no-gutters pt-2 justify-content-center">
                                       <div id="timer" class="timer">
                                          <div>
                                             <span class="cdown day">
                                               <span id="days"></span> 
                                                <p>Days</p>
                                             </span>
                                             <span class="cdown hour">
                                                <span id="hours"></span> 
                                                <p>Hour</p>
                                             </span>
                                             <span class="cdown minutes">
                                                <span id="minutes"></span> 
                                                <p>Min</p>
                                             </span>
                                             <span>
                                                <span id="seconds"></span> 
                                                <p>Sec</p>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    @foreach($campaigns as $campaign)
                                    @if($campaign->type == 0 )
                                    <input type="hidden" id="draw_date" value="{{ Carbon\Carbon::parse($campaign->draw_date)->format('d M Y') }}">
                                    <!--<p>{{ $campaign->campaign_description }}</p> Temporary commented by Prashant-->
                                    <p class="text-center">Don’t miss out on your chance to win a luxury watch!</p>
                                    <h3 class="text-center">Next Competition</h3>
                                    @endif
                                    @endforeach
                                    <div class="row no-gutters pt-2 justify-content-center">
                                       <div class="timer">
                                          <div>
                                             <span class="cdown day">
                                             <span id="cdays"></span> 
                                                <p>Days</p>
                                             </span>
                                             <span class="cdown hour">
                                             <span id="chours"></span> 
                                                <p>Hour</p>
                                             </span>
                                             <span class="cdown minutes">
                                             <span id="cminutes"></span> 
                                                <p>Min</p>
                                             </span>
                                             <span>
                                             <span id="cseconds"></span> 
                                                <p>Sec</p>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="gray-bg pt-80 pb-80 watch-container">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6">
                        <div class="dealy-product-img wow fadeInLeft ">
                           <img src="{{ asset('public/assets/images/home/'.$home->image_1) }}" alt="image" class="img-fluid" >
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 dealy-product-content-left text-center  d-flex flex-column align-items-center justify-content-center"> <!-- changed col-lg-5 to col-lg-6 and added classes after text-center by Prashant - 12-11-2020 -->
                        <!--<h2>{{ $home->para_1_heading }}</h2> Temporary commented by Prashant-->
                        <!--<p>{!! $home->para_1 !!}</p> Temporary commented by Prashant-->
                        <div class="section-title text-center mb-3 detail-text"><!-- Added by Prashant-->
                            <h2>{{ $home->para_1_heading }}</h2>
                        </div>
                        <p class="text-center">{!! $home->para_1 !!}.</p> <!-- Added by Prashant-->
                        <a href="{{ route('front.charity') }}">Find out More</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="ads-area pt-85 pb-45 ads-padding hm-ads">
               <div class="container">
                  <div class="row no-gutters">
                     <div class="col-lg-6 col-md-6 ">
                        <div class="hm-wrapper mb-40">
                           <div class="ads-img">
                              <a href="#"><img src="{{ asset('public/assets/front/img/banner/watch-ads.jpg') }}"  class="img-fluid"  alt="image"></a>
                           </div>
                          
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6">
                        <div class="hm-wrapper mb-40 mt-140">
                           <div class="ads-img">
                              <a href="#"><img src="{{ asset('public/assets/front/img/banner/watch-ads2.jpg') }}"  class="img-fluid"  alt="image"></a>
                           </div>
                          
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class=" watch-container pt-80 pb-80 custom-res-pt-80"> <!-- pt-80 pb-80 added and removed mt-3, mb-40 by Prashant-->
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 dealy-product-content-left text-center d-flex flex-column align-items-center justify-content-center"> <!--  mt-130 removed and added classes after text-center by Prashant - 12-11-2020 -->
                     <!--<h2>{{ $home->para_2_heading }}</h2> Temporary commented by Prashant-->
                        <!--<p>{!! $home->para_2 !!}</p> Temporary commented by Prashant-->
                        <div class="section-title text-center mb-3 detail-text"><!-- Added by Prashant-->
                            <h2>{{ $home->para_2_heading }}</h2>
                        </div>
                        <p class="text-center">{!! $home->para_2 !!}</p> <!-- Added by Prashant-->
                        <!--<a href="{{ $home->para_2_find_out_more }}">Find out More</a>-->
                     </div>
                     <div class="col-lg-6 col-md-6">
                        <div class="dealy-product-img wow fadeInLeft">
                           <img src="{{ asset('public/assets/images/home/'.$home->image_2) }}" alt="image" class="img-fluid">
                        </div>
                     </div>
                  </div>
               </div>
            </div>


            @include('front.chunks.footer')
         </div>


         <!-- modal -->
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="ion-android-close" aria-hidden="true"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">
                  <div class="modal-body">
                     <div class="video_wrapper video_wrapper_full js-videoWrapper">
                     <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="{{ $home->video_url }}"></iframe>
                      </div>                       
                     </div>
                  </div>
               </div>
            </div>
         </div>

@endsection
@section('scripts')
<script>
     $( window ).load(function() {
      
           $('#hero-mask text').css({'font-size':'8.5rem', 'opacity': '1', 'transform': 'translateY(0rem)'});
 });   
// Auto Complete Section
  $('#prod_name').on('keyup',function(){
     var search = encodeURIComponent($(this).val());
      if(search == ""){
        $(".autocomplete").hide();
      }
      else{
        $(".autocomplete").show();
        $("#myInputautocomplete-list").load(mainurl+'/autosearch/product/'+search);

      }
    });
// Auto Complete Section Ends

$(document).on('click', function(e) {
    var container = $(".autocomplete-items");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
    $(".autocomplete").hide();
    }
});


// Time Reverse Counter

function makeTimer() {

	//		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
		var endTime = new Date($('#draw_date').val());			
			endTime = (Date.parse(endTime) / 1000);

			var now = new Date();
			now = (Date.parse(now) / 1000);

			var timeLeft = endTime - now;

			var days = Math.floor(timeLeft / 86400); 
			var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
			var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
			var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
			if (hours < "10") { hours = "0" + hours; }
			if (minutes < "10") { minutes = "0" + minutes; }
			if (seconds < "10") { seconds = "0" + seconds; }

			$("#days").html(days);
			$("#hours").html(hours);
			$("#minutes").html(minutes);
			$("#seconds").html(seconds);		

	}


function makeTimerComing() {

//		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	

  var endTime = new Date($('#draw_date').val());			
    endTime.setDate(endTime.getDate() + 3);   
    endTime = (Date.parse(endTime) / 1000);


    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var days = Math.floor(timeLeft / 86400); 
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    $("#cdays").html(days);
    $("#chours").html(hours);
    $("#cminutes").html(minutes);
    $("#cseconds").html(seconds);		

}

	setInterval(function() { makeTimer(); }, 1000);
  setInterval(function() { makeTimerComing(); }, 1000);
  
  $(window).on('scroll', function (event) {
        var scroll = $(window).scrollTop();
        if (scroll > 350) {

           $('#hero-mask text').css({'font-size':(($(this).scrollTop()*0)+0)+'rem', 'transform': 'translateY(-14rem)', 'opacity': '0'});
        } else {
         $('#hero-mask text').css({'font-size':(($(this).scrollTop()*.000)+8.5)+'rem','transform': 'translateY(-0.5rem)', 'opacity': '1'});
        }
    });

    $('#exampleModal').on('shown.bs.modal', function () {
  $('#video')[0].play();
})
$('#exampleModal').on('hidden.bs.modal', function () {
  $('#video')[0].pause();
})
</script>
@endsection