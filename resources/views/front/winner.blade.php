
@extends('layouts.front')
@section('styles')
   
@endsection
@section('content')
<div class="header-height"></div>


<div class=" mt-120">
<div class="product-cart-area hm-3-padding cart-content">
   <div class="container white-box">
      
         <h1 class="pt-40">
            Winners
         </h1>
     
         <div class="d-flex flex-wrap pb-40">
         @foreach($winners as $winner)
            <div class="col-md-6 winner-box">

               <div class="row">
                  <div class="col-md-6 winner-img">
                     <div class="img-thumb">
                        <img src="{{ asset('public/assets/front/img/team/winner-1.png') }}" alt="">
                     </div>
                  </div>
                  <div class="col-md-6 gift-box"> 
                     <div class="gift-img">    
                        @php $images = json_decode($winner->campaign->prize_images) @endphp
                         <img src="{{ asset('public/assets/images/campaigns')}}/{{$images[0]}}" alt="">
                     </div>
                     <p class="small-black-title">{{ $winner->campaign->prize_name }}</p>
                     </div>
               </div>
               <div class="row mt-20">
                  <div class="col-md-12">
                     <h3>Winner of the</h3>
                     <h2>{{ $winner->campaign->name }}</h2>
                    <hr/>
                     <h4>{{ $winner->user->name }}</h4>
                     <h5>{{ $winner->user->country }}</h5>
                     <p class="small-black-title">Campaign no. #{{ $winner->campaign->id }}
                     </p>
                     <p>{{ $winner->winner_comment }}
                     </p>
                     <div class="update-cart mt-20 justify-content-center ml-0">
                        <a class="btn-style cancel-style" href="#">
                        <span>Watch Draw</span>
                        </a>
                     </div>
                  </div>
               </div>

              
            </div>
            @endforeach

        </div>


   </div>
</div>
<div class="enter-competition text-center pb-30">
<div class="container">
 <div class="white-height">&nbsp;</div>
 <div class="col-md-12">
<div class="update-cart mt-20">
   <a class="btn-style white-style" href="#">
   <span>Enter Competition</span>
   </a>
</div>
</div>
</div>
</div>

@endsection