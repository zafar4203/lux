@extends('layouts.front')

@section('content')

            <!-- Breadcrumb Area start -->
            <section class="breadcrumb-area mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="breadcrumb-content">
<!--                                <h1 class="breadcrumb-hrading">Compare Page</h1>-->
                                <ul class="breadcrumb-links">
                                    <li><a href="index.html">Home</a></li>
                                    <li>Compare</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Breadcrumb Area End -->

	<!-- Compare Area Start -->
	<section class="compare-page">


		            <!-- Compare area start -->
		<div class="compare-area mtb-60px">
			<div class="container">
				<div class="row">
					@if(isset($products))
                        <div class="col-12">
                            <form action="#">
                                <!-- Compare Table -->
                                <div class="compare-table table-responsive">
                                    <table class="table mb-0">
                                        <tbody>
	<tr>
		<td class="first-column">Product</td>
		@foreach($products as $product)
		<td class="product-image-title">
			<a href="compare.html#" class="image">
				<img class="img-fluid" src="{{ $product['item']['thumbnail'] ? asset('public/assets/images/thumbnails/'.$product['item']['thumbnail']):asset('assets/images/noimage.png') }}" alt="Compare product['item']">
			</a>
			@php
				$prod = \App\models\Product::query()->where("id",$product['item']['id'])->first();
			@endphp
			<a href="compare.html#" class="category">{{ $prod->category->name }}</a>
			<a href="{{ route('front.product', $product['item']['slug']) }}" class="title">{{ $product['item']['name'] }}</a>
		</td>
		@endforeach
	</tr>
	<tr>
		<td class="first-column">Description</td>
		@foreach($products as $product)
		<td class="pro-desc">
			@php
				$prod = \App\models\Product::query()->where("id",$product['item']['id'])->first();
			@endphp

			<p>{{ $prod->details }}</p>
		</td>
		@endforeach
	</tr>
	<tr>
		<td class="first-column">Price</td>
		@foreach($products as $product)
			@php
				$prod = \App\models\Product::query()->where("id",$product['item']['id'])->first();
			@endphp

		<td class="pro-price">{{ $prod->showPrice() }}</td>
		@endforeach
	</tr>
	<tr>
		<td class="first-column">Color</td>
		@foreach($products as $product)
		<td class="pro-color">
			@if(!empty($product->color))
			
					@foreach($product->color as $key => $data1)
						<li>
							<span class="box"  data-color="{{ $product->color[$key] }}" style="background-color: {{ $product->color[$key] }}"></span>
						</li>
						@php
							$is_first = false;
						@endphp
					@endforeach
			@else
				No Color
			@endif
		</td>
		@endforeach
	</tr>
	<tr>
		<td class="first-column">Stock</td>
		@foreach( $products as $product)
			<td class="pro-stock">In Stock</td>
		@endforeach
	</tr>
	<tr>
		<td class="first-column">Add to cart</td>
		@foreach( $products as $product)
		<td class="pro-addtocart">
			<a href="javascript:;" data-href="{{ route('product.cart.add',$product['item']['id']) }}" class="btn__bg add-to-cart">{{ $langg->lang75 }}</a>
		</td>
		@endforeach
	</tr>
	<tr>
		<td class="first-column">Delete</td>
		@foreach($products as $product)
		<td class="pro-remove c{{$product['item']['id']}}">
			<button><i class="ion-trash-b compare-remove" data-href="{{ route('product.compare.remove',$product['item']['id']) }}" data-class="c{{$product['item']['id']}}"></i></button>
		</td>
		@endforeach

	</tr>
	<tr>
		<td class="first-column">Rating</td>
		@foreach($products as $product)
		<td class="pro-ratting">
			<i class="ion-android-star"></i>
			<i class="ion-android-star"></i>
			<i class="ion-android-star"></i>
			<i class="ion-android-star"></i>
			<i class="ion-android-star-half"></i>
		</td>
		@endforeach
	</tr>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
						@else
						<div class="col-md-12 mt-5 mb-5 pt-5 pb-b">
							<h1 class="text-center">Empty</h1>
						</div>
						@endif
                    </div>
                </div>
            </div>
            <!-- Compare area end -->

	</section>
	<!-- Compare Area End -->

@endsection