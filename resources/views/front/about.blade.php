@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
      
         <!-- banner start -->
         
         
             <div class="header-height"></div>
         <div class="slider-area slider-2 ">
            <div class="container">
      <div class="row">
        <div class="col-lg-12 mb-20">
          <div class="about-info">
            <h1 class="title">
            About Us
            </h1>
           </div>
        </div>
      </div>
    </div>
    </div>
         <div class="white-bg">
            <div class="container">
            
               <div class="row mb-40">
                  <div class="col-md-6">
                     <a href="#"><img src="{{ asset('public/assets/images/about') }}/{{ $about->image }}" class="img-fluid" alt=""></a>
                     </div>
                      <div class="col-md-6 pt-md-0 pt-3">
                     
                      <p>{{ $about->main_para }}</p> 
                     </div>
                  </div>
                  <h3 class="text-center pt-30 pb-40"> {{ $about->main_heading }}</h3>
                  <div class="row mb-md-4">
                   <div class="col-md-6"><h4>{{ $about->heading_1 }}</h4><p>{{ $about->para_1 }}</p></div>
                  
                   <div class="col-md-6"><h4>{{ $about->heading_2 }}</h4><p>{{ $about->para_2 }}</p></div>
                  </div>
<div class="row  mb-md-5 mb-4">
                    <div class="col-md-6"><h4>{{ $about->heading_3 }}</h4><p>{{ $about->para_3 }}</p></div>
                  
                    <div class="col-md-6"><h4>{{ $about->heading_4 }}</h4><p>{{ $about->para_4 }}</p></div>
                  </div>
               </div>
            </div>

@include('front.chunks.footer')
@endsection