@extends('layouts.front')
@section('title' , 'Product')

@section('styles')
    <style>
.custom-control-label {
    font-size: 15px;
}
.custom-control-input:checked~.custom-control-label::before {
    color: #fff;
    border-color: #fe4c50;
    background-color: #fe4c50;
}
.custom-control-input {
    position: absolute;
    left: 0;
    z-index: -1;
    width: 1rem;
    height: 1.25rem;
    opacity: 0;
}
.share-icons a{
        margin-right:10px;
}
.product-overview {
    margin: 10px 0;
}
    </style>
@endsection

@section('content')
        
         <!-- banner start -->
         <div class="slider-area slider-2 product-details-page">
            <div class="product-details-area hm-3-padding ptb-130 pb-0">
               <div class="container-fluid">
                   <div class="row">
                       <div class="col-lg-6">
                           <div class="product-details-img-content">
                               <div class="product-details-tab mr-40">
                                   <div class="product-details-large tab-content">
                                       <!--<div class="tab-pane active" id="pro-details1">-->
                                       <!--    <div class="product-img">-->
                                       <!--        <a href="assets/img/product/pin2.png">-->
                                       <!--            <img src="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('public/assets/images/products/'.$productt->photo)}}" alt="">-->
                                       <!--        </a>-->
                                       <!--    </div>-->
                                       <!--</div>-->
                                       
                                       <!-- Added by Prashant 23-11-2020 -->
                                       <div class="xzoom-container" id="product-dec-slider2">
                                           <img class="xzoom" id="xzoom-default" src="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('public/assets/images/products/'.$productt->photo)}}" xoriginal="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('public/assets/images/products/'.$productt->photo)}}" alt="">
                                          <!--<img class="xzoom" id="xzoom-default" src="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/preview/01_b_car.jpg" xoriginal="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/original/01_b_car.jpg" />-->
                                          <div class="imga">
                                          <div id="galleries_images" class="xzoom-thumbs product-details-xzoom-thumbs mt-12 product-dec-slider2 owl-carousel"> <!-- removed classes for checking " product-details-small nav " by Prashant 22-11-2020 -->
                                                <a href="{{asset('public/assets/images/products/'.$productt->photo)}}">
                                                    <img class="xzoom-gallery" src="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('public/assets/images/products/'.$productt->photo)}}" alt="">
                                                </a>

                                            @foreach($productt->galleries as $gal)
                                                <a href="{{asset('public/assets/images/galleries/'.$gal->photo)}}">
                                                    <img class="xzoom-gallery" src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" alt="">
                                                </a>
                                            @endforeach
                                          </div>
                                            
                                            <!--<a href="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/original/01_b_car.jpg"><img class="xzoom-gallery" width="80" src="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/thumbs/01_b_car.jpg"  xpreview="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/preview/01_b_car.jpg" title="The description goes here"></a>-->
                                              
                                            <!--<a href="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/original/02_o_car.jpg"><img class="xzoom-gallery" width="80" src="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/preview/02_o_car.jpg" title="The description goes here"></a>-->
                                              
                                            <!--<a href="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/original/03_r_car.jpg"><img class="xzoom-gallery" width="80" src="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/preview/03_r_car.jpg" title="The description goes here"></a>-->
                                              
                                            <!--<a href="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/original/04_g_car.jpg"><img class="xzoom-gallery" width="80" src="http://www.jqueryscript.net/demo/Feature-rich-Product-Gallery-With-Image-Zoom-xZoom/images/gallery/preview/04_g_car.jpg" title="The description goes here"></a>-->
                                          </div>
                                        </div>
                                    <!-- Added by Prashant 23-11-2020 -->
                                      
                                   </div>
                                   
                                <!-- Commented by Prashant 23-11-2020 -->
                                    <!--<div class="product-details-small nav mt-12 product-dec-slider owl-carousel">-->
                        
                                    <!--    @foreach($productt->galleries as $gal)-->
                                    <!--    <a href="pt-{{$productt->id}}">-->
                                    <!--        <img src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" alt="">-->
                                    <!--    </a>-->
                                    <!--    @endforeach-->
                                    <!--</div>-->
                                <!-- Commented by Prashant 23-11-2020 -->
                               </div>
                           </div>
                       </div>
                       <div class="col-lg-6">
                           <div class="product-details-content padding-box60 pb-0">
                               <h2>{{ $productt->name }}</h2>
                               <p>{{ $productt->short_desc }}</p>
                               <div class="product-price">
                                      @if($productt->previous_price)
                                      <span style="color:red"><strike><small>Was {{$productt->showPreviousPrice()}}</small></strike></span><br>
                                     @endif
                                   <span>{{$productt->showPrice()}}</span>
                               </div>




                               <input type="hidden" id="product_price" value="{{ round($productt->vendorPrice() * $curr->value,2) }}">

<input type="hidden" id="product_id" value="{{ $productt->id }}">
<input type="hidden" id="curr_pos" value="{{ $gs->currency_format }}">
<input type="hidden" id="curr_sign" value="{{ $curr->sign }}">
<div class="info-meta-3">
  <!--<ul class="meta-list">-->
    @if (!empty($productt->attributes))
      @php
        $attrArr = json_decode($productt->attributes, true);
      @endphp
    @endif
    @if (!empty($attrArr))
      <div class="product-attributes"> <!-- my-4 removed class by Prashant 23-11-2020 -->
        <div class="row">
        @foreach ($attrArr as $attrKey => $attrVal)
            @if($attrKey == 'colour')
            <div class="col-6">
                <div class="product-overview">
                <input type="hidden" class="keys" value="">
                <input type="hidden" class="values" value="">
                <p>Colour</p>
                <div class="custom-select-div d-inline-block">
                    <div class="custom-select-arrow"><i class="ion-ios-arrow-down"></i></div>
                    <select name="{{ $attrKey }}" class="color-change form-control">
                        
                    @php $i = 0; @endphp
                    @foreach ($attrVal['values'] as $optionKey => $optionVal)
    
                            @if (!empty($attrVal['prices'][$optionKey]))
                                +
                                {{$curr->sign}} {{$attrVal['prices'][$optionKey] * $curr->value}}
                            @endif
                                <!--<p>White Gold</p>-->
                                @if($i == 0) 
                                <option selected id="{{$attrKey}}{{ $optionKey }}" class="product-attr" data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}" data-value="default" value="{{ $optionVal }}">Default</option>
                                @endif
                                <option id="{{$attrKey}}{{ $optionKey }}" class="product-attr"  data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}" data-value="{{ $optionVal }}" value="{{ $optionVal }}">{{ $optionVal }}</option>
                    @php $i++; @endphp
                    @endforeach
                    </select>
                </div>
                </div>
        
                @foreach ($attrVal['images'] as $optionKey => $optionVal)
                    <input type="hidden" id="color_{{$attrKey}}{{ $optionKey }}" value=" @php echo json_encode($optionVal); @endphp" />
                @endforeach
            </div>
            @endif

          @if($attrKey != 'colour')
          @if (array_key_exists("details_status",$attrVal) && $attrVal['details_status'] == 1)
            <div class="col-6">
                <div class="form-group product-overview"><!-- mb-2 removed class by Prashant 23-11-2020 -->
                <input type="hidden" class="keys" value="">
                <input type="hidden" class="values" value="">
                <p for="" class="text-capitalize mb-0">{{ str_replace("_", " ", $attrKey) }} :</p>
                <div class="custom-select-div d-inline-block">
                    <div class="custom-select-arrow"><i class="ion-ios-arrow-down"></i></div>
                <select name="{{ $attrKey }}" class="form-control">
                @foreach ($attrVal['values'] as $optionKey => $optionVal)
                    @if (!empty($attrVal['prices'][$optionKey]))
                        +
                        {{$curr->sign}} {{$attrVal['prices'][$optionKey] * $curr->value}}
                    @endif
                    <option id="{{$attrKey}}{{ $optionKey }}" class="product-attr"  data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}" value="{{ $optionVal }}">{{ $optionVal }}</option>

                @endforeach
                </select>
                </div>
                </div>
            </div>
          @endif
          @endif
        @endforeach
        </div>
    @endif


<div class="row">
                               <div class="col-6">
                                   <div class="product-overview">
                                       <p class="mb-0">Quantity</p>
                               
                               <div class="quickview-plus-minus">
                             
                                   <!-- <div class="cart-plus-minus ">
                                       <input type="text" value="02" name="qtybutton" min="0" max="10" class="cart-plus-minus-box">
                                   </div> -->
                                    <div class="custom-select-div d-inline-block">
                                        <div class="custom-select-arrow"><i class="ion-ios-arrow-down"></i></div>
                                        <select class="qttotal form-control" id="select-product-quantity">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                       </select>
                                    </div>
                               </div>
                                   </div>
                               </div>
                               </div>
      </div>
                               <div class="product-categories d-flex flex-column">
                                 <a class="btn-style cr-btn" href="javascript:;" id="addcrt"><span>Add to cart</span></a>
                                 <a class="btn-style cancel-style add-to-wish" data-href="{{ route('user-wishlist-add',$productt->id) }}" href="javascript:;"><span>Wishlist</span></a>
                                  
                               </div>
                             
                               <h6>Share On</h6>
<div class="share-icons">
@php $url = route('front.product',$productt->slug); @endphp
<!-- Facebook -->
<a href="http://www.facebook.com/sharer.php?u={{$url}}" target="_blank"><i class="social ti-facebook"></i></a>

<!-- Twitter -->
<a href="http://twitter.com/share?url={{$url}}&text=Simple Share Buttons&hashtags=simplesharebuttons" target="_blank"><i class="social ti-twitter"></i></a>

<!-- Google+ -->
<a href="https://plus.google.com/share?url={{$url}}" target="_blank"><i class="social ti-google"></i></a>

<!-- Digg -->
<!-- <a href="http://www.digg.com/submit?url={{$url}}" target="_blank">Digg</a> -->

<!-- Reddit -->
<!-- <a href="http://reddit.com/submit?url={{$url}}&title=Simple Share Buttons" target="_blank">Reddit</a> -->

<!-- LinkedIn -->
<a href="http://www.linkedin.com/shareArticle?mini=true&url={{$url}}" target="_blank"><i class="social ti-linkedin"></i></a>

<!-- Pinterest -->
<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="social ti-pinterest"></i></a>

<!-- StumbleUpon-->
<!-- <a href="http://www.stumbleupon.com/submit?url={{$url}}&title=Simple Share Buttons" target="_blank">StumbleUpon</a> -->

<!-- Email -->
<a href="mailto:?Subject=Simple Share Buttons&Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{$url}}"><i class="social ti-envelope"></i></a>
</div>

                           </div>
                       </div>
                   </div>
               </div>
           </div>
           
         </div>
         </div>
         <div class="white-bg pt-5 pt-md-0">
            <div class="watch-container">
               <div class="container">
                  
                  <div class="d-flex flex-column flex-md-row">
                      <div class="col-md-8">
                          <div class="row">
                             <div class="col-lg-10 dealy-product-content-left"> <!--  mt-80 removed by Prashant -->
                                 <h3>Product Description</h3>
                               <p class="">{!! $productt->details !!}</p>
                               <!--<p class="big-italic-font mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
                             </div>
                             <div class="col-md-10">
                                <div class="row">
                                   <div class="col-md-10">
                                   {!! $productt->policy !!}
                                   <!--<ol>
                                       <li>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                       <li>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                   </ol>-->
                                 </div>
                              </div>
                           </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <!--<div class="row">-->
                    
                   <!--      <div class="col-md-8">-->
                   <!--         <div class="row">-->
                   <!--            <div class="col-md-10">-->
                   <!--            <ol>-->
                   <!--                <li>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>-->
                   <!--                <li>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>-->
                   <!--            </ol>-->
                   <!--      </div>-->
                   <!--   </div>-->
                   <!--</div>-->
                         <!--<div class="col-md-4">-->
                            <!--<div class="banner-wrapper mrg-mb-md">-->
                            <!--   <div class="address-box height400" >-->
                            <!--      <h5 class="border-botton-white">Details</h5>-->
                                 
                            <!--      <ul class="quick-link">-->
                            <!--        {!! $productt->side_details !!}-->
                            <!--      </ul>-->
                              
                            <!--   </div>-->
                            <!--</div>-->
                            
                            <div class="custom-dark-lux-box luxwatch-box mt-150 mt-sm-30">
                               <div class="big-title-lux">Delivery & <br/> Returns</div>
                               <p class="mt-120 link-visit"><a href="{{ route('front.page','delivery') }}" class="">Find Out More </a></p>
                            </div>
                         <!--</div>-->
                      <!--</div>-->
                      </div>
                  </div>
                  <!--<p><span class="gray-small-text">Editor's Note</span></p>-->
                  
                 
               </div>
            </div>
            <!-- <div class="banner-area  contact-footer-bg height600 neg-top-40">
               <div class="container white-gold-box pt-100">
                  <div class="row">
                  <div class="col-md-12">
                  <h5 class="border-botton-white pb-20">Materials</h5>
                              <h2>{{ $productt->materials }}</h2>
                            </div>
                            </div>
                            <div class="row mt-120">
                            <div class="col-md-6">&nbsp;</div>
                            <div class="col-md-6 "> <div class="footer-white-box">
                                <h2>Bella Diamond</h2>
                                <p>
                                    <a href="#">Show All Bella Diamond</a>
                                </p>
                            </div></div>
                            </div>
                            
                  </div>
                 
            </div> -->
            <div class="product-area pt-80 pb-80">
               <div class="container">
                  <div class="login-register-wrapper">
                     <div class="resent-tab-tab-list nav">
                         <a class="active border-right-black" data-toggle="tab" href="#lg6">
                             <h4>  You may also like </h4>
                         </a>
                         <a data-toggle="tab" href="#lg7">
                             <h4> Recently Viewed </h4>
                         </a>
                     </div>
                     <div class="tab-content">
                         <div id="lg6" class="tab-pane active">
                            <div class="col-md-12 mt-50">
                              <div class="owl-slider">
                                 <div id="carousel3" class="owl-carousel">
                                    @foreach($prods as $prod)
                                    <div class="item">
                                       <div class="product-wrapper white-box">
                                          <p class="pt-10 text-center brown-text"><a href="#" >&nbsp;</a></p>
                                            <div class="star-top">
                                            @php $check=0; @endphp
                                            @foreach($wishs as $wishlist)
                                                @if($wishlist->product_id == $prod->id)
                                                @php $check=1; @endphp
                                                @endif
                                            @endforeach
                                           <a href="javascript:;" class="add-to-wish" id="{{ $prod->id }}" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($check == 0) ion-ios-star-outline @else ion-ios-star @endif"></i></a>

                                            </div>
                                          <div class="product-img">
                                              <a href="{{ route('front.product',$prod->slug) }}">
                                                <img src="{{filter_var($prod->photo, FILTER_VALIDATE_URL) ?$prod->photo:asset('public/assets/images/products/'.$prod->photo)}}" alt="">
                                              </a>
                                          </div>
                                          <div class="product-content text-center">
                                            <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName() }}</a></h4>
                                            <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->category->name }}</a></h4>
                                            <div class="product-price">
                                                <span>{{ $prod->setCurrency() }}</span>
                                            </div>
                                          </div>
                                       </div>
                                    </div>
                                @endforeach
                                 </div>
                              </div>

                            </div>
                         </div>
                         <div id="lg7" class="tab-pane">
                           <div class="col-md-12 mt-50">
                              <div class="owl-slider">
                                 <div id="carousel4" class="owl-carousel">

                                 @foreach($recentlyViewed as $prod)
                                 <div class="item">
                                    <div class="product-wrapper white-box">
                                       <!--<p class="pt-10 text-center brown-text"><a href="#" ><span></span>New Brand</a></p>-->
                                       <div class="star-top">
                                        @php $check=0; @endphp
                                        @foreach($wishs as $wishlist)
                                            @if($wishlist->product_id == $prod->id)
                                            @php $check=1; @endphp
                                            @endif
                                        @endforeach
                                           <a href="javascript:;" class="add-to-wish" id="{{ $prod->id }}" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($check == 0) ion-ios-star-outline @else ion-ios-star @endif"></i></a>
                                       </div>
                                       <div class="product-imga">
                                          <a href="{{ route('front.product',$prod->slug) }}">
                                              <img src="{{ $prod->thumbnail ? asset('public/assets/images/thumbnails/'.$prod->thumbnail):asset('public/assets/images/noimage.png') }}" alt="">
                                          </a>
                                      </div>
                                       <div class="product-content text-center">
                                          <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName()}}</a></h4>
                                          <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->category->name }}</a></h4>
                                          <div class="product-price">
                                              <span>{{ $prod->setCurrency() }}</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach

                                 </div>
                              </div>

                            </div>
                         
                         </div>
                     </div>
                 </div>
                 
            </div>
            <!--<div class="banner-area hm1-banner product-footer-bg mt-90">-->
            <!--   <div class="container">-->
            <!--      <div class="row  pb-0">-->
            <!--         <div class="col-md-6 col-lg-6">-->
            <!--            <div class="banner-wrapper mrg-mb-md ">-->
            <!--               <div class="product-footer-box">-->
            <!--                  <h4>Delivery</h4>-->
                              
            <!--                  <p>{{ $gs->delivery_short_desc }}</p>-->
            <!--                     <p class="link-visit"><a href="{{ route('front.page' , 'delivery') }}" class="">Find Out More </a></p>-->
            <!--                     <h4 class="mt-60">Returns</h4>-->
            <!--                  <p>{{ $gs->return_short_desc }}</p>-->
                              
            <!--                     <p class="link-visit"><a href="{{ route('front.page' , 'return') }}" class="">Find Out More </a></p>-->
            <!--               </div>-->
            <!--            </div>-->
            <!--         </div>-->
            <!--         <div class="col-md-2 col-lg-2"></div>-->
            <!--         <div class="col-md-4 col-lg-4 pt-0 pb-30">-->
            <!--            <div class="luxwatch-box">-->
            <!--               <div class="big-title-lux">Delivery & <br/> Returns</div>-->
            <!--               <p class="mt-120 link-visit"><a href="" class="">Find Out More </a></p>-->
            <!--            </div>-->
            <!--         </div>-->
            <!--      </div>-->
            <!--   </div>-->
            <!--</div>-->
            </div>
            </div>
            
            
            @include('front.chunks.footer')
@endsection

@section('scripts')

<script>


let myCarousel; //a variable thats hold owlCarousel object
function myCarouselStart() {
    myCarousel = $('#galleries_images').owlCarousel({
        loop: false,
        autoplay: false,
        autoplayTimeout: 5000,
        navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
        nav: true,
        item: 4,
        margin: 12,
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 4
            },
            1000: {
                items: 4
            }
        }
    });

}

myCarouselStart();
 $('.color-change').on('change',  function(){
    var code = $(this).children(":selected").attr("id");
    var value = $(this).children(":selected").data('value');
    if(value == 'default'){
        location.reload();
    }
    else{
    code = code.replace('colour','');
    var pid = {{$productt->id}};
    $.ajax({
        type: "GET",
        url:mainurl+"/images/color/"+pid+"/"+code,
        success:function(data){
            console.log(data.images);

            if(data.images.length > 0){
                $('#galleries_images').html('');
                myCarousel.trigger("destroy.owl.carousel");
                for(var i=0;i<data.images.length;i++){
                    if(i == 0){
                        $('#xzoom-default').attr('src',data.images[i]);    
                        $('#xzoom-default').attr('xoriginal',data.images[i]);    
                    }
                        console.log(data.images[i]);
                        var img = "<img class='xzoom-gallery my-images' src='"+data.images[i]+"' alt=''>";
                        $('#galleries_images').append(img);                                                   
                }
                myCarouselStart();

            }

        }
    });
    }
  
});
  


$('.imga').on('click', '.my-images', function() {
    var src = $(this).attr('src');
    $('#xzoom-default').attr('src',src);    
    $('#xzoom-default').attr('xoriginal',src);    

});


   
// $('#addcrt').click(function(){
//     if($('.color-change').val() == 'default'){
        
//     }
// });
</script>

@endsection