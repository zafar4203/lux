@extends('layouts.front')
@section('content')

         <!-- banner start -->
         <div class="slider-area contact-bg slider-2 ">
            <div class="container ">
               <h1> Latest News </h1>
            </div>
         </div>
         <div class="white-bg">
            <div class="container">
               <div class="breadcrumb-content text-left">
                  <ul>
                     <li>
                        <a href="index.html">Home</a>
                     </li>
                     <li> Latest News </li>
                    
                  </ul>
               </div>
            
             
               <div class="col-md-12 mb-20">
                   <ul class="news-bottom">
                    @foreach($blogs as $blog)
                    <li>
                     <div class="row">
                         <div class="col-md-6"> 
                             <img src="{{ $blog->photo ? asset('assets/images/blogs/'.$blog->photo):asset('assets/images/noimage.png') }}" class="img-fluid" alt="">
                         </div>
                         <div class="col-md-6">
                           
                           <h4> {{ $blog->title }}</h4>
                           <p><small>{{ $blog->created_at->format('d M Y') }}</small></p>
                           <p> {!! $blog->details !!}</p>  
                           
                         </div>
                      
                     </div>

                    </li>
                    @endforeach
                   </ul>
               </div>
            

               </div>
            </div>
            @include('front.chunks.footer')
@endsection