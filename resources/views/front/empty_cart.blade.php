@extends('layouts.front')

@section('styles')
    <style>
        .header-height{
            margin-top:15%;
        }
    </style>
@endsection
@section('content')

<div class="header-height"></div>

            <!-- Breadcrumb Area End -->
            <!-- cart area start -->
            <div class="cart-main-area mtb-60px">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 mx-auto">
                            <img class="img-fluid" src="{{ asset('assets/images/cart_is_empty.png') }}" alt="">                    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 mx-auto">
                            <div class="cart-shiping-update-wrapper">
                                <div class="mx-auto cart-shiping-update text-center">
                                    <a style="width:100%; font-size:18px;" href="{{ route('front.index') }}">Continue Shopping</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- cart area end -->

@endsection

