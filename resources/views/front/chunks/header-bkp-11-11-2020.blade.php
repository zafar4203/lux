<header>
   <div class="header-area transparent-bar black-headero">
   <div class="container">
   <div class="row sticky-hide">
      <div class="col text-left">
         <div class="language-currency">
            <!-- <div class="currency ">
               <select class="select-header orderby">
                  <option value="">$currency</option>
               </select>
               </div>
               <div class="border-right-white"></div>
               <div class="language">
               <select class="select-header orderby">
                  <option value="">English</option>
                  <option value="">French </option>
               </select>
               </div> -->
         </div>
      </div>
      <div class="col d-md-block hide-center">
         &nbsp;
      </div>
      <div class="col text-right">
         @if(Auth::user())
         <div class="signin"><a href="{{ route('user-dashboard') }}" class="white">My Account</a></div>
         @else
         <div class="signin"><a href="{{ route('user-profile') }}" class="white">Sign in/Register</a></div>
         @endif
      </div>
   </div>
   <div class="row mobile-box">
   <div class="col-lg-12 col-md-12 d-md-block">
   <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-1 col-5 mobile-header-hide-col">&nbsp;</div>
      <div class="col-lg-8 col-md-6  col-sm-4 d-md-block">
         <div class="logo-menu-wrapper text-center">
            <div class="logo sticky-hide">
               <a href="{{ route('front.index') }}"><img src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/img/logo/logo.png"  /></a>
            </div>
         </div>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-7 col-7 mt-40 sticky-hide">
         <div class="header-site-icon">
            <div class="header-search same-style">
               <button class="sidebar-trigger-search">
               <span class="ti-search"></span>
               </button>
            </div>
            <div class="header-login same-style">
               <a href="{{ route('user-wishlists') }}">
               <button class="sidebar-trigger">
               <i class="ti-star"></i>
               <span id="wishlist-count" class="count-style">@if(Auth::user()) {{ Auth::user()->wishlistCount() }} @else 0 @endif</span>
               </button>
               </a>
            </div>
            <div class="header-cart same-style">
               <a href="{{ route('front.cart') }}">
               <button class="sidebar-trigger">
               <i class="ti-shopping-cart"></i>
               <span id="cart-count" class="count-style">{{ Session::has('cart') ? count(Session::get('cart')->items) : '0' }}</span>
               </button>
               </a>
            </div>
         </div>
      </div>
   </div>
 
            </div>
         </div>
      </div>
        <div class="main-menu mt-20 " style="margin:0 auto">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <nav class="navbar navbar-expand-lg">
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon">
               <span></span>
               <span></span>
               <span></span>
               </span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto" style=" text-align: center; margin: 0 auto;">
                     <li><a href="{{ route('front.campaign') }}">Enter Competition</a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class=" dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Campaign <i class="ion-ios-arrow-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <div class="container">
                              <div class="row no-gutters">
                                 <div class="col-md-3"> 
                                     <a class="dropdown-item" href="{{ route('front.campaign') }}">Enter Competition</a>
                                    <a class="dropdown-item" href="{{ route('front.how_it_work') }}">How it works</a>
                                    <a class="dropdown-item" href="{{ route('front.winner') }}">Previous Winners</a>
                                    <a class="dropdown-item" href="{{ route('front.faq') }}">FAQ's</a>
                                   
                                 </div>
                                 <div class="col-md-3">
                                    <div class="jwell-box">
                                       <div class="jwell-title-lux">Campaign</div>
                                       <p class="mt-40 link-visit"><a href="" class=""> Discover</a></p>
                                    </div>
                                    </div
                                    <!-- /.col-md-3  -->
                                    <div class="col-md-6 text-right box-img-baaner">
                                       <img src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/img/banner/campaining.jpg" alt="" class="img-fluid">
                                    </div>
                                    <!-- /.col-md-6  -->
                                 </div>
                              </div>
                           </div>
                     </li>
                     <li class="nav-item dropdown">
                     <a class="dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Watch Accessories <i class="ion-ios-arrow-down"></i></a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                     <div class="container">
                     <div class="row no-gutters">
                     <div class="col-md-3"> 
                     @foreach($categories as $cat)
                     @if($cat->slug == "watch-accessories")
                     @foreach($cat->subs as $sub)
                     <a class="dropdown-item" href="{{ route('front.category' , $cat->slug , $sub->slug) }}">{{ $sub->name }}</a>
                     @endforeach
                     @endif
                     @endforeach
                     </div>
                     <div class="col-md-3">
                     <div class="jwell-box">
                     <div class="jwell-title-lux">
                     FINE JEWELS</div>
                     <p class="mt-40 link-visit"><a href="" class=""> Discover</a></p>
                     </div>
                     </div
                     <!-- /.col-md-3  -->
                     <div class="col-md-6 text-right box-img-baaner">
                     <img src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/img/banner/jwellery.jpg" alt="" class="img-fluid">
                     </div>
                     <!-- /.col-md6  -->
                     </div>
                     </div>
                     </div>
                     </li>
                    
                     <li class="nav-item dropdown">
                     <a class=" dropdown-toggle" href="{{ route('front.category' , 'jewellery-accessories') }}" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Jewel Accessories <i class="ion-ios-arrow-down"></i></a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                     <div class="container">
                     <div class="row no-gutters">
                     <div class="col-md-3"> 
                     @foreach($categories as $cat)
                     @if($cat->slug == "jewellery-accessories")
                     @foreach($cat->subs as $sub)
                     <a class="dropdown-item" href="{{ route('front.category' , $cat->slug , $sub->slug) }}">{{ $sub->name }}</a>
                     @endforeach
                     @endif
                     @endforeach
                     </div>
                     <div class="col-md-3">
                     <div class="jwell-box">
                     <div class="jwell-title-lux"> 
                     FINE JEWELS</div>
                     <p class="mt-40 link-visit"><a href="" class=""> Discover</a></p>
                     </div>
                     </div
                     <!-- /.col-md-3  -->
                     <div class="col-md-6 text-right box-img-baaner">
                     <img src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/img/banner/jwellery.jpg" alt="" class="img-fluid">
                     </div>
                     <!-- /.col-md-6  -->
                     </div>
                     </div>
                     </li>
                     <li><a href="{{ route('front.charity') }}">Charities</a></li>
                  </ul>
                  </div>
                  </li></ul>
                  </div>
            </nav>
         
            </div>
            </div>
            </div>
   </div>
</header>
<!-- main-search start -->
<div class="main-search-active">
   <div class="sidebar-search-icon">
      <button class="search-close"><span class="ti-close"></span></button>
   </div>
   <div class="sidebar-search-input">
      <form id="searchForm" action="{{ route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')]) }}" method="GET">
         @if (!empty(request()->input('sort')))
         <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
         @endif
         @if (!empty(request()->input('minprice')))
         <input type="hidden" name="minprice" value="{{ request()->input('minprice') }}">
         @endif
         @if (!empty(request()->input('maxprice')))
         <input type="hidden" name="maxprice" value="{{ request()->input('maxprice') }}">
         @endif
         <div class="form-search">
            <input  id="prod_name" name="search" placeholder="{{ $langg->lang2 }}" value="{{ request()->input('search') }}" autocomplete="off" type="text">                     
            <div class="autocomplete">
               <div id="myInputautocomplete-list" class="autocomplete-items"></div>
            </div>
            <button type="submit">
            <i class="ti-search"></i>
            </button>
         </div>
      </form>
   </div>
   </div>
</header>