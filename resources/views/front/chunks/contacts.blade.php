<div class="banner-area hm1-banner contact-footer-bg mt-280">
      <div class="container">
         <div class="row  pb-0">
            <div class="col-md-6 col-lg-6">
               <div class="banner-wrapper mrg-mb-md neg-top-120">
                  <div class="address-box">
                     <h4>Contact US</h4>
                     <h5 class="border-botton-white">Store Inquiries</h5>
                     <p>{{ $gs->contact_store_inquiries }}</p>
                     <h5 class="border-botton-white">Online inquires</h5>
                     <p>{{ $gs->contact_online_inquiries }}</p>
                     <h5 class="mt-40">Opening Hours </h5>
                     <!-- <p class="big-font-timming">Tuesday 11am - 7pm </p> -->
                     <ul class="quick-link">
                        <li>{{ $gs->all_timing }}</li>
                        <li>{{ $gs->sunday_timing }} </li>
                        <li>{{ $gs->browsing_timing }} </li>
                     </ul>
                     <p class="footer-link"><a href="#">View all opening hours</a></p>
                  </div>
               </div>
            </div>
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-md-4 col-lg-4 pt-100 pb-0">
               <!--<div class="luxwatch-box">-->
               <!--   <div class="big-title-lux">Visiting <br/>Lux watches</div>-->
               <!--   <p class="mt-90 link-visit"><a href="" class="">Plan your visit </a></p>-->
               <!--</div>-->
            </div>
         </div>
      </div>
   </div>
