<footer class="footer-bg footer-padding">
<div class="container-fluid">
   <div class="footer-logo">  <a href="{{ route('front.index') }}"><img src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/img/logo/logo.png"  /></a></div>
</div>
<div class="container-fluid">
   <div class="footer-top pt-50 pb-25">
      <div class="row">
         <!-- <div class="col-lg-2 col-md-2">
            <div class="footer-widget mb-30">
               <div class="footer-widget-title">
                  <h3>The Store </h3>
               </div>
               <div class="footer-widget-content">
                  <ul class="quick-link">
                     <li><a href="#">Store Reopening</a></li>
                     <li><a href="#">Store Guide</a></li>
                     <li><a href="#">Restaurants</a></li>
                     <li><a href="">Services</a></li>
                     <li><a href="#">News Events</a></li>
                     <li><a href="#">Visitor Guidelines</a></li>
                  </ul>
               </div>
            </div>
         </div> -->
         <div class="col-lg-2 col-md-2">&nbsp;</div>
         <div class="col-lg-3 col-md-3">
            <div class="footer-widget mb-30">
               <div class="footer-widget-title">
                  <h3>Shopping online</h3>
               </div>
               <div class="footer-widget-content">

                  <ul class="quick-link">
                  @foreach($footer_pages as $single)
                    @if($single->category == "shopping online" && $single->visible == 0)
                        @if($single->type == 0)
                            <li><a href="{{ route('front.page', $single->slug) }}">{{ $single->name }}</a></li>
                        @else
                            <li><a href="{{ url($single->slug) }}">{{ $single->name }}</a></li>
                        @endif
                    @endif
                  @endforeach

                      
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3">
            <div class="footer-widget mb-30">
               <div class="footer-widget-title">
                  <h3>Customer Service</h3>
               </div>
               <div class="footer-widget-content">
                  <ul class="quick-link">
                  @foreach($footer_pages as $single)
                    @if($single->category == "customer service" && $single->visible == 0)
                        @if($single->type == 0)
                            <li><a href="{{ route('front.page', $single->slug) }}">{{ $single->name }}</a></li>
                        @else
                            <li><a href="{{ url($single->slug) }}">{{ $single->name }}</a></li>
                        @endif
                    @endif
                  @endforeach                    
                    
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-lg-2 col-md-2">
            <div class="footer-widget mb-30">
               <div class="footer-widget-title">
                  <h3>About Us</h3>
               </div>
               <div class="footer-widget-content">
                  <ul class="quick-link">
                  @foreach($footer_pages as $single)
                    @if($single->category == "about" && $single->visible == 0)
                        @if($single->type == 0)
                            <li><a href="{{ route('front.page', $single->slug) }}">{{ $single->name }}</a></li>
                        @else
                            <li><a href="{{ url($single->slug) }}">{{ $single->name }}</a></li>
                        @endif
                    @endif
                  @endforeach

                  </ul>
               </div>
            </div>
         </div>
          <div class="col-lg-2 col-md-2">&nbsp;</div>
         <!-- <div class="col-lg-3 col-md-5">
            <div class="footer-widget mb-30">
               <div class="footer-widget-title">
                  <h3>Opening Hours</h3>
               </div>
               <ul class="quick-link">
                  <li>Monday-Saturday:11am-7pm </li>
                  <li>Sunday:11.30am-6pm* </li>
                  <li>browsing only 11.30am-12pm </li>
               </ul>
               <p class="footer-link"><a href="#">View all opening hours</a></p>
            </div>
         </div> -->
      </div>
      <div class="row signin-box">
         <div class="col-md-12 text-center">
           stay up to date with the latest Lux Dxb news <a href="{{ route('front.subscribeForm_web') }}"><button class="btn signup"> Sign up to emails</button></a>
         </div>
      </div>
   </div>
   <div class="footer-bottom border-top-1 ptb-15">
      <div class="row">
         <div class="col-md-2 col-7 pr-md-0 m-auto">
            <div class="footer-payment-method">
               <a><img class="img-fluid" alt="" src="public/assets/front/img/icon-img/7.png"></a>
            </div>
         </div>
         <div class="col-md-10 col-12">
            <div class="social-media">
               <div class="copyright text-right">
                  Follow us 
                  
                @if(App\Models\Socialsetting::find(1)->f_status == 1)
                    <a href="{{ App\Models\Socialsetting::find(1)->facebook }}" target="_blank"><i class="ti-facebook"></i></a> 
                @endif


               <!-- @if(App\Models\Socialsetting::find(1)->t_status == 1)
                    <a href="{{ App\Models\Socialsetting::find(1)->twitter }}" target="_blank"><i class="ti-twitter"></i></a>
                @endif-->
                @if(App\Models\Socialsetting::find(1)->insta_status == 1)
                    <a href="{{ App\Models\Socialsetting::find(1)->instagram }}" target="_blank"><i class="ti-instagram"></i></a>
                @endif

                @if(App\Models\Socialsetting::find(1)->tiktok_status == 1)
                    <a href="{{ App\Models\Socialsetting::find(1)->tiktok }}" target="_blank"><i class="t3-tiktok"></i></a>
                @endif
  
                @if(App\Models\Socialsetting::find(1)->l_status == 1)
                    <a href="{{ App\Models\Socialsetting::find(1)->linkedin }}" target="_blank"><i class="ti-linkedin"></i></a>
                @endif

                @if(App\Models\Socialsetting::find(1)->youtube_status == 1)
                    <a href="{{ App\Models\Socialsetting::find(1)->youtube }}" target="_blank"><i class="ti-youtube"></i></a>
                @endif
                  <!-- <a href="#" target="_blank"><i class="ti-facebook"></i></a> 
                  <a href="#" target="_blank"><i class="ti-twitter"></i></a>
                  <a href="#" target="_blank"><i class="ti-pinterest-alt"></i></a>
                  <a href="#" target="_blank"><i class="ti-instagram"></i></a>
                  <a href="#" target="_blank"><i class="ti-youtube"></i></a> -->
               </div>
            </div>
         </div>
      </div>
      <div class="row mt-40">
         <div class="col-md-8 col-12">
            <div class="footer-bottom-link">
               <a href="{{ route('front.page','accessibility') }}">Cookies policy</a>
               <a href="{{ route('front.terms') }}">Terms & Conditions</a>
               <a href="{{ route('front.privacy') }}">Security & Privacy Policy</a>
            </div>
         </div>
         
         <div class="col-md-4 col-12">
            <div class="copyright-payment">
               <div class="copyright text-right">
                  <p> © 2020 All RIght Reserved.</p>
                  <p>Designed and Developed by <a href="https://softgates.ae/">Softgates Technologies</a></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</footer>
