@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
     
         <!-- banner start -->
         <div class="slider-area contact-bg slider-2 ">
            <div class="container ">
               <h1>LUX Privacy </h1>
            </div>
         </div>
         <div class="white-bg">
            <div class="container">
               <div class="breadcrumb-content text-left">
                  <ul>
                     <li>
                        <a href="index.html">Home</a>
                     </li>
                    
                     <li>Privacy Policy</li>
                  </ul>
               </div>
               <div class="row">
                  <div class="col-md-12">
                  <h3>{{ $privacy->main_heading }}</h3>
                  {!! $privacy->main_para !!}

                     <div id="accordion" class="accordion mb-20 mt-30 black-pg">
                        <div class="card mb-0">
                           <div class="card-header collapsed active-acc" data-toggle="collapse" href="#collapseOne">
                              <a class="card-title">
                              {{ $privacy->heading_1 }}
                              </a>
                           </div>
                           <div id="collapseOne" class="card-body collapse show" data-parent="#accordion" >
                             <div class="card-body">
                             {!! $privacy->para_1 !!}
                           </div></div>
                           <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                              <a class="card-title">
                              {{ $privacy->heading_2 }}
                              </a>
                           </div>
                           <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                              <div class="card-body">
                              {!! $privacy->para_2 !!}
                              </div>
                           </div>
                           <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                              <a class="card-title">
                              {{ $privacy->heading_3 }}
                              </a>
                           </div>
                           <div id="collapseThree" class="collapse" data-parent="#accordion" >
                              <div class="card-body">
                              {!! $privacy->para_3 !!}                             
                              </div>
                           </div>
                           <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
                              <a class="card-title">
                              {{ $privacy->heading_4 }}
                              </a>
                           </div>
                           <div id="collapsefour" class="collapse" data-parent="#accordion" >
                              <div class="card-body">
                           
                              {!! $privacy->para_4 !!}                           
                           
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            @include('front.chunks.footer')
@endsection