@extends('layouts.front')
@section('styles')
<style>
    .login-form-container input[type="email"]{
        background-color: #fff;
        border: medium none;
        color: #7d7d7d;
        font-size: 14px;
        font-weight: 500;
        height: 50px;
        padding: 0 15px;
        margin-bottom: 4px;
        width:100%;
        border: 1px solid #A6A6A6;
    }
    </style>  
@endsection
@section('content')

         <!-- banner start -->
                 <div class="header-height"></div>
         <div class="slider-area slider-2 ">
            <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="about-info">
            <h1 class="title">
           Contact Us
            </h1>
           </div>
        </div>
      </div>
    </div>
    </div>
       
         <div class="white-bg">
            <div class=" watch-container">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 dealy-product-content-left mt-20">
                        
                        <p>Please note, we are currently experiencing delivery delays due to due to extended processing times and external courier disruptions. For more information, please see our orders FAQs.</p>
                        <p>   Need some help? If our FAQs page hasn't already answered your question, send us a message and we will respond as soon as possible. If your query relates to a speciﬁc order or product, please include your order number or product details.</p>
                        <p>  The Customer Service Team are available Monday - Saturday 10am-7pm, and Sunday 11am-6pm.</p>
                     </div>
                  </div>
                  <div class="login-form-container contact-form-container mt-50">
                     <div class="login-form">

                    <form id="contactform" action="{{route('front.contact.submit')}}" method="POST">
                        {{csrf_field()}}
                            @include('includes.admin.form-both')  

                           <div class="row">
                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" name="name" placeholder="{{ $langg->lang47 }} *" required="">
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" name="email"  placeholder="{{ $langg->lang49 }} *" required="">
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="phone"  placeholder="{{ $langg->lang48 }} *">
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-12 col-md-12">
                                 <div class="form-group">
                                    <label>Your Message</label>
                                    <textarea name="text" placeholder="{{ $langg->lang50 }} *" required=""></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <p class="text-left">
                                     <input type="hidden" name="to" value="{{ $ps->contact_email }}">
                                    <button type="submit" class="btn-style2 cr-btn"><span>Send Message</span></button>
                                 </p>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <hr/>
               </div>
            </div>
            <div class="banner-area hm1-banner contact-footer-bg mt-280">
               <div class="container">
                  <div class="row  pb-0">
                     <div class="col-md-6 col-lg-6">
                        <div class="banner-wrapper mrg-mb-md neg-top-120">
                           <div class="address-box">
                              <h4>Contact US</h4>
                              <h5 class="border-botton-white">Store Inquiries</h5>
                              <p>+97(0)33322233</p>
                              <h5 class="border-botton-white">Online inquires</h5>
                              <p>customer.support@luxwatches.com</p>
                              <h5 class="mt-40">Opening Hours </h5>
                              <p class="big-font-timming">Tuesday 11am - 7pm </p>
                              <ul class="quick-link">
                                 <li>Monday-Saturday:11am-7pm </li>
                                 <li>Sunday:11.30am-6pm* </li>
                                 <li>browsing only 11.30am-12pm </li>
                              </ul>
                              <p class="footer-link"><a href="#">View all opening hours</a></p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-2 col-lg-2"></div>
                     <div class="col-md-4 col-lg-4 pt-100 pb-0">
                        <div class="luxwatch-box">
                           <div class="big-title-lux">Visiting <br/>Lux watches</div>
                           <p class="mt-90 link-visit"><a href="" class="">Plan your visit </a></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>


            @include('front.chunks.footer')

@endsection 