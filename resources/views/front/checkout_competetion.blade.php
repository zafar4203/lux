@extends('layouts.front')

@section('styles')
    <style>
    .login-form-container select{
        background-color: #fff;
        border: medium none;
        color: #7d7d7d;
        font-size: 14px;
        font-weight: 500;
        height: 50px;
        padding: 0 15px;
        margin-bottom: 4px;
        border: 1px solid #A6A6A6;
    }
    .black{
        color:black !important;
        font-weight:bold;
    }   
    .none{
        display:none;
    }
    .sin-sidebar ul li ul {
        padding-left: 0px;
    }
    .addresses .box{        
        border: 1px solid red;
        padding: 5px 10px;
        color: black;
        font-weight: bold;
    }
    </style>
@endsection


@section('content')
<div class="white-bg">
            <div class="container">
            <div class="row">
               <div class="col-md-8 col-sm-8">
                  <div class="row">
                     <div class="col-md-11 payment">
                        <div class="breadcrumb">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs" role="tablist">
                             <li role="presentation">
                                 <a href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab" class="active">1. DELIVERY</a>     </li>
                            <li id="last" role="presentation">
                                <a id="payment_tab">2. PAYMENT</a>
                            </li>
                           
                           </ul>
                         
                           <!-- Tab panes -->
                           <div class="tab-content">
                             <div role="tabpanel" class="tab-pane active" id="delivery">
                             <h6 class="mt-30"><strong>DELIVER TO</strong></h6>
                             <div class="login-form-container login-form-container2">
                              <div class="login-form">
                              <form action="{{ route('stripe.submit-competetion') }}" method="POST" class="checkout-form">
                                @include('includes.form-success')
                                @include('includes.form-error')

                                {{ csrf_field() }}

                                    <div class="form-group">
                                       <label>Full Name</label>
                                       <input type="text" name="name" placeholder="{{ $langg->lang152 }}" required value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->name : '' }}">
                                    </div>
                                    <div class="form-group">
                                       <label>Phone Number</label>
                                       <input id="phone" type="text" name="phone"
														placeholder="{{ $langg->lang153 }}" required
														value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->phone : '' }}">
                                    </div>
                                    <div class="form-group">
                                       <label>Email Address</label>
                                       <input id="email" type="text" name="email"
														placeholder="{{ $langg->lang154 }}" required
														value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->email : '' }}">
                                    </div>

                                    <h6 class="mt-40 mb-10"><strong>POSTAL ADDRESS</strong></h6>
                                    <div class="col-md-12">
                                        <div class="row addresses">
                                        @foreach($addresses as $address)
                                            <div class="col-md-3 text-center box"><span class="select_address" id="{{$address->id}}">{{ $address->title }}</span>
                                                
                                                <input type="hidden" value="{{$address->country}}" id="country_{{$address->id}}" />
                                                <input type="hidden" value="{{$address->city}}" id="city_{{$address->id}}" />
                                                <input type="hidden" value="{{$address->state}}" id="state_{{$address->id}}" />
                                                <input type="hidden" value="{{$address->postal_code}}" id="postal_{{$address->id}}" />
                                                <input type="hidden" value="{{$address->address_1}}" id="address1_{{$address->id}}" />
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                       <label>Country</label>
                                            <select id="country" name="customer_country" required>
                                                @include('includes.countries')
                                            </select>
                                    </div>

                                    <div class="form-group">
                                       <label>ADDRESS</label>
                                       <input type="text" id="address1" name="address"
														placeholder="{{ $langg->lang155 }}" required
														value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->address : '' }}">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label>Town / City</label>
                                       <input type="text" name="city" id="city"
														placeholder="{{ $langg->lang158 }}" required
														value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->city : '' }}">
                                    </div>

                                    <div class="form-group">
                                       <label>postal code</label>
                                       <input id="postal" type="text" name="zip"
														placeholder="{{ $langg->lang159 }}" required
														value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->zip : '' }}">
                                    </div>
                                    <!--<p class="text-left link-login"> <a data-toggle="tab" href="#lg2">Add a gift Message</a></p>
                                    <hr/>-->

                                    <!-- <div class="form-group">
                                       <h6>Delivery options</h6>
                                       <div class="form-check">
                                          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked="">
                                          <label class="form-check-label" for="gridRadios1">
                                          DHL Express AED 30
                                          </label>
                                       </div>                                      
                                    </div> -->
                                   
                                 
                                  
                                    <p class="text-center pt-30">Pay Securley with</p>
                                    <ul class="visa-box text-center">
                                       <li><a href=""><img src="{{ asset('public/assets/front/img/icon-img/visa-img.png') }}"></a></li>
                                       <li><a href=""><img src="{{ asset('public/assets/front/img/icon-img/visa-img2.png') }}"></a></li>
                                       <li><a href=""><img src="{{ asset('public/assets/front/img/icon-img/master.png') }}"></a></li>
                                    </ul>

                            <input type="hidden" id="shipping-cost" name="shipping_cost" value="0">
                            <input type="hidden" id="packing-cost" name="packing_cost" value="0">



                            <input type="hidden" name="total" id="grandtotal" value="{{ Session::get('competetion')['grandtotal'] }}">
                            <input type="hidden" id="tgrandtotal" value="{{ Session::get('competetion')['grandtotal'] }}">
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->id : '' }}">



                              
                              </div>
                              
                           </div>

                             </div>
                             
                             <div role="tabpanel" class="tab-pane " id="payment">

                              <!--<hr/> Commented by Prashant - 23-11-2020 -->
                              <div class="login-form-container login-form-container2 pt-0">
                                 <div class="login-form">

            <div class="login-form-container pb-0">
            <div class="checkbox-form">						
                <h3>Secure Card Payment</h3>
                <div class="row">
                    <div class="col-md-12">
                    <input type="hidden" name="method" value="Stripe">
                    <!--<div class="checkout-form-list">-->
                    <!--    <label><span class="float-left">Card Number</span><span class="float-right"><img src="{{ asset('public/assets/front/img/icon-img/visa.png') }}"></span></span></label>-->
                    <!--    <input class="card-elements" name="cardNumber" type="number" placeholder="Card Number" autocomplete="off" autofocus="" oninput="validateCard(this.value);">                    </div>-->
                        <div class="form-group">
                    		<label for="cardNumber">Card number</label>
                    		<div class="input-group">
                    			<input class="card-elements form-control mb-0" name="cardNumber" type="text" placeholder="1234 5678 1234 5678" autocomplete="off" autofocus="" oninput="validateCard(this.value);">
                    			<div class="input-group-append">
                    				<span class="input-group-text text-muted">
                    					<i class="ti-credit-card"></i>
                    				</span>
                    			</div>
                    		</div>
                    	</div> <!-- form-group.// -->
                    </div>
                    <div class="col-md-6">
                        <label>Expiry Month</label>										
                        <input class="form-control card-elements" name="month" type="number" placeholder="Expire Month">
                    </div>
                    <!-- <div class="col-md-1">  
                        <label class="mt-30">/</label>	
                    </div> -->
                    <div class="col-md-6">
                        <label>Expiry Year</label>										
                        <input class="form-control card-elements" name="year" type="number" placeholder="Expire Year">
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="checkout-form-list">
                            <label>Name on Card</label>
                            <input class="card-elements" name="cardName" type="text" placeholder="Card Holder Name" autocomplete="off" autofocus="">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="checkout-form-list">
                            <label>Security Code</label>
                            <input class="card-elements form-control mb-0" name="cardCVC" type="number" placeholder="Cvv" autocomplete="off" oninput="validateCVC(this.value);">
                        </div>
                    </div>
                    <!--<div class="col-md-5 pt-20"> card's <br/> CVV number</div> commented by Prashant - 23-11-2020 -->
                    </div>		
                </div>
                                                            
            </div>
            <!-- <div class="form-check mt-20">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios10" value="option1" checked="">
            <label class="form-check-label" for="gridRadios10">
                use delivery address as billing address
            </label>
            </div> -->
            
            <input type="hidden" id="donate" name="donate" />
            <p class="text-center mt-20">
                <button type="submit" class="btn-style2 width100"><span>Pay Securely Now</span><b></b></button>
            </p>
            <p>By click 'Pay Securely Now' you are agreeing to the LUX DXB <a href="#" class="text-decoration">Terms & Conditions</a> & <a href="#" class="text-decoration">Privacy Policy</a></p>
            <div class="payment-hr">
            <hr/></div>
            <p class="text-left">
            This  is a Secure 256-bit SSL Encrypted Payment Your's Safe.
        </p>
        <p class="text-center pt-40">Pay Securley with</p>
        <ul class="visa-box text-center">
            <li><a href=""><img src="{{ asset('public/assets/front/img/icon-img/visa-img.png') }}"></a></li>
            <li><a href=""><img src="{{ asset('public/assets/front/img/icon-img/visa-img2.png') }}"></a></li>
            <li><a href=""><img src="{{ asset('public/assets/front/img/icon-img/master.png') }}"></a></li>
        </ul>
        </form>
    </div>
        
    </div>
    </div>

    
                           </div>
                         
                         </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 gray-bg pt-20 pt-30">
                  <div class="row">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="sin-sidebar category-sidebar gray-bg mb-0">
                           <div class="fix">
<ul id="cat-treeview">
<li class="open">  <a href="#">ORDER SUMMARY</a>
        <ul>
            <li>
                @php $product_images = json_decode($campaign->product_images); @endphp
                    <div class="recent-post-wrapper mb-25">
                <div class="recent-post-img">
                    <a href="#">
                        <img src="{{ $product_images[0] ? asset('public/assets/images/campaigns/'.$product_images[0]):asset('public/assets/images/noimage.png') }}" alt="">
                    </a>
                </div>

                        <div class="recent-post-content">
                            <h4><a href="#">{{ str_limit($campaign->product_title , 20) }}</a></h4>
                            <div class="qty-box">
                            <ul>
                                <li>
                                    QTY:{{  Session::get('competetion')['qty']  }}<br>
                                    <span>{{ App\Models\Product::convertPrice($campaign->product_price) }}</span>
                                </li>
                            </ul>
                        </div>

                    </div>
            
                </div>
            

           
            </li>
        </ul>
        <div class="shop-total recent-product">
        
            <ul>
                <li>
                1 item sub total
                <span>{{ App\Models\Product::convertPrice($campaign->product_price * Session::get('competetion')['qty']) }}</span>
                </li>
                <li>
                Discount ({{ Session::get('competetion')['discount_value']}}%)
                @php 
                    $subtotal = App\Models\Product::convertOnlyPrice($campaign->product_price * (int)Session::get('competetion')['qty']);
                    $d_value = (int) Session::get('competetion')['discount_value'];                    
                    $discount = $d_value/100 * (float)$subtotal;
                    $grandtotal = round((float)$subtotal - (float)$discount , 0);
                @endphp
                <span>{{ $curr->sign }}{{ $discount }}</span>
                </li>
                <li>
                Grand Total
                <span>{{ $curr->sign }}{{ $grandtotal }}</span>
                </li>
                
            
            </ul>
        </div>
    </li>
    </ul>
                        </div> </div>
                     </div> 
                     <div class="col-md-12 donate-radio">
                            <div class="form-group">
                                   
                                       <div class="form-check">
                                          <input class="form-check-input" type="radio" name="donate1" id="donateone" value="1" checked="">
                                          <label class="form-check-label" for="donateone">
                                          Donate One <!-- "I will" removed as per client requirement -->
                                          </label>
                                       </div>
                                       </div>
                                        <div class="form-group">
                                       <div class="form-check">
                                          <input class="form-check-input" type="radio" name="donate1" id="donateall" value="{{ Session::get('competetion')['qty']}}">
                                          <label class="form-check-label" for="donateall">
                                          Donate All <!-- "I will" removed as per client requirement -->
                                          </label>
                                       </div>
                                       </div>
                                         <div class="form-group">
                                         <div class="form-check">
                                          <input class="form-check-input" type="radio" name="donate1" id="CustomeDonation" value="custom">
                                          <label class="form-check-label" for="CustomeDonation">
                                         Custom Donation
                                          </label>
                                       </div>                                      
                                            <input class="none" type="number" name="otherDonation" id="otherDonation"/>                                    
                                    </div>
                     </div>
                    <div class="col-md-12 text-center">  
                                       <button type="submit" id="submit" class="btn-style2 width100"><span>Pay Securely Now</span><b></b></button>
                                    </div>
                     </div>
               </div>
            </div>
         </div>

		 @include('front.chunks.footer')
@endsection
@section('scripts')

<script>
$('#donate').val(1);
$('input[type=radio][name=donate1]').change(function() {
    var value = this.value;
    if (value == 'custom') {
        $('#otherDonation').css('display' , 'block');
    }else{
        $('#donate').val(value);
    }
});

$('#otherDonation').on('input', function() {
    $('#donate').val(this.value);
});

$('#submit').on("click", function(e){
    e.preventDefault();
    let valid = true;
    $('[required]').each(function() {
        if ($(this).is(':invalid') || !$(this).val()) valid = false;
    })
    if (!valid) alert("error please fill all fields!");
    else{
        $('#payment_tab').attr('aria-controls',"payment");
        $('#payment_tab').attr('role',"tab");
        $('#payment_tab').attr('data-toggle',"tab");
        $('#payment_tab').attr('href',"#payment");

        $('#payment_tab').click();
    }
    })
</script>

<script src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript">
	$('a.payment:first').addClass('active');
	$('.checkout-form').prop('action',$('a.payment:first').data('form'));
	$($('a.payment:first').attr('href')).load($('a.payment:first').data('href'));


		var show = $('a.payment:first').data('show');
		if(show != 'no') {
			$('.pay-area').removeClass('d-none');
		}
		else {
			$('.pay-area').addClass('d-none');
		}
	$($('a.payment:first').attr('href')).addClass('active').addClass('show');
	
		   $('.submit-loader').hide();
</script>


<script type="text/javascript">

var coup = 0;
var pos = {{ $gs->currency_format }};

@if(isset($checked))

	$('#comment-log-reg1').modal('show');

@endif

var mship = $('.shipping').length > 0 ? $('.shipping').first().val() : 0;
var mpack = $('.packing').length > 0 ? $('.packing').first().val() : 0;
mship = parseFloat(mship);
mpack = parseFloat(mpack);

$('#shipping-cost').val(mship);
$('#packing-cost').val(mpack);
var ftotal = parseFloat($('#grandtotal').val()) + mship + mpack;
ftotal = parseFloat(ftotal);
      if(ftotal % 1 != 0)
      {
        ftotal = ftotal.toFixed(2);
      }
		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+ftotal)
		}
		else{
			$('#final-cost').html(ftotal+'{{ $curr->sign }}')
		}

$('#grandtotal').val(ftotal);



$('.shipping').on('click',function(){
	mship = $(this).val();

$('#shipping-cost').val(mship);
var ttotal = parseFloat($('#tgrandtotal').val()) + parseFloat(mship) + parseFloat(mpack);
ttotal = parseFloat(ttotal);
      if(ttotal % 1 != 0)
      {
        ttotal = ttotal.toFixed(2);
      }
		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+ttotal);
		}
		else{
			$('#final-cost').html(ttotal+'{{ $curr->sign }}');
		}
	
$('#grandtotal').val(ttotal);

})


    $("#check-coupon-form").on('submit', function () {
        var val = $("#code").val();
        var total = $("#grandtotal").val();
        var ship = 0;
            $.ajax({
                    type: "GET",
                    url:mainurl+"/carts/coupon/check",
                    data:{code:val, total:total, shipping_cost:ship},
                    success:function(data){
                        if(data == 0)
                        {
                        	toastr.error(langg.no_coupon);
                            $("#code").val("");
                        }
                        else if(data == 2)
                        {
                        	toastr.error(langg.already_coupon);
                            $("#code").val("");
                        }
                        else
                        {
                            $("#check-coupon-form").toggle();
                            $(".discount-bar").removeClass('d-none');

							if(pos == 0){
								$('#total-cost').html('{{ $curr->sign }}'+data[0]);
								$('#discount').html('{{ $curr->sign }}'+data[2]);
							}
							else{
								$('#total-cost').html(data[0]+'{{ $curr->sign }}');
								$('#discount').html(data[2]+'{{ $curr->sign }}');
							}
								$('#grandtotal').val(data[0]);
								$('#tgrandtotal').val(data[0]);
								$('#coupon_code').val(data[1]);
								$('#coupon_discount').val(data[2]);
								if(data[4] != 0){
								$('.dpercent').html('('+data[4]+')');
								}
								else{
								$('.dpercent').html('');									
								}


var ttotal = parseFloat($('#grandtotal').val()) + parseFloat(mship) + parseFloat(mpack);
ttotal = parseFloat(ttotal);
      if(ttotal % 1 != 0)
      {
        ttotal = ttotal.toFixed(2);
      }

		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+ttotal)
		}
		else{
			$('#final-cost').html(ttotal+'{{ $curr->sign }}')
		}	

                        	toastr.success(langg.coupon_found);
                            $("#code").val("");
                        }
                      }
              }); 
              return false;
    });



</script>


<script type="text/javascript">
var ck = 0;

	$('#final-btn').on('click',function(){
		ck = 1;
	})


        $(document).on('submit','#step1-form',function(){
        	$('#preloader').hide();
            var val = $('#sub').val();
            var total = $('#grandtotal').val();
			total = Math.round(total);
                if(val == 0)
                {
                var handler = PaystackPop.setup({
                  key: '{{$gs->paystack_key}}',
                  email: $('input[name=email]').val(),
                  amount: total * 100,
                  currency: "{{$curr->name}}",
                  ref: ''+Math.floor((Math.random() * 1000000000) + 1),
                  callback: function(response){
                    $('#ref_id').val(response.reference);
                    $('#sub').val('1');
                    $('#final-btn').click();
                  },
                  onClose: function(){
                  	window.location.reload();
                  	
                  }
                });
                handler.openIframe();
                    return false;                    
                }
                else {
                	$('#preloader').show();
                    return true;   
                }
        });
</script>
<!-- Added by Prashant 22-11-2020 -->
<script>
    $('#submit').click(function() {
        if ($('#payment_tab').hasClass('active')){
            $('#submit').css('display','none');
        } else {
            $('#submit').css('display','block');
          }
    });

    $('.select_address').click(function(){

var id = this.id;
var country = $('#country_'+id).val();
var city = $('#city_'+id).val();
var address1 = $('#address1_'+id).val();
var address2 = $('#address2_'+id).val();
var state = $('#state_'+id).val();
var postal = $('#postal_'+id).val();


$("#country").val(country);
$("#state").val(state);
$("#city").val(city);
$("#address1").val(address1);
$("#address2").val(address2);
$("#postal").val(postal);


});

</script>
<!-- Added by Prashant 22-11-2020 -->
@endsection