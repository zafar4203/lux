@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
       
         <!-- banner start -->
         <div class="header-height"></div>
         <div class="slider-area slider-2 ">
            <div class="container">
      <div class="row mb-40">
        <div class="col-lg-12">
          <div class="about-info">
            <h1 class="title">
             Career
            </h1>
           </div>
        </div>
      </div>
    </div>
         </div>
         <div class="white-bg">
            <div class="container">
            
               <div class="row">
                   <div class="col-md-12">

                    <div class="row mb-40">
                        <div class="col-md-6">
                           <a href="#"><img src="{{ asset('public/assets/images/career') }}/{{ $career->image }}" class="img-fluid" alt=""></a>
                           </div>
                            <div class="col-md-6">
                           
                            <p>{!! $career->para_1 !!}</p> 
                           </div>
                        </div>
                     
      
                   </div>
               </div>
               <h3 class="mt-30 mb-20 text-left">{{ $career->main_heading }}</h3>
               <div class=" mb-20">
                   <!-- <ul class="careers-bottom">

                    <li> -->
                     <div class="row">
                         <div class="col-md-12">
                           
                           <!-- <h4>{{ $career->heading_1 }}</h4> -->
                           {!! $career->para_2 !!}
                         </div>
                      
                     </div>

                    <!-- </li> -->
                    <!-- <li>
                        <div class="row">
                            <div class="col-md-12">
                              
                              <h4> {{ $career->heading_2 }}</h4>
                              {!! $career->para_3 !!}
                            </div>
                         
                        </div>
   
                       </li>
                       <li>
                        <div class="row">
                            <div class="col-md-12">                              
                            <h4>{{ $career->heading_3 }}</h4>
                              {!! $career->para_4 !!}
                            </div>                        
                        </div>   
                       </li> -->
                   <!-- </ul> -->
               </div>
            

               </div>
            </div>
@include('front.chunks.footer')
@endsection
