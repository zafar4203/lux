@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
        
         <!-- banner start -->
         <div class="slider-area contact-bg slider-2 ">
            <div class="container ">
               <h1>LUX LEGAL </h1>
            </div>
         </div>
         <div class="white-bg">
            <div class="container">
               <div class="breadcrumb-content text-left">
                  <ul>
                     <li>
                        <a href="index.html">Home</a>
                     </li>
                     <li>Legal </li>
                     <li>Terms & Conditions </li>
                  </ul>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div id="accordion" class="accordion mb-20 mt-30 black-pg">
                        <div class="card mb-0">
                           <div class="card-header collapsed active-acc" data-toggle="collapse" href="#collapseOne">
                              <a class="card-title">
                              {{ $term->title_1 }}
                              </a>
                           </div>
                           <div id="collapseOne" class="card-body collapse show" data-parent="#accordion" >
                               {!! $term->details_1 !!}
                           </div>
                           <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                              <a class="card-title">
                              {{ $term->title_2 }}
                              </a>
                           </div>
                           <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                              <div class="card-body">
                              {!! $term->details_2 !!}
                              </div>
                           </div>
                           <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                              <a class="card-title">
                              {{ $term->title_2 }}
                              </a>
                           </div>
                           <div id="collapseThree" class="collapse" data-parent="#accordion" >
                              <div class="card-body">
                              {!! $term->details_3 !!}
                              </div>
                           </div>
                           <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
                              <a class="card-title">
                              {{ $term->title_4 }}
                              </a>
                           </div>
                           <div id="collapsefour" class="collapse" data-parent="#accordion" >
                              <div class="card-body">
                              {!! $term->details_4 !!}                        
                           
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @include('front.chunks.footer')
@endsection