@extends('layouts.front')
@section('styles')
  <style>
    .header-height{
      margin-top:10%;
    }
    .message{
        margin-top:2%;
        margin-bottom:10%;
    }
  </style>
@endsection
@section('content')
<div class="header-height"></div>
<section class="about slider-2">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="about-info">
            <h3 class="text-center message">
                No Competetion is Live!
            </h3>

          </div>
        </div>
      </div>
    </div>
  </section>
  @include('front.chunks.footer')

@endsection