@extends('layouts.front')
@section('styles')
  <style>
    .slider-2.inner-content-page{
      margin-top:10%;
    margin-bottom: 3%;
    }
  </style>
@endsection
@section('content')

<!--<div class="header-height"></div> Commented by Prashant 06-12-2020 -->

        <section class="about slider-2 inner-content-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h1>Subscribe</h1>
                        <p>We have sold out of tickets for our most recent competetion and we will be giving live with our next competetion soon!</p>
                        <p>Signup to notified as soon as we go live!</p>
                    </div>
                </div>
        
                <div class="row" style="margin-bottom:5%;">
                    <div class="col-md-6 mx-auto">
                    <div class="login-form-container">
                        <div class="login-form login-register-form signin-form">
                        @if(Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>                    
                        @endif
                        <form action="{{ route('front.subscribe') }}" method="post">
                        @csrf
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" value="{{ old('first_name') }}" placeholder="Enter First Name" name="first_name" required />
                                @error('first_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" value="{{ old('last_name') }}" placeholder="Enter Last Name" name="last_name" required />
                                @error('last_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="text" name="email" placeholder="Enter Email Address" value="{{ old('email') }}" required />
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                        </div>
                        </div>
                        </div>
                </div>
            </div>
        </section>
        
@include('front.chunks.footer')   
@endsection

