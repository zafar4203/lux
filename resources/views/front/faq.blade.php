@extends('layouts.front')
@section('styles')
  
@endsection
@section('content')

    
      <!-- banner start -->
   
             <div class="header-height"></div>
         <div class="slider-area slider-2 ">
            <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="about-info">
            <h1 class="title">
            LUX FAQs 
            </h1>
           </div>
        </div>
      </div>
    </div>
    </div>
      <div class="white-bg">
      <div class="container">
     
        @foreach($cats as $cat)
         <h2 class="mt-20">{{ $cat->category }}</h2>
         <div id="accordion" class="accordion mb-20 mt-30">
            <div class="card mb-0">                
               @foreach($faqs as $faq)
               @if($faq->type == $cat->id)
               <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$faq->id}}">
                  <a class="card-title">
                    {{ $faq->title }}
                  </a>
               </div>
               <div id="{{$faq->id}}" class="card-body collapse" data-parent="#accordion" >
                    {!! $faq->details !!}
               </div>
               @endif
               @endforeach
            </div>
         </div>
         <hr/>
         @endforeach

      </div>

@endsection 