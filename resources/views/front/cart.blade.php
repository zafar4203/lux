@extends('layouts.front')
@section('content')
<div class="header-height"></div>
   
      
         <div class="white-bg mt-120">
         <div class="product-cart-area hm-3-padding  pb-80 cart-content">
            <div class="container-fluid">
               
                  <h1>
                     Shopping Bag
                  </h1>
              
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="table-content table-responsive cart-table">
                        <table>
                          <tbody>
                            @if(Session::has('cart'))
                            @php
                                $discount = 0;
                            @endphp
                            @foreach($products as $product)
                            @php
                                $discount = $discount + App\Models\Product::find($product['item']['id'])->dsc_amt;
                            @endphp

                            @if(isset($product['camp_id']))
                            <tr class="cart-border-top cremove{{ $product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}">
                            @else
                            <tr class="cart-border-top cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}">
                            @endif
                                 <td class="product-thumbnail">
                                    <img width="45px" src="{{ $product['item']['photo'] ? asset('public/assets/images/products/'.$product['item']['photo']):asset('public/assets/images/noimage.png') }}" alt="">
                                 </td>
                                 <td class="product-name">




                    <a class="product-title-box mb-0" href="{{ route('front.product', $product['item']['slug']) }}">{{mb_strlen($product['item']['name'],'utf-8') > 35 ? mb_substr($product['item']['name'],0,35,'utf-8').'...' : $product['item']['name']}}</a>
                    @if(isset($product['camp_id']))                    
                    @php                     
                        $camp = \App\Models\Campaign::query()->where("id",$product['camp_id'])->first(); 
                    @endphp
                    <a href="{{ route('front.campaign' , $camp->id) }}"><small>({{ $camp->name }})</small></a>
                    @endif


                    @if(!empty($product['keys']))
                    @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                        <span style="margin-bottom:0px !important;">{{ ucwords(str_replace('_', ' ', $key))  }} : </span> {{ $value }} <br>
                     @endforeach
                    @endif
                    <div class="row golden-text">
                        <div class="col-sm-12"><a class="removecart cart-remove" data-class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])) }}" href="javascript:;"><i aria-hidden="true" style="font-style: normal;">x</i> Remove</a> <a class="add-to-wish" data-href="{{ route('user-wishlist-add',$product['item']['id']) }}"><i class="ion-ios-star-outline" aria-hidden="true"></i> Add to wishlist</a></div>
                    </div> 
                    </td>
                    <!--<td class="product-price">-->
                    <!--    <div class="mt-5">-->
                    <!--    @php $check = 0 @endphp-->

                    <!--    @if(!empty($product['keys']))-->
                    <!--        @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)-->
                    <!--            @if(ucwords(str_replace('_', ' ', $key)) == "Colour")-->
                    <!--            <p style="margin-bottom:0px !important;">{{ ucwords(str_replace('_', ' ', $key))  }} : </p> {{ $value }} <br>-->
                    <!--            @php $check = 1; @endphp-->
                    <!--            @endif-->
                    <!--        @endforeach-->
                    <!--    @endif-->

                    <!--    @if($check == 0)-->
                    <!--        <p style="margin-bottom:0px !Important;">Colors:</p> <span>No Color</span>-->
                    <!--    @endif-->
                    <!--    </div>-->
                    <!--</td>-->
                    <td class="product-quantity">
                    @if($product['item']['type'] == 'Physical')
<input type="hidden" class="prodid" value="{{$product['item']['id']}}">  
@if(isset($product['camp_id']))
<input type="hidden" class="itemid" value="{{$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">     
@else
<input type="hidden" class="itemid" value="{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">     
@endif
<input type="hidden" class="size_qty" value="{{$product['size_qty']}}">     
<input type="hidden" class="size_price" value="{{$product['item']['price']}}">   

<!--<div class="plus-minus">-->
<!--        <div class="dec qtybutton reducing">-</div>-->
<!--        <span class="qttotal1" @if(isset($product['camp_id'])) id="qty{{$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" @else id="qty{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" @endif>{{ $product['qty'] }}</span>-->
<!--        <div class="inc qtybutton adding">+</div>-->
<!--</div>-->
@endif
                        <div class="quantity-range">
                            <label>Quantity </label>
                             <!--<input class="input-text qty text" type="number" step="1" min="0" value="1" title="Qty" size="4">-->
                            <select id="{{$product['item']['id']}}" class="quantity-select custom-select">
                              <option {{$product['qty'] == 1 ? 'selected' : '' }} value="1">1</option>
                              <option {{$product['qty'] == 2 ? 'selected' : '' }} value="2">2</option>
                              <option {{$product['qty'] == 3 ? 'selected' : '' }} value="3">3</option>
                              <option {{$product['qty'] == 4 ? 'selected' : '' }} value="4">4</option>
                              <option {{$product['qty'] == 5 ? 'selected' : '' }} value="5">5</option>
                              <option {{$product['qty'] == 6 ? 'selected' : '' }} value="6">6</option>
                              <option {{$product['qty'] == 7 ? 'selected' : '' }} value="7">7</option>
                              <option {{$product['qty'] == 8 ? 'selected' : '' }} value="8">8</option>
                              <option {{$product['qty'] == 9 ? 'selected' : '' }} value="9">9</option>
                              <option {{$product['qty'] == 10 ? 'selected' : '' }} value="10">10</option>
                            </select>
                        </div>
                
                    </td>
                    <td class="product-cart-delete">
                    <div class="update-checkout-cart">
                        <!--<div class="update-cart">-->
                        <!--    <button class="btn-style"><span>Update</span></button>-->
                        <!--</div>-->
                        <div class="update-cart">
                            <a class="btn-style cancel-style" href="#">
                            @if(isset($product['camp_id']))
                            <span class="removecart cart-remove" data-class="cremove{{ $product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])) }}">Delete</span>
                            @else
                            <span class="removecart cart-remove" data-class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])) }}">Delete</span>
                            @endif
                            </a>
                        </div>
                    </div>
                    
                    </td>
                    <td class="product-cart-total">
                        <span class="custome-hide">Product Total</span>
                        <span>{{ App\Models\Product::convertPrice($product['item'] ['price']) }}</span>
                    </td>
                    <td class="product-subtotal">  
                        <span class="custome-hide">Product Sub Total</span>
                        @if($product['size_qty'])
                            @if(isset($product['camp_id']))
                            <input type="hidden" id="stock{{$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="{{$product['size_qty']}}">
                            @else
                            <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="{{$product['size_qty']}}">
                            @endif
                        @elseif($product['item']['type'] != 'Physical') 
                            @if(isset($product['camp_id']))
                            <input type="hidden" id="stock{{$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="1">
                            @else
                            <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="1">
                            @endif
                        @else
                            @if(isset($product['camp_id']))
                            <input type="hidden" id="stock{{$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="{{$product['stock']}}">
                            @else
                            <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="{{$product['stock']}}">
                            @endif
                        @endif

                            @if(isset($product['camp_id']))
                            <span id="prc{{$product['item']['id'].$product['camp_id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">
                                {{ App\Models\Product::convertPrice($product['price']) }}                 
                            </span>
                            @else
                            <span id="prc{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">
                                {{ App\Models\Product::convertPrice($product['price']) }}                 
                            </span>
                            @endif
                            
                    </td>
                
                </tr>
                @endforeach
                @endif
            </tbody>
                        </table>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-8">
                     <div class="cart-shiping-update">
                        <div class="cart-shipping">
                           <a class="btn-style " href="{{ route('front.index') }}">
                           <span>Continue shopping</span>
                           </a>
                        </div>
                       
                     </div>
                  </div>
                  <div class="col-md-4">
                    
                    <input type="hidden" id="d-val" value="{{ App\Models\Product::convertPrice($discount)}}">
                    <input type="hidden" id="c-val" value="{{ $curr->sign }}">
                    <input type="hidden" id="c-sign-length" value="{{ strlen($curr->sign) }}">
                    <input type="hidden" class="coupon-total" id="grandtotal" value="{{ Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00' }}">
                    <input type="hidden" id="coupon_val" value="0">

                        <div class="shop-total">
                         
                           <ul>
                              <li>
                                 1 item sub total
                                 <span class="carttotal">{{ Session::has('cart') ? App\Models\Product::convertPrice($totalPrice) : '0.00' }}</span>
                              </li>
                              @php 
                                  $total = App\Models\Product::convertOnlyPrice($mainTotal) - App\Models\Product::convertOnlyPrice($discount);
                                  $delivery_charges =   App\Models\Product::convertOnlyPrice($gs->delivery_charges);
                                  $free_delivery =   App\Models\Product::convertOnlyPrice($gs->free_delivery);
                                  if($total > $free_delivery){
                                    $delivery_charges = 0;
                                  }  
                                  $grandtotal = $total + $delivery_charges;
                              @endphp
                              <input type="hidden" id="free_delivery" value="{{$free_delivery}}">
                              <input type="hidden" id="delivery_charges" value="{{$delivery_charges}}">
                              <li class="order-total">
                                Delivery
                                <span id="delivery" class="delivery"> {{$curr->sign}} {{ $delivery_charges }}</span>
                              </li>
                              
                              <li class="order-total">
                                 Total
                                 <span class="main-total">{{$curr->sign}} {{ Session::has('cart') ?  number_format($grandtotal)  : '0.00' }}</span>
                              </li>
                           </ul>
                        </div>
                        <div class="cart-btn text-center mb-15">
                           <a href="{{ route('front.checkout') }}">Proceed to checkout</a>
                        </div>
                        
                   
                  </div>
               </div>
            
            </div>
         </div>

         <div class="product-area pb-80 hm-3-padding">
            <div class="container-fluid">
               <div class="tab-content">
                   <div class="row">
                  <div class="col-md-12">
                  <h3>Recently Viewed</h3>
                     <div class="owl-slider mt-3">
                        <div id="carousel3" class="owl-carousel">
                            @foreach($prods as $prod)
                           <div class="item">
                              <div class="product-wrapper white-box">
                                 <!--<p class="pt-10 text-center brown-text"><a href="#" ><span></span>New Brand</a></p> Commented by Prashant -->
                                 <div class="star-top">
                                    @php $check=0; @endphp
									@foreach($wishs as $wishlist)
									@if($wishlist->product_id == $prod->id)
									@php $check=1; @endphp
									@endif
									@endforeach
									<a href="javascript:;" class="add-to-wish" id="{{ $prod->id }}" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($check == 0) ion-ios-star-outline @else ion-ios-star @endif"></i></a>
                                 </div>
                                 <div class="product-img">
                                    <a href="{{ route('front.product',$prod->slug) }}">
                                        <img src="{{filter_var($prod->photo, FILTER_VALIDATE_URL) ?$prod->photo:asset('public/assets/images/products/'.$prod->photo)}}" alt="">
                                    </a>
                                 </div>
                                 <div class="product-content text-center">
                                    <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName() }}</a></h4>
                                    <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->category->name }}</a></h4>
                                    <div class="product-price">
                                       <span>{{ $prod->setCurrency() }}</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                            @endforeach
                           </div>
                        </div>
                     </div>
                 </div>
               </div>
               </div>
            </div>
         </div>

         @include('front.chunks.footer')
         </div>
@endsection
@section('scripts')
    <script>
        $('.quantity-select').on('change',function(){
        var value = this.value;

        var pid =  $(this).parent().parent().find('.prodid').val();
        var itemid =  $(this).parent().parent().find('.itemid').val();
        var size_qty = $(this).parent().parent().find('.size_qty').val();
        var size_price = $(this).parent().parent().find('.size_price').val();
        var stck = $("#stock"+itemid).val();
        // var qty = $("#qty"+itemid).html();
        var qty = value;
                
        $.ajax({
          type: "GET",
          url:mainurl+"/addbyselect",
          data:{id:pid,itemid:itemid,size_qty:size_qty,size_price:size_price,qty:qty},
          success:function(data){

            if(data == 'digital') {
                toastr.error(langg.already_cart);
             }
            else if(data == 0) {
                toastr.error(langg.out_stock);
              }
            else {
                var disc = parseFloat(parseFloat($("#coupon_val").val())+parseFloat($("#d-val").val().slice($("#c-sign-length").val())).toFixed(2));
                var main_t = parseFloat(parseFloat(data[99].slice($("#c-sign-length").val()) - $('#coupon_val').val()) - $('#d-val').val().slice($("#c-sign-length").val())).toFixed(2);
                $(".discount").html($("#c-val").val()+" "+parseInt(disc).toLocaleString());
                $(".carttotal").html($("#c-val").val()+" "+parseFloat(data[0].slice($("#c-sign-length").val())).toLocaleString());
                $(".main-total").html($("#c-val").val()+" "+parseInt(main_t).toLocaleString());
                $(".coupon-total").val(data[3]);
                $("#prc"+itemid).html($("#c-val").val()+" "+parseFloat(data[2].slice($("#c-sign-length").val())).toLocaleString());
                $("#prct"+itemid).html(data[2]);
                $("#cqt"+itemid).html(data[1]);
                $("#qty"+itemid).html(data[1]);
                $("#cart-paisa").html(data[9]);
                $("#delivery").html(data[98]);

              }
             }
          });

        })
    </script>
@endsection
