@extends('layouts.front')
@section('styles')
    <style>
        #discount-box{
            display:none;
        }
    .process {
        background-color: #0F1F2F;
        color: #fff;
        display: block;
        letter-spacing: 1px;
        padding: 20px 10px 20px;
    }
    .process:hover {
        background-color: #AB8E66;
        color: #fff;
    }
    #remaining_box{
        padding:20px;
        font-weight:bold;
    }
    .block-disable{
        cursor:no-drop !important;
    }
    .share-icons a{
        margin-right:10px;
    }
    p{
        margin-bottom:3px !important;
    }
    .product-size ul li{
        cursor:pointer !important;
    }
    </style>
@endsection
@section('content')

<div class="header-height"></div>
   
      
         <div class="white-bg mt-120">
         <div class="product-cart-area hm-3-padding  pb-80 cart-content">
            <div class="container">
               
                  <h1>
                     Competition
                  </h1>
              
                  <div class="row competition-box-outer ">
                     <div class="col-md-6">
                        <!--<h4 class="mt-30 text-capitalize text-center">{{ $campaign->name }}</h4>-->
                         <div class="product-details-img-content competition-box">
                             <div class="product-details-tab">
        <div class="product-details-large tab-content">
            <!--<div class="tab-pane active" id="pro-details1">-->
            @php 
            $prize_images = json_decode($campaign->prize_images); 
            $product_images = json_decode($campaign->product_images);
            @endphp             
                <div class="share-box">
                <a href="#"> <i class="ion-android-share"></i></a>
                </div>
            <!--    <div class="div-img">-->
            <!--        <a href="assets/img/product/pin2.png">-->
            <!--            <img src="{{ asset('public/assets/images/campaigns')}}/{{$prize_images[0]}}" alt="">-->
            <!--        </a>                -->
            <!--</div>-->
        
        <!--</div>-->
        
        <!-- Added by Zafar 23-11-2020 -->
             <div class="xzoom-container">
                <div class="div-img">
                    <img class="xzoom6"  src="{{ asset('public/assets/images/campaigns')}}/{{$prize_images[0]}}" xoriginal="{{ asset('public/assets/images/campaigns')}}/{{$prize_images[0]}}" alt="">
                </div>
              <div class="xzoom-thumbs product-details-xzoom-thumbs mt-12 product-dec-slider2 owl-carousel"> <!-- removed classes for checking " product-details-small nav " by Zafar 22-11-2020 -->
                @php $i=0; @endphp
                @foreach($prize_images as $image)
                <a @if($i == 0) class="active" @endif href="{{ asset('public/assets/images/campaigns')}}/{{$image}}">
                    <img class="xzoom-gallery6" src="{{ asset('public/assets/images/campaigns')}}/{{$image}}" alt="">
                </a>
                @php $i++; @endphp
                @endforeach
              </div>
            </div>
        <!-- Added by Zafar 23-11-2020 -->
        </div>
            
        <!-- Commented by Zafar 23-11-2020 -->
            <!--<div class="product-details-small nav mt-12 product-dec-slider2 owl-carousel">-->
            <!--    @php $i = 0; @endphp-->
            <!--    @foreach($prize_images as $image)-->
            <!--    <a @if($i == 0) class="active" @endif href="#pro-details1">-->
            <!--        <img src="{{ asset('public/assets/images/campaigns')}}/{{$image}}" alt="">-->
            <!--    </a>-->
            <!--    @php $i++; @endphp-->
            <!--    @endforeach            -->
            <!--</div>-->
        <!-- Commented by Zafar 23-11-2020 -->
        </div>
                         </div>

                         <div class=" dealy-product-content-left mt-40"> <!-- changed mt120 to mt40 by Zafar 12-11-2020 -->
                             <h4 class="mt-30">{{ $campaign->prize_title }}</h4>
                           <p class="big-italic-font black-text mb-0">{!! $campaign->prize_description!!}</p>
                           <p class="big-italic-font black-text mb-4">{!! $campaign->campaign_description !!}</p> <!-- big-italic-font added by Zafar -->
                            <p style="margin-top: -10%;" class="big-italic-font black-text mb-4">{!! $campaign->campaign_editor_note !!}</p> <!-- big-italic-font added by Zafar -->
                           
                            <!--<p><span class="gray-small-text">Editor Note</span></p> commneted by Zafar-->
                            <!--<p class="black-text">{{ $campaign->campaign_editor_note }}</p> commneted by Zafar, moved below-->
                         </div>
                     </div>
                    
                     <div class="col-md-6 mt-30">
                        <div class="row">
                           <div class="col-md-6"></div>
                           <div class="col-md-6">
                              <p class=""><small>{{ $campaign->sold_out }} sold out {{ $campaign->quantity }}</small></p>
 
<div class="progress">
  @php 
  $width = 0;
  $value = $campaign->sold_out/$campaign->quantity;
  $width = $value*100;
  @endphp
  <div class="progress-bar bg-secondary progress-bar-striped" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
 
</div>
                           </div>
                        </div>
                        
                        <div class="product-details-img-content  competition-box">
                            <h4 class="mt-30">{{ $campaign->product_title }}</h4>
                            <p class="big-italic-font black-text mb-4">{!! $campaign->product_description !!}</p> <!-- big-italic-font added by Zafar -->
                            
                           <div class="product-details-tab">
                               <div class="product-details-large tab-content position-relative">
                                   <!--<div class="tab-pane active" id="pro-details1">-->
                                    <div class="share-box">
                                       <!--<a href="#"> <i class="ion-ios-heart-outline"></i></a> commented by Zafar-->
                                       <a href="#"> <i class="ion-android-share"></i></a>
                                     </div>
                                       <!--<div class="div-img">-->
                                       <!--    <a href="assets/img/product/pin2.png">-->
                                       <!--        <img src="{{ $product_images[0] ? asset('public/assets/images/campaigns/'.$product_images[0]):asset('public/assets/images/noimage.png') }}" alt="">-->
                                       <!--    </a>-->
                                       <!--</div>-->
                                   <!--</div>-->
                                
                                  <!-- Added by Zafar 23-11-2020 -->
                                    <div class="xzoom-container">
                                        <div class="div-img">
                                            <img class="xzoom4" id="xzoom-default" src="{{ $product_images[0] ? asset('public/assets/images/campaigns/'.$product_images[0]):asset('public/assets/images/noimage.png') }}" xoriginal="{{ $product_images[0] ? asset('public/assets/images/campaigns/'.$product_images[0]):asset('public/assets/images/noimage.png') }}" alt="">
                                        </div>
                                        
                                      <div class="xzoom-thumbs product-details-xzoom-thumbs mt-12 product-dec-slider3 owl-carousel"> <!-- removed classes for checking " product-details-small nav " by Zafar 22-11-2020 -->
                                        
                                        @php $i=0; @endphp
                                        @foreach($product_images as $photo)
                                        <a @if($i == 0) class="active" @endif href="{{ asset('public/assets/images/campaigns')}}/{{$photo}}">
                                            <img class="xzoom-gallery4" src="{{ asset('public/assets/images/campaigns')}}/{{$photo}}" alt="">
                                        </a>
                                        @endforeach
                                        @php $i++; @endphp
                                      </div>
                                    </div>
                                <!-- Added by Zafar 23-11-2020 -->
                               </div>
                               <!--<div class="product-details-small nav mt-12 product-dec-slider3 owl-carousel">-->
                               <!--     @php $i=0; @endphp-->
                               <!--     @foreach($product_images as $photo)-->
                               <!--    <a @if($i == 0) class="active" @endif href="#pro-details1">-->
                               <!--        <img src="{{ asset('public/assets/images/campaigns')}}/{{$photo}}" alt="">-->
                               <!--    </a>-->
                               <!--    @endforeach-->
                               <!--    @php $i++; @endphp                                   -->
                               <!--</div>-->
                           </div>
                       </div>

                       <div class="product-discount-perct mt-30">
                           <ul>
                               <li>
                                   <p>20-25</p>
                                   <small>10%</small>
                               </li>
                               <li>
                                   <p>30-50</p>
                                   <small>20%</small>
                               </li>
                               <li>
                                   <p>55-65</p>
                                   <small>25%</small>
                               </li>
                               <li>
                                   <p>70-95</p>
                                   <small>30%</small>
                               </li>
                               <li>
                                   <p>100</p>
                                   <small>35%</small>
                               </li>
                           </ul>
                       </div>
                       <div class="product-size mt-30">
                       <input type="hidden" class="qttotal" value="1" />
                       <input type="hidden" id="product_price" value="{{ $campaign->product_price }}" />
                       <input type="hidden" id="currency_sign" value="{{ $curr->sign }}" />
                       <input type="hidden" id="currency_value" value="{{ $curr->value }}" />
                       <input type="hidden" id="campaign_id" value="{{ $campaign->id }}" />

                       <input type="hidden" id="discount_send" value="" />
                       <input type="hidden" id="discount_value_send" value="" />
                       <input type="hidden" id="subtotal_send" value="" />
                       <input type="hidden" id="grandtotal_send" value="" />
                        <ul>
                        <li>
                            <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">1 </a> <!--  title="0% discount" removed by Zafar as per requirement -->
                        </li>
                        <li>
                            <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">2</a> <!--  title="0% discount" removed by Zafar as per requirement -->
                        </li>
                        <li>
                            <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">3</a> <!--  title="0% discount" removed by Zafar as per requirement -->
                        </li>
                        <li>
                            <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">4</a> <!--  title="0% discount" removed by Zafar as per requirement -->
                        </li>
                        <li>
                            <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">5</a> <!--  title="0% discount" removed by Zafar as per requirement -->
                        </li>
                        <li>
                            <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">10</a> <!--  title="0% discount" removed by Zafar as per requirement -->
                          </li>
                          <li>
                              <a class="select" data-toggle="tooltip" data-placement="top" data-value="0">15</a> <!--  title="0% discount" removed by Zafar as per requirement -->
                          </li>
                          <li>
                              <a class="select" data-toggle="tooltip" data-placement="top" data-value="10">20</a> <!--  title="10% discount" removed by Zafar as per requirement -->
                          </li>
                          <li>
                              <a class="select" data-toggle="tooltip" data-placement="top" data-value="10">25</a> <!--  title="10% discount" removed by Zafar as per requirement -->
                          </li>
                          <li>
                              <a class="select" data-toggle="tooltip" data-placement="top" data-value="20">30</a> <!--  title="20% discount" removed by Zafar as per requirement -->
                          </li>
                          <li>
                           <a class="select" data-toggle="tooltip" data-placement="top" data-value="20">35</a> <!--  title="20% discount" removed by Zafar as per requirement -->
                       </li>
                       <li>
                           <a class="select" data-toggle="tooltip" data-placement="top" data-value="20">40</a> <!--  title="20% discount" removed by Zafar as per requirement -->
                       </li>
                       <li>
                           <a class="select" data-toggle="tooltip" data-placement="top" data-value="20">45</a> <!--  title="20% discount" removed by Zafar as per requirement -->
                       </li>
                       <li>
                           <a class="select" data-toggle="tooltip" data-placement="top" data-value="20">50</a> <!--  title="20% discount" removed by Zafar as per requirement -->
                       </li>
                       <li>
                           <a class="select" data-toggle="tooltip" data-placement="top" data-value="25">55</a> <!--  title="25% discount" removed by Zafar as per requirement -->
                       </li>
                       <li>
                        <a class="select" data-toggle="tooltip" data-placement="top" data-value="25">60</a> <!--  title="25% discount" removed by Zafar as per requirement -->
                    </li>
                    <li>
                        <a class="select" data-toggle="tooltip" data-placement="top" data-value="25">65</a> <!--  title="25% discount" removed by Zafar as per requirement -->
                    </li>
                    <li>
                        <a class="select" data-toggle="tooltip" data-placement="top" data-value="30"> 70</a> <!--  title="30% discount" removed by Zafar as per requirement -->
                    </li>
                    <li>
                        <a class="select" data-toggle="tooltip" data-placement="top" data-value="30">75</a> <!--  title="30% discount" removed by Zafar as per requirement -->
                    </li>
                    <li>
                        <a class="select" data-toggle="tooltip" data-placement="top" data-value="30">80</a> <!--  title="30% discount" removed by Zafar as per requirement -->
                    </li>
                    <li>
                     <a class="select" data-toggle="tooltip" data-placement="top" data-value="30">85</a> <!--  title="30% discount" removed by Zafar as per requirement -->
                 </li>
                 <li>
                     <a class="select" data-toggle="tooltip" data-placement="top" data-value="30">90</a> <!--  title="30% discount" removed by Zafar as per requirement -->
                 </li>
                 <li>
                     <a class="select" data-toggle="tooltip" data-placement="top" data-value="30">95</a> <!--  title="30% discount" removed by Zafar as per requirement -->
                 </li>
                 <li>
                     <a class="select" data-value="35" data-toggle="tooltip" data-placement="top">100</a> <!--  title="35% discount" removed by Zafar as per requirement -->
                 </li>
                
                        </ul>
                    </div>
                    <div id="discount-box"><div class="shop-total mt-4"> <!-- mt-4 added by Zafar -->
                         
                           <ul class="pb-0">
                              <li>
                                <span>Sub total</span>  <!-- span added by Zafar -->                                  <!-- span added by Zafar -->
                                 <span id="subtotal">AED 500</span>
                              </li>
                              <li>
                                 <span>Competition Entry Tickets</span>  <!-- span added by Zafar -->
                                 <span id="ticket"></span>
                              </li>
                              <li class="order-total">
                               <span>Discount <small id="discount_percentage">(10%)</small></span> <!-- span added by Zafar -->
                                 <span id="discount">AED 50</span>
                              </li>
                              
                              <li class="order-total">
                                 <span> <!-- span added by Zafar -->
                                     Grand Total <br>
                                     <small>(Inclusive of VAT)</small>
                                 </span>
                                 <span id="grandtotal">AED 550</span>
                              </li>
                           </ul>
                        </div></div>
                        <div id="remaining_box" class="text-center"></div>
                    <div class="cart-btn text-center mt-20 mb-15">
                     <button class="process btn-block" id="addcampcrt" disabled href="javascript:;">Proceed to checkout</button>
                  </div>

<h6>Share On</h6>
<div class="share-icons">
@php $url = route('front.campaign'); @endphp
<!-- Facebook -->
<a href="http://www.facebook.com/sharer.php?u={{$url}}" target="_blank"><i class="social ti-facebook"></i></a>

<!-- Twitter -->
<a href="http://twitter.com/share?url={{$url}}&text=Simple Share Buttons&hashtags=simplesharebuttons" target="_blank"><i class="social ti-twitter"></i></a>

<!-- Google+ -->
<a href="https://plus.google.com/share?url={{$url}}" target="_blank"><i class="social ti-google"></i></a>

<!-- Digg -->
<!-- <a href="http://www.digg.com/submit?url={{$url}}" target="_blank">Digg</a> -->

<!-- Reddit -->
<!-- <a href="http://reddit.com/submit?url={{$url}}&title=Simple Share Buttons" target="_blank">Reddit</a> -->

<!-- LinkedIn -->
<a href="http://www.linkedin.com/shareArticle?mini=true&url={{$url}}" target="_blank"><i class="social ti-linkedin"></i></a>

<!-- Pinterest -->
<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="social pinterest"></i></a>

<!-- StumbleUpon-->
<!-- <a href="http://www.stumbleupon.com/submit?url={{$url}}&title=Simple Share Buttons" target="_blank">StumbleUpon</a> -->

<!-- Email -->
<a href="mailto:?Subject=Simple Share Buttons&Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{$url}}"><i class="social ti-envelope"></i></a>
</div>



                     </div>
                 </div>
           
            
            </div>
         </div>
         <div class="product-area pt-40 pb-80 d-none">
            <div class="container">
               <div class="login-register-wrapper">
                  <div class="resent-tab-tab-list nav">
                      <a class="active border-right-black" data-toggle="tab" href="#lg6">
                          <h4>  You may also like </h4>
                      </a>
                      <a data-toggle="tab" href="#lg7">
                          <h4> Recently Viewed </h4>
                      </a>
                  </div>
                  <div class="tab-content">
                      <div id="lg6" class="tab-pane active">
                         <div class="col-md-12 mt-50">
                           <div class="owl-slider">
                              <div id="carousel3" class="owl-carousel">
                                @foreach($campaigns as $camp)
                                 <div class="item">
                                    <div class="product-wrapper white-box">
                                       <p class="pt-10 text-center brown-text"><a href="#" >&nbsp;</a></p>
                                       <div class="star-top">
                                        @php $check=0; @endphp
                                       </div>
                                       <a href="{{ route('front.campaign') }}">
                                       <div class="product-img">
                                           @php $images = json_decode($campaign->product_images); @endphp
                                            <img src="{{ asset('public/assets/images/campaigns/'.$images[0]) }}" alt="">
                                       </div>
                                       </a>

                                       <div class="product-content text-center">
                                            <h4><a href="{{ route('front.campaign') }}">{{ $camp->product_title }}</a></h4>
                                            <div class="product-price">
                                                <span>{{DB::table('currencies')->where(['is_default' => 1])->first()->sign}} {{ number_format($camp->prize_worth) }}</span>
                                            </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                              </div>
                           </div>

                         </div>
                      </div>
                      <div id="lg7" class="tab-pane">
                        <div class="col-md-12 mt-50">
                           <div class="owl-slider">
                              <div id="carousel4" class="owl-carousel">

                                 @foreach($recentlyViewed as $prod)
                                 <div class="item">
                                    <div class="product-wrapper white-box">
                                       <p class="pt-10 text-center brown-text"><a href="#" ><span></span>New Brand</a></p>
                                       <div class="star-top">
                                        @php $check=0; @endphp
                                        @foreach($wishs as $wishlist)
                                            @if($wishlist->product_id == $prod->id)
                                            @php $check=1; @endphp
                                            @endif
                                        @endforeach
                                           <a href="javascript:;" class="add-to-wish" id="{{ $prod->id }}" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($check == 0) ion-ios-star-outline @else ion-ios-star @endif"></i></a>
                                       </div>
                                       <div class="product-imga">
                                          <a href="{{ route('front.product',$prod->slug) }}">
                                              <img src="{{ $prod->thumbnail ? asset('public/assets/images/thumbnails/'.$prod->thumbnail):asset('public/assets/images/noimage.png') }}" alt="">
                                          </a>
                                      </div>
                                       <div class="product-content text-center">
                                          <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName()}}</a></h4>
                                          <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->category->name }}</a></h4>
                                          <div class="product-price">
                                              <span>{{ $prod->setCurrency() }}</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                                 
                              </div>
                           </div>

                         </div>
                      
                      </div>
                  </div>
              </div>
              
         </div>
        <!-- <div class="banner-area hm1-banner product-footer-bg mt-50">
            <div class="container">
               <div class="row  pb-0">
                  <div class="col-md-6 col-lg-6">
                     <div class="banner-wrapper mrg-mb-md ">
                        <div class="product-footer-box">
                           <h4>Delivery</h4>
                           
                           <p>{{ $gs->delivery_short_desc }}</p>
                              <p class="link-visit"><a href="{{ route('front.page' , 'delivery') }}" class="">Find Out More </a></p>
                              <h4 class="mt-60">Returns</h4>
                           <p>{{ $gs->return_short_desc }}</p>
                           
                              <p class="link-visit"><a href="{{ route('front.page' , 'return') }}" class="">Find Out More </a></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2 col-lg-2"></div>
                  <div class="col-md-4 col-lg-4 pt-0 pb-30">
                     <div class="luxwatch-box">
                        <div class="big-title-lux">Delivery & <br/> Returns</div>
                        <p class="mt-120 link-visit"><a href="{{ route('front.page' , 'return') }}" class="">Find Out More </a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>-->
         </div>
         @include('front.chunks.footer')
@endsection
@section('scripts')
<script>
        var quantity = {{$campaign->quantity}};
        var sold_out = {{$campaign->sold_out}};
        var remaining = parseInt(quantity) - parseInt(sold_out);

        var selected = false;
        $(document).ready(function(){
            $('.select').click(function(){
                $('.qttotal').val($(this).html());
            });
        });


        $(".select").on("mouseover", function () {
            if(!selected){
                var discount_value = $(this).data('value');
                var quantity = $(this).text();
                var product_price = $('#product_price').val();
                var currency_sign = $('#currency_sign').val();
                var currency_value = $('#currency_value').val();
                var ticket = $('#ticket').val();

                product_price = product_price * currency_value;  

                var subtotal = Math.floor(product_price * quantity);
                var discount = Math.floor(discount_value/100 * subtotal);
                var grandtotal = Math.floor(subtotal - discount);
                $('#discount-box').css('display','block');
                $('#subtotal').text(currency_sign+ " " +subtotal.toLocaleString());
                $('#discount').text(currency_sign+ " " +discount.toLocaleString());
                $('#ticket').text(quantity);
                $('#discount').val(discount_value);
                $('#grandtotal').text(currency_sign+ " " +grandtotal.toLocaleString());

                $('#discount_percentage').text("("+discount_value+")%");            

                if(quantity > remaining && remaining != 0){
                      $('#discount-box').css('display','none');  
                      $('#remaining_box').css('display','block');
                      $('#remaining_box').text('Sorry ' +remaining + ' Tickets Available');  
                }else{
                      $('#discount-box').css('display','block');  
                      $('#remaining_box').css('display','none');  
                }
            }
        });

        $('.select').click(function(){
            if(selected){
                $('.select').each(function(index,item){
                    $(this).css('background','#ffffff');                    
                })                
            }
            $(this).css('background','#AB8E66');
            selected = true;


            var discount_value = $(this).data('value');
            var quantity = $(this).text();
            var product_price = $('#product_price').val();
            var currency_sign = $('#currency_sign').val();
            var currency_value = $('#currency_value').val();
            var ticket = $('#ticket').val();

            product_price = product_price * currency_value;  

            var subtotal = Math.floor(product_price * quantity);
            var discount = Math.floor(discount_value/100 * subtotal);
            var grandtotal = Math.floor(subtotal - discount);
            $('#discount-box').css('display','block');
            $('#subtotal').text(currency_sign+ " " +subtotal.toLocaleString());
            $('#discount').text(currency_sign+ " " +discount.toLocaleString());
            $('#ticket').text(quantity);
            $('#grandtotal').text(currency_sign+ " " +grandtotal.toLocaleString());
            $('#discount_percentage').text("("+discount_value+")%");            


            $('#discount_send').val(discount);
            $('#discount_value_send').val(discount_value);
            $('#subtotal_send').val(subtotal);
            $('#grandtotal_send').val(grandtotal);

            $('.qttotal').val(quantity);

                if(quantity > remaining && remaining != 0){
                    $('#discount-box').css('display','none');  
                    $('#remaining_box').css('display','block');
                    $('#remaining_box').text('Sorry ' +remaining + ' Tickets Available');  
                    $('#addcampcrt').attr('disabled',true);
                }else{
                    $('#discount-box').css('display','block');  
                    $('#remaining_box').css('display','none');  
                    $('#addcampcrt').attr('disabled',false);
                }

        });
        
        // $('.progress-bar-striped').css('width','100% !important');
        var i = 0;
        var counterBack = setInterval(function(){
          if (i < {{$width}}){
            $('.progress-bar-striped').css('width', i+'%');
          } else {
            clearInterval(counterBack);
          }
          i++;
        }, 50);
    </script>
@endsection