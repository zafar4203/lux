@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
     
<div class="header-height"></div>
            <!-- main-search start -->
            <div class="main-search-active">
                <div class="sidebar-search-icon">
                    <button class="search-close"><span class="ti-close"></span></button>
                </div>
                <div class="sidebar-search-input">
                    <form>
                        <div class="form-search">
                            <input id="search" class="input-text" value="" placeholder="Search Entire Store" type="search">
                            <button>
                                <i class="ti-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
          
          
            <div class="about-us-area hm-3-padding pt-125 pb-125">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="about-us-title">
                                <div class="banner-wrapper mrg-mb-md">
                                    <div class="banner-img" data-toggle="modal" data-target="#exampleModal2" href="#">
                                       <img src="{{ asset('public/assets/images/how/'.$how->image) }}" alt="">
                                    </div>
                                 </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="about-us-details">
                                <div class="about-us-title">
                                    <h1>How it Works </h1>
                                </div>
                                <p class="about-us-pera-mb">{!! $how->para_1 !!}</p>
                                <p>{!! $how->para_2 !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="services-area hm-3-padding h3-services pb-100">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single-services orange mb-30">
                               
                                <div class="services-text">
                                    <h5>{{ $how->box_1_heading }}</h5>
                                    <p>{!! $how->box_1_details !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single-services yellow mb-30">
                            
                                <div class="services-text">
                                    <h5>{{ $how->box_2_heading }}</h5>
                                    <p>{!! $how->box_2_details !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single-services purple mb-30">
                             
                                <div class="services-text">
                                    <h5>{{ $how->box_3_heading }}</h5>
                                    <p>{!! $how->box_3_details !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single-services sky mb-30">
                              
                                <div class="services-text">
                                    <h5>{{ $how->box_4_heading }}</h5>
                                    <p>{!! $how->box_4_details !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                  

           @include('front.chunks.footer')
@endsection