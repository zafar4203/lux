@extends('layouts.front')
@section('title' , 'Login')
@section('styles')
    <style>
    
    
.login-bg {
background-image: url({{ asset("assets/images/".$gs->header_login) }});
        background-position: top center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
}

        .pl-0{
            padding-left:0px !important;
        }
        .pr-0{
            padding-right:0px !important;
        }
        .register-form form .group-input select{
            border: 1px solid #e7ab3c;
            width:100%;    
            border-right:0px;
            padding-left:10px;
            border-right:0px;
        }
        .nice-select {
            height:46px !important;
        }



.login-form-container input[type="number"]{
    width:100%;
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
}

.login-form-container select {
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
    border-right:0px;
}
    </style>
@endsection

@section('class', 'login-bg')

@section('content')
<div class="header-height"></div>
         <div class="login-register-area ptb-130 hm-3-padding">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-7 ml-auto mr-auto">
                     <div class="login-register-wrapper">
                        <div class="login-register-tab-list nav">
                           <a class="active" data-toggle="tab" id="log" href="#lg1">
                              <h4> Sign In </h4>
                           </a>
                           <a data-toggle="tab" id="reg" href="#lg2">
                              <h4> Register </h4>
                           </a>
                        </div>
                        <div class="tab-content">
                           <div id="lg1" class="tab-pane active">
                              <div class="login-form-container">
                                 <div class="login-form login-register-form signin-form">
                                 @include('includes.admin.form-login')
                                 <form class="mloginform" action="{{ route('user.login.submit') }}" method="post">
                                 @csrf
                                       <div class="form-group">
                                          <label>Email Address</label>
                                          <input type="text" name="email" required />
                                       </div>
                                       <div class="form-group">
                                          <label>Password</label>
                                          <input type="password" name="password" required />
                                       </div>
                                       <div class="button-box">
                                          <div class="login-toggle-btn">
                                             <input type="checkbox" id="save-pass" name="remember" id="mrp" {{ old('remember') ? 'checked' : '' }}>
                                             <label>Remember me</label>
                                             <a href="{{ route('user-forgot') }}">Forgot Password?</a>
                                          </div>
                                          <p class="text-center"> <button type="submit" class="btn-style2 cr-btn"><span>Sign in</span></button></p>
                                          <p class="big-font">Don't Have An ONLINE Account</p>
                                          <p class="text-center link-login"> <a data-toggle="tab" id="lg-reg" href="#lg2">Register now</a></p>
                                       </div>
                                       <input type="hidden" name="modal" value="1">
                                        <input class="mauthdata" type="hidden" value="{{ $langg->lang177 }}">     
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <div id="lg2" class="tab-pane">
                              <div class="login-form-container">
                                 <div class="login-form login-register-form signup-form">
                                     @include('includes.admin.form-login')
                                    <form id="registerform" action="{{route('user-register-submit')}}" method="post">
                                    {{ csrf_field() }}
                                       <div class="form-group">
                                          <label>First Name</label>
                                          <input type="text" name="f_name" required />
                                       </div>
                                       <div class="form-group">
                                          <label>Last Name</label>
                                          <input type="text" name="l_name" required />
                                       </div>
                                       

                        <div class="row">
                            <div class="col-md-12">
                                <label>Phone Number</label>
                            </div>
                                <div class="col-md-3 mobile-small-size pr-0">
                                    <div class="form-group">
                                    <select name="country_code" id="" required>
                                    @foreach($countries as $country)
                                        <option value="{{$country->phone_code}}">{{$country->country_code}} ({{$country->phone_code}})</option>
                                    @endforeach
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-9 pl-0 mobile-large-size">
                                    <div class="form-group">
                                        <input type="number" name="phone" placeholder="Enter Phone Number" value="{{ old('phone') }}" id="phone" required>
                                    </div>
                                </div>
                            </div>

                                       <div class="form-group">
                                          <label>Email Address</label>
                                          <input type="text" name="email" required />
                                       </div>
                                       <div class="form-group">
                                          <label>Password</label>
                                          <input type="password" name="password" required />
                                       </div>
                                       <div class="form-group">
                                          <label>Confirm Password</label>
                                          <input type="password" name="password_confirmation" required />
                                       </div>
                                       <div class="form-group">
                                          <h5>Terms and Condition</h5>
                                          <div class="form-check neg-check"> 
                                             <input class="form-check-input" type="checkbox">
                                             <label class="form-check-label neg-check-2" >I agree to the <a href=""><u>Terms and conditions</u></a></label>                                        
                                          </div>

                                       <p>
                                       <h5>Communication</h5>
                                       <span>If you would like to stay up to date on all the latest products, competitions and events please tick below and agree to receiving marketing material.</span>
                                       </p>
                                       
                                          <div class="form-check pl-2">
                                             <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                             <label class="form-check-label" for="gridRadios1">
                                             Please keep me updated
                                             </label>
                                          </div>
                                          <div class="form-check pl-2">
                                             <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                             <label class="form-check-label" for="gridRadios2">
                                             No, thank you
                                             </label>
                                          </div>
                                       </div>

                                       <p class="text-center">
                                          <button type="submit" class="btn-style2 cr-btn"><span>Register</span></button>
                                       </p>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

@endsection


@section('scripts')
<script>
    $('#log').click(function(){
        $('#lg2').css('display','none');
        $('#lg1').css('display','block');
    });

    $('#reg').click(function(){
        $('#lg2').css('display','block');
        $('#lg1').css('display','none');
    });
    
    $('#lg-reg').click(function(){
        $('#lg2').css('display','block');
        $('#lg1').css('display','none');
        $('#reg').addClass('active');
        $('#log').removeClass('active');
    });

</script>
@endsection