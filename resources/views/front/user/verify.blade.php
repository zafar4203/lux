@extends('layouts.front')

@section('title' , 'Login')

@section('styles')
    <style>

.login-form-container input[type="number"]{
    width:100%;
    background-color: #fff;
    border: medium none;
    color: #7d7d7d;
    font-size: 14px;
    font-weight: 500;
    height: 50px;
    padding: 0 15px;
    margin-bottom: 4px;
    border: 1px solid #A6A6A6;
}

.login-register-tab-list.nav a{
    width:100%;
}
    </style>
@endsection
@section('class', 'login-bg')
@section('content')


    <div class="header-height"></div>
         <div class="login-register-area ptb-130 hm-3-padding">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-7 ml-auto mr-auto">
                     <div class="login-register-wrapper">

                     <div class="login-register-tab-list nav">
                           <a class="active">
                              <h4> Verify Code </h4>
                           </a>
                        </div>

                        <div class="tab-content">
                              <div class="login-form-container">
                                 <div class="">
                                 @include('includes.admin.form-login')

                                    @if(!session()->has('error'))
                                        <div class="alert alert-info">
                                            <p class="text-left">     
                                                Please Enter Verification Code Sent to you.         
                                            </p> 
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                    <div class="alert alert-danger">
                                    <button type="button" class="close alert-close"><span>×</span></button>
                                        <p class="text-left">
                                            {{ session()->get('error') }}
                                        </p> 
                                    </div>
                                    @endif

                                    <form action="{{ route('verify-account') }}" method="post">
                                    @csrf
                                       <div class="form-group">
                                          <label>Code to Verify</label>
                                          <input type="number" name="code" id="code" required>
                                       </div>
                                       <div class="button-box">
                                          <p class="text-center"> <button type="submit" class="btn-style2 cr-btn"><span>Verify Code</span></button></p>
                                       </div>
                                       <input type="hidden" name="modal" value="1">
                                        <input class="mauthdata" type="hidden" value="{{ $langg->lang177 }}">     
                                    </form>
                                 </div>
                              </div>
                           </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>



@endsection
@section('scripts')
<script>

</script>
@endsection