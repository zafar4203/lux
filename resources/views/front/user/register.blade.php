@extends('layouts.front');
@section('title' , 'Register');
@section('styles')
    <style>
        .red{
            color:red;
        }

        .register-login-section {
            padding-top:0px !important;
        }
        .register-form form .group-input input{
            border:1px solid #e7ab3c;
        }
        #pswd-div i ,#confirm-pswd-div i {
            position:absolute;
            bottom:12px;
            right:15px;
            font-size:20px;
            cursor: pointer;
        }

        .breacrumb-section {
            padding-top: 120px !important;
        }

        .register-form form .group-input input{
            height: 35px;
        }
        .register-form form .group-input, .login-form form .group-input {
            margin-bottom: 5px;
        }
        .register-form form .group-input label {
            margin-bottom: 0px !important;
        }
        .register-form h2{
            margin-bottom:10px;
        }
        .signup-form .switch-login {
            text-align: center;
            margin-top: 10px !important;
        }
        .register-form form .group-input select{
            height: 35px;
            border: 1px solid #e7ab3c;
            width:100%;    
            border-right:0px;
            padding-left:10px;
        }

        .register-btn{
            height: 35px;
            padding-top:6px !important;
        }
        .pl-0{
            padding-left:0px !important;
        }

        .pr-0{
            padding-right:0px !important;
        }
        </style>

@endsection
@section('content')
 <!-- Breadcrumb Section Begin -->
 <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href=""><!--<i class="fa fa-home"></i>--> Home</a>
                        <span>Register</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Register Section Begin -->
    <div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="register-form signup-form">
                        <h2>Register</h2>
                        @include('includes.admin.form-login')
                        <form id="registerform" action="{{route('user-register-submit')}}" method="post">
                            {{ csrf_field() }}
                            <div class="group-input">
                                <label for="username">Name *</label>
                                @if($errors->has('name'))
                                    <span class="red small-text">{{ $errors->first('name') }}</span>
                                @endif
                                <input type="text" name="name" placeholder="Enter Your Name" value="{{ old('name') }}" id="username" required>
                            </div>

                            <div class="group-input">
                                <label for="email">Email address *</label>
                                @if($errors->has('email'))
                                    <span class="red small-text">{{ $errors->first('email') }}</span>
                                @endif
                                <input type="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}" id="email" required>
                            </div>

                            <div class="group-input">
                                <label for="email">Phone * </label>
                                @if($errors->has('phone'))
                                    <span class="red small-text">{{ $errors->first('phone') }}</span>
                                @endif
                                <div class="row">
                                    <div class="col-md-3 pr-0">
                                        <select name="country_code" id="">
                                            <option value="971">UAE (971)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9 pl-0">
                                        <input type="number" name="phone" placeholder="Enter Phone Number" value="{{ old('phone') }}" id="phone" required>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div id="pswd-div" class="group-input">
                                        <label for="password">Password *</label>
                                        @if($errors->has('password'))
                                            <span class="red small-text">{{ $errors->first('password') }}</span>
                                        @endif
                                        <input type="password" name="password" placeholder="Password" id="password" required>
                                        <i class="fa fa-eye" id="togglePassword"></i>
                                    </div>                                
                                </div>
                                <div class="col-md-6">
                                    <div id="confirm-pswd-div" class="group-input">
                                        <label for="confirmPassword">Confirm Password *</label>
                                        <input type="password" name="password_confirmation" placeholder="Confirm Password" id="confirmPassword" required>
                                        <i class="fa fa-eye" id="confirmTogglePassword"></i>
                                    </div>                                    
                                    </div>
                                </div>
                            <button type="button" id="submit" class="site-btn register-btn">REGISTER</button>
                        </form>
                        <div class="switch-login">
                            <a href="{{ route('user.login') }}" class="or-login">Or Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->
@endsection
@section('scripts')
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');    

        const ctogglePassword = document.querySelector('#confirmTogglePassword');
        const cpassword = document.querySelector('#confirmPassword');    
        
        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });


        ctogglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = cpassword.getAttribute('type') === 'password' ? 'text' : 'password';
            cpassword.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });

        $('#submit').click(function(){
            if(isEmail($('#email').val())){                
                $('#submit').submit();
            }else{
                alert("Please provide valid Email");
            }
        });

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
            return regex.test(email);
        }
    </script>
@endsection