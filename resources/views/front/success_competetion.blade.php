@extends('layouts.front')
@section('styles')
    <style>
    
        a.link:hover{
            color:#007bff;
        }
        .header-height{
            margin-top:10%;
        }
        .black-headero{
            display:none;
        }
    </style>
@endsection
@section('content')

@include('front.chunks.header_black')

<div class="header-height"></div>

<section class="tempcart">


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <!-- Starting of Dashboard data-table area -->
<div class="content-box section-padding add-product-1">
    <div class="top-area">
            <div class="content">
                <h4 class="heading">
                    {{ $langg->order_title }}
                </h4>
                <p class="text">
                    {{ $langg->order_text }}
                </p>
                <a href="{{ route('front.index') }}" class="link">{{ $langg->lang170 }}</a>
                </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

                <div class="product__header">
                    <div class="row reorder-xs">
                        <div class="col-lg-12">
                            <div class="product-header-title">
                                <h2>{{ $langg->lang285 }} {{$order->order_number}}</h2>
                    </div>   
                </div>
                    @include('includes.form-success')
                        <div class="col-md-12" id="tempview">
                            <div class="dashboard-content">
                                <div class="view-order-page" id="print">
                                    <p class="order-date">{{ $langg->lang301 }} {{date('d-M-Y',strtotime($order->created_at))}}</p>

    <div class="billing-add-area">
        <div class="row">
            <div class="col-md-6">
                <h5>{{ $langg->lang287 }}</h5>
                <address>
                    {{ $langg->lang288 }} {{$order->customer_name}}<br>
                    {{ $langg->lang289 }} {{$order->customer_email}}<br>
                    {{ $langg->lang290 }} {{$order->customer_phone}}<br>
                    {{ $langg->lang291 }} {{$order->customer_address}}<br>
                    {{$order->customer_city}}-{{$order->customer_zip}}
                </address>
            </div>
            <div class="col-md-6">
                <h5>{{ $langg->lang292 }}</h5>
                <p>{{ $langg->lang293 }} {{$order->currency_sign}} {{ number_format(round($order->pay_amount * $order->currency_value , 2)) }}</p>
                <p>{{ $langg->lang294 }} {{$order->method}}</p>

                    {{$order->method}} {{ $langg->lang295 }} <p>{{$order->charge_id}}</p>
                    {{$order->method}} {{ $langg->lang296 }} <p id="ttn">{{$order->txnid}}</p>
            </div>
        </div>
    </div>

                    <br>
                    <div class="table-responsive">
                            <table  class="table">
                                <h4 class="text-center">{{ $langg->lang308 }}</h4>
                                <thead>
                                <tr>

                                    <th width="30%">{{ $langg->lang310 }}</th>
                                    <th width="10%">Quantity</th>
                                    <th width="10%">Price</th>
                                    <th width="20%">Sub Total</th>
                                    <th width="20%">Discount <small>({{ $order->discount_value }})%</small></th>
                                    <th width="10%">Total</th>
                                </tr>
                                </thead>
                                <tbody>

            <tr>
                <td>{{$order->campaign->name}}</td>
                <td>{{$order->totalQty}}</td>
                <td>{{$order->currency_sign}} {{number_format(App\Models\Product::convertOnlyPrice($campaign->product_price))}}</td>
                @php 
                    $subtotal = App\Models\Product::convertOnlyPrice($campaign->product_price * $order->totalQty);
                    $d_value = (int) $order->discount_value;                    
                    $discount = $d_value/100 * (float)$subtotal;
                    $total = round((float)$subtotal - (float)$discount , 0);
                @endphp
                <td>{{$order->currency_sign}} {{number_format($subtotal)}}</td>
                <td>{{$order->currency_sign}} {{number_format($discount)}}</td>
                <td>{{$order->currency_sign}} {{number_format($total)}}</td>
            </tr>



                                </tbody>
                            </table>

                                                        </div>



                    <br>


                    <div class="table-responsive">
                            <table  class="table">
                                <h4 class="text-center">Tickets</h4>
                                <thead>
                                <tr>
                                    <th width="10%">id</th>
                                    <th width="10%">Status</th>
                                    <th width="20%">Campaign</th>
                                    <th width="40%">Uuid</th>
                                    <th width="20%">Draw Date</th>
                                </tr>
                                </thead>
                                <tbody>
            @php $i=1; @endphp                    
            @foreach($order->tickets as $ticket)
            <tr>
                <td>{{$i}}</td>
                <td>@if($ticket->status == 0) <span class="badge badge-success"> Active </span> @else <span class="badge badge-danger"> In Active </span> @endif</td>
                <td>{{$order->campaign->name}}</td>
                <td>{{$ticket->uuid}}</td>
                <td>{{$order->campaign->draw_date}}</td>
            </tr>
            @php $i++; @endphp                    
            @endforeach



                                </tbody>
                            </table>

                                                        </div>






                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                <!-- Ending of Dashboard data-table area -->
            </div>


  </section>

   @include('front.chunks.footer')
@endsection