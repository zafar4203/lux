@extends('layouts.front')
@section('styles')
 
@endsection

@section('content')
<div class="header-height"></div>
    
      
         <div class="white-bg mt-120" style="z-index: inherit;">
         <div class="product-cart-area hm-3-padding  pb-80 cart-content">
            <div class="shop-page shop-page-sidebar bg-off-white pt-10">
                <div class="container">
                    <!-- Added temporary by Prashant - 12-11-2020 -->
                    <div class="row mb-4">
                                <div class="col-md-6">
                                    <div class="breadcrumb-content text-left">
                                        <ul>

                    <li><a href="{{route('front.index')}}">{{ $langg->lang17 }}</a></li>
                      @if (!empty($cat))
                        @if (!empty($subcat))
                        <li>
                          <a href="{{route('front.category', $cat->slug)}}">{{ $cat->name }}</a>  
                        </li>                       
                        @else
                        <li>{{ $cat->name }}</li>
                        @endif
                      @endif
                      @if (!empty($subcat))
                        @if (!empty($childcat))
                        <li>
                          <a href="{{route('front.category', [$cat->slug, $subcat->slug])}}">{{ $subcat->name }}</a>
                        </li>
                          @else
                          <li>{{ $subcat->name }}</li>
                        @endif
                      @endif
                      @if (!empty($childcat))
                          <li>{{ $childcat->name }}</li>
                      @endif
                      @if (empty($childcat) && empty($subcat) && empty($cat))
                      <li><a href="{{route('front.category')}}">{{ $langg->lang36 }}</a></li>
                      @endif



                                        </ul>
                                     </div>
                                </div>
                                <div class="col-md-6 text-center text-md-right mt-3 mt-md-0 mobile-jc-between"><big>{{count($prods)}} products Found</big> <a class="mobile-filter" data-toggle="modal" data-target="#exampleModalRight"><img src="http://fastvisaservicesdubai.com/Demo/LuxV3/public/assets/front/images/icons/filter-icon.png"></a> </div>
                            </div>
                    <!-- Added temporary by Prashant - 12-11-2020 -->
                    <div class="row  pr-lux">
                        
                        <div class="col-md-9 col-xs-12 shop-products order-2 order-md-1">
                    <!--        <div class="row ">-->
                    <!--            <div class="col-md-6">-->
                    <!--                <div class="breadcrumb-content text-left">-->
                    <!--                    <ul>-->

                    <!--<li><a href="{{route('front.index')}}">{{ $langg->lang17 }}</a></li>-->
                    <!--  @if (!empty($cat))-->
                    <!--    @if (!empty($subcat))-->
                    <!--    <li>-->
                    <!--      <a href="{{route('front.category', $cat->slug)}}">{{ $cat->name }}</a>  -->
                    <!--    </li>                       -->
                    <!--    @else-->
                    <!--    <li>{{ $cat->name }}</li>-->
                    <!--    @endif-->
                    <!--  @endif-->
                    <!--  @if (!empty($subcat))-->
                    <!--    @if (!empty($childcat))-->
                    <!--    <li>-->
                    <!--      <a href="{{route('front.category', [$cat->slug, $subcat->slug])}}">{{ $subcat->name }}</a>-->
                    <!--    </li>-->
                    <!--      @else-->
                    <!--      <li>{{ $subcat->name }}</li>-->
                    <!--    @endif-->
                    <!--  @endif-->
                    <!--  @if (!empty($childcat))-->
                    <!--      <li>{{ $childcat->name }}</li>-->
                    <!--  @endif-->
                    <!--  @if (empty($childcat) && empty($subcat) && empty($cat))-->
                    <!--  <li><a href="{{route('front.category')}}">{{ $langg->lang36 }}</a></li>-->
                    <!--  @endif-->



                    <!--                    </ul>-->
                    <!--                 </div>-->
                    <!--            </div>-->
                    <!--            <div class="col-md-6 text-center text-md-right mt-3 mt-md-0"><big>{{count($prods)}} products Found</big></div>-->
                    <!--        </div>-->
                           <div class="row equal " id="ajaxContent"> <!-- pt-40  temporary removed -->
                               <!---product-->
                               <div class="col-md-4 lux-box-div">
                                <div class="product-wrapper white-box">                                
                                    @if(!empty($cat))
                                        {{ $cat->name }}
                                    @endif                                
                                 </div>
                               </div>
                                <!---end product-->
                                    @include('includes.product.filtered-products')
                                 <!---product-->
                           </div>
                        </div>
                        @include('includes.catalog')
                       
                    </div>
                    <!-- <div class="row">
                     <div class="col-md-12">
                        <div class="pagination-style text-center mt-30">
                           <ul>
                               <li>
                                   <a class="active" href="#">1</a>
                               </li>
                               <li>
                                   <a href="#">2</a>
                               </li>
                               <li>
                                   <a href="#">3</a>
                               </li>
                               <li>
                                   <a href="#">
                                       <i class="ion-chevron-right"></i>
                                   </a>
                               </li>
                           </ul>
                       </div>
                     </div>
                  </div> -->
                </div>
            </div>
           
         </div>

         @include('front.chunks.footer')

@endsection

@section('scripts')
<script>

  $(document).ready(function() {
    var brands = [];
    // when dynamic attribute changes
    $(".attribute-input, #sortby").on('change', function() {
      $("#ajaxLoader").show();
      filter();
    });

    $('input[name="brand[]"]').on('change', function (e) {
        e.preventDefault();
        brands = [];
        $('input[name="brand[]"]:checked').each(function(){
            brands.push($(this).val());
        });
        $("#ajaxLoader").show();        
        filter();
    });

    // when price changed & clicked in search button
    $(".filter-btn").on('click', function(e) {
      e.preventDefault();
      $("#ajaxLoader").show();
      filter();
    });

    $('input[type=radio][name=sort]').change(function() {
        filter();
    });

  function filter() {
    let filterlink = '';

    if ($("#prod_name").val() != '' && $("#prod_name").val() != undefined) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?search='+$("#prod_name").val();
      } else {
        filterlink += '&search='+$("#prod_name").val();
      }
    }

    if (brands.length > 0) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+'brands='+brands;
      } else {
        filterlink += '&brands='+brands;
      }
    }

    $(".attribute-input").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });

    if ($('input[name="sort"]:checked').val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$('input[name="sort"]:checked').attr('name')+'='+$('input[name="sort"]:checked').val();
      } else {
        filterlink += '&'+$('input[name="sort"]:checked').attr('name')+'='+$('input[name="sort"]:checked').val();
      }
    }

    if ($("#minamount").val() != '' && $("#minamount").val() != undefined) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#minamount").attr('name')+'='+$("#minamount").val();
      } else {
        filterlink += '&'+$("#minamount").attr('name')+'='+$("#minamount").val();
      }
    }

    if ($("#maxamount").val() != '' && $("#maxamount").val() != undefined) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      } else {
        filterlink += '&'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      }
    }

    // console.log(filterlink);
    console.log(encodeURI(filterlink));
    $("#ajaxContent").load(encodeURI(filterlink), function(data) {
      // add query string to pagination
      addToPagination();
      $("#ajaxLoader").fadeOut(1000);
    });
  }

  // append parameters to pagination links
  function addToPagination() {
    // add to attributes in pagination links
    $('ul.pagination li a').each(function() {
      let url = $(this).attr('href');
      let queryString = '?' + url.split('?')[1]; // "?page=1234...."

      let urlParams = new URLSearchParams(queryString);
      let page = urlParams.get('page'); // value of 'page' parameter

      let fullUrl = '{{route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')])}}?page='+page;

      $(".attribute-input").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      if ($('input[name="sort"]:checked').val() != '') {
        fullUrl += '&sort='+encodeURI($('input[name="sort"]:checked').val());
      }

      if ($("#minamount").val() != '' && $("#minamount").val() != undefined) {
        fullUrl += '&min='+encodeURI($("#minamount").val());
      }

      if ($("#maxamount").val() != '' && $("#maxamount").val() != undefined) {
        fullUrl += '&max='+encodeURI($("#maxamount").val());
      }

      $(this).attr('href', fullUrl);
    });
  }

  $(document).on('click', '.categori-item-area .pagination li a', function (event) {
    event.preventDefault();
    if ($(this).attr('href') != '#' && $(this).attr('href')) {
      $('#preloader').show();
      $('#ajaxContent').load($(this).attr('href'), function (response, status, xhr) {
        if (status == "success") {
          $('#preloader').fadeOut();
          $("html,body").animate({
            scrollTop: 0
          }, 1);

          addToPagination();
        }
      });
    }
  });
});

</script>
@endsection