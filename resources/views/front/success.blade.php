@extends('layouts.front')
@section('styles')
    <style>
    
        a.link:hover{
            color:#007bff;
        }
        .header-height{
            margin-top:10%;
        }
        .black-headero{
            display:none;
        }
    </style>
@endsection
@section('content')
@include('front.chunks.header_black')
<div class="header-height"></div>

<section class="tempcart custom-resp-pt-110"> <!-- custom-resp-pt-110 added by Prashant -->

@if(!empty($tempcart))

        <div class="container">
            
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <a href="{{ route('front.index') }}" class="link">{{ $langg->lang170 }}</a>
                    <!-- Starting of Dashboard data-table area -->
<div class="content-box section-padding add-product-1 pt-5">
    <div class="top-area mb-4">
            <div class="content">
                <h4 class="heading">
                    {{ $langg->order_title }}
                </h4>
                <p class="text">
                    {{ $langg->order_text }}
                </p>
                
                </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

                <div class="product__header">
                    <div class="row reorder-xs">
                        <div class="col-lg-12">
                            <div class="product-header-title">
                                <h2>{{ $langg->lang285 }} {{$order->order_number}}</h2>
                    </div>   
                </div>
                    @include('includes.form-success')
                        <div class="col-md-12" id="tempview">
                            <div class="dashboard-content">
                                <div class="view-order-page" id="print">
                                    <p class="order-date">{{ $langg->lang301 }} {{date('d-M-Y',strtotime($order->created_at))}}</p>


@if($order->dp == 1)

    <div class="billing-add-area">
        <div class="row">
            <div class="col-md-6">
                <h5>{{ $langg->lang287 }}</h5>
                <address>
                    {{ $langg->lang288 }} {{$order->customer_name}}<br>
                    {{ $langg->lang289 }} {{$order->customer_email}}<br>
                    {{ $langg->lang290 }} {{$order->customer_phone}}<br>
                    {{ $langg->lang291 }} {{$order->customer_address}}<br>
                    {{$order->customer_city}}-{{$order->customer_zip}}
                </address>
            </div>
            <div class="col-md-6">
                <h5>{{ $langg->lang292 }}</h5>
                <p class="mb-0">{{ $langg->lang293 }} {{$order->currency_sign}}{{ round($order->pay_amount * $order->currency_value , 0) }}</p>
                <p>{{ $langg->lang294 }} {{$order->method}}</p>

                @if($order->method != "Cash On Delivery")
                    @if($order->method=="Stripe")
                        {{$order->method}} {{ $langg->lang295 }} <p>{{$order->charge_id}}</p>
                    @endif
                    {{$order->method}} {{ $langg->lang296 }} <p id="ttn">{{$order->txnid}}</p>

                @endif
            </div>
        </div>
    </div>

@else

<div class="billing-add-area mt-5 pt-2">
    <div class="row">
        <div class="col-md-6 custom-border">
            <h5>{{ $langg->lang287 }}</h5>
            <address>
                {{ $langg->lang288 }} {{$order->customer_name}}<br>
                {{ $langg->lang289 }} {{$order->customer_email}}<br>
                {{ $langg->lang290 }} {{$order->customer_phone}}<br>
                {{ $langg->lang291 }} {{$order->customer_address}}<br>
                {{$order->customer_city}}-{{$order->customer_zip}}
            </address>
        </div>
        <div class="col-md-6 text-left text-md-right">
            <h5>{{ $langg->lang292 }}</h5>
            <p class="mb-0">{{ $langg->lang293 }} {{$order->currency_sign}} {{ number_format(round($order->pay_amount * $order->currency_value , 0)) }}</p>
            <p>{{ $langg->lang294 }} {{$order->method}}</p>

            @if($order->method != "Cash On Delivery")
                @if($order->method=="Stripe")
                    {{$order->method}} {{ $langg->lang295 }} <p>{{$order->charge_id}}</p>
                @endif
                @if($order->method=="Paypal")
                {{$order->method}} {{ $langg->lang296 }} <p id="ttn">{{ isset($_GET['tx']) ? $_GET['tx'] : '' }}</p>
                @else
                {{$order->method}} {{ $langg->lang296 }} <p id="ttn">{{$order->txnid}}</p>
                @endif

            @endif
        </div>
    </div>
</div>
@endif
                                                        <br>
                                                        <div class="table-responsive">
                            <table  class="table">
                                <h4 class="text-center">{{ $langg->lang308 }}</h4>
                                <thead>
                                <tr>

                                    <th width="60%">{{ $langg->lang310 }}</th>
                                    <th width="20%">{{ $langg->lang539 }}</th>
                                    <th width="10%">{{ $langg->lang314 }}</th>
                                    <th width="10%">{{ $langg->lang315 }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                    @foreach($tempcart->items as $product)
                                    <tr>
                                            <td>{{ $product['item']['name'] }}</td>
                                            <td>
                                                <b>{{ $langg->lang311 }}</b>: {{$product['qty']}} <br>
                                                @if(!empty($product['size']))
                                                <b>{{ $langg->lang312 }}</b>: {{ $product['item']['measure'] }}{{str_replace('-',' ',$product['size'])}} <br>
                                                @endif
                                                @if(!empty($product['color']))
                                                <div class="d-flex mt-2">
                                                <b>{{ $langg->lang313 }}</b>:  <span id="color-bar" style="border: 10px solid #{{$product['color'] == "" ? "white" : $product['color']}};"></span>
                                                </div>
                                                @endif
                                                    @if(!empty($product['keys']))
                                                    @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                                                        <b>{{ ucwords(str_replace('_', ' ', $key))  }} : </b> {{ $value }} <br>
                                                    @endforeach
                                                    @endif
                                            </td>
                                            <td>{{$order->currency_sign}} {{number_format(round($product['item']['price'] * $order->currency_value , 0))}}</td>
                                            <td>{{$order->currency_sign}} {{number_format(round($product['price'] * $order->currency_value,0))}}</td>
                                    </tr>
                                    @endforeach



                                </tbody>
                            </table>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                <!-- Ending of Dashboard data-table area -->
            </div>

@endif

  </section>

   @include('front.chunks.footer')
@endsection