@extends('layouts.front')
@section('styles')
  <style>
    .header-height{
      margin-top:10%;
    }
    .slider-2.inner-content-page{
      margin-top:10%;
    margin-bottom: 3%;
    }
  </style>
@endsection
@section('content')
<!--<div class="content-header-height"></div>-->
<section class="about slider-2 inner-content-page">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="about-info">
            <h1 class="title">
              {{ $page->title }}
            </h1>
            <p>
              {!! $page->details !!}
            </p>

          </div>
        </div>
      </div>
    </div>
  </section>
  @include('front.chunks.footer')

@endsection