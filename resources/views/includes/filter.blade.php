<div class="shop-top-bar">
                                <!-- Left Side start -->
                                <div class="shop-tab nav mb-res-sm-15">
<!--
                                    <a class="active" href="" data-toggle="tab">
                                        <i class="fa fa-th show_grid"></i>
                                    </a>
                                    <a href="" data-toggle="tab">
                                        <i class="fa fa-list-ul"></i>
                                    </a>
-->
                                    <p>There Are {{ count($prods) }} Products.</p>
                                </div>
                                <!-- Left Side End -->
                                <!-- Right Side Start -->
                                <div class="select-shoing-wrap">
                                    <div class="shot-product">
                                        <p>Sort By:</p>
                                    </div>
                    <div class="shop-select">
                        <select id="sortby" class="sorting" name="sort">
                            <option value="date_desc">{{$langg->lang65}}</option>
                            <option value="date_asc">{{$langg->lang66}}</option>
                            <option value="price_asc">{{$langg->lang67}}</option>
                            <option value="price_desc">{{$langg->lang68}}</option>            
                        </select>
                    </div>
                                </div>
                                <!-- Right Side End -->
                            </div>