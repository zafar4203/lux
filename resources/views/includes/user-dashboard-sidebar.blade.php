<div class="col-md-4">
                     <div class="left-box-panel">
                       <div class="name-box">
                          <p class="">Hello, </p>
                          <h1>{{ Auth::user()->name }}</h1>

                         
                       </div>
<nav class="account-nav">
            <ul>
                @php 

                  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
                  {
                    $link = "https"; 
                  }
                  else
                  {
                    $link = "http"; 
                      
                    // Here append the common URL characters. 
                    $link .= "://"; 
                      
                    // Append the host(domain name, ip) to the URL. 
                    $link .= $_SERVER['HTTP_HOST']; 
                      
                    // Append the requested resource location to the URL 
                    $link .= $_SERVER['REQUEST_URI']; 
                  }      

                @endphp
             <li>
                <a href="{{ route('user-dashboard') }}" class="{{ $link == route('user-dashboard') ? 'active':'' }}"><i class="ti-home"></i> ACCOUNT HOME
                </a>
              </li>
              <li>
                <a href="{{ route('user-campaigns') }}" class="{{ $link == route('user-campaigns') ? 'active':'' }}"><i class="ti-announcement"></i> Campaigns
                </a>
              </li>
          
              


              <!-- <li><a href="account.html" class="active"><i class="ti-announcement"></i> CAMPAIGNS</a></li>
                  <li ><a href="account.html"><i class="ti-home"></i> ACCOUNT HOME</a></li>
                  <li ><a href="orders-resturns.html"><i class="ti-package"></i> ORDERS & RETURNS</a></li>
                  <li ><a href="manage-address.html"><i class="ti-location-pin"></i> MANAGE ADDRESSES</a></li>
                  <li ><a href="personal-details.html"><i class="ti-user"></i> PERSONAL DETAILS</a></li>
                  <li ><a href="updatepassword.html"><i class="ti-lock"></i> UPDATE PASSWORD </a></li>
                  <li ><a href="paymentcards.html"><i class="ti-credit-card"></i> PAYMENT CARDS</a></li>
                  <li ><a href="communication.html"><i class="ti-comments"></i> COMMUNICATION PREFERNCES </a></li>
                  <li ><a href="signout.html"><i class="ion-log-out"></i> SIGNOUT </a></li> -->




              <li>
                <a class="{{ $link == route('user-orders') ? 'active':'' }}" href="{{ route('user-orders') }}"><i class="ti-package"></i> ORDERS & RETURNS</a>
              </li>

              <!-- @if($gs->is_affilate == 1)

                <li class="{{ $link == route('user-affilate-code') ? 'active':'' }}">
                    <a href="{{ route('user-affilate-code') }}">{{ $langg->lang202 }}</a>
                </li>

                <li class="{{ $link == route('user-wwt-index') ? 'active':'' }}">
                    <a href="{{route('user-wwt-index')}}">{{ $langg->lang203 }}</a>
                </li>

              @endif -->


              <!-- <li class="{{ $link == route('user-order-track') ? 'active':'' }}">
                  <a href="{{route('user-order-track')}}">{{ $langg->lang772 }}</a>
              </li> -->

              <!-- <li class="{{ $link == route('user-messages') ? 'active':'' }}">
                  <a href="{{route('user-messages')}}">{{ $langg->lang232 }}</a>
              </li> -->

              <!-- <li class="{{ $link == route('user-message-index') ? 'active':'' }}">
                  <a href="{{route('user-message-index')}}">{{ $langg->lang204 }}</a>
              </li> -->

              <!-- <li class="{{ $link == route('user-dmessage-index') ? 'active':'' }}">
                  <a href="{{route('user-dmessage-index')}}">{{ $langg->lang250 }}</a>
              </li> -->

              <li>
                <a class="{{ $link == route('user-address') ? 'active':'' }}" href="{{ route('user-address') }}"><i class="ti-location-pin"></i> MANAGE ADDRESSES</a></li>
                </a>
              </li>


              <li>
                <a class="{{ $link == route('user-profile') ? 'active':'' }}" href="{{ route('user-profile') }}"><i class="ti-user"></i> PERSONAL DETAILS</a></li>
                </a>
              </li>

              <li>
               <a href="{{ route('user-reset') }}" class="{{ $link == route('user-reset') ? 'active':'' }}"><i class="ti-lock"></i> UPDATE PASSWORD
               </a>
              </li>

              <!-- <li>
                <a href="{{ route('user-wishlists') }}" class="{{ $link == route('user-wishlists') ? 'active':'' }}">
                  Wishlists
                </a>
              </li> -->


              <!-- <li>
                <a href="{{ route('user-payment-cards') }}" class="{{ $link == route('user-payment-cards') ? 'active':'' }}"><i class="ti-credit-card"></i> PAYMENT CARDS
                </a>
              </li> -->

              <li>
                <a href="{{ route('user-communication') }}" class="{{ $link == route('user-wishlists') ? 'active':'' }}">
                <i class="ti-comments"></i>
                  Communication Preferences
                </a>
              </li>




              <li>
                <a href="{{ route('user-logout') }}"><i class="ion-log-out"></i> SIGNOUT
                </a>
              </li>

          </ul>
  </nav>
  </div>
</div>