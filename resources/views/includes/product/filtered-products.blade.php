@if (count($prods) > 0)
	@foreach ($prods as $key => $prod)
        <!---product-->
    <div class="col-md-4">
        <div class="product-wrapper white-box">
            <p class="pt-10 text-center brown-text mb-0"><a href="#" ><span></span>{{ $prod->brand }}</a></p>
            <div class="star-top">
                <a href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="ion-ios-star-outline"></i></a>
            </div>
            <a href="{{ route('front.product',$prod->slug) }}">
            <div class="product-img">
                    <img src="{{ $prod->thumbnail ? asset('public/assets/images/thumbnails/'.$prod->thumbnail):asset('public/assets/images/noimage.png') }}" alt="">
            </div>
            </a>

            <div class="product-content text-center">
                <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName()}}</a></h4>
                <h4><a href="{{ route('front.product',$prod->slug) }}">{{ $prod->category->name }}</a></h4>
                <div class="product-price">
                    <span>{{ $prod->setCurrency() }}</span>
                </div>
            </div>
            <a class="add-to-cart-btn cart-btn add-to-cart" data-href="{{ route('product.cart.add',$prod->id) }}" href="">
                Add to Cart
            </a>
            </div>
        </div>
    <!---end product-->
	@endforeach    
	<div class="col-md-12 product-list-pagination">
			{!! $prods->appends(['search' => request()->input('search')])->links() !!}                        
	</div>

@else
	<div class="col-lg-12">
		<div class="page-center">
				<h4 class="text-center">{{ $langg->lang60 }}</h4>
		</div>
	</div>
@endif


@if(isset($ajax_check))


<script type="text/javascript">


// Tooltip Section


$('[data-toggle="tooltip"]').tooltip({
});
$('[data-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});




$('[rel-toggle="tooltip"]').tooltip();

$('[rel-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});


// Tooltip Section Ends

</script>

@endif