<div class="col-md-3 col-xs-12 sidebar-container order-1 order-md-2">
<div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
        
        
        <div class="sin-sidebar category-sidebar">
            
            <h3 class="sidebar-title">FILTER</h3>
        
            

            <div class="sidebar-wrapper fix">
            <!-- treeview start -->
                <ul id="cat-treeview" class="treeview">
                    <li class="closed"><a href="#" class="">Sort By</a>
                    <ul>
                        <li> <div class="form-check">
                            <input class="form-check-input" type="radio" name="sort" id="gridRadios1" value="date_desc" checked="">
                            <label class="form-check-label" for="gridRadios3">
                            Recommended
                            </label>
                        </div></li>
                        <li>  
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="date_asc">
                                <label class="form-check-label" for="gridRadios4">
                                New In
                                </label>
                            </div>
                        </li>
                        <li>  <div class="form-check">
                            <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="price_asc">
                            <label class="form-check-label" for="gridRadios5">
                            Price (low to High)
                            </label>
                        </div></li>
                        <li class="last">  <div class="form-check">
                            <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="price_desc">
                            <label class="form-check-label" for="gridRadios6">
                            Price (High to low )
                            </label>
                        </div></li>
                    </ul>
                </li>
                    <li class="closed"><a href="#">Categories</a>
                        <ul class="hiden-box">
                        @foreach ($categories as $element)
                            <li><a href="{{route('front.category', $element->slug)}}{{!empty(request()->input('search')) ? '?search='.request()->input('search') : ''}}">{{$element->name}}</a></li>
                        @endforeach
                        </ul>
                    </li>
                    <li class="closed"><a href="#" class="">Brands</a>
                        <ul class="cus-scroll" tabindex="0" style="display: none; overflow: hidden; outline: none;">
                            @foreach($brands as $brand)
                            <li><a href="#"><input type="checkbox" class="brands" name="brand[]" value="{{$brand->id}}" > {{$brand->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>


                    @if ((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true))))
<form id="attrForm" action="{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">


  @if (!empty($cat) && !empty(json_decode($cat->attributes, true)))
      @foreach ($cat->attributes as $key => $attr)

      <li class="closed collapsed expandable"><a href="#">{{ $attr->name}}</a>
      <ul class="cus-scroll" tabindex="0" style="display: none; overflow: hidden; outline: none;">
              @if (!empty($attr->attribute_options))
                  @foreach ($attr->attribute_options as $key => $option)
                        <li><a href="#"><input type="checkbox" class="attribute-input" name="{{$attr->input_name}}[]" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}" > {{$option->name}}</a></li>
                  @endforeach
                @endif  
        </ul>
    </li>

      @endforeach
      @endif


  </form>
  @endif




                    <!-- <li class="closed collapsed expandable lastExpandable"><div class="hitarea closed-hitarea collapsed-hitarea expandable-hitarea lastExpandable-hitarea"></div><a href="#">Type Price</a>
                    <ul style="display: none;">
                        <li class="last">
                            <div class="mt-20">
                                <div id="price-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="width: 70%; left: 13.3333%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 13.3333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 83.3333%;"></span></div>
                                <div class="price-values fix">
                                    <p>Range</p>
                                    <input type="text" id="price-amount" readonly="">
                                </div>
                            </div>
                        </li>
                    </ul>
                    </li> -->
                    
                </ul>
            </div>
        </div>
        
    </div>
    

    
    
</div>
</div>




<div class="modal fade drawer right-align" id="exampleModalRight" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="sidebar-wrapper fix">
             <!--treeview start -->
                <ul id="cat-treeview-1" class="treeview">
                    <li class="closed collapsed expandable"><div class="hitarea closed-hitarea collapsed-hitarea expandable-hitarea"></div><a href="#" class="">Sort By</a>
                    <ul>
                        <li> <div class="form-check">
                            <input class="form-check-input" type="radio" name="sort" id="gridRadios1" value="date_desc" checked="">
                            <label class="form-check-label" for="gridRadios3">
                            Recommended
                            </label>
                        </div></li>
                        <li>  
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="date_asc">
                                <label class="form-check-label" for="gridRadios4">
                                New In
                                </label>
                            </div>
                        </li>
                        <li>  <div class="form-check">
                            <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="price_asc">
                            <label class="form-check-label" for="gridRadios5">
                            Price (low to High)
                            </label>
                        </div></li>
                        <li class="last">  <div class="form-check">
                            <input class="form-check-input" type="radio" name="sort" id="gridRadios2" value="price_desc">
                            <label class="form-check-label" for="gridRadios6">
                            Price (High to low )
                            </label>
                        </div></li>
                    </ul>
                </li>
                    <li class="closed collapsed expandable"><div class="hitarea closed-hitarea collapsed-hitarea expandable-hitarea"></div>  <a href="#">Categories</a>
                        <ul class="hiden-box2">
                        @foreach ($categories as $element)
                            <li><a href="{{route('front.category', $element->slug)}}{{!empty(request()->input('search')) ? '?search='.request()->input('search') : ''}}">{{$element->name}}</a></li>
                        @endforeach
                        </ul>
                    </li>
                    <li class="closed collapsed expandable"><div class="hitarea closed-hitarea collapsed-hitarea expandable-hitarea"></div><a href="#" class="">Brands</a>
                        <ul class="cus-scroll" tabindex="0" style="display: none; overflow: hidden; outline: none;">
                            @foreach($brands as $brand)
                            <li><a href="#"><input type="checkbox" class="brands" name="brand[]" value="{{$brand->id}}" > {{$brand->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>


                    @if ((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true))))
<form id="attrForm" action="{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">


  @if (!empty($cat) && !empty(json_decode($cat->attributes, true)))
      @foreach ($cat->attributes as $key => $attr)

      <li class="closed collapsed expandable"><div class="hitarea closed-hitarea collapsed-hitarea expandable-hitarea"></div><a href="#">{{ $attr->name}}</a>
      <ul class="cus-scroll" tabindex="0" style="display: none; overflow: hidden; outline: none;">
              @if (!empty($attr->attribute_options))
                  @foreach ($attr->attribute_options as $key => $option)
                        <li><a href="#"><input type="checkbox" class="attribute-input" name="{{$attr->input_name}}[]" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}" > {{$option->name}}</a></li>
                  @endforeach
                @endif  
        </ul>
    </li>

      @endforeach
      @endif


  </form>
  @endif

                </ul>
            </div>
      </div>
    </div>
  </div>
</div>