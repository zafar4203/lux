<div id="quick-details" class="row product-details-page py-0">
<div class="row">
<div class="col-md-5 col-sm-12 col-xs-12">
    <div class="tab-content quickview-big-img">
        <div id="pro-1" class="tab-pane fade show active">
            <img src="{{asset('public/assets/images/thumbnails/'.$product->thumbnail)}}" alt="" />
        </div>

        @foreach($product->galleries as $gal)
        <div id="pro-{{ $gal->id}}" class="tab-pane fade">
            <img src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" alt="" />
        </div>
       @endforeach
    </div>
    <!-- Thumbnail Large Image End -->
    <!-- Thumbnail Image End -->
    <div class="quickview-wrap mt-15">
        <div class="quickview-slide-active owl-carousel nav owl-nav-style owl-nav-style-2" role="tablist">
            <a class="active" data-toggle="tab" href="#pro-1"><img src="{{asset('public/assets/images/thumbnails/'.$product->thumbnail)}}" alt="" /></a>            
            @foreach($product->galleries as $gal)
                <a data-toggle="tab" href="#pro-{{$gal->id}}"><img src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" alt="" /></a>
            @endforeach
        </div>
    </div>
</div>
  <div class="col-md-7 col-sm-12 col-xs-12">
      <div class="product-details-content quickview-content">
          <h2><a target="_blank" href="{{ route('front.product',$product->slug) }}">{{ $product->name }}</a></h2>
          @if( $product->sku != null )
            <p class="reference">
            {{ $langg->lang77 }}: {{ $product->sku }}
            </p>
            @endif

          <div class="pro-details-rating-wrap">
              <div class="rating-product">
                  <i class="ion-android-star"></i>
                  <i class="ion-android-star"></i>
                  <i class="ion-android-star"></i>
                  <i class="ion-android-star"></i>
                  <i class="ion-android-star"></i>
              </div>
              <span class="read-review"><a class="reviews" href="">{{count($product->ratings)}} {{ $langg->lang80 }}</a></span>

                @if($product->product_condition != 0)
                    <div class="{{ $product->product_condition == 2 ? 'mybadge' : 'mybadge1' }}">
                    {{ $product->product_condition == 2 ? 'New' : 'Used' }}
                    </div>
                @endif

          </div>
          <div class="pricing-meta">
              <ul>
                  <li id="msizeprice" class="old-price not-cut">{{ $product->showPrice() }}</li>
              </ul>
          </div>

          <p>{{ $product->details }}</p>

          @if(!empty($product->size))
          <div class="mproduct-size mt-2">
              <p class="title"><strong>{{ $langg->lang88 }}</strong></p>
              <ul class="siz-list">
                  @php
                      $is_first = true;
                  @endphp
                  @foreach($product->size as $key => $data1)
                      <li class="{{ $is_first ? 'active' : '' }}">
              <span class="box">{{ $data1 }}
                  <input type="hidden" class="msize" value="{{ $data1 }}">
        <input type="hidden" class="msize_qty" value="{{ $product->size_qty[$key] }}">
        <input type="hidden" class="msize_key" value="{{$key}}">
        <input type="hidden" class="msize_price" value="{{ round($product->size_price[$key] * $curr->value,2) }}">
              </span>
                      </li>
                      @php
                          $is_first = false;
                      @endphp
                  @endforeach
                  <li>
              </ul>
          </div>
      @endif

      @if(!empty($product->color))
          <div class="mproduct-color mt-2">
              <p class="title"><strong>{{ $langg->lang89 }}</strong></p>
              <ul class="color-list">
                  @php
                      $is_first = true;
                  @endphp
                  @foreach($product->color as $key => $data1)
                      <li class="{{ $is_first ? 'active' : '' }}">
                          <span class="box"  data-color="{{ $product->color[$key] }}" style="background-color: {{ $product->color[$key] }}"></span>
                      </li>
                      @php
                          $is_first = false;
                      @endphp
                  @endforeach

              </ul>
          </div>
      @endif

      @if(!empty($product->size))
          <input type="hidden" class="product-stock" id="stock" value="{{ $product->size_qty[0] }}">
      @else
          @php
              $stck = (string)$product->stock;
          @endphp
          @if($stck != null)
              <input type="hidden" class="product-stock"  value="{{ $stck }}">
          @elseif($product->type != 'Physical')
              <input type="hidden" class="product-stock"  value="0">
          @else
              <input type="hidden" class="product-stock" value="">
          @endif

      @endif
      <input type="hidden" id="mproduct_price" value="{{ round($product->vendorPrice() * $curr->value,2) }}">
      <input type="hidden" id="mproduct_id" value="{{ $product->id }}">
      <input type="hidden" id="mcurr_pos" value="{{ $gs->currency_format }}">
      <input type="hidden" id="mcurr_sign" value="{{ $curr->sign }}">


      @if (!empty($product->attributes))
              @php
                $attrArr = json_decode($product->attributes, true);
                // dd($attrArr);
              @endphp
            @endif
            @if (!empty($attrArr))
              <div class="product-attributes">
                <div class="row">
                @foreach ($attrArr as $attrKey => $attrVal)
                  @if (array_key_exists("details_status",$attrVal) && $attrVal['details_status'] == 1)

                <div class="col-lg-6">
                    <div class="form-group mb-2">
                      <strong for="" class="text-capitalize">{{ str_replace("_", " ", $attrKey) }} :</strong>
                      <div class="">
                      @foreach ($attrVal['values'] as $optionKey => $optionVal)
                        <div class="custom-control custom-radio">
                          <input type="hidden" class="keys" value="">
                          <input type="hidden" class="values" value="">
                          <input type="radio" id="{{$attrKey}}{{ $optionKey }}" name="{{ $attrKey }}" class="custom-control-input mproduct-attr"  data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}" value="{{ $optionVal }}" {{ $loop->first ? 'checked' : '' }}>
                          <label class="custom-control-label" for="{{$attrKey}}{{ $optionKey }}">{{ $optionVal }}
                          @if (!empty($attrVal['prices'][$optionKey]))
                            +
                            {{$curr->sign}} {{$attrVal['prices'][$optionKey] * $curr->value}}
                          @endif
                        </label>
                        </div>
                      @endforeach
                      </div>
                    </div>
                </div>
                @endif 
                @endforeach
              </div>
              </div>
            @endif




          <div class="pro-details-quality">
              <div class="cart-plus-minus"><div class="dec qtybutton">-</div>
                  <input class="cart-plus-minus-box qttotal" type="number" name="qtybutton" value="1">
              <div class="inc qtybutton">+</div></div>
              <div class="pro-details-cart btn-hover">
                  <a href="javascript:;" id="maddcrt">+ Add To Cart</a>
              </div>
          </div>
          <div class="pro-details-wish-com">
              <div class="pro-details-wishlist">
                  @if(Auth::guard('web')->check())
                      <a href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$product->id) }}"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a>
                  @else
                      <a href="{{ route('user.login') }}"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a>
                @endif
                </div>
              <div class="pro-details-compare">
                  <a href=""><i class="ion-ios-shuffle-strong"></i>Add to compare</a>
              </div>
          </div>

            @if($product->ship != null)
            <p class="estimate-time">{{ $langg->lang86 }}: <b> {{ $product->ship }}</b></p>
            @endif
            
          <div class="pro-details-social-info">
              <span>Share</span>
              <div class="social-info">
                  <ul>
                      <li>
                          <a href=""><i class="ion-social-facebook"></i></a>
                      </li>
                      <li>
                          <a href=""><i class="ion-social-twitter"></i></a>
                      </li>
                      <li>
                          <a href=""><i class="ion-social-google"></i></a>
                      </li>
                      <li>
                          <a href=""><i class="ion-social-instagram"></i></a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div>
</div>


<!-- <script src="{{asset('public/assets/front/js/quicksetup.js')}}"></script> -->

<script type="text/javascript">

//   magnific popup activation


var sizes = "";
var size_qty = "";
var size_price = "";
var size_key = "";
var colors = "";
var mtotal = "";
var mstock = $('.product-stock').val();
var keys = "";
var values = "";
var prices = "";

$('.mproduct-attr').on('change',function(){

var total;
total = mgetAmount()+mgetSizePrice();
total = total.toFixed(2);
var pos = $('#mcurr_pos').val();
var sign = $('#mcurr_sign').val();
if(pos == '0')
{
$('#msizeprice').html(sign+total);
}
else {
$('#msizeprice').html(total+sign);
}
});



function mgetSizePrice()
{

var total = 0;
if($('.mproduct-size .siz-list li').length > 0)
{
total = parseFloat($('.mproduct-size .siz-list li.active').find('.msize_price').val());
}
return total;
}


function mgetAmount()
{
var total = 0;
var value = parseFloat($('#mproduct_price').val());
var datas = $(".mproduct-attr:checked").map(function() {
return $(this).data('price');
}).get();

var data;
for (data in datas) {
total += parseFloat(datas[data]);
}
total += value;
return total;
}


// Product Details Product Size Active Js Code
$('.mproduct-size .siz-list .box').on('click', function () {

$('.modal-total').html('1');
var parent = $(this).parent();
size_qty = $(this).find('.msize_qty').val();
size_price = $(this).find('.msize_price').val();
size_key = $(this).find('.msize_key').val();
sizes = $(this).find('.msize').val();
      $('.mproduct-size .siz-list li').removeClass('active');
      parent.addClass('active');
total = mgetAmount()+parseFloat(size_price);
stock = size_qty;
total = total.toFixed(2);
var pos = $('#mcurr_pos').val();
var sign = $('#mcurr_sign').val();
if(pos == '0')
{
$('#msizeprice').html(sign+total);
}
else {
$('#msizeprice').html(total+sign);
}

});

// Product Details Product Color Active Js Code
$('.mproduct-color .color-list .box').on('click', function () {
colors = $(this).data('color');
var parent = $(this).parent();
$('.mproduct-color .color-list li').removeClass('active');
parent.addClass('active');

});

$('.modal-minus').on('click', function () {
var el = $(this);
var $tselector = el.parent().parent().find('.modal-total');
total = $($tselector).text();
if (total > 1) {
  total--;
}
$($tselector).text(total);
});

$('.modal-plus').on('click', function () {
var el = $(this);
var $tselector = el.parent().parent().find('.modal-total');
total = $($tselector).text();
if(mstock != "")
{
  var stk = parseInt(mstock);
  if(total < stk)
  {
      total++;
      $($tselector).text(total);
  }
}
else {
  total++;
}
$($tselector).text(total);
});

$("#maddcrt").on("click", function(){
var qty = $('.qttotal').val();
var pid = $(this).parent().parent().parent().parent().find("#mproduct_id").val();

if($('.mproduct-attr').length > 0)
{
values = $(".mproduct-attr:checked").map(function() {
return $(this).val();
}).get();

keys = $(".mproduct-attr:checked").map(function() {
return $(this).data('key');
}).get();


prices = $(".mproduct-attr:checked").map(function() {
return $(this).data('price');
}).get();

}



$.ajax({
  type: "GET",
  url:mainurl+"/addnumcart",
  data:{id:pid,qty:qty,size:sizes,color:colors,size_qty:size_qty,size_price:size_price,size_key:size_key,keys:keys,values:values,prices:prices},
  success:function(data){
      if(data == 'digital') {
          toastr.error(langg.already_cart);
      }
      else if(data == 0) {
          toastr.error(langg.out_stock);
      }
      else {
          $("#cart-count").html(data[0]);
          $("#cart-paisa").html(data[9]);
          $("#cart-items").load(mainurl+'/carts/view');
          toastr.success(langg.add_cart);
      }
  }
});
});


$(document).on("click", "#mqaddcrt" , function(){
      var qty = $('.modal-total').html();
      var pid = $(this).parent().parent().parent().parent().find("#mproduct_id").val();
 
if($('.mproduct-attr').length > 0)
{
values = $(".mproduct-attr:checked").map(function() {
return $(this).val();
}).get();

keys = $(".mproduct-attr:checked").map(function() {
return $(this).data('key');
}).get();


prices = $(".mproduct-attr:checked").map(function() {
return $(this).data('price');
}).get();

}

 
  window.location = mainurl+"/addtonumcart?id="+pid+"&qty="+qty+"&size="+sizes+"&color="+colors.substring(1, colors.length)+"&size_qty="+size_qty+"&size_price="+size_price+"&size_key="+size_key+"&keys="+keys+"&values="+values+"&prices="+prices;
 

 

     });


</script>