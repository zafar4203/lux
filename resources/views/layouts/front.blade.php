<!doctype html>
<html class="no-js" lang="zxx">
   <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        @if(isset($page->meta_tag) && isset($page->meta_description))
        <meta name="keywords" content="{{ $page->meta_tag }}">
        <meta name="description" content="{{ $page->meta_description }}">
		<title>{{$gs->title}}</title>
        @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
        <meta name="keywords" content="{{ $blog->meta_tag }}">
        <meta name="description" content="{{ $blog->meta_description }}">
		<title>{{$gs->title}}</title>
        @elseif(isset($productt))
		<meta name="keywords" content="{{ !empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): '' }}">
		<meta name="description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}">
	    <meta property="og:title" content="{{$productt->name}}" />
	    <meta property="og:description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}" />
	    <meta property="og:image" content="{{asset('assets/images/thumbnails/'.$productt->thumbnail)}}" />
	    <meta name="author" content="Manara">
    	<title>{{substr($productt->name, 0,11)."-"}}{{$gs->title}}</title>
        @else
        <meta name="keywords" content="{{ $seo->meta_keys }}">
	    <meta name="author" content="Manara">
		<title>{{$gs->title}}</title>
        @endif
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Luxury Watches | @yield('title')</title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo/logo.png" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&display=swap" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:ital,wght@1,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
        
        <link href="https://fonts.googleapis.com/css2?family=Fira+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('public/assets/front/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/chosen.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/themify-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/meanmenu.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/toastr.css') }}">
        <script src="{{ asset('public/assets/front/js/vendor/modernizr-2.8.3.min.js') }}"></script>  

    <style>
        .js-cookie-consent .cookie-consent{
            position:absolute;
            bottom:0;
        }
    </style>
    @yield('styles')
    </head>


    <body class="@yield('class')">

      <div class="wrapper">
       
        @if(Route::currentRouteName() == 'front.campaign' 
        || Route::currentRouteName() == 'front.cart'
        || Route::currentRouteName() == 'user-dashboard'
        || Route::currentRouteName() == 'user-orders'
        || Route::currentRouteName() == 'user-order'
        || Route::currentRouteName() == 'user-reset'
        || Route::currentRouteName() == 'user-profile'
        || Route::currentRouteName() == 'user-communication'
        || Route::currentRouteName() == 'user-address'
        || Route::currentRouteName() == 'user-payment-cards'
        || Route::currentRouteName() == 'front.category'
        || Route::currentRouteName() == 'payment.return'
        || Route::currentRouteName() == 'front.winner'
        || Route::currentRouteName() == 'front.page'
        || Route::currentRouteName() == 'user-wishlists'        
        || Route::currentRouteName() == 'user-campaigns'        
        || Route::currentRouteName() == 'front.charity' 
        || Route::currentRouteName() == 'front.career'
        || Route::currentRouteName() == 'front.faq'
        || Route::currentRouteName() == 'front.about'
        || Route::currentRouteName() == 'front.contact'
        || Route::currentRouteName() == 'front.how_it_work'
        || Route::currentRouteName() == 'show_address_form'
        || Route::currentRouteName() == 'front.message'
        || Route::currentRouteName() == 'edit_address_form'
        || Route::currentRouteName() == 'front.subscribeForm'
                || Route::currentRouteName() == 'front.subscribeForm_web'
        || Route::currentRouteName() == 'front.product'
)
        @include('front.chunks.header_black')

        @elseif(Route::currentRouteName() == 'front.checkout' || Route::currentRouteName() == 'front.checkout_competetion' )
            @include('front.chunks.header_checkout')
        @elseif(Route::currentRouteName() == 'user.login' )
            @include('front.chunks.header_login')
        @else
            @include('front.chunks.header')
        @endif

        @include('cookieConsent::index')
        <input type="hidden" value="{{route('user.login')}}" id="user_login_url" />
        @yield('content')
      </div>
            
      <!-- all js here -->
      <!--Commented Temporary by Prashant-->
      
      <!--<script src="{{ asset('public/assets/front/js/vendor/jquery-1.12.0.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/popper.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/bootstrap.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/isotope.pkgd.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/imagesloaded.pkgd.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/jquery.counterup.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/waypoints.min.js') }}"></script>-->

      <!--<script src="{{ asset('public/assets/front/js/jquery.treeview.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/jquery-ui.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/jquery.nicescroll.min.js') }}"></script>-->

      <!--<script src="{{ asset('public/assets/front/js/owl.carousel.min.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/plugins.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/main.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/main2.js') }}"></script>-->

      <!--<script src="{{ asset('public/assets/front/js/custom2.js') }}"></script>-->
      <!--<script src="{{ asset('public/assets/front/js/toastr.js') }}"></script>-->
      
      <!--Commented Temporary by Prashant-->
      
      <!-- all js here -->
      <script src="{{ asset('public/assets/front/js/vendor/jquery-1.12.0.min.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/popper.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/bootstrap.min.js') }}"></script>
       <!--<script src="{{ asset('public/assets/front/js/bootstrap-better-nav.min.js') }}"></script>-->
       <script src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/js/bootstrap-better-nav.min.js"></script>
      <script src="{{ asset('public/assets/front/js/isotope.pkgd.min.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/imagesloaded.pkgd.min.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/jquery.counterup.min.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/waypoints.min.js') }}"></script>

      <script src="{{ asset('public/assets/front/js/jquery.treeview.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/jquery-ui.min.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/jquery.nicescroll.min.js') }}"></script>

      <script src="{{ asset('public/assets/front/js/owl.carousel.min.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/plugins.js') }}"></script>
      <!--<script src="{{ asset('public/assets/front/js/main.js') }}"></script>-->
      <script src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/js/main.js"></script>
      
      <script src="{{ asset('public/assets/front/js/main2.js') }}"></script>
      <!--<script src="http://fastvisaservicesdubai.com/Demo/lux/public/assets/front/js/main2.js"></script>-->

      <script src="{{ asset('public/assets/front/js/custom2.js') }}"></script>
      <script src="{{ asset('public/assets/front/js/toastr.js') }}"></script>
      
      <script src='https://unpkg.com/xzoom/dist/xzoom.min.js'></script>
    <script src='https://hammerjs.github.io/dist/hammer.min.js'></script>
    <!--<script src='https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/js/foundation.min.js'></script>-->
    <script src="{{ asset('public/assets/front/js/setup.js') }}"></script>

      <script type="text/javascript">
      
        var mainurl = "{{url('/')}}";
        var gs      = {!! json_encode($gs) !!};
        var langg    = {!! json_encode($langg) !!};
      </script>

      <script>
      
         $('#productCarousel').carousel({
         interval: 5000
         });
         
            $(document).ready(function() {    
  $('.navbar-nav > li.dropdown').hover(function() {
$('div.dropdown-menu', this).stop(true, true).slideDown('699');
$(this).addClass('open');
      }, function() {
$('div.dropdown-menu', this).stop(true, true).slideUp('600');
$(this).removeClass('open');
      });
   });
   


    $(document).ready(function() {
        var wrapper = $('.bill-new-address'); //Fields wrapper
        var add_button= $('.addnewButton'); //Add button ID

        $(add_button).click(function( e ) { //on add input button click
    
            e.preventDefault();
            var text = $('#add_form').html();
            $(wrapper).append(text); //add input box
        });

         $(document).on("click",".remove-btn", function(e){ //user click on remove text
            e.preventDefault(); 
            $(e.target).closest('.spacer').remove();
        });
    });

      
        $(".closed ").click(function(){
  $("ul").removeClass("hiden-box");
});
    $(".closed ").click(function(){
  $("ul").removeClass("hiden-box2");
});
      </script>
      @yield('scripts')

   </body>
</html>