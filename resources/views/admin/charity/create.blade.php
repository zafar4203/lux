@extends('layouts.admin')
@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('Charity Page') }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                          <a href="javascript:;">{{ __('Page Settings') }} </a>
                        </li>
                        <li>
                          <a href="{{ route('admin-charity-create') }}">{{ __('Add') }}</a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="add-product-content1">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">

                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                        
                        @include('includes.admin.form-both') 
                      <form id="geniusform" action="{{route('admin-charity-create')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        @if(!empty($charity))
                            <input type="hidden" value="{{ $charity->id}}" name="id" />
                        @endif

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 1') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <textarea class="nic-edit" name="para_1" placeholder="{{ __('Paragraph 1') }}" required="">@if($charity) {{ $charity->para_1 }} @endif</textarea>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 2') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="para_2" placeholder="{{ __('Paragraph 2') }}" required="">@if($charity) {{ $charity->para_2 }} @endif</textarea>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Get in Touch Text') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="get_in_touch_text" placeholder="{{ __('Get in Touch Text') }}" required="" @if($charity) value="{{ $charity->get_in_touch_text }}" @endif>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Call us now Text') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="call_us_now_text" placeholder="{{ __('Call Us Now Text') }}" required="" @if($charity) value="{{ $charity->call_us_now_text }}" @endif>
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Write us an email Text') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="write_email_text" placeholder="{{ __('Write Us Now Email text') }}" required="" @if($charity) value="{{ $charity->write_email_text }}" @endif>
                          </div>
                        </div>


                <div class="row">
                  <div class="col-lg-4">
                      <div class="left-area">
                      <h4 class="heading">{{ __('Set Image 1') }} *</h4>
                      </div>
                  </div>
                  <div class="col-lg-7">
                      <div class="img-upload">
                          <div id="image-preview" class="img-preview" style="background: url({{ asset('assets/admin/images/upload.png') }});">
                              <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                              <input type="file" name="image_1" class="img-upload">
                          </div>
                      </div>
                  </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Set Image 2') }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="img-upload">
                            <div id="image-preview" class="img-preview" style="background: url({{ asset('assets/admin/images/upload.png') }});">
                                <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                                <input type="file" name="image_2" class="img-upload">
                            </div>
                        </div>
                    </div>
                </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <button class="addProductSubmit-btn" type="submit">{{ __('Update Charity') }}</button>
                          </div>
                        </div>

                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection