@extends('layouts.admin')
@section('content')

    <div class="content-area">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-12">
                        <h4 class="heading">{{ __("Add Vendor") }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
                        <ul class="links">
                            <li>
                                <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }} </a>
                            </li>
                            <li>
                                <a href="{{ route('admin-vendor-index') }}">{{ __("Vendors") }}</a>
                            </li>
                            <li>
                                <a href="{{ route('admin-vendor-create') }}">{{ __("Add") }}</a>
                            </li>
                        </ul>
                </div>
            </div>
        </div>


<div class="add-product-content1">
<div class="row">
    <div class="col-lg-12">
        <div class="product-description">
            <div class="body-area">
            <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                @include('includes.admin.form-both') 

            <form id="geniusform" action="{{ route('admin-vendor-store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Name") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="name" placeholder="{{ __("Name") }}" required="" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Email") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="email" class="input-field" name="email" placeholder="{{ __("Email Address") }}" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Phone") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="row mb-0">
                            <div class="col-md-3 pr-0">
                                <select class="mb-0" name="country_code" id="">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->phone_code }}">{{ $country->country_code }} ({{ $country->phone_code }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-9 pl-0">
                                <input type="number" class="input-field" name="phone" placeholder="{{ __("Phone") }}" value="{{ old('phone') }}">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Password") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="password" placeholder="{{ __("Password") }}" value="{{ old('password') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Zip Code") }} </h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="zip" placeholder="{{ __("Zip Code") }}" value="{{ old('zip') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("City") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <select name="city" class="input-field">
                            @foreach($cities as $city)
                                <option value="{{$city->city_name}}">{{$city->city_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Country") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <select name="country" class="input-field">
                            @foreach($countries as $country)
                                <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Shop Name") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="shop_name" placeholder="{{ __("Shop Name") }}" required="" value="{{ old('shop_name') }}">
                    </div>
                </div>




                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Shop Details") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                    <textarea class="nic-edit" name="shop_details" placeholder="{{ __("Details") }}">{{ old('shop_details') }}</textarea> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Owner Name") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="owner_name" placeholder="{{ __("Owner Name") }}" required="" value="{{ old('owner_name') }}">
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Shop Number") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="shop_number" placeholder="{{ __("Shop Number") }}" required="" value="{{ old('shop_number') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Shop Address") }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="shop_address" placeholder="{{ __("Shop Address") }}" value="{{ old('shop_address') }}">
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Registration Number") }} </h4>
                                <p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="reg_number" placeholder="Registration Number" value="{{ old('reg_number') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Message") }} </h4>
                                <p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input type="text" class="input-field" name="shop_message" placeholder="Message" value="{{ old('shop_message') }}">
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                                <h4 class="heading">{{ __("Ban Vendor") }} </h4>
                                <p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <input class="mr-0" type="radio" name="ban" value="0" checked/> Dont Ban User
                        <input class="ml-3" type="radio" name="ban" value="1" /> Ban User
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                    <div class="left-area">
                        
                    </div>
                    </div>
                    <div class="col-lg-7">
                    <button class="addProductSubmit-btn" type="submit">{{ __("Submit") }}</button>
                    </div>
                </div>

            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection