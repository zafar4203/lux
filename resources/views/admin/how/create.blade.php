@extends('layouts.admin')
@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('How it Works Page') }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                          <a href="javascript:;">{{ __('Page Settings') }} </a>
                        </li>
                        <li>
                          <a href="{{ route('admin-how-work-create') }}">{{ __('Add') }}</a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="add-product-content1">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">

                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                        
                        @include('includes.admin.form-both') 
                      <form id="geniusform" action="{{route('admin-how-work-create')}}" method="POST" enctype="multipart/form-data" novalidate>
                        {{csrf_field()}}

                        @if(!empty($how))
                            <input type="hidden" value="{{ $how->id}}" name="id" />
                        @endif

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 1') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <textarea class="nic-edit" name="para_1" placeholder="{{ __('Paragraph 1') }}" required="">@if($how) {{ $how->para_1 }} @endif</textarea>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 2') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="para_2" placeholder="{{ __('Paragraph 2') }}" required="">@if($how) {{ $how->para_2 }} @endif</textarea>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 1 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="box_1_heading" placeholder="{{ __('Box 1 Heading') }}" required="" @if($how) value="{{ $how->box_1_heading }}" @endif>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 1 Details') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="box_1_details" placeholder="{{ __('Box 1 Details') }}" required="">@if($how) {{ $how->box_1_details }} @endif</textarea>
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 2 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="box_2_heading" placeholder="{{ __('Box 2 Heading') }}" required="" @if($how) value="{{ $how->box_2_heading }}" @endif>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 2 Details') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                          <textarea class="nic-edit" name="box_2_details" placeholder="{{ __('Box 2 Details') }}" required="">@if($how) {{ $how->box_2_details }} @endif</textarea>
                          </div>
                        </div>


                                                <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 3 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="box_3_heading" placeholder="{{ __('Box 3 Heading') }}" required="" @if($how) value="{{ $how->box_3_heading }}" @endif>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 3 Details') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                          <textarea class="nic-edit" name="box_3_details" placeholder="{{ __('Box 3 Details') }}" required="">@if($how) {{ $how->box_3_details }} @endif</textarea>
                          </div>
                        </div>


                                                <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 4 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="box_4_heading" placeholder="{{ __('Box 4 Heading') }}" required="" @if($how) value="{{ $how->box_4_heading }}" @endif>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Box 4 Details') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <textarea class="nic-edit" name="box_4_details" placeholder="{{ __('Box 4 Details') }}" required="">@if($how) {{ $how->box_4_details }} @endif</textarea>
                          </div>
                        </div>




                <div class="row">
                  <div class="col-lg-4">
                      <div class="left-area">
                      <h4 class="heading">{{ __('Set Image') }} *</h4>
                      </div>
                  </div>
                  <div class="col-lg-7">
                      <div class="img-upload">
                          <div id="image-preview" class="img-preview" style="background: @if($how)@if($how->image) url({{ asset('public/assets/images/how/'.$how->image) }}); @else url({{ asset('public/assets/admin/images/upload.png') }}); @endif @endif">
                              <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                              <input type="file" name="image" class="img-upload">
                          </div>
                      </div>
                  </div>
                </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <button class="addProductSubmit-btn" type="submit">{{ __('Update Page') }}</button>
                          </div>
                        </div>

                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection