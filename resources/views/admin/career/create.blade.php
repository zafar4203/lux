@extends('layouts.admin')
@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('Add New Career') }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                          <a href="javascript:;">{{ __('Page Settings') }} </a>
                        </li>
                        <li>
                          <a href="{{ route('admin-career-create') }}">{{ __('Add') }}</a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="add-product-content1">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">

                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                        
                        @include('includes.admin.form-both') 
                      <form id="geniusform" action="{{route('admin-career-create')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        @if(!empty($career))
                            <input type="hidden" value="{{ $career->id}}" name="id" />
                        @endif

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 1') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <textarea class="nic-edit" name="para_1" id="" cols="30" rows="10">@if($career) {{ $career->para_1 }} @endif</textarea>
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Main Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="main_heading" placeholder="{{ __('Main Heading') }}" required="" @if($career) value="{{ $career->main_heading }}" @endif>
                          </div>
                        </div>

                        <!-- <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Heading 1') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="heading_1" placeholder="{{ __('Heading 1') }}" @if($career) value="{{ $career->heading_1 }}" @endif>
                          </div>
                        </div> -->

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 2') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="para_2" id="" cols="30" rows="10">@if($career) {{ $career->para_2 }} @endif</textarea>
                          </div>
                        </div>


                        <!-- <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Heading 2') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="heading_2" placeholder="{{ __('Heading 2') }}" @if($career) value="{{ $career->heading_2 }}" @endif>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 3') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="para_3" id="" cols="30" rows="10">@if($career) {{ $career->para_3 }} @endif</textarea>
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Heading 3') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="heading_3" placeholder="{{ __('Heading 3') }}" @if($career) value="{{ $career->heading_3 }}" @endif>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 4') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="para_4" id="" cols="30" rows="10">@if($career) {{ $career->para_4 }} @endif</textarea>
                          </div>
                        </div> -->

                <div class="row">
                <div class="col-lg-4">
                    <div class="left-area">
                    <h4 class="heading">{{ __('Set Feature Image') }} *</h4>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="img-upload">
                        <div id="image-preview" class="img-preview" style="background: url(@if($career) {{ asset('public/assets/images/career/'.$career->image) }} @else{{ asset('assets/admin/images/upload.png') }}@endif);">
                            <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                            <input type="file" name="image" class="img-upload">
                        </div>
                    </div>
                </div>
                </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <button class="addProductSubmit-btn" type="submit">{{ __('Update Career') }}</button>
                          </div>
                        </div>

                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection