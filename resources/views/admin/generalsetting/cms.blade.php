@extends('layouts.admin')

@section('content')

            <div class="content-area">

              <div class="add-product-content1">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">
                      <h3 class="mt-3 mb-5 text-center">Home Page Data</h3>
                      @include('includes.admin.form-both') 
                      <form id="geniusformdata" action="{{route('admin-cms-update')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Campaign Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_campaign_heading" placeholder="{{ __('Enter Campaign Heading') }}" required="" value="{{ $gs->home_campaign_heading }}">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Campaign Description') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_campaign_desc" placeholder="{{ __('Enter campaign Description') }}" required="" value="{{ $gs->home_campaign_desc }}">
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Campaign Prize Given Away') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_prize_given_away" placeholder="{{ __('Enter Prize Given Away') }}" required="" value="{{ $gs->home_prize_given_away }}">
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Home Section1 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_section1_heading" placeholder="{{ __('Enter Home Section1 heading') }}" required="" value="{{ $gs->home_section1_heading }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Home Section1 Description') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_section1_description" placeholder="{{ __('Enter Home Section1 Description') }}" required="" value="{{ $gs->home_section1_description }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Home Section2 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_section2_heading" placeholder="{{ __('Enter Home Section1 heading') }}" required="" value="{{ $gs->home_section2_heading }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Home Section2 Description') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="home_section2_description" placeholder="{{ __('Enter Home Section1 Description') }}" required="" value="{{ $gs->home_section2_description }}">
                          </div>
                        </div>




                        <h3 class="mt-5 mb-5 text-center">Delivery & Returns</h3>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Delivery Charges') }} *</h4>
                                <p class="sub-heading">{{ __('(Enter Felivery Charges in Dollar($$)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="number" step="0.01" class="input-field" name="delivery_charges" placeholder="{{ __('Enter Delivery Charges') }}" required="" value="{{ $gs->delivery_charges }}">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Free Delivery After') }} *</h4>
                                <p class="sub-heading">{{ __('(Enter Charges in Dollar ($$))') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="number" step="0.01" class="input-field" name="free_delivery" placeholder="{{ __('Enter Free Delivery Charges') }}" required="" value="{{ $gs->free_delivery }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Delivery Short Description') }} *</h4>
                                <p class="sub-heading">{{ __('(Product & Campaign Page)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="delivery_short_desc" placeholder="{{ __('Enter Delivery Short Description') }}" required="" value="{{ $gs->delivery_short_desc }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Return Short Description') }} *</h4>
                                <p class="sub-heading">{{ __('(Product & Campaign Page)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="return_short_desc" placeholder="{{ __('Enter Return Short Description') }}" required="" value="{{ $gs->return_short_desc }}">
                          </div>
                        </div>

                        <h3 class="mt-5 mb-5 text-center">Contact Us Section</h3>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Contact Store Inquiries') }} *</h4>
                                <p class="sub-heading">{{ __('Store Inquiries') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="contact_store_inquiries" placeholder="{{ __('Enter Contact Store Inquiries') }}" value="{{ $gs->contact_store_inquiries }}">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Contact Online Inquiries') }} *</h4>
                                <p class="sub-heading">{{ __('Contact Online Inquiries') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="contact_online_inquiries" placeholder="{{ __('Enter Contact Online Inquiries') }}" value="{{ $gs->contact_online_inquiries }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('All Timing') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="all_timing" placeholder="{{ __('Enter All Timing') }}" value="{{ $gs->all_timing }}">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Sunday Timing') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="sunday_timing" placeholder="{{ __('Enter Sunday Timing') }}" value="{{ $gs->sunday_timing }}">
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Browsing Timing') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="browsing_timing" placeholder="{{ __('Enter Browsing Timing') }}" value="{{ $gs->browsing_timing }}">
                          </div>
                        </div>



                        <br>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <button class="addProductSubmit-btn" type="submit">{{ __('Save Data') }}</button>
                          </div>
                        </div>



                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection