@extends('layouts.admin') 

@section('content')  
<input type="hidden" id="headerdata" value="{{ __('PAGE') }}">
<div class="content-area">
    <div class="mr-breadcrumb">
        <div class="row">
            <div class="col-lg-12">
                    <h4 class="heading">{{ __('Pages') }}</h4>
                    <ul class="links">
                        <li>
                            <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                            <a href="javascript:;">{{ __('General Settings') }} </a>
                        </li>
                        <li>
                            <a href="{{ route('admin-footer-check-index') }}">{{ __('Footer Check Pages') }}</a>
                        </li>
                    </ul>
            </div>
        </div>
    </div>
    <div class="product-area">
        <div class="row">
            <div class="col-lg-12">
                <div class="mr-table allproduct">

                    @include('includes.admin.form-success') 
                    @csrf
                    <div class="table-responsiv">
                    <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th width="20%">{{ __('Page Title') }}</th>
                                <th width="40%">{{ __('Category') }}</th>
                                <th width="30%">{{ __('Show in Footer') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->name }}</td>
                                <td>
                                    <select id="{{ $page->id }}" class="select-box mt-2" name="category" id="">
                                        <option {{$page->category == 'shopping online' ?'selected':''}} value="shopping online">Shopping Online</option>
                                        <option {{$page->category == 'customer service' ?'selected':''}} value="customer service">Customer Service</option>
                                        <option {{$page->category == 'about' ?'selected':''}} value="about">About</option>
                                    </select>
                                </td>
                                <!-- <th>{{ __('Header') }}</th>
                                <th>{{ __('Footer') }}</th> -->
                                <td>
                                    <input id="{{ $page->id }}" type="radio" class="radio" {{$page->visible == 0 ? 'checked':''}}  name="status_{{$page->id}}" value="0">Show
                                    <input id="{{ $page->id }}" class="ml-3 radio" type="radio" {{$page->visible == 1 ? 'checked':''}} name="status_{{$page->id}}" value="1">Hide
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection    
@section('scripts')
    <script>
        $('.select-box').on('change', function() {
            var value = $(this).val();
            var id = this.id;
            $.ajax({
                method:"POST",
                url:mainurl+'/admin/update_footer_page',
                headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
                data:{
                    id:id,
                    category:value
                },
                success:function(data){
                        console.log(data);
                        $('#'+id).val(data.category).prop('selected', true);
                        $('.alert-success').show();
                        $('.alert-success p').html('Page Category Updated Successfully');
                    }
                });
        });


        $('.radio').on('click', function() {
            var value = $(this).val();
            var id = this.id;

            $.ajax({
                method:"POST",
                url:mainurl+'/admin/update_footer_page',
                headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
                data:{
                    id:id,
                    visible:value
                },
                success:function(data){
                        console.log(data);
                        $('.alert-success').show();
                        $('.alert-success p').html('Visibility Status Updated Successfully');
                    }
                });
        });
    </script>
@endsection   