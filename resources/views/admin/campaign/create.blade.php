@extends('layouts.admin')

@section('styles')

<link href="{{asset('assets/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
<style>
  .ui-datepicker{
    background:lightgrey !important;
  }
  .img-upload #image-preview input , .img-upload #image-preview label{
    z-index: 0 !important;
}
</style>
@endsection

@section('content')

            <div class="content-area">

      <div class="mr-breadcrumb">
        <div class="row">
          <div class="col-lg-12">
              <h4 class="heading">{{ __('Add New Campaign') }} <a class="add-btn" href="{{route('admin-campaign-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
              <ul class="links">
                <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                </li>
                <li>
                  <a href="{{ route('admin-campaign-index') }}">{{ __('Campaigns') }}</a>
                </li>
                <li>
                  <a href="{{ route('admin-campaign-create') }}">{{ __('Add New Campaign') }}</a>
                </li>
              </ul>
          </div>
        </div>
      </div>

      <div class="add-product-content1">
        <div class="row">
          <div class="col-lg-12">
            <div class="product-description">
              <div class="body-area">
                <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                @include('includes.admin.form-both') 
              <form id="geniusform" action="{{route('admin-campaign-create')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Campaign Name') }}*</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="text" class="input-field" name="name" placeholder="{{ __('Enter Campaign name') }}" required value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Quantity') }} *</h4>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="number" class="input-field" name="quantity" placeholder="{{ __('Enter Quantity') }}" required value="">
                        </div>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                  <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Draw Date') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="date" class="input-field" name="draw_date" placeholder="{{ __('Select a Draw date') }}" required>
                      </div>
                    </div>
                  </div>

                <!-- <div class="col-md-6">

                  <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Product Name') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                          <input type="text" class="input-field" name="product_name" placeholder="{{ __('Enter Product Name') }}" required>
                      </div>
                  </div>
                </div> -->

                </div>


                <div class="row">

                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Product Title') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                           <input type="text" class="input-field" name="product_title" placeholder="{{ __('Enter Product Title') }}" required>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Product Price') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                                <input type="number" class="input-field" name="product_price" placeholder="{{ __('Enter Product Price') }}" required>
                          </div>
                        </div>
                    </div>                
                </div>


                <div class="row">
                <div class="col-md-12">

                    <div class="row">
                      <div class="col-lg-2">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Product Description') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-10">
                        <textarea class="nic-edit" name="product_description" placeholder="{{ __('Enter Product Description') }}"></textarea>
                      </div>
                    </div>


                    </div>


                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Prize Title') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                          <input type="text" class="input-field" name="prize_title" placeholder="{{ __('Enter Prize Title') }}" required>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Prize Worth') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                          <input type="number" class="input-field" name="prize_worth" placeholder="{{ __('Enter Prize Worth') }}" required>
                        </div>
                      </div>
                    </div>

                   </div>

                <div class="row">
                <div class="col-md-12">

                  <div class="row">
                    <div class="col-lg-2">
                      <div class="left-area">
                          <h4 class="heading">{{ __('Prize Description') }} *</h4>
                          <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                      </div>
                    </div>
                    <div class="col-lg-10">
                        <textarea class="nic-edit" name="prize_description" placeholder="{{ __('Enter Prize Description') }}"></textarea>
                    </div>
                  </div>
              </div>

<div class="col-md-12">

<div class="row">
  <div class="col-lg-2">
    <div class="left-area">
        <h4 class="heading">{{ __('Campaign Description') }} *</h4>
        <p class="sub-heading">{{ __('(In Any Language)') }}</p>
    </div>
  </div>
  <div class="col-lg-10">
     <textarea class="nic-edit" name="campaign_description" placeholder="{{ __('Enter Campaign Description') }}" ></textarea>
  </div>
</div>


</div>
                </div>

                <div class="row">
                <div class="col-md-12">

<div class="row">
  <div class="col-lg-2">
    <div class="left-area">
        <h4 class="heading">{{ __('Campaign Editor Note') }} *</h4>
        <p class="sub-heading">{{ __('(In Any Language)') }}</p>
    </div>
  </div>
  <div class="col-lg-10">
    <textarea class="nic-edit" name="campaign_editor_note" placeholder="{{ __('Enter Campaign Editor Note') }}" ></textarea>
  </div>
</div>


</div>

<div class="col-md-6">


  <div class="row mt-3">
    <div class="col-lg-4">
      <div class="left-area">
          <h4 class="heading">{{ __('Campaign Type') }} *</h4>
          <p class="sub-heading">{{ __('(In Any Language)') }}</p>
      </div>
    </div>
    <div class="col-lg-7">
        <select name="type" id="">
          <option value="0">Live</option>
          <option value="1">Closed</option>
          <option value="2">Coming Soon</option>
        </select>
    </div>
  </div>
</div>
                </div>                

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">Set Product Images *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <div class="img-upload">
                                <div id="image-preview" class="img-preview" style="background: url(http://localhost/offerbazar/assets/admin/images/upload.png);">
                                    <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>Upload Icon</label>
                                    <input type="file" name="product_images[]" multiple class="img-upload" required id="image-upload">
                                  </div>
                            </div>
                          </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">Set Prize Images *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <div class="img-upload">
                                <div id="image-preview" class="img-preview" style="background: url(http://localhost/offerbazar/assets/admin/images/upload.png);">
                                    <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>Upload Icon</label>
                                    <input type="file" name="prize_images[]" multiple class="img-upload" required id="image-upload">
                                  </div>
                            </div>
                          </div>
                       </div>
                    </div>

              </div>


                <br>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="left-area">
                      
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <button class="addProductSubmit-btn" type="submit">{{ __('Create New Campaign') }}</button>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    var dateToday = new Date();
    var dates =  $( "#from,#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
        onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
          instance = $(this).data("datepicker"),
          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
          dates.not(this).datepicker("option", option, date);
    }
});
</script>

@endsection

