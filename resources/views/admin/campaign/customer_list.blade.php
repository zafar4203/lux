@extends('layouts.admin') 
@section('styles')
    <style>
        .dt-button{
            background:#1f224f;
            color:white;
            border:1px solid #1f224f;
            border-radius:2px;
        }
    </style>
@endsection
@section('content')  
    <input type="hidden" id="headerdata" value="{{ __('COUPON') }}">
    <div class="content-area">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-8">
                        <h4 class="heading">{{ __('Campaigns') }}</h4>
                        <ul class="links">
                            <li>
                                <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                            </li>
                            <li>
                                <a href="{{ route('admin-campaign-index') }}">{{ __('Campaigns') }}</a>
                            </li>
                        </ul>
                </div>

            @if($campaign->type != 1)
            <div class="col-sm-4 mt-4 float-right text-right">
                <a class="add-btn" href="{{route('admin-campaign-closed' , $id )}}">
                     {{ __('Close Campaign') }}
                </a>
            </div>
            @endif

            </div>


        </div>
        <div class="product-area">
            <div class="row">
                <div class="col-lg-12">
                    <div class="mr-table allproduct">

    @include('includes.admin.form-success')  
    <div class="table-responsiv">
            <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{ __('User Id') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Email') }}</th>
                        <th>{{ __('Ticket') }}</th>
                        <th>{{ __('Purchase Date') }}</th>
                        <th>{{ __('Campaign Name') }}</th>
                        <th>{{ __('Draw Date') }}</th>
                        @if($winner == 0)
                        <th>{{ __('Winner') }}</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                @foreach($tickets as $ticket)
                    <tr>
                        <td>{{ $ticket->user->id }}</td>
                        <td>{{ $ticket->user->name }}</td>
                        <td>{{ $ticket->user->email }}</td>
                        <td>{{ $ticket->uuid }}</td>
                        <td>{{ $ticket->created_at }}</td>
                        <td>{{ $ticket->campaign->name }}</td>
                        <td>{{ $ticket->campaign->draw_date }}</td>
                        @if($winner == 0)
                            <td><a href="{{ route('admin-campaign-winner',$ticket->id) }}">Make Winner</a></td>
                        @endif

                    </tr>
                @endforeach
                </tbody>
            </table>
    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



{{-- ADD / EDIT MODAL --}}

    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
    
    
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
            <div class="submit-loader">
                    <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">
            </div>
        <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
        </div>
    </div>
    </div>
</div>

{{-- ADD / EDIT MODAL ENDS --}}


{{-- DELETE MODAL --}}

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

	<div class="modal-header d-block text-center">
		<h4 class="modal-title d-inline-block">{{ __('Confirm Delete') }}</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
	</div>

      <!-- Modal body -->
      <div class="modal-body">
            <p class="text-center">{{ __('You are about to delete this Campaign.') }}</p>
            <p class="text-center">{{ __('Do you want to proceed?') }}</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
            <a class="btn btn-danger btn-ok">{{ __('Delete') }}</a>
      </div>

    </div>
  </div>
</div>

{{-- DELETE MODAL ENDS --}}

@endsection    



@section('scripts')


{{-- DATA TABLE --}}

  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
  <!-- <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>   -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>   -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>  

    <script type="text/javascript">

		var table = $('#geniustable').DataTable({
			   ordering: false,
               processing: true,
               dom: 'Bfrtip',
                buttons: [
                    'pdf', 'print','csv'
                ]
            //    serverSide: true,
            //    ajax: '{{ route('admin-campaign-datatables') }}',
            //    columns: [
            //             { data: 'name', name: 'name' },
            //             { data: 'product_id', name: 'product_id' },
            //             { data: 'prize_id', name: 'prize_id' },
			// 			{ data: 'draw_date', name: 'draw_date' },
			// 			{ data: 'quantity', name: 'quantity' },
            //             { data: 'sold_out', name: 'sold_out' },
            //             { data: 'status', searchable: false, orderable: false},
            // 			{ data: 'action', searchable: false, orderable: false }

            //          ],
            //     language : {
            //     	processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'
            //     },
			// 	drawCallback : function( settings ) {
	    	// 			$('.select').niceSelect();	
			// 	}
            });

		$('#geniustable_filter input[type="search"]').attr('placeholder', 'Search By Code...');


									


{{-- DATA TABLE ENDS--}}


</script>




@endsection   