@extends('layouts.admin')

@section('styles')

<link href="{{asset('assets/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
<style>
  .ui-datepicker{
    background:lightgrey !important;
  }
  .img-upload #image-preview input , .img-upload #image-preview label{
    z-index: 0 !important;
}
  .remove-prize , .remove-product{
    cursor:pointer;
  }
</style>
@endsection

@section('content')
      <div id="top"></div> 
      <div class="content-area">

      <div class="mr-breadcrumb">
        <div class="row">
          <div class="col-lg-12">
              <h4 class="heading">{{ __('Edit Campaign') }} <a class="add-btn" href="{{route('admin-campaign-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
              <ul class="links">
                <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                </li>
                <li>
                  <a href="{{ route('admin-campaign-index') }}">{{ __('Campaigns') }}</a>
                </li>
                <li>
                <a href="{{ route('admin-campaign-edit',$data->id) }}">{{ __('Edit Campaign') }}</a>
                </li>
              </ul>
          </div>
        </div>
      </div>

      <div class="add-product-content1">
        <div class="row">
          <div class="col-lg-12">
            <div class="product-description">
              <div class="body-area">
                <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                @include('includes.admin.form-both')
              <form id="geniusform" action="{{route('admin-campaign-update',$data->id)}}" method="POST" enctype="multipart/form-data" novalidate>
                {{csrf_field()}}

                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Campaign Name') }}*</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="text" class="input-field" name="name" placeholder="{{ __('Enter Campaign name') }}" required="" value="{{$data->name}}" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Quantity') }} *</h4>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="number" class="input-field" name="quantity" placeholder="{{ __('Enter Quantity') }}" required="" value="{{$data->quantity}}" value="">
                        </div>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                  <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Draw Date') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="date" class="input-field" name="draw_date" id="from" placeholder="{{ __('Select a Draw date') }}" required="" value="{{$data->draw_date}}" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">



                  </div>
                </div>


                <div class="row">

                    <div class="col-md-6">

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Product Title') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                          <input type="text" class="input-field" name="product_title" placeholder="{{ __('Enter Product Title') }}" value="{{$data->product_title}}" required="">
                        </div>
                      </div>


                      </div>



                    <div class="col-md-6">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Product Price') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="number" class="input-field" step="0.01" name="product_price" value="{{ $data->product_price }}" placeholder="{{ __('Enter Product Price') }}" required>
                          </div>
                        </div>
                    </div>

                
                </div>


                <div class="row">
                <div class="col-md-12">

                    <div class="row">
                      <div class="col-lg-2">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Product Description') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-10">
                        <textarea class="nic-edit" name="product_description" placeholder="{{ __('Enter Product Description') }}" required="">{{$data->product_description}}</textarea>
                      </div>
                    </div>


                    </div>


                    <div class="col-md-6">

<div class="row">
  <div class="col-lg-4">
    <div class="left-area">
        <h4 class="heading">{{ __('Prize Title') }} *</h4>
        <p class="sub-heading">{{ __('(In Any Language)') }}</p>
    </div>
  </div>
  <div class="col-lg-7">
    <input type="text" class="input-field" name="prize_title" placeholder="{{ __('Enter Prize Title') }}" value="{{$data->prize_title}}" required="">
  </div>
</div>


</div>


                    <div class="col-md-6">

                    <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Prize Worth') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="number" class="input-field" name="prize_worth" placeholder="{{ __('Enter Prize Worth') }}" value="{{$data->prize_worth}}" required="">
                      </div>
                    </div>


                    </div>
                </div>

                <div class="row">
                <div class="col-md-12">

                  <div class="row">
                    <div class="col-lg-2">
                      <div class="left-area">
                          <h4 class="heading">{{ __('Prize Description') }} *</h4>
                          <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                      </div>
                    </div>
                    <div class="col-lg-10">
                      <textarea class="nic-edit" name="prize_description" placeholder="{{ __('Enter Prize Description') }}" required="">{{$data->prize_description}}</textarea>
                    </div>
                  </div>


                  </div>

                    <div class="col-md-12">

                    <div class="row">
                      <div class="col-lg-2">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Campaign Description') }} *</h4>
                            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                        </div>
                      </div>
                      <div class="col-lg-10">
                        <textarea class="nic-edit" name="campaign_description" placeholder="{{ __('Enter Campaign Description') }}" required="">{{$data->campaign_description}}</textarea>
                      </div>
                    </div>


                    </div>
                </div>

                <div class="row">
                <div class="col-md-12">

                <div class="row">
                  <div class="col-lg-2">
                    <div class="left-area">
                        <h4 class="heading">{{ __('Campaign Editor Note') }} *</h4>
                        <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                    </div>
                  </div>
                  <div class="col-lg-10">
                    <textarea class="nic-edit" name="campaign_editor_note" placeholder="{{ __('Enter Campaign Editor Note') }}" required="">{{$data->campaign_editor_note}}</textarea>
                  </div>
                </div>


</div>


<div class="col-md-6">

<div class="row">
  <div class="col-lg-4">
    <div class="left-area">
        <h4 class="heading">{{ __('Campaign Type') }} *</h4>
        <p class="sub-heading">{{ __('(In Any Language)') }}</p>
    </div>
  </div>
  <div class="col-lg-7">
    <select name="type" id="">
        <option @if($data->type == 0) selected @endif value="0">Live</option>
        <option @if($data->type == 1) selected @endif value="1">Closed</option>
        <option @if($data->type == 2) selected @endif value="2">Coming Soon</option>        
    </select>
  </div>
</div>


</div>
                </div>                

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">Set Product Images *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <div class="img-upload">
                                <div id="image-preview" class="img-preview" style="background: url(http://localhost/offerbazar/assets/admin/images/upload.png);">
                                    <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>Upload Icon</label>
                                    <input type="file" name="product_images[]" multiple class="img-upload" id="image-upload">
                                  </div>
                            </div>
                            <div id="product_images_view" class="row">
                                @php
                                  $product_images = json_decode($data->product_images);
                                @endphp
                                @if($product_images)
                                @foreach($product_images as $image)
                                <div id="prod_{{$image}}" class="col-md-6 mt-3">
                                  <img src="{{ asset('public/assets/images/campaigns/'.$image) }}" class="img-responsive" alt="{{$image}}">
                                  <p id="{{ $image }}" class="text-center btn-danger remove-product">Remove</p>
                                </div>
                                @endforeach
                                @endif
                            </div>
                          </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">Set Prize Images *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <div class="img-upload">
                                <div id="image-preview" class="img-preview" style="background: url(http://localhost/offerbazar/assets/admin/images/upload.png);">
                                    <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>Upload Icon</label>
                                    <input type="file" name="prize_images[]" multiple class="img-upload" id="image-upload">
                                </div>
                            </div>
                            <div id="prize_images_view" class="row">
                                @php
                                  $prize_images = json_decode($data->prize_images);
                                @endphp
                                @if($prize_images)
                                @foreach($prize_images as $image)
                                <div id="prize_{{$image}}" class="col-md-6 mt-3">
                                  <img src="{{ asset('public/assets/images/campaigns/'.$image) }}" class="img-responsive" alt="{{$image}}">
                                  <p id="{{ $image }}" class="text-center btn-danger remove-prize">Remove</p>
                                </div>
                                @endforeach
                                @endif
                            </div>
                          </div>
                       </div>
                    </div>

              </div>



                <br>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="left-area">
                      
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <input type="hidden" id="campaign" value="campaign" />
                    <button class="addProductSubmit-btn update" type="submit">{{ __('Update Campaign') }}</button>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    var dateToday = new Date();
    var dates =  $( "#from,#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
        onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
          instance = $(this).data("datepicker"),
          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
          dates.not(this).datepicker("option", option, date);
    }
});

$('.remove-product').on('click' , function(){
  var id = this.id;
  $.ajax({
    method:"POST",
    url:mainurl+'/admin/campaign/delete_product_images',
    headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
    data:{
        image:id,
        id:{{ $data->id }}
    },
    beforeSend:function(){
      $('#product_images_view').css('opacity','0.2');
    },
    complete:function(){
      $('#product_images_view').css('opacity','1');
    },
    success:function(data){
            if(data == 1){
              $('.alert-success').show();
              $('.alert-success p').html('Image Deleted Successfully');
              location.reload();
            }
        }
    });
});


$('.remove-prize').click(function(){
  var id = this.id;
  $.ajax({
    method:"POST",
    url:mainurl+'/admin/campaign/delete_prize_images',
    headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
    data:{
        image:id,
        id:{{ $data->id }}
    },
    beforeSend:function(){
      $('#prize_images_view').css('opacity','0.2');
    },
    complete:function(){
      $('#prize_images_view').css('opacity','1');
    },
    success:function(data){
            if(data == 1){
              $('.alert-success').show();
              $('.alert-success p').html('Image Deleted Successfully');
              location.reload();
            }
        }
    });
});
</script>

@endsection

