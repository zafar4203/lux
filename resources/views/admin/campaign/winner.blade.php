@extends('layouts.admin')

@section('styles')

<link href="{{asset('assets/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
<style>
  .ui-datepicker{
    background:lightgrey !important;
  }
  .img-upload #image-preview input , .img-upload #image-preview label{
    z-index: 0 !important;
}
</style>
@endsection

@section('content')

            <div class="content-area">

      <div class="mr-breadcrumb">
        <div class="row">
          <div class="col-lg-12">
              <h4 class="heading">{{ __('Add New Winner') }} <a class="add-btn" href="{{route('admin-campaign-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
              <ul class="links">
                <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                </li>
                <li>
                  <a href="{{ route('admin-campaign-index') }}">{{ __('Campaigns') }}</a>
                </li>
                <li>
                  <a href="{{ route('admin-campaign-winner' , $ticket->id) }}">{{ __('Add New Winner') }}</a>
                </li>
              </ul>
          </div>
        </div>
      </div>

      <div class="add-product-content1">
        <div class="row">
          <div class="col-lg-12">
            <div class="product-description">
              <div class="body-area">
                <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                @include('includes.admin.form-both') 
              <form id="geniusform" action="{{route('admin-campaign-winner-create')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">Campaign Id *</h4>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="text" class="input-field" name="campaign_id" value="{{ $ticket->campaign->id }}" readonly/>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">Ticket Id *</h4>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="number" class="input-field" name="ticket_id" placeholder="{{ __('Enter Quantity') }}" readonly value="{{$ticket->id}}" />
                        </div>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">User Id </h4>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="text" class="input-field" name="user_id"  value="{{ $ticket->user->id }}" readonly />
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                  <div class="row">
                      <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('View URL') }} *</h4>
                        </div>
                      </div>
                      <div class="col-lg-7">
                        <input type="text" class="input-field" name="video_url" placeholder="{{ __('Enter Video Url') }}" />
                      </div>
                    </div>

                  </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Winner Comments') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <input type="text" class="input-field" name="winner_comment" placeholder="{{ __('Enter Winner Comments') }}" />
                          </div>
                        </div>
                    </div>



                    </div>
                




                <br>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="left-area">
                      
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <button class="addProductSubmit-btn" type="submit">{{ __('Make Winner') }}</button>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    var dateToday = new Date();
    var dates =  $( "#from,#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
        onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
          instance = $(this).data("datepicker"),
          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
          dates.not(this).datepicker("option", option, date);
    }
});
</script>

@endsection

