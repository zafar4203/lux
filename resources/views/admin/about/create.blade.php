@extends('layouts.admin')
@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('About Us Page') }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="add-product-content1">
              <div class="row">
                <div class="col-lg-12">
                  <div class="product-description">
                    <div class="body-area">

                      <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                      
                      @include('includes.admin.form-both') 
                    <form id="geniusform" action="{{route('admin-about-create')}}" method="POST" enctype="multipart/form-data">
                      {{csrf_field()}}

                      @if(!empty($about))
                            <input type="hidden" value="{{ $about->id}}" name="id" />
                      @endif

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Main Paragraph') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                          <textarea class="nic-edit" name="main_para" placeholder="{{ __('Main Paragraph') }}" required="">@if($about) {{ $about->main_para }} @endif</textarea>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Main Heading') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="text" class="input-field" name="main_heading" placeholder="{{ __('Main Heading') }}" required="" @if($about) value="{{ $about->main_heading }}" @endif>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Heading 1') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="text" class="input-field" name="heading_1" placeholder="{{ __('Heading 1') }}" required="" @if($about) value="{{ $about->heading_1 }}" @endif>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Paragraph 1') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                           <textarea class="nic-edit" name="para_1" placeholder="{{ __('Paragraph 1') }}" required="">@if($about) {{ $about->para_1 }} @endif</textarea>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Heading 2') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="text" class="input-field" name="heading_2" placeholder="{{ __('Heading 2') }}" required="" @if($about) value="{{ $about->heading_2 }}" @endif>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Paragraph 2') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                           <textarea class="nic-edit" name="para_2" placeholder="{{ __('Paragraph 2') }}" required="">@if($about) {{ $about->para_2 }} @endif</textarea>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Heading 3') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="text" class="input-field" name="heading_3" placeholder="{{ __('Heading 3') }}" required="" @if($about) value="{{ $about->heading_3 }}" @endif>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Paragraph 3') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <textarea class="nic-edit" name="para_3" placeholder="{{ __('Paragraph 3') }}" required="">@if($about) {{ $about->para_3 }} @endif</textarea>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Heading 4') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <input type="text" class="input-field" name="heading_4" placeholder="{{ __('Heading 4') }}" required="" @if($about) value="{{ $about->heading_4 }}" @endif>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                              <h4 class="heading">{{ __('Paragraph 4') }} *</h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                          </div>
                        </div>
                        <div class="col-lg-7">
                            <textarea class="nic-edit" name="para_4" placeholder="{{ __('Paragraph 4') }}" required="">@if($about) {{ $about->para_4 }} @endif</textarea>
                        </div>
                      </div>

              <div class="row">
              <div class="col-lg-4">
                  <div class="left-area">
                  <h4 class="heading">{{ __('Set Image') }} *</h4>
                  </div>
              </div>
              <div class="col-lg-7">
                  <div class="img-upload">
                      <div id="image-preview" class="img-preview" style="background: url({{ asset('assets/admin/images/upload.png') }});">
                          <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                          <input type="file" name="image" class="img-upload">
                      </div>
                  </div>
              </div>
              </div>


                      <div class="row">
                        <div class="col-lg-4">
                          <div class="left-area">
                            
                          </div>
                        </div>
                        <div class="col-lg-7">
                          <button class="addProductSubmit-btn" type="submit">{{ __('Update About') }}</button>
                        </div>
                      </div>

                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection