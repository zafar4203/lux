@extends('layouts.admin')

@section('styles')

<link href="{{asset('assets/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
<style>
  .ui-datepicker{
    background:lightgrey !important;
  }
  .img-upload #image-preview input , .img-upload #image-preview label{
    z-index: 0 !important;
}
</style>
@endsection

@section('content')

    <div class="content-area">

      <div class="mr-breadcrumb">
        <div class="row">
          <div class="col-lg-12">
              <h4 class="heading">{{ __('Add New Prize') }} <a class="add-btn" href="{{route('admin-prize-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
              <ul class="links">
                <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                </li>
                <li>
                  <a href="{{ route('admin-prize-index') }}">{{ __('Prizes') }}</a>
                </li>
                <li>
                  <a href="{{ route('admin-prize-create') }}">{{ __('Add New Prize') }}</a>
                </li>
              </ul>
          </div>
        </div>
      </div>

      <div class="add-product-content1">
        <div class="row">
          <div class="col-lg-12">
            <div class="product-description">
              <div class="body-area">
                <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                @include('includes.admin.form-both') 
              <form id="geniusform" action="{{route('admin-prize-create')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}

    <div class="row">
        <div class="col-lg-4">
            <div class="left-area">
                <h4 class="heading">{{ __('name') }} *</h4>
                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
            </div>
            </div>
            <div class="col-lg-7">
            <input type="text" class="input-field" name="name" placeholder="{{ __('Enter prize name') }}" required="" value="">
            </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="left-area">
                <h4 class="heading">{{ __('Description') }} *</h4>
            </div>
        </div>
        <div class="col-lg-7">
            <input type="text" class="input-field" name="description" placeholder="{{ __('Enter Description') }}" required="" value="">
        </div>
        </div>


    <div class="row">
        <div class="col-lg-4">
        <div class="left-area">
            <h4 class="heading">{{ __('Enter Price') }} *</h4>
            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
        </div>
        </div>
        <div class="col-lg-7">
        <input type="text" class="input-field" name="price" placeholder="{{ __('Enter a Price') }}" required="">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
        <div class="left-area">
            <h4 class="heading">{{ __('Price in Words') }} *</h4>
            <p class="sub-heading">{{ __('(In Any Language)') }}</p>
        </div>
        </div>
        <div class="col-lg-7">
            <input type="text" class="input-field" name="price_eng" placeholder="{{ __('Enter a Price in English Words') }}" required="">
        </div>
    </div>




                <div class="row">
                  <div class="col-lg-4">
                    <div class="left-area">
                        <h4 class="heading">Set Icon *</h4>
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <div class="img-upload">
                        <div id="image-preview" class="img-preview" style="background: url(http://localhost/offerbazar/assets/admin/images/upload.png);">
                            <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>Upload Icon</label>
                            <input type="file" name="images[]" class="img-upload" id="image-upload" multiple>
                          </div>
                    </div>

                  </div>
                </div>


                <br>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="left-area">
                      
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <button class="addProductSubmit-btn" type="submit">{{ __('Create New Offer') }}</button>
                  </div>
                  
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    var dateToday = new Date();
    var dates =  $( "#from,#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
        onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
          instance = $(this).data("datepicker"),
          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
          dates.not(this).datepicker("option", option, date);
    }
});
</script>

@endsection

