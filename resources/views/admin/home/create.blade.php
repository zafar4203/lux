@extends('layouts.admin')
@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('Home Page') }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                          <a href="javascript:;">{{ __('Page Settings') }} </a>
                        </li>
                        <li>
                          <a href="{{ route('admin-home-create') }}">{{ __('Add') }}</a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="add-product-content1">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">

                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                        
                        @include('includes.admin.form-both') 
                      <form id="geniusform" action="{{route('admin-home-create')}}" method="POST" enctype="multipart/form-data" novalidate>
                        {{csrf_field()}}

                        @if(!empty($home))
                            <input type="hidden" value="{{ $home->id}}" name="id" />
                        @endif

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Video Url') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" name="video_url" placeholder="Enter Video Url" value="@if($home) {{ $home->video_url }} @endif" class="form-control" />
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 1 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" name="para_1_heading" placeholder="Enter Paragraph 1 Heading" value="@if($home) {{ $home->para_1_heading }} @endif" class="form-control" />
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 1 Find Out More') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                          <input type="text" name="para_1_find_out_more" placeholder="Enter Paragraph 1 Heading" value="@if($home) {{ $home->para_1_find_out_more }} @endif" class="form-control" />
                          </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 1') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <textarea class="nic-edit" name="para_1" placeholder="{{ __('Paragraph 1') }}" required="">@if($home) {{ $home->para_1 }} @endif</textarea>
                          </div>
                        </div>



                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 2 Heading') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" name="para_2_heading" placeholder="Enter Paragraph 2 Heading" value="@if($home) {{ $home->para_2_heading }} @endif" class="form-control" />
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 2 Find Out More') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                          <input type="text" name="para_2_find_out_more" placeholder="Enter Paragraph 2 Heading" value="@if($home) {{ $home->para_2_find_out_more }} @endif" class="form-control" />
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Paragraph 2') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <textarea class="nic-edit" name="para_2" placeholder="{{ __('Paragraph 2') }}" required="">@if($home) {{ $home->para_2 }} @endif</textarea>
                          </div>
                        </div>





                <div class="row">
                  <div class="col-lg-4">
                      <div class="left-area">
                      <h4 class="heading">{{ __('Set Image 1') }} *</h4>
                      </div>
                  </div>
                  <div class="col-lg-7">
                      <div class="img-upload">
                          <div id="image-preview" class="img-preview" style="background: @if($home)@if($home->image_1) url({{ asset('public/assets/images/home/'.$home->image_1) }}); @else url({{ asset('public/assets/admin/images/upload.png') }}); @endif @endif">
                              <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                              <input type="file" name="image_1" class="img-upload">
                          </div>
                      </div>
                  </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="left-area">
                            <h4 class="heading">{{ __('Set Image 2') }} *</h4>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="img-upload">
                            <div id="image-preview" class="img-preview" style="background: @if($home)@if($home->image_2) url({{ asset('public/assets/images/home/'.$home->image_2) }}); @else url({{ asset('public/assets/admin/images/upload.png') }}); @endif @endif">
                                <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                                <input type="file" name="image_2" class="img-upload">
                            </div>
                        </div>
                    </div>
                </div>


                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <button class="addProductSubmit-btn" type="submit">{{ __('Update Home Page') }}</button>
                          </div>
                        </div>

                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection