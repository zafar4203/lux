@extends('layouts.vendor') 
@section('title' , 'Orders')
@section('styles')
    <style>
    .btn-primary , 
    .btn-primary:hover{
        background-color:#2d3274;
        border-color:#2d3274;
    }
    </style>
@endsection
@section('content')  
                    <div class="content-area">
                        <div class="mr-breadcrumb">
                            <div class="row">
                                <div class="col-lg-12">
                                        <h4 class="heading">{{ $langg->lang443 }}</h4>
                                        <ul class="links">
                                            <li>
                                                <a href="{{ route('vendor-dashboard') }}">{{ $langg->lang441 }} </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">{{ $langg->lang442 }}</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('vendor-order-index') }}">{{ $langg->lang443 }}</a>
                                            </li>
                                        </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-lg-12">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="search">Order Number:</label>
                                                    <input type="text" class="form-control" id="search">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="od_st">Order Status:</label>
                                                    <select class="form-control" id="od_st">
                                                        <option value=""></option>
                                                        <option value="pending">Pending</option>
                                                        <option value="processing">Processing</option>
                                                        <option value="completed">Completed</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="pm">Payment Method:</label>
                                                    <select class="form-control" id="pm">
                                                        <option value=""></option>
                                                        <option value="stripe">Stripe</option>
                                                        <option value="paypal">Paypal</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="startDate">Start Date:</label>
                                                    <input type="date" class="form-control" id="startDate">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="endDate">End Date:</label>
                                                    <input type="date" class="form-control" id="endDate">
                                                </div>
                                            </div>
                                            <div class="col-md-3 mt-1">
                                                <div class="form-group mt-4">
                                                    <button id="filter" class="btn btn-primary">Filter</button>
                                                    <button id="clearFilter" class="btn btn-primary">Clear Filter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        <div class="product-area mt-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mr-table allproduct">
                                        @include('includes.form-success') 

                                        <div class="table-responsiv">
                                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>{{ $langg->lang534 }}</th>
                                                            <th>Date</th>
                                                            <th>{{ $langg->lang535 }}</th>
                                                            <th>{{ $langg->lang536 }}</th>
                                                            <th>{{ $langg->lang537 }}</th>
                                                            <th>Status</th>
                                                            <th>{{ $langg->lang538 }}</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>


                                              <tbody>
                    @foreach($orders as $orderr) 
                    @php 

                    $qty = $orderr->sum('qty');
                    $price = $orderr->sum('price');                                       
                    @endphp
                    @foreach($orderr as $order)


@php 

  if($user->shipping_cost != 0){
      $price +=  round($user->shipping_cost * $order->order->currency_value , 2);
    }
  if(App\Models\Order::where('order_number','=',$order->order->order_number)->first()->tax != 0){
      $price  += ($price / 100) * App\Models\Order::where('order_number','=',$order->order->order_number)->first()->tax;
    }    

@endphp

                        <tr>
                            <td> <a href="{{route('vendor-order-invoice',$order->order_number)}}">{{ $order->order->order_number}}</a></td>
                            <td>{{ \Carbon\Carbon::parse($order->order->created_at)->format('m/d/Y')}}</td>
                            <td>{{$qty}}</td>
                            <td>{{$curr->sign}}{{round($order->order->pay_amount * $curr->value, 0)}}</td>                            

                            <td>{{$order->order->method}}</td>
                            <td>{{ $order->status }}</td>
                            <td>

                            <div class="action-list">
                                <a href="{{route('vendor-order-show',$order->order->order_number)}}" class="btn btn-primary product-btn"><i class="fa fa-eye"></i> {{ $langg->lang539 }}</a>
                            </div>

                            </td>
                            <td>
                                    <select style="margin-bottom:0px !important;" class="vendor-btn {{ $order->status }}">
                                        <option value="{{ route('vendor-order-status',['slug' => $order->order->order_number, 'status' => 'pending']) }}" {{  $order->status == "pending" ? 'selected' : ''  }}>{{ $langg->lang540 }}</option>
                                        <option value="{{ route('vendor-order-status',['slug' => $order->order->order_number, 'status' => 'processing']) }}" {{  $order->status == "processing" ? 'selected' : ''  }}>{{ $langg->lang541 }}</option>
                                        <option value="{{ route('vendor-order-status',['slug' => $order->order->order_number, 'status' => 'completed']) }}" {{  $order->status == "completed" ? 'selected' : ''  }}>{{ $langg->lang542 }}</option>
                                        <option value="{{ route('vendor-order-status',['slug' => $order->order->order_number, 'status' => 'declined']) }}" {{  $order->status == "declined" ? 'selected' : ''  }}>{{ $langg->lang543 }}</option>
                                    </select>
                            </td>
                        </tr>

                                                  @break
                    @endforeach

                                                  @endforeach
                                                  </tbody>
                                                    
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

{{-- ORDER MODAL --}}

<div class="modal fade" id="confirm-delete2" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="submit-loader">
        <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">
    </div>
    <div class="modal-header d-block text-center">
        <h4 class="modal-title d-inline-block">{{ $langg->lang544 }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p class="text-center">{{ $langg->lang545 }}</p>
        <p class="text-center">{{ $langg->lang546 }}</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{ $langg->lang547 }}</button>
            <a class="btn btn-success btn-ok order-btn">{{ $langg->lang548 }}</a>
      </div>

    </div>
  </div>
</div>

{{-- ORDER MODAL ENDS --}}


@endsection    

@section('scripts')
	<!-- Custom Js-->
    <script src="{{asset('public/assets/vendor/js/myscript.js')}}"></script>

{{-- DATA TABLE --}}


    <script type="text/javascript">


$('.vendor-btn').on('change',function(){
          $('#confirm-delete2').modal('show');
          $('#confirm-delete2').find('.btn-ok').attr('href', $(this).val());

});

    var table = $('#geniustable').DataTable({
            sDom: 'lrtip',
            ordering: false
        });

    table.column( 5 ).visible( false );
    // table.column( 6 ).visible( false );

    $(document).on('change', '#od_st', function() {
        var value = $(this).val();
        table.columns(5).search(value).draw();
    });

    $(document).on('keyup', '#search', function() {
        var value = $(this).val();
        table.search(value).draw();
    });

    $(document).on('change', '#pm', function() {
        var value = $(this).val();
        table.columns(4).search(value).draw();
    });


    $('#filter').on('click', function(e){
    e.preventDefault();
    var startDate = $('#startDate').val(),
        endDate = $('#endDate').val();

    $.fn.dataTableExt.afnFiltering.length = 0;
    
    filterByDate(6, startDate, endDate); // We call our filter function
    
    table.draw(); // Manually redraw the table after filtering
  });

  $('#clearFilter').on('click', function(e){
    e.preventDefault();
    $.fn.dataTableExt.afnFiltering.length = 0;
    $('#od_st').val("");
    $('#search').val("");
    $('#pm').val("");

    table.columns(4).search($('#pm').val()).draw();
    table.columns(5).search($('#od_st').val()).draw();
    table.search('').draw(); 
  });  

    // Date range filter
    minDateFilter = "";
    maxDateFilter = "";

  var filterByDate = function(column, startDate, endDate) {

	var sDay = new Date(startDate);
        sDay.setDate(sDay.getDate() -1);

    var eDay = new Date(endDate);
        eDay.setDate(eDay.getDate());

    var minDateFilter = sDay.getTime();
    var maxDateFilter = eDay.getTime();

    $.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        if (typeof aData._date == 'undefined') {
            aData._date = new Date(aData[1]).getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
            if (aData._date <= minDateFilter) {
                console.log("Min " + minDateFilter);
                return false;
            }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date >= maxDateFilter) {
                console.log("Max " + maxDateFilter);
                return false;
            }
        }

        return true;
    });


	};

    </script>
{{-- DATA TABLE --}}
    
@endsection   