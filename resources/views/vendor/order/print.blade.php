<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="{{$seo->meta_keys}}">
        <meta name="author" content="GeniusOcean">

        <title>{{$gs->title}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
		<link href="{{asset('public/assets/admin/css/bootstrap.min.css')}}" rel="stylesheet" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('public/assets/print/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('public/assets/print/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/assets/print/css/style.css')}}">
  <link href="{{asset('public/assets/print/css/print.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="icon" type="image/png" href="{{asset('public/assets/images/'.$gs->favicon)}}"> 
  <style type="text/css">
@page { size: auto;  margin: 0mm; }
@page {
  size: A4;
  margin: 0;
}
@media print {
  html, body {
    width: 210mm;
    height: 287mm;
  }

html {

}
::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
  </style>

  <style>
        .bg-grey{
            background:#e0d9d9 !important;
        }
        table.main {
            margin-bottom:0px !important;
        }
        table.special ,table.special tr ,table.special td{
            border:1px solid grey;
        }
        table.main , table.main tr , table.main td{
            margin-bottom:0px !important;
            border-bottom:0px;
        }
        table.main-last tr:last td{
            margin-bottom:0px !important;
        }

        .bt-1{
            border-top:1px solid grey !important;
        }

    </style>
</head>
/* <body onload="window.print();"> */
<body>    
<div class="order-table-wrap">
<div class="invoice-wrap">
    <div class="invoice__title">
        <div class="row">
            <div class="col-lg-12 text-right">
                <a class="btn print" href="{{route('vendor-order-print',$order->order_number)}}"
                target="_blank"><i class="fa fa-print"></i> {{route('vendor-order-print',$order->order_number)}}</a>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-3">
                <div class="invoice__logo text-left">
                   <img src="{{ asset('assets/images/'.$gs->invoice_logo) }}" alt="woo commerce logo">
                </div>
            </div>
            <div class="col-sm-4">
            @if(isset($user))
                <p><strong>Company Details </strong></p>    
                <span><strong>Company Name :</strong> {{ $user->shop_name }}</span><br>
                <span><strong>Company Address :</strong> {{ $user->shop_address }}</span><br>
                <span><strong>Shop Number :</strong> {{ $user->shop_number }}</span><br>
                <br>
                <span> <strong>Reg No :</strong> {{$user->reg_number}}</span>
            @endif
            </div>
            <div class="col-sm-4">
                <p><strong>{{ $langg->lang587 }}</strong></p>
                <span><strong>Name</strong>: {{ $order->customer_name}}</span><br>
                <span><strong>Phone:</strong> {{ $order->customer_phone}}</span><br>
                <span><strong>Email:</strong> {{ $order->customer_email}}</span><br>
                <span><strong>{{ $langg->lang560 }}</strong>: {{ $order->customer_address }}</span><br>
                <span><strong>{{ $langg->lang562 }}</strong>: {{ $order->customer_city }}</span><br>
                <span><strong>{{ $langg->lang561 }}</strong>: {{ $order->customer_country }}</span>
            </div>
        </div>
    </div>
    <br>
    <div class="row invoice__metaInfo">

    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="invoice_table">
                <div class="mr-table">
                    <div class="table-responsive">
                        <table class="main table table-hover dt-responsive" cellspacing="0"
                            width="100%">
                            <tr>
                                <th width="70%" class="bt-1 text-center">Shipping Details</th>
                                <th class="bg-grey bt-1 text-center">Invoice No</th>
                                <th class="bg-grey bt-1 text-center">Date</th>
                            </tr>
                        </table>
                        <table class="main table table-hover special dt-responsive" cellspacing="0"
                            width="100%">
                            <tr>
                                <th width="10%">Name</th>
                                <td class="bt-0" width="60%">{{ $order->shipping_name == null ? $order->customer_name : $order->shipping_name}}</td>
                                <td class="bt-0"></td>
                                <td class="bt-0"></td>
                            </tr>
                        </table>
                        <table class="main table table-hover dt-responsive" cellspacing="0"
                            width="100%">
                            <tr>
                                <th width="10%">Address</th>
                                <td width="25%">{{ $order->shipping_address == null ? $order->customer_address : $order->shipping_address }}</td>
                                <td width="10%">City/State</td>
                                <td class="br-0" width="25%">{{ $order->shipping_city == null ? $order->customer_city : $order->shipping_city }}/{{ $order->shipping_country == null ? $order->customer_country : $order->shipping_country }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>

                        <table class="main table table-hover dt-responsive" cellspacing="0"
                            width="100%">
                            <tr>
                                <th class="bb-1" width="10%">Phone</th>
                                <td class="bb-1" width="25%">{{ $order->customer_phone}}</td>
                                <td class="bb-1" width="10%">Email</td>
                                <td class="br-0 bb-1" width="25%">{{ $order->customer_email}}</td>
                                <td class="bb-1"></td>
                                <td class="bb-1"></td>
                            </tr>
                        </table>
                        <table id="example2" class="main table table-hover dt-responsive" cellspacing="0"
                            width="100%" >
                            <thead>
                                <tr>
                                    <th class="bg-grey text-center">Id</th>
                                    <th class="bg-grey text-center">Particulars</th>
                                    <th class="bg-grey text-center">Size</th>
                                    <th class="bg-grey text-center">Unit</th>
                                    <th class="bg-grey text-center">Color</th>
                                    <th class="bg-grey text-center">Qty</th>
                                    <th class="bg-grey text-center">Price</th>
                                    <th class="bg-grey text-center">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $subtotal = 0;
                                $data = 0;
                                $tax = 0;
                                $vat = 0;
                                @endphp
                                @foreach($cart->items as $product)
                
                                @php
                                    $vat = $vat + App\Models\Product::find($product['item']['id'])->vat_amt;
                                @endphp

                        @if($product['item']['user_id'] != 0)
                            @if($product['item']['user_id'] == $user->id)

                                <tr>
                                    <td class="bl-0 br-0 text-center">{{$product['item']['id']}}</td>
                                    <td class="text-center" width="30%">
                                        @if($product['item']['user_id'] != 0)
                                        @php
                                        $user = App\Models\User::find($product['item']['user_id']);
                                        @endphp
                                        @if(isset($user))
                                        <a target="_blank"
                                            href="{{ route('front.product', $product['item']['slug']) }}">{{ $product['item']['name']}}</a>
                                        @else
                                        <a href="javascript:;">{{$product['item']['name']}}</a>
                                        @endif

                                        @else
                                        <a href="javascript:;">{{ $product['item']['name']}}</a>
                                        @endif
                                    </td>


    <td class="text-center">
                @if(!empty($product['keys']))
                    @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                        @if(ucwords(str_replace('_', ' ', $key)) == "Sizes")
                        {{ $value }}
                        @endif
                    @endforeach
                @endif
    </td>
    <!-- <td>
    @if(!empty($product['keys']))

        @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
        <p>

            <b>{{ ucwords(str_replace('_', ' ', $key))  }} : </b> {{ $value }} 

        </p>
        @endforeach

        @endif

            </td> -->
                                    <td class="text-center">
                                    </td>
                                    <td class="text-center">

                                        @if(!empty($product['keys']))
                                            @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                                                @if(ucwords(str_replace('_', ' ', $key)) == "Colour")
                                                {{ $value }} 
                                                @endif
                                            @endforeach
                                        @endif

                                    </td>
                                    <td class="text-center">
                                         {{$product['qty']}} {{ $product['item']['measure'] }}
                                    </td>
                                    <td>
                                                {{$curr->sign}}{{ round($product['item']['price'] * $curr->value , 0) }}   
                                    </td>
                              
                                    <td>{{$curr->sign}}{{ round($product['price'] * $curr->value , 0) }}</td>
                                    @php
                                    $subtotal += round($product['price'] * $curr->value, 0);
                                    @endphp

                                </tr>

                            @endif
                        @endif
                                @endforeach
                            </tbody>

                            <tfoot>
                            <tr  style="border-right:1px solid lightgrey; border-left:1px solid lightgrey;">
                                    <td class="bg-grey text-right" colspan="7">{{ $langg->lang597 }}</td>
                                    <td>{{$curr->sign}}{{ round($subtotal, 0) }}</td>
                                </tr>
                                @if(Auth::user()->id == $order->vendor_shipping_id)
                                @if($order->shipping_cost != 0)
                                    @php 
                                    $price = round(($order->shipping_cost / $order->currency_value),0);
                                    @endphp
                                    @if(DB::table('shippings')->where('price','=',$price)->count() > 0)
                                    <tr>
                                        <td colspan="6">{{ DB::table('shippings')->where('price','=',$price)->first()->title }}({{$order->currency_sign}})</td>
                                        <td>{{ round($order->shipping_cost , 0) }}</td>
                                    </tr>
                                    @endif
                                @endif
                                @endif
                                @if(Auth::user()->id == $order->vendor_packing_id)
                                @if($order->packing_cost != 0)
                                    @php 
                                    $pprice = round(($order->packing_cost / $order->currency_value),0);
                                    @endphp
                                    @if(DB::table('packages')->where('price','=',$pprice)->count() > 0)
                                    <tr>
                                        <td colspan="6">{{ DB::table('packages')->where('price','=',$pprice)->first()->title }}({{$order->currency_sign}})</td>
                                        <td>{{ round($order->packing_cost , 0) }}</td>
                                    </tr>
                                    @endif
                                @endif
                                @endif

                                @if($order->tax != 0)
                                <tr  style="border-right:1px solid lightgrey; border-left:1px solid lightgrey;">
                                    <td colspan="6">{{ $langg->lang599 }}({{$order->currency_sign}})</td>
                                    @php
                                        $tax = ($subtotal / 100) * $order->tax;
                                        $subtotal =  $subtotal + $tax;
                                    @endphp
                                    <td>{{round($tax, 0)}}</td>
                                </tr>
                                @endif



                                <tr style="border-right:1px solid lightgrey; border-left:1px solid lightgrey;border-bottom:1px solid lightgrey;">
                                    <td class="text-left">Amount in Words:</td>
                                    <td colspan="4"></td>
                                    <td class="bg-grey text-right" colspan="2">Net Amount</td>
                                    @php
                                    if($order->coupon_discount != null)
                                        $discount = round($order->coupon_discount  * $curr->value , 0);
                                    else
                                        $discount = 0;
                                @endphp
                                <td>{{$curr->sign}}{{ round(($subtotal  - $discount - ($order->inbuild_discount * $curr->value) ), 0) }}

                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <table class="main-last table table-hover dt-responsive" cellspacing="0"
                            width="100%">
                            <tr>
                                <th width="50%">Terms and Conditions</th>
                                <th width="20%">Receiver Signature</th>
                                <th width="30%">For Lux Ecommerce Store</th>
                            </tr>

                            <tr>
                                <td rowspan="2" height="100px" width="50%"></td>
                                <td rowspan="2" width="20%"></td>
                                <td rowspan="2" width="30%"></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
setTimeout(function () {
window.close();
}, 500);
</script>

</body>
</html>
