@extends('layouts.vendor')

@section('title' , 'Invoice')

@section('styles')
    <style>
        .bg-grey{
            background:#e0d9d9 !important;
        }
        table.main {
            margin-bottom:0px !important;
        }
        table ,tr , td{
            border:1px solid black;
        }
        table.main , table.main tr , table.main td{
            margin-bottom:0px !important;
            border-bottom:0px;
        }
        table.main-last tr:last td{
            margin-bottom:0px !important;
        }

    </style>
@endsection
@section('content')
<div class="content-area">
                    <div class="mr-breadcrumb">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="heading">{{ $langg->lang586 }} <a class="add-btn" href="{{ route('vendor-order-show',$order->order_number) }}"><i class="fas fa-arrow-left"></i> {{ $langg->lang550 }}</a></h4>
                                <ul class="links">
                                    <li>
                                        <a href="{{ route('vendor-dashboard') }}">{{ $langg->lang441 }} </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">{{ $langg->lang443 }}</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">{{ $langg->lang586 }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
    <div class="order-table-wrap">
        <div class="invoice-wrap">
            <div class="invoice__title">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <a class="btn  add-newProduct-btn print" href="{{route('vendor-order-print',$order->order_number)}}"
                        target="_blank"><i class="fa fa-print"></i> {{ $langg->lang607 }}</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="invoice__logo text-left">
                           <img width="200px" src="{{ asset('assets/images/'.$gs->invoice_logo) }}" alt="woo commerce logo">
                        </div>
                    </div>
                    <div class="col-sm-4">
                    @if(isset($user))
                        <p><strong>Company Details </strong></p>    
                        <span><strong>Company Name :</strong> {{ $user->shop_name }}</span><br>
                        <span><strong>Company Address :</strong> {{ $user->shop_address }}</span><br>
                        <span><strong>Shop Number :</strong> {{ $user->shop_number }}</span><br>
                        <br>
                        <span> <strong>Reg No :</strong> {{$user->reg_number}}</span>
                    @endif
                    </div>
                    <div class="col-sm-4">
                        <p><strong>{{ $langg->lang587 }}</strong></p>
                        <span><strong>Name</strong>: {{ $order->customer_name}}</span><br>
                        <span><strong>Phone:</strong> {{ $order->customer_phone}}</span><br>
                        <span><strong>Email:</strong> {{ $order->customer_email}}</span><br>
                        <span><strong>{{ $langg->lang560 }}</strong>: {{ $order->customer_address }}</span><br>
                        <span><strong>{{ $langg->lang562 }}</strong>: {{ $order->customer_city }}</span><br>
                        <span><strong>{{ $langg->lang561 }}</strong>: {{ $order->customer_country }}</span>
                    </div>
                </div>
            </div>
            <br>
            <div class="row invoice__metaInfo">

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="invoice_table">
                        <div class="mr-table">
                            <div class="table-responsive">
                                <table class="main table table-hover dt-responsive" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <th width="70%" class="text-center">Shipping Details</th>
                                        <th class="bg-grey text-center">Invoice No</th>
                                        <th class="bg-grey text-center">Date</th>
                                    </tr>
                                </table>
                                <table class="main table table-hover dt-responsive" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <th width="10%">Name</th>
                                        <td width="60%">{{ $order->shipping_name == null ? $order->customer_name : $order->shipping_name}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <table class="main table table-hover dt-responsive" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <th width="10%">Address</th>
                                        <td width="25%">{{ $order->shipping_address == null ? $order->customer_address : $order->shipping_address }}</td>
                                        <td width="10%">City/State</td>
                                        <td width="25%">{{ $order->shipping_city == null ? $order->customer_city : $order->shipping_city }}/{{ $order->shipping_country == null ? $order->customer_country : $order->shipping_country }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>

                                <table class="main table table-hover dt-responsive" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <th width="10%">Phone</th>
                                        <td width="25%">{{ $order->customer_phone}}</td>
                                        <td width="10%">Email</td>
                                        <td width="25%">{{ $order->customer_email}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <table id="example2" class="main table table-hover dt-responsive" cellspacing="0"
                                    width="100%" >
                                    <thead>
                                        <tr>
                                            <th class="bg-grey text-center">Id</th>
                                            <th class="bg-grey text-center">Particulars</th>
                                            <th class="bg-grey text-center">Size</th>
                                            <th class="bg-grey text-center">Unit</th>
                                            <th class="bg-grey text-center">Color</th>
                                            <th class="bg-grey text-center">Qty</th>
                                            <th class="bg-grey text-center">Price</th>
                                            <th class="bg-grey text-center">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $subtotal = 0;
                                        $data = 0;
                                        $tax = 0;
                                        $vat = 0;
                                        @endphp
                                        @foreach($cart->items as $product)
                        
                                        @php
                                            $vat = $vat + App\Models\Product::find($product['item']['id'])->vat_amt;
                                        @endphp

                                @if($product['item']['user_id'] != 0)
                                    @if($product['item']['user_id'] == $user->id)

                                        <tr>
                                            <td class="text-center">{{$product['item']['id']}}</td>
                                            <td width="30%">
                                                @if($product['item']['user_id'] != 0)
                                                @php
                                                $user = App\Models\User::find($product['item']['user_id']);
                                                @endphp
                                                @if(isset($user))
                                                <a target="_blank"
                                                    href="{{ route('front.product', $product['item']['slug']) }}">{{ $product['item']['name']}}</a>
                                                @else
                                                <a href="javascript:;">{{$product['item']['name']}}</a>
                                                @endif

                                                @else
                                                <a href="javascript:;">{{ $product['item']['name']}}</a>
                                                @endif
                                            </td>


            <td class="text-center">
                @if(!empty($product['keys']))
                    @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                        @if(ucwords(str_replace('_', ' ', $key)) == "Sizes")
                        {{ $value }}
                        @endif
                    @endforeach
                @endif
            </td>
            <!-- <td>
            @if(!empty($product['keys']))

                @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                <p>

                    <b>{{ ucwords(str_replace('_', ' ', $key))  }} : </b> {{ $value }} 

                </p>
                @endforeach

                @endif

            </td> -->
                            <td class="text-center">
                            
                            </td>
                            <td class="text-center">
                            @if(!empty($product['keys']))
                                @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)
                                    @if(ucwords(str_replace('_', ' ', $key)) == "Colour")
                                     {{ $value }} 
                                    @endif
                                @endforeach
                            @endif

                            </td>
                            <td class="text-center">
                                    {{$product['qty']}} {{ $product['item']['measure'] }}
                            </td>
                            <td>
                                        {{$curr->sign}}{{ round($product['item']['price'] * $curr->value) }}   
                            </td>
                                      
                                            <td>{{$curr->sign}}{{ round($product['price'] * $curr->value , 0) }}</td>
                                            @php
                                            $subtotal += round($product['price'] * $curr->value, 0);
                                            @endphp

                                        </tr>

                                    @endif
                                @endif
                                        @endforeach
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <td  class="bg-grey" colspan="7">{{ $langg->lang597 }}</td>
                                            <td>{{$curr->sign}}{{ round($subtotal) }}</td>
                                        </tr>
                                        @if(Auth::user()->id == $order->vendor_shipping_id)
                                        @if($order->shipping_cost != 0)
                                            @php 
                                            $price = round(($order->shipping_cost / $order->currency_value),0);
                                            @endphp
                                            @if(DB::table('shippings')->where('price','=',$price)->count() > 0)
                                            <tr>
                                                <td colspan="6">{{ DB::table('shippings')->where('price','=',$price)->first()->title }}({{$order->currency_sign}})</td>
                                                <td>{{ round($order->shipping_cost , 0) }}</td>
                                            </tr>
                                            @endif
                                        @endif
                                        @endif
                                        @if(Auth::user()->id == $order->vendor_packing_id)
                                        @if($order->packing_cost != 0)
                                            @php 
                                            $pprice = round(($order->packing_cost / $order->currency_value),0);
                                            @endphp
                                            @if(DB::table('packages')->where('price','=',$pprice)->count() > 0)
                                            <tr>
                                                <td colspan="6">{{ DB::table('packages')->where('price','=',$pprice)->first()->title }}({{$order->currency_sign}})</td>
                                                <td>{{ round($order->packing_cost , 0) }}</td>
                                            </tr>
                                            @endif
                                        @endif
                                        @endif

                                        @if($order->tax != 0)
                                        <tr>
                                            <td colspan="6">{{ $langg->lang599 }}({{$order->currency_sign}})</td>
                                            @php
                                                $tax = ($subtotal / 100) * $order->tax;
                                                $subtotal =  $subtotal + $tax;
                                            @endphp
                                            <td>{{round($tax, 0)}}</td>
                                        </tr>
                                        @endif

                                        <!-- @if($order->coupon_discount != null)
                                        <tr>
                                            <td class="bg-grey" colspan="7">Discount</td>
                                            <td>{{$curr->sign}}{{round($order->coupon_discount * $curr->value , 2)}}</td>
                                        </tr>
                                        @endif -->

                                        <!-- <tr>
                                            <td class="bg-grey" colspan="7">In Build Discount</td>
                                            <td>{{$curr->sign}}{{round($order->inbuild_discount * $curr->value)}}</td>
                                        </tr> -->

                                        <!-- <tr>
                                            <td class="bg-grey" colspan="7"> Vat <small>Exclusive</small> </td>
                                                <td>{{$curr->sign}}{{ $vat * $curr->value }}
                                            </td>
                                        </tr> -->

                                        <tr>
                                            <td class="text-left">Amount in Words:</td>
                                            <td colspan="4"></td>
                                            <td class="bg-grey" colspan="2">Net Amount</td>
                                            @php
                                                if($order->coupon_discount != null)
                                                    $discount = round($order->coupon_discount  * $curr->value , 2);
                                                else
                                                    $discount = 0;
                                            @endphp
                                            <td>{{$curr->sign}}{{ round((round($subtotal)  - $discount - round($order->inbuild_discount * $curr->value)), 0) }}
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="main-last table table-hover dt-responsive" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <th width="50%">Terms and Conditions</th>
                                        <th width="20%">Receiver Signature</th>
                                        <th width="30%">For Lux Ecommerce Store</th>
                                    </tr>

                                    <tr>
                                        <td rowspan="2" height="100px" width="50%"></td>
                                        <td rowspan="2" width="20%"></td>
                                        <td rowspan="2" width="30%"></td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Content Area End -->
</div>
</div>
</div>

@endsection
