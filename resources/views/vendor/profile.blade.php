@extends('layouts.vendor')
@section('styles')
	<style>
		.nicEdit-main{
			overflow-x:scroll;
			padding:10px;
		}
.img img{
	height:200px;
	width:200px;
	border-radius:50%;
}

	</style>
@endsection
@section('content')

						<div class="content-area">
							<div class="mr-breadcrumb">
								<div class="row">
									<div class="col-lg-12">
											<h4 class="heading">{{ $langg->lang434 }}</h4>
											<ul class="links">
												<li>
													<a href="{{ route('vendor-dashboard') }}">{{ $langg->lang441 }} </a>
												</li>
												<li>
													<a href="{{ route('vendor-profile') }}">{{ $langg->lang434 }} </a>
												</li>
											</ul>
									</div>
								</div>
							</div>
							<div class="add-product-content1">
								<div class="row">
									<div class="col-lg-12">
										<div class="product-description">
											<div class="body-area">

				                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
											<form id="geniusform" action="{{ route('vendor-profile-update') }}" method="POST" enctype="multipart/form-data">
												{{csrf_field()}}

                      						 @include('includes.vendor.form-both')  

			<div class="row mb-5">
				<div class="col-3 mx-auto">
					<div class="upload-img">
						<div class="img mb-3">
							<img id="blah" src="{{ $data->shop_logo ? asset('assets/images/vendorbanner/'.$data->shop_logo):asset('public/assets/images/'.$gs->user_image) }}">
						</div>
							<input type="file" id="files" class="input-field" name="shop_logo">
					</div>
				</div>
			</div>
												
	<div class="row">
		<div class="col-lg-4">
			<div class="left-area">
					<h4 class="heading">Shop Name *</h4>
			</div>
		</div>
		<div class="col-lg-7">
			<input type="text" class="input-field" name="shop_name" placeholder="Enter Shop Name " required="" value="{{$data->shop_name}}">
		</div>
	</div>


	<div class="row">
		<div class="col-lg-4">
			<div class="left-area">
					<h4 class="heading">{{ $langg->lang458 }} *</h4>
			</div>
		</div>
		<div class="col-lg-7">
			<input type="text" class="input-field" name="owner_name" placeholder="{{ $langg->lang458 }}" required="" value="{{$data->owner_name}}">
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4">
			<div class="left-area">
					<h4 class="heading">{{ $langg->lang459 }} *</h4>
			</div>
		</div>
		<div class="col-lg-7">
			<input type="text" class="input-field" name="shop_number" placeholder="{{ $langg->lang459 }}" required="" value="{{$data->shop_number}}">
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4">
			<div class="left-area">
					<h4 class="heading">{{ $langg->lang460 }} *</h4>
			</div>
		</div>
		<div class="col-lg-7">
			<input type="text" class="input-field" name="shop_address" placeholder="{{ $langg->lang460 }}" required="" value="{{$data->shop_address}}">
		</div>
	</div>


	<div class="row">
		<div class="col-lg-4">
			<div class="left-area">
					<h4 class="heading">{{ $langg->lang461 }}</h4>
					<p class="sub-heading">{{ $langg->lang462 }}</p>
			</div>
		</div>
		<div class="col-lg-7">
			<input type="text" class="input-field" name="reg_number" placeholder="{{ $langg->lang461 }}" required="" value="{{$data->reg_number}}">
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4">
			<div class="left-area">
					<h4 class="heading">{{ $langg->lang463 }} *</h4>
			</div>
		</div>
		<div class="col-lg-7">
			<textarea class="input-field nic-edit" name="shop_details" placeholder="{{ $langg->lang463 }}">{{$data->shop_details}}</textarea>
		</div>
	</div>



	<div class="row">
		<div class="col-lg-4">
		<div class="left-area">
			
		</div>
		</div>
		<div class="col-lg-7">
		<button class="addProductSubmit-btn" type="submit">{{ $langg->lang464 }}</button>
		</div>
	</div>

</form>


											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

@endsection
@section('scripts')
	<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#blah').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#files").change(function(){
			readURL(this);
		});
		
	</script>
@endsection